make_ideal_circuit rect8x8.lev -f level -g and -i 8 -l 8 -d 0 -k 1 -r
make_ideal_circuit tri2d.lev -f level -g and -i 8 -l 4 -d 2 -k 1 -r
make_ideal_circuit tri2dk4.lev -f level -g and -i 8 -l 13 -d 2 -k 4 -r
make_ideal_circuit cblock3.lev -f level -g and -c 16 1 12 4 4 12 -r
