{
  "version": "0.1",
  "setup": {
    "number_of_PIs": 10,
    "number_of_POs": 19,
    "number_of_FFs": 20,
    "number_of_patterns": 3,
    "cycle_timing_template": {
      "force_pi": 0,
      "init_ff": 1,
      "measure_po": 2,
      "pulse_clk": 3,
      "observe_ff": 4
    }
  },
  "scan_test": [
    {
      "pattern": 0,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000010100",
          "init_ff": "01110110101101101100",
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 1,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 2,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 3,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 4,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 5,
          "measure_po": "100XXXXXXXXXXX00XXX",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "XXXXXX00000XXXXXXXXX"
        }
      ]
    },
    {
      "pattern": 1,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0111000000",
          "init_ff": "01010001101101111001",
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 1,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 2,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 3,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 4,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 5,
          "measure_po": "100XXXXXXXXXXX00XXX",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "XXXXXX00000XXXXXXXXX"
        }
      ]
    },
    {
      "pattern": 2,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101001000",
          "init_ff": "00101000001110000000",
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 1,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 2,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 3,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 4,
          "pulse_clk": [
            "all"
          ]
        },
        {
          "cycle": 5,
          "measure_po": "100XXXXXXXXXXX00XXX",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "XXXXXX00000XXXXXXXXX"
        }
      ]
    }
  ]
}
