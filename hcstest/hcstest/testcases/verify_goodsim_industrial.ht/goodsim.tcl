#read_circuit s27.bench
#read_circuit s1196.bench
read_circuit ../circuits/exp.bench -l ../circuits/exp.grp.lev -n ../circuits/exp.name
create_ocl_context -p 1
create_ocl_queue -p 1 -d 0
set_ocl_options -g 1024 1 1 -l 128 1 1 -c buffer
set_random_patterns 3 -c 2

set_simulator_options -l ocl_cpu -p 1 -d 0
simulate_patterns -f random -s
write_patterns ocl-cpu.pat -r
delete_patterns

set_simulator_options -l host
simulate_patterns -f random -s
write_patterns host.pat -r
delete_patterns

set_simulator_options -l host -e on
simulate_patterns -f random -s
write_patterns host.event.pat -r
delete_patterns

# Update configuration switch later
delete_circuit
read_circuit ../circuits/exp.bench -l ../circuits/exp.grp.lev -n ../circuits/exp.name
create_ocl_context -p 0
create_ocl_queue -p 0 -d 0
set_simulator_options -l gpu -p 0 -d 0 -g 0
simulate_patterns -f random -s
write_patterns gpu.pat -r
delete_patterns

delete_circuit
read_circuit ../circuits/exp.bench -l ../circuits/exp.grp.lev -n ../circuits/exp.name
set_simulator_options -l gpu -p 0 -d 0 -g 1
simulate_patterns -f random -s
write_patterns gpu.p1.pat -r
delete_patterns

# verify patterns
#exec diff references/ref.pat ocl-cpu.pat
#exec diff references/ref.pat host.pat
#exec diff references/ref.pat host.event.pat
#exec diff references/ref.pat gpu.pat
#exec diff references/ref.pat gpu.p1.pat

