#read_circuit s27.bench
#read_circuit s1196.bench
read_circuit ../circuits/exp.bench -l ../circuits/exp.grp.lev -n ../circuits/exp.name
set_random_patterns 3 -c 2

set_simulator_options -l host
simulate_patterns -f random -s
write_patterns host.pat -r
delete_patterns

set_simulator_options -l host -e on
simulate_patterns -f random -s
write_patterns host.event.pat -r
delete_patterns

