#read_circuit s27.bench
#read_circuit s1196.bench
read_circuit ../circuits/s38417.bench

set CPU_PFM_ID 0
set GPU_PFM_ID 1

create_ocl_context -p $CPU_PFM_ID
create_ocl_queue -p  $CPU_PFM_ID -d 0
set_ocl_options -g 1024 1 1 -l 128 1 1 -c buffer
set_random_patterns 3 -c 6

set_simulator_options -l ocl_cpu -p $CPU_PFM_ID -d 0
simulate_patterns -f random -s
write_patterns ocl-cpu.pat -r
delete_patterns

set_simulator_options -l host
simulate_patterns -f random -s
write_patterns host.pat -r
delete_patterns

set_simulator_options -l host -e on
simulate_patterns -f random -s
write_patterns host.event.pat -r
delete_patterns

## for ocl_vec mode
delete_circuit
read_circuit ../circuits/s38417.bench
release_ocl_context -p $CPU_PFM_ID
create_ocl_context -p $CPU_PFM_ID
create_ocl_queue -p $CPU_PFM_ID -d 0
set_ocl_options -g 1024 1 1 -l 128 1 1 -c buffer
set_random_patterns 3 -c 6
set_simulator_options -l ocl_cpu -p $CPU_PFM_ID -d 0 -v on -e off
simulate_patterns -f random -s
write_patterns ocl-cpu.vec.pat -r
delete_patterns

# Update configuration switch later
delete_circuit
read_circuit ../circuits/s38417.bench
create_ocl_context -p $GPU_PFM_ID
create_ocl_queue -p $GPU_PFM_ID -d 0
set_simulator_options -l gpu -p $GPU_PFM_ID -d 0 -g 0
simulate_patterns -f random -s
write_patterns gpu.pat -r
delete_patterns

delete_circuit
read_circuit ../circuits/s38417.bench
set_simulator_options -l gpu -p $GPU_PFM_ID -d 0 -g 1
simulate_patterns -f random -s
write_patterns gpu.p1.pat -r
delete_patterns

# verify patterns
puts "diff references/ref.pat ocl-cpu.pat"
exec diff references/ref.pat ocl-cpu.pat
puts "diff references/ref.pat ocl-cpu.vec.pat"
exec diff references/ref.pat ocl-cpu.vec.pat
puts "diff references/ref.pat host.pat"
exec diff references/ref.pat host.pat
puts "diff references/ref.pat host.event.pat"
exec diff references/ref.pat host.event.pat
puts "diff references/ref.pat gpu.pat"
exec diff references/ref.pat gpu.pat
puts "diff references/ref.pat gpu.p1.pat"
exec diff references/ref.pat gpu.p1.pat

