#read_circuit s27.bench
#read_circuit s1196.bench
read_circuit ../circuits/s38417.bench

set CPU_PFM_ID 0
set GPU_PFM_ID 1

set_random_patterns 1500 -c 6

create_ocl_context -p $CPU_PFM_ID
create_ocl_queue -p $CPU_PFM_ID -d 0
set_ocl_options -g 1024 1 1 -l 128 1 1 -c buffer

## vector mode with ocl_cpu
set_simulator_options -l ocl_cpu -p $CPU_PFM_ID -d 0 --vector_mode on
simulate_patterns -f random -s
write_patterns ocl-cpu.vec.rand.pat -r
delete_patterns

set_simulator_options -l ocl_cpu -p $CPU_PFM_ID -d 0 --vector_mode on
read_patterns ocl-cpu.vec.rand.pat
simulate_patterns -f external -s
write_patterns ocl-cpu.vec.ext.pat -r
delete_patterns


# host with non-event
set_simulator_options -l host
simulate_patterns -f random -s
write_patterns host.pat -r
delete_patterns


# host with event driven
set_simulator_options -l host -e on
simulate_patterns -f random -s
write_patterns host.event.pat -r
delete_patterns

read_patterns host.pat
simulate_patterns -f external -s
write_patterns host.event.ex.pat -r
delete_patterns

# verify patterns
puts "diff host.pat host.event.pat"
exec diff host.pat host.event.pat
puts "diff host.pat host.event.ex.pat"
exec diff host.pat host.event.ex.pat
puts "diff host.pat ocl-cpu.vec.rand.pat"
exec diff host.pat ocl-cpu.vec.rand.pat
puts "diff host.pat ocl-cpu.vec.ext.pat"
exec diff host.pat ocl-cpu.vec.ext.pat
