read_circuit ../circuits/s38417.bench --optimize --skip_buf_inv_opt

#set_random_patterns 3 -c 6
#set_random_patterns 129 -c 6
#set_random_patterns 128
set_random_patterns 15000 -c 6

set_simulator_options -l host
simulate_patterns -f random -s
write_patterns host.pat -r
#write_patterns host.1500c6.pat -r
delete_patterns

set_simulator_options -l host -e on
simulate_patterns -f random -s
write_patterns host.event.pat -r
delete_patterns

create_ocl_context -p 0
create_ocl_queue -p 0 -d 0
set_ocl_options -g 1024 1 1 -l 128 1 1 -c buffer
set_simulator_options -l gpu -p 0 -d 0 -g 1 -e on
simulate_patterns -f random -s
write_patterns gpu.p1.event.pat -r
delete_patterns

# verify patterns
#exec diff references/ref.pat gpu.p1.pat
exec diff host.pat host.event.pat
exec diff host.pat gpu.p1.event.pat
#exec diff references/host.1500.pat gpu.p1.event.pat
#exec diff references/ref.pat gpu.p1.event.pat

