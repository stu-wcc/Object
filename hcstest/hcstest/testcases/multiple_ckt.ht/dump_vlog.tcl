exec cp ../circuits/s27.bench .

read_circuit s27.bench
write_circuit s27.x10.lev --format lev --multiply 10 --replace
delete_circuit

read_circuit s27.bench -l s27.x10.lev -n s27.name --optimize --skip_buf_inv_opt
delete_circuit

read_circuit s27.bench -l s27.x10.opt.lev -n s27.name
write_circuit s27.x10.v --format vlog --replace


# check file difference
puts "diff s27.x10.lev     ./references/s27.x10.lev"
exec diff s27.x10.lev     ./references/s27.x10.lev
puts "diff s27.x10.opt.lev ./references/s27.x10.opt.lev"
exec diff s27.x10.opt.lev ./references/s27.x10.opt.lev
puts "diff s27.x10.v       ./references/s27.x10.v"
exec diff s27.x10.v       ./references/s27.x10.v
