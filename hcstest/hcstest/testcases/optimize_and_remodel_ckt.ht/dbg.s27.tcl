read_circuit s27.bench

set_random_patterns 3
set_simulator_options -l host

simulate_patterns -f random -s

write_patterns s27.3.pat -r

delete_patterns

set_random_patterns 150 -c 6

simulate_patterns -f random -s

write_patterns s27.150c6.pat -r
