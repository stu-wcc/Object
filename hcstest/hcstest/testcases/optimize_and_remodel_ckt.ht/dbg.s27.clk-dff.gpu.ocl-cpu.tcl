read_circuit s27.bench -l s27.clk-dff.lev -n s27.name --optimize --skip_buf_inv_opt

#delete_circuit

#read_circuit s27.bench -l s27.clk-dff.opt.lev -n s27.name

add_clocks 0 --id 19

set_random_patterns 150 -c 6
#set_random_patterns 3

set_ocl_options -g 1024 1 1 -l 128 1 1 -c buffer

## ocl_cpu
create_ocl_context -p 1
create_ocl_queue -p 1 -d 0
set_simulator_options -l ocl_cpu -p 1 -d 0

simulate_patterns -f random -s
#write_patterns s27.clk-dff.3.ocl_cpu.pat -r
write_patterns s27.clk-dff.150c6.ocl_cpu.pat -r
delete_patterns

create_ocl_context -p 0
create_ocl_queue -p 0 -d 0

set_simulator_options -l gpu -p 0 -d 0 -g 1


simulate_patterns -f random -s
#write_patterns s27.clk-dff.3.gpu.pat -r
write_patterns s27.clk-dff.150c6.gpu.pat -r
delete_patterns

delete_circuit

read_circuit s27.bench -l s27.clk-dff.lev -n s27.name --optimize --skip_buf_inv_opt
add_clocks 0 --id 19

## ocl_cpu vec
release_ocl_context -p 1
create_ocl_context -p 1
create_ocl_queue -p 1 -d 0
set_simulator_options -l ocl_cpu -p 1 -d 0 -v on
simulate_patterns -f random -s
write_patterns s27.clk-dff.150c6.ocl_cpu.vec.pat -r
delete_patterns

#exec diff s27.clk-dff.3.ocl_cpu.pat references/s27.3.pat
#exec diff s27.clk-dff.3.gpu.pat s27.clk-dff.3.ocl_cpu.pat
#exec diff s27.clk-dff.3.gpu.pat references/s27.3.pat
exec diff s27.clk-dff.150c6.gpu.pat references/s27.150c6.pat
exec diff s27.clk-dff.150c6.ocl_cpu.pat references/s27.150c6.pat
exec diff s27.clk-dff.150c6.ocl_cpu.vec.pat references/s27.150c6.pat
