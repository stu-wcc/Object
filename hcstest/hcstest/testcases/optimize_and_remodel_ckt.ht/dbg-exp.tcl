#read_circuit ../circuits/exp.bench -l exp.mod.lev -n ../circuits/exp.name --optimize
read_circuit ../circuits/exp.bench -l exp.mod.opt.lev -n ../circuits/exp.name

set_random_patterns 64 -c 4

add_clocks 0 --id 1

set_simulator_options -l host

simulate_patterns -f random -s
write_patterns exp.host.pat -r
delete_patterns

set_simulator_options -l host -e on
simulate_patterns -f random -s
write_patterns exp.host-e.pat -r
delete_patterns

exec diff exp.host.pat exp.host-e.pat
