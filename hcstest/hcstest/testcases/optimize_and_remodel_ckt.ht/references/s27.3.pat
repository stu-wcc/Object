{
  "version": "0.1",
  "setup": {
    "number_of_PIs": 4,
    "number_of_POs": 1,
    "number_of_FFs": 3,
    "number_of_patterns": 3,
    "cycle_timing_template": {
      "force_pi": 0,
      "init_ff": 1,
      "measure_po": 2,
      "pulse_clk": 3,
      "observe_ff": 4
    }
  },
  "scan_test": [
    {
      "pattern": 0,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 1,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0111",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 2,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    }
  ]
}
