{
  "version": "0.1",
  "setup": {
    "number_of_PIs": 4,
    "number_of_POs": 1,
    "number_of_FFs": 3,
    "number_of_patterns": 150,
    "cycle_timing_template": {
      "force_pi": 0,
      "init_ff": 1,
      "measure_po": 2,
      "pulse_clk": 3,
      "observe_ff": 4
    }
  },
  "scan_test": [
    {
      "pattern": 0,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 1,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0111",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 2,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 3,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1111",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 4,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 5,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 6,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "010",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 7,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 8,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 9,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 10,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 11,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "010",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 12,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0100",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 13,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 14,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1111",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 15,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 16,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "011",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "011"
        }
      ]
    },
    {
      "pattern": 17,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "011",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 18,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 19,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 20,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 21,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 22,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 23,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 24,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "011",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 25,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1110",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 26,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 27,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 28,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "011",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 29,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1111",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 30,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 31,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 32,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 33,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 34,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 35,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 36,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 37,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 38,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1110",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 39,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 40,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 41,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0100",
          "init_ff": "011",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "011"
        }
      ]
    },
    {
      "pattern": 42,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 43,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1001",
          "init_ff": "000",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 44,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1111",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 45,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 46,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 47,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1111",
          "init_ff": "010",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 48,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 49,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0110",
          "init_ff": "011",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 50,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 51,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0100",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "011"
        }
      ]
    },
    {
      "pattern": 52,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 53,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 54,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 55,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0110",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 56,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 57,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1110",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 58,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "000",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 59,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 60,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 61,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1110",
          "init_ff": "011",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 62,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 63,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 64,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 65,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 66,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 67,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 68,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 69,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "011",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "011"
        }
      ]
    },
    {
      "pattern": 70,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "011",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "011"
        }
      ]
    },
    {
      "pattern": 71,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 72,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 73,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1001",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 74,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 75,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1001",
          "init_ff": "011",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 76,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 77,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 78,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 79,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "000",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 80,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "010",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 81,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 82,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0111",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 83,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 84,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 85,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 86,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1111",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 87,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 88,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1001",
          "init_ff": "000",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 89,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 90,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0110",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 91,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 92,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1001",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 93,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 94,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0100",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 95,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 96,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 97,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0100",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 98,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 99,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 100,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1110",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 101,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "010",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 102,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 103,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 104,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 105,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "000",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 106,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1001",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 107,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 108,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 109,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 110,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 111,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1110",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 112,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 113,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 114,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "011",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 115,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 116,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 117,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 118,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 119,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0111",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 120,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 121,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 122,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 123,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 124,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 125,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 126,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "010",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 127,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0110",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 128,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0110",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 129,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    },
    {
      "pattern": 130,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1000",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 131,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "011",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 132,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1101",
          "init_ff": "000",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 133,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 134,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 135,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1111",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 136,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 137,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1010",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 138,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 139,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0001",
          "init_ff": "011",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "011"
        }
      ]
    },
    {
      "pattern": 140,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0110",
          "init_ff": "100",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 141,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0010",
          "init_ff": "001",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 142,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0110",
          "init_ff": "011",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 143,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0000",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 144,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1011",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 145,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0111",
          "init_ff": "011",
          "measure_po": "0",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "010"
        }
      ]
    },
    {
      "pattern": 146,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1001",
          "init_ff": "110",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "100"
        }
      ]
    },
    {
      "pattern": 147,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "1100",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "101"
        }
      ]
    },
    {
      "pattern": 148,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0011",
          "init_ff": "101",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "000"
        }
      ]
    },
    {
      "pattern": 149,
      "cycles": [
        {
          "cycle": 0,
          "force_pi": "0101",
          "init_ff": "111",
          "measure_po": "1",
          "pulse_clk": [
            "all"
          ],
          "observe_ff": "001"
        }
      ]
    }
  ]
}
