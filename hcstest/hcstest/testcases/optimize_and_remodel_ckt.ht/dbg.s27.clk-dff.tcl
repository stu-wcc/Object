read_circuit s27.bench -l s27.clk-dff.lev -n s27.name --optimize --skip_buf_inv_opt

#delete_circuit

#read_circuit s27.bench -l s27.clk-dff.opt.lev -n s27.name

set_random_patterns 3
set_simulator_options -l host
#set_simulator_options -l host

#add_clocks 0 --id 5

add_clocks 0 --id 19

simulate_patterns -f random -s

write_patterns s27.clk-dff.3.pat -r


delete_patterns
set_random_patterns 150 -c 6

set_simulator_options -e on -t on
simulate_patterns -f random -s
write_patterns s27.clk-dff.150c6.e2.pat -r

delete_patterns

set_simulator_options -e on -t off
simulate_patterns -f random -s
write_patterns s27.clk-dff.150c6.e4.pat -r

delete_patterns

set_simulator_options -e off -t off
simulate_patterns -f random -s
write_patterns s27.clk-dff.150c6.ne4.pat -r

delete_patterns

exec diff s27.clk-dff.3.pat references/s27.3.pat
exec diff s27.clk-dff.150c6.e2.pat references/s27.150c6.pat
exec diff s27.clk-dff.150c6.e4.pat references/s27.150c6.pat
exec diff s27.clk-dff.150c6.ne4.pat references/s27.150c6.pat
