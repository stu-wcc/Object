read_circuit s38417.bench
create_ocl_context -p 1
create_ocl_queue -p 1 -d 0
set_random_patterns 1024 -c 6
set_ocl_options -g 1024 -l 256
set_simulator_options -l gpu -p 1 -d 0
simulate_patterns -f random -s
