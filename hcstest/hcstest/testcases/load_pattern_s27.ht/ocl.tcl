read_circuit s27.bench
create_ocl_context -p 0
create_ocl_queue -p 0 -d 0
set_random_patterns 1024 -c 6
set_simulator_options -l gpu -p 0 -d 0
simulate_patterns -f random -s
