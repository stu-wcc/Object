/////////////////////////////////////////////////////////////////
//                   AppCommonSetting.cpp
/////////////////////////////////////////////////////////////////

#include "AppCommonSetting.h"

AppCommonSetting::AppCommonSetting() : _teeStreamPtr( NULL ),
                                 _logOfsPtr( NULL ),
                                 _execPath( "" ),
                                 _logFileName( "" )
{
   ;
}


AppCommonSetting::~AppCommonSetting() {
   for( CMD_MAP_ITERATOR it=_commandMap.begin(); it!=_commandMap.end(); ++it) {
      if( it->second ) {
         delete it->second; // free memory for Command class
      }
   }
}
