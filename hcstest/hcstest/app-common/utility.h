/////////////////////////////////////////////////////////////////
//                         utility.h
//   Common utility functions and classes
//
/////////////////////////////////////////////////////////////////

#ifndef _UTILITY_H
#define _UTILITY_H

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <sys/sysinfo.h>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <ratio>
#include <random>
#include <chrono>
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>
#include "defines.h"

const unsigned MASK_1HOT_32[32] =
{
   0x00000001,
   0x00000002,
   0x00000004,
   0x00000008,
   0x00000010,
   0x00000020,
   0x00000040,
   0x00000080,
   0x00000100,
   0x00000200,
   0x00000400,
   0x00000800,
   0x00001000,
   0x00002000,
   0x00004000,
   0x00008000,
   0x00010000,
   0x00020000,
   0x00040000,
   0x00080000,
   0x00100000,
   0x00200000,
   0x00400000,
   0x00800000,
   0x01000000,
   0x02000000,
   0x04000000,
   0x08000000,
   0x10000000,
   0x20000000,
   0x40000000,
   0x80000000,
};


// The index is # of bits to be masked ( or # of parallel patterns 1-32)
//   As array index start from 0, all 0s is used at first entry.
const unsigned MASK_UPTO_32[33] =
{
   0x00000000,
   0x00000001,
   0x00000003,
   0x00000007,
   0x0000000f,
   0x0000001f,
   0x0000003f,
   0x0000007f,
   0x000000ff,
   0x000001ff,
   0x000003ff,
   0x000007ff,
   0x00000fff,
   0x00001fff,
   0x00003fff,
   0x00007fff,
   0x0000ffff,
   0x0001ffff,
   0x0003ffff,
   0x0007ffff,
   0x000fffff,
   0x001fffff,
   0x003fffff,
   0x007fffff,
   0x00ffffff,
   0x01ffffff,
   0x03ffffff,
   0x07ffffff,
   0x0fffffff,
   0x1fffffff,
   0x3fffffff,
   0x7fffffff,
   0xffffffff,
};


// The index stands for bit 0 to bit 63
//const uint64_t MASK_1HOT_64[64] =
const unsigned long MASK_1HOT_64[64] =
{
   0x0000000000000001,
   0x0000000000000002,
   0x0000000000000004,
   0x0000000000000008,
   0x0000000000000010,
   0x0000000000000020,
   0x0000000000000040,
   0x0000000000000080,
   0x0000000000000100,
   0x0000000000000200,
   0x0000000000000400,
   0x0000000000000800,
   0x0000000000001000,
   0x0000000000002000,
   0x0000000000004000,
   0x0000000000008000,
   0x0000000000010000,
   0x0000000000020000,
   0x0000000000040000,
   0x0000000000080000,
   0x0000000000100000,
   0x0000000000200000,
   0x0000000000400000,
   0x0000000000800000,
   0x0000000001000000,
   0x0000000002000000,
   0x0000000004000000,
   0x0000000008000000,
   0x0000000010000000,
   0x0000000020000000,
   0x0000000040000000,
   0x0000000080000000,
   0x0000000100000000,
   0x0000000200000000,
   0x0000000400000000,
   0x0000000800000000,
   0x0000001000000000,
   0x0000002000000000,
   0x0000004000000000,
   0x0000008000000000,
   0x0000010000000000,
   0x0000020000000000,
   0x0000040000000000,
   0x0000080000000000,
   0x0000100000000000,
   0x0000200000000000,
   0x0000400000000000,
   0x0000800000000000,
   0x0001000000000000,
   0x0002000000000000,
   0x0004000000000000,
   0x0008000000000000,
   0x0010000000000000,
   0x0020000000000000,
   0x0040000000000000,
   0x0080000000000000,
   0x0100000000000000,
   0x0200000000000000,
   0x0400000000000000,
   0x0800000000000000,
   0x1000000000000000,
   0x2000000000000000,
   0x4000000000000000,
   0x8000000000000000
};

// The index is # of bits to be masked ( or # of parallel patterns 1-64)
//   As array index start from 0, all 0s is used at first entry.
//const uint64_t MASK_UPTO_64[65] =
const unsigned long MASK_UPTO_64[65] =
{
   0x0000000000000000,
   0x0000000000000001,
   0x0000000000000003,
   0x0000000000000007,
   0x000000000000000f,
   0x000000000000001f,
   0x000000000000003f,
   0x000000000000007f,
   0x00000000000000ff,
   0x00000000000001ff,
   0x00000000000003ff,
   0x00000000000007ff,
   0x0000000000000fff,
   0x0000000000001fff,
   0x0000000000003fff,
   0x0000000000007fff,
   0x000000000000ffff,
   0x000000000001ffff,
   0x000000000003ffff,
   0x000000000007ffff,
   0x00000000000fffff,
   0x00000000001fffff,
   0x00000000003fffff,
   0x00000000007fffff,
   0x0000000000ffffff,
   0x0000000001ffffff,
   0x0000000003ffffff,
   0x0000000007ffffff,
   0x000000000fffffff,
   0x000000001fffffff,
   0x000000003fffffff,
   0x000000007fffffff,
   0x00000000ffffffff,
   0x00000001ffffffff,
   0x00000003ffffffff,
   0x00000007ffffffff,
   0x0000000fffffffff,
   0x0000001fffffffff,
   0x0000003fffffffff,
   0x0000007fffffffff,
   0x000000ffffffffff,
   0x000001ffffffffff,
   0x000003ffffffffff,
   0x000007ffffffffff,
   0x00000fffffffffff,
   0x00001fffffffffff,
   0x00003fffffffffff,
   0x00007fffffffffff,
   0x0000ffffffffffff,
   0x0001ffffffffffff,
   0x0003ffffffffffff,
   0x0007ffffffffffff,
   0x000fffffffffffff,
   0x001fffffffffffff,
   0x003fffffffffffff,
   0x007fffffffffffff,
   0x00ffffffffffffff,
   0x01ffffffffffffff,
   0x03ffffffffffffff,
   0x07ffffffffffffff,
   0x0fffffffffffffff,
   0x1fffffffffffffff,
   0x3fffffffffffffff,
   0x7fffffffffffffff,
   0xffffffffffffffff,
};

namespace po = boost::program_options;
namespace pt = boost::posix_time;

typedef enum {
         SECOND_PRECISION,
   MILLI_SECOND_PRECISION,
   MICRO_SECOND_PRECISION
} TimePrecision;


// Convert unsigned mask to binary string
std::string mask32ToBinary( unsigned val );
std::string mask64ToBinary( uint64_t val );

// Check file existence
bool fileExists( const std::string& fileName );

// Get file extension
std::string getFileExtension( const std::string& fileName);

// Get path of executable
std::string executablePath(const char *argv0);
std::string executablePathFallback(const char *argv0);

// Test if a string is binary (0 and 1) or tri-state (0, 1 and X)
bool isBinaryString( const std::string& str );
bool isTristateString( const std::string& str );

// Print string to both stdout and file
void tee( FILE *f, char const *fmt, ...);

// Print string to both stdout and log file
//void hprintf( char const *fmt, ...);

// Get current date and time in milli or micro seconds
const std::string currentDateTime( TimePrecision tp=MICRO_SECOND_PRECISION );

// Measure elapsed time
double elapsedSysTimeInSeconds( std::chrono::system_clock::time_point& start_t,
                                std::chrono::system_clock::time_point& end_t );

double elapsedCpuTimeInSeconds( std::clock_t & start_cpu_t,
                                std::clock_t & end_cpu_t );

// Monitor memory usage
void getMemoryUsage(double& vm_usage, double& resident_set);

// Hybrid stream class to print on both stdout and log file
// It can be replace with boost logging utility later.
class Hstream {
   public:
   std::ofstream coss;
   Hstream(void) { ;}
   ~Hstream(void) { ;}
   Hstream& operator<< (std::ostream& (*pfun)(std::ostream&)) {
      pfun(coss);
      pfun(std::cout);
      return *this;
   };
};

template <class T> Hstream& operator<< (Hstream& st, T val)
{
   st.coss << val;
   std::cout << val;
   return st;
};


//////////////////////////////////////////////////////////////////
//         Wrapper class for random number generation
//  Random patterns are generated 64 bits per pass with Mersenne Twister
//    pseudo-random generator instead of previous rand(). It is much
//    more efficent. Below is the code snippet of refactoring old
//    rand() with MT19977 for random pattern generation.
//
//  NOTE:
//    In order to ensure repeatablility, random pattern generator
//    need to be re-seeded upon each call of "simulate_patterns".
//
//       if( 1 ) {
//          val = ((htDB::instance())->randomGen()).mt64Rand();
//          val &= MASK_UPTO_64[parallel];
//       } else {
//          uint64_t mask = 1;
//          for( int p=0; p<parallel; p++ ) {
//             int dice = ((htDB::instance())->randomGen()).getInt(0,1);
//             if( dice ) { // set corresponding bit to 1
//                val |= mask;
//             }
//             mask = mask << 1; // left shift mask by 1
//          }
//       }
//
//////////////////////////////////////////////////////////////////
class Random {
   public:
      Random() { ; }
      ~Random() { ; }

      void seed( unsigned int s ) { srand(s); }

      int getInt( int low, int high ) {
         if( low>=high ) {
            return low;
         } else {
            return low + (rand()%(high-low+1));
         }
      }

      // Wrapper function for Mersenne Twister pseudo-random generator
      void seedMt32Rand( unsigned s ) { _mt32Rand.seed( s ); }
      void seedMt64Rand( unsigned s ) { _mt64Rand.seed( s ); }
      unsigned mt32Rand() { return _mt32Rand(); }
      uint64_t mt64Rand() { return _mt64Rand(); }


   private:
      std::mt19937    _mt32Rand;  // 32-bit unsigned random number
      std::mt19937_64 _mt64Rand;  // 64-bit unsigned random number
};

#endif
