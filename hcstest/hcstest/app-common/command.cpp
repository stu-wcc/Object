/////////////////////////////////////////////////////////////////
//                       command.cpp
/////////////////////////////////////////////////////////////////

#include "command.h"

//////////////////////////////////////////////////////////////////////
//   Convert objv[] of tcl command to argv such that it can be
//      parsed by boost option parser
//////////////////////////////////////////////////////////////////////
void getArgvFromTclCmd( char** argv,
                           int objc,
                Tcl_Obj *CONST objv[] ) {
   for( int i=0; i<objc; i++) {
      argv[i] = Tcl_GetString( objv[i] );
   }
}

//////////////////////////////////////////////////////////////////////
// Macro extension to define C function/procedure for a tcl command
// 1st argument is the name of C function
// 2nd argument is the corresponding command class
//////////////////////////////////////////////////////////////////////
DEFINE_TCL_CMD_PROC(                HelpCmdProc,                 HelpCmd );
DEFINE_TCL_CMD_PROC(          ReportDateCmdProc,           ReportDateCmd );
DEFINE_TCL_CMD_PROC(   ReportMemoryUsageCmdProc,    ReportMemoryUsageCmd );


///////////////////////////////////////////////////////////////////////
// Central place to register all user-defined tcl commands
// newTclCommand() is a macro of Tcl_CreateObjCommand and sentences
//   to update command map in appCommonDB.
// For example,
//   Tcl_CreateObjCommand( interp, "help", tclCmdHelp,
//                       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
//   (appCommonDB::instance())->commandMap()["help"] = new HelpCmd;
///////////////////////////////////////////////////////////////////////
void registerSharedTclCommands( Tcl_Interp* interp ) {

   newTclCommand( "help",
                   HelpCmd,
                   HelpCmdProc );
   newTclCommand( "report_date",
                   ReportDateCmd,
                   ReportDateCmdProc );
   newTclCommand( "report_memory_usage",
                   ReportMemoryUsageCmd,
                   ReportMemoryUsageCmdProc );
}


// Constructor for Command
Command::Command():_options("options"),
                   _hidden ( false )
{
   ;
}


// Destructor for Command
Command::~Command() {
   ;
}


bool Command::parse(int argc, char** argv) {
   // Print in log 'command' comment for every user-defined command
   //   until a generic mechanism is added for command dispatcher which
   //   will work for built-in command as well.
   //   Now do that for stdout as well.
   //OfsLog << InfoCommand;
   HLog << InfoCommand;
   for( int ix=0; ix<argc; ix++) {
      //OfsLog << argv[ix] << " ";
      HLog << argv[ix] << " ";
   }
   //OfsLog << std::endl; // Equvalent to "\n" + flush()
   HLog << std::endl; // Equvalent to "\n" + flush()

   // Clear variables map before parsing
   _variablesMap.clear();
   try {
      po::store(po::command_line_parser(argc, argv)
                .options( _options )
                .positional( _posOptions)
                .run(), _variablesMap);

      po::notify( _variablesMap );

   } catch(std::exception& e) {
      HLog << InfoError << e.what() << std::endl;
      return false;

   } catch(...) {
      HLog << InfoError << "Unknown error!" << std::endl;
      return false;
   }
   return true;
}

// Constructor for HelpCmd
HelpCmd::HelpCmd() : Command() {
   _options.add_options()
      ("cmd", po::value< std::string >(), "print help message for a command");
   _posOptions.add("cmd", 1);
}


// Destructor for HelpCmd
HelpCmd::~HelpCmd() {
   ;
}

bool HelpCmd::action() {
   bool result=true;
   if( _variablesMap.count("cmd")) {
      std::string cmd_name = _variablesMap["cmd"].as<std::string>();
      CMD_MAP_ITERATOR it =
         (appCommonDB::instance()->commandMap()).find(cmd_name);
      if((appCommonDB::instance()->commandMap()).end()==it) {
         HLog << InfoError << "Command '" << cmd_name << "' is unknown\n";
         result = false;
      } else {
         if( it->second->isHidden() ) {
            HLog << InfoError << "Command '" << cmd_name << "' is unknown\n";
         }
         (it->second)->help();
      }
   } else {
      HLog << "List of supported commands:\n";
      for( CMD_MAP_CONST_ITERATOR
         it =(appCommonDB::instance()->commandMap()).begin();
         it!=(appCommonDB::instance()->commandMap()).end();
         ++it) {
         if( !(it->second->isHidden()) && ("help"!=it->first) ) {
            HLog << it->first << "\n";
         }
      }
   }
   HLog.flush();
   return result;
}


void HelpCmd::help() const {
   HLog << "Usage: help [command]\n";
   HLog.flush();
}

// Constructor for ReportDateCmd
ReportDateCmd::ReportDateCmd() : Command() {
   _options.add_options()
      ("precision,p",po::value<std::string>(),"specify precision in s|ms|us");
}


// Destructor for ReportDateCmd
ReportDateCmd::~ReportDateCmd() {
   ;
}

bool ReportDateCmd::action() {
   bool result=true;
   if( _variablesMap.count("precision")) {
      std::string p_str = _variablesMap["precision"].as<std::string>();
      if( "s"==p_str ) {
         HLog << currentDateTime( SECOND_PRECISION ) << "\n";
      } else if( "ms"==p_str ) {
         HLog << currentDateTime( MILLI_SECOND_PRECISION ) << "\n";
      } else if( "us"==p_str ) {
         HLog << currentDateTime( MICRO_SECOND_PRECISION ) << "\n";
      } else {
         HLog << InfoError << "unsupported precision '" << p_str << "'\n";
         help();
         result = false;
      }
   } else { // default precision is in seconds
      HLog << currentDateTime( SECOND_PRECISION ) << "\n";
   }
   HLog.flush();
   return result;
}


void ReportDateCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: report_date [-p [--precision] s|ms|us ]\n";
   HLog.flush();
}


// Constructor for ReportMemoryUsageCmd
ReportMemoryUsageCmd::ReportMemoryUsageCmd() : Command() {
   //_options.add_options()
   //   ("platform,p", po::value<int>(), "specify OpenCL platform id")
   //   ("device,d", po::value<int>(), "specify OpenCL device id")
   //   ("verbose,v", po::bool_switch()->default_value(false),
   //                   "print verbose information");
}


// Destructor for ReportMemoryUsageCmd
ReportMemoryUsageCmd::~ReportMemoryUsageCmd() {
   ;
}


void ReportMemoryUsageCmd::help() const {
   HLog << "Usage: report_memory_usage\n";
   HLog.flush();
}


bool ReportMemoryUsageCmd::action() {
   double vm_usage=0.0;
   double resident_set=0.0;

   getMemoryUsage( vm_usage, resident_set );

   HLog << "@ " << currentDateTime( SECOND_PRECISION )
        << std::endl;
   HLog << " Virtual Memory (KiB): " << (unsigned long long)vm_usage << "\n"
        << "Resident Memory (KiB): " << (unsigned long long)resident_set
        << std::endl;
   return true;
}
