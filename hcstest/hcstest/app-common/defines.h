/////////////////////////////////////////////////////////////////
//                         defines.h
//   Common #define's shared among various applications
//
/////////////////////////////////////////////////////////////////

#ifndef _DEFINES_H
#define _DEFINES_H

#define InfoCommand "//  command: "
#define InfoComment "//  "
#define InfoNote    "//  Note: "
#define InfoError   "//  Error: "
#define InfoWarn    "//  Warning: "
#define InfoCont    "//    "
#define TwoSpace    "  "
#define FourSpace   "    "

#define ALL_ONES_64 0xffffffffffffffff
#define ALL_ONES_32 0xffffffff
#define ALL_ZEROS   0

#endif
