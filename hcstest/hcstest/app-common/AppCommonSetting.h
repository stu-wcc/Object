/////////////////////////////////////////////////////////////////
//                      AppCommonSetting.h
//  Class to define common application settings. It is
//  delcared as singleton class.
/////////////////////////////////////////////////////////////////

#ifndef _APP_COMMON_DB_H
#define _APP_COMMON_DB_H

#include <map>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>
#include "utility.h"
#include "singleton.h"
#include "command.h"

#define HLog (appCommonDB::instance())->teeStream()
#define OfsLog (appCommonDB::instance())->logOfstream()

typedef std::map< std::string, class Command* >::iterator
   CMD_MAP_ITERATOR;
typedef std::map< std::string, class Command* >::const_iterator
   CMD_MAP_CONST_ITERATOR;

namespace bio = boost::iostreams;

typedef bio::tee_device< std::ostream, std::ofstream > TeeDevice;
typedef bio::stream< TeeDevice > TeeStream;

class AppCommonSetting {
   public:
   AppCommonSetting();
   ~AppCommonSetting();

   std::map< std::string, class Command* >& commandMap() { return _commandMap; }

   void teeStream( TeeStream & ts ) { _teeStreamPtr = &ts; }
   TeeStream& teeStream() { return *(_teeStreamPtr); }

   void logOfstream( std::ofstream & ofs ) { _logOfsPtr = &ofs; }
   std::ofstream& logOfstream() { return *(_logOfsPtr); }

   void execPath( const std::string & path ) { _execPath = path; }
   const std::string & execPath() const { return _execPath; }

   void logFileName( const std::string & name ) { _logFileName = name; }
   const std::string & logFileName() const { return _logFileName; }


   protected:

   private:
   std::map< std::string, class Command* > _commandMap;
   TeeStream * _teeStreamPtr;
   std::ofstream * _logOfsPtr;
   std::string _execPath;
   std::string _logFileName;

};

typedef Singleton< AppCommonSetting > appCommonDB;  // global declaration of configuration DB

#endif
