/////////////////////////////////////////////////////////////////
//                       command.h
//  Macro, function, and class defintion for app-common tcl
//  commands.
/////////////////////////////////////////////////////////////////

#ifndef _COMMAND_H
#define _COMMAND_H

#include <tcl.h>
#include "utility.h"
#include "AppCommonSetting.h"

/////////////////////////////////////////////////////////////
//   Macro to create a C function for tcl commands
/////////////////////////////////////////////////////////////

// Macro to define tcl command procedure
#define DEFINE_TCL_CMD_PROC(cmd_function, cmd_class)\
 int cmd_function(    ClientData clientData,     \
                     Tcl_Interp* interp,         \
                             int objc,           \
                  Tcl_Obj *CONST objv[] )        \
{                                                \
   std::vector< char* > argvv( objc );           \
   getArgvFromTclCmd( &(argvv[0]), objc, objv);  \
   cmd_class cmd;                                \
   if( cmd.parse(objc, &argvv[0]) ) {            \
      if( cmd.action() ) {                       \
          return TCL_OK;                         \
      }                                          \
   } else {                                      \
     cmd.help();                                 \
   }                                             \
   return TCL_ERROR;                             \
}                                                \

// Macro to register tcl command
#define newTclCommand(cmd_name, cmd_class, cmd_function)\
   Tcl_CreateObjCommand( interp,                        \
                         cmd_name,                      \
                         cmd_function,                  \
                         (ClientData)NULL,              \
                         (Tcl_CmdDeleteProc *)NULL);    \
   (appCommonDB::instance())->commandMap()[cmd_name] = new cmd_class;


void registerSharedTclCommands( Tcl_Interp* interp );


/////////////////////////////////////////////////////////////
//    Base class for user-defined command
/////////////////////////////////////////////////////////////
class Command {
   public:
      Command();  // constructor
      virtual ~Command(); // destructor
      virtual bool parse(int argc, char** argv); // parse command option
      virtual bool action() =0;
      virtual void help() const =0;
      bool isHidden() const { return _hidden; }
      void hide() { _hidden = true; }

   protected:
      po::options_description _options;
      po::positional_options_description _posOptions;
      po::variables_map  _variablesMap;

   private:
      bool _hidden;

};

/////////////////////////////////////////////////////////////
//    Class definition for 'help' command
/////////////////////////////////////////////////////////////
class HelpCmd : public Command {
   public:
      HelpCmd();
      ~HelpCmd();
      bool action();
      void help() const;

   protected:
   private:
};

/////////////////////////////////////////////////////////////
//    Class definition for 'report_date' command
/////////////////////////////////////////////////////////////
class ReportDateCmd : public Command {
   public:
      ReportDateCmd();
      ~ReportDateCmd();
      bool action();
      void help() const;

   protected:
   private:
};


/////////////////////////////////////////////////////////////
//    Class definition for 'report_memory_usage' command
/////////////////////////////////////////////////////////////
class ReportMemoryUsageCmd : public Command {
   public:
      ReportMemoryUsageCmd();
      ~ReportMemoryUsageCmd();
      bool action();
      void help() const;

   protected:
   private:
};


// Get 'argc' and 'argv' of tcl command; to be parsed by option parser
void getArgvFromTclCmd( char** argv,
                           int objc,
                Tcl_Obj *CONST objv[] );

#endif
