/////////////////////////////////////////////////////////////////
//                        singleton.h
//   Template class to instantiate singleton class object.
//
/////////////////////////////////////////////////////////////////

#ifndef _SINGLETON_H
#define _SINGLETON_H

#include <stddef.h>   // defines NULL
#include <assert.h>

template <class T>
class Singleton {
   public:
      static T* instance() {
         if( !_pInstance) _pInstance = new T;
         assert( _pInstance != NULL );
         return _pInstance;
      }

   protected:
      Singleton();
      ~Singleton();

   private:
      Singleton( Singleton const&);
      Singleton& operator=( Singleton const&);
      static T* _pInstance;
};

template <class T> T* Singleton<T>::_pInstance=NULL;

#endif
