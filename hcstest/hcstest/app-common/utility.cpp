/////////////////////////////////////////////////////////////////
//                       utility.cpp
/////////////////////////////////////////////////////////////////

#include "utility.h"

// convert mask to binary string
std::string mask32ToBinary( unsigned val) {
   unsigned mm = 0x80000000;
   std::string str("");
   for( int i=0; i<32; i++) {
      if( val & mm ) {
          str += "1";
      } else {
          str += "0";
      }
      mm >>= 1;
   }
   return str;
}


std::string mask64ToBinary( uint64_t val ) {
   uint64_t mm = 0x8000000000000000;
   std::string str("");
   for( int i=0; i<64; i++) {
      if( val & mm ) {
          str += "1";
      } else {
          str += "0";
      }
      mm >>= 1;
   }
   return str;
}


// Check file existence
bool fileExists( const std::string& fileName ) {
   std::ifstream inFile( fileName.c_str() );
   return inFile.is_open();
}


// Get file extension
std::string getFileExtension( const std::string& fileName) {
   if( fileName.find_last_of(".") !=  std::string::npos) {
      return fileName.substr( fileName.find_last_of(".")+1 );
   }
   return "";
}

// Get path of executable
std::string executablePath( const char *argv0 ) {
   char buf[1024] = {0};
   ssize_t size = readlink("/proc/self/exe", buf, sizeof(buf));
   if( size == 0 || size == sizeof(buf) ) {
      return executablePathFallback(argv0);
   }
   std::string path(buf, size);
   boost::system::error_code ec;
   boost::filesystem::path p(
      boost::filesystem::canonical(
         path, boost::filesystem::current_path(), ec));
   return p.make_preferred().string();
}

// Get absolute path of executable fallback
std::string executablePathFallback(const char *argv0) {
   if( NULL==argv0 || 0==argv0[0] ) {
      return "";
   }
   boost::system::error_code ec;
   boost::filesystem::path p(
      boost::filesystem::canonical(
         argv0, boost::filesystem::current_path(), ec));
   return p.make_preferred().string();
}


bool isBinaryString( const std::string& str ) {
   if( 0==str.size() ) {
      return false;
   }
   for( std::string::const_iterator it=str.begin(); it!=str.end(); ++it ) {
      if( '0'!=*it && '1'!=*it ) {
         return false;
      }
   }
   return true;
}


bool isTristateString( const std::string& str ) {
   if( 0==str.size() ) return false;
   for( std::string::const_iterator it=str.begin(); it!=str.end(); ++it ) {
      if( '0'!=*it && '1'!=*it && 'X'!=*it ) { // Disallow lowercase 'x'
         return false;
      }
   }
   return true;
}


// Printf string to both stdout and file
void tee( FILE *fp, char const *fmt, ...) {
   va_list ap;
   va_start(ap, fmt);
   vprintf(fmt, ap);
   va_end(ap);
   va_start(ap, fmt);
   vfprintf(fp, fmt, ap);
   va_end(ap);
}


// Print string to both stdout and log file, if needed
/*
extern FILE * logFile;
void hprintf( char const *fmt, ...) {
   va_list ap;
   va_start(ap, fmt);
   vprintf(fmt, ap);
   va_end(ap);
   va_start(ap, fmt);
   vfprintf(logFile, fmt, ap);
   va_end(ap);
}
*/

// Obtain current date and time with precision up to micro or milli seconds.
// "tp" can be SECOND, MILLI_SECOND or MICRO_SECOND
const std::string currentDateTime( TimePrecision tp ) {
   std::string timeStr("");
   switch( tp ) {
      case SECOND_PRECISION: {
         pt::ptime current_date_seconds = pt::second_clock::local_time();
         timeStr = pt::to_simple_string( current_date_seconds );
         break;
      }
      case MICRO_SECOND_PRECISION: {
         pt::ptime current_date_useconds = pt::microsec_clock::local_time();
         timeStr = pt::to_simple_string( current_date_useconds );
         break;
      }
      case MILLI_SECOND_PRECISION: {
         pt::ptime current_date_useconds = pt::microsec_clock::local_time();
         long milliseconds =
            current_date_useconds.time_of_day().total_milliseconds();
         pt::time_duration current_time_milliseconds =
            pt::milliseconds(milliseconds);
         pt::ptime current_date_mseconds( current_date_useconds.date(),
                                          current_time_milliseconds);

         timeStr = pt::to_simple_string( current_date_mseconds );
         timeStr = timeStr.replace(timeStr.end()-3, timeStr.end(), "");
         break;
      }
   }
   return timeStr;
}


double elapsedSysTimeInSeconds( std::chrono::system_clock::time_point& start_t,
                                std::chrono::system_clock::time_point& end_t )
{
   std::chrono::duration<double> time_span =
      std::chrono::duration_cast<std::chrono::duration<double>>(end_t-start_t);
   return time_span.count();
}


double elapsedCpuTimeInSeconds( std::clock_t & start_cpu_t,
                                std::clock_t & end_cpu_t )
{
   double cpu_duration = (end_cpu_t - start_cpu_t) / (double)CLOCKS_PER_SEC;
   return cpu_duration;
}


void getMemoryUsage(double& vm_usage, double& resident_set) {

   vm_usage     = 0.0;
   resident_set = 0.0;

   // the two fields we want
   unsigned long vsize;
   long rss;
   {
      std::string ignore;
      std::ifstream ifs("/proc/self/stat", std::ios_base::in);
      ifs >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
          >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
          >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
          >> ignore >> ignore >> ignore >> ignore >> vsize >> rss;
   }

   // in case x86-64 is configured to use 2MB pages
   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024;
   vm_usage = vsize / 1024.0;
   resident_set = rss * page_size_kb;
}
