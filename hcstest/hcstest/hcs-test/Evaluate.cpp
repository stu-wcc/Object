//////////////////////////////////////////////////////////////////
//                       Evaluate.cpp
//
//  function definition for logic gate evaluation in host cpu
//
//////////////////////////////////////////////////////////////////

#include "app-common/utility.h"
#include "HtSetting.h"
#include "Evaluate.h"
#include "Event.h"
#include "ExampleCL.h"
#include "EvaluateCL.h"
#include <assert.h>
#include <algorithm>
#include "StaticLearning.h"

#include <inttypes.h> // to allow macro PRIu64
#define __STDC_FORMAT_MACROS 1

Evaluate::Evaluate() : _cktPtr( NULL ),
                       _numCycles( 0 ),
#ifdef HT_PROFILE
                       _numGateEvals( 0 ),
#endif
                       _simSysTime( 0 ),
                       _simCpuTime( 0 )

{
   ;
}


Evaluate::Evaluate( LevelizedNetlist & ckt ) : _cktPtr( &ckt ),
                                               _numCycles( 0 ),
#ifdef HT_PROFILE
                                               _numGateEvals( 0 ),
#endif
                                               _simSysTime( 0 ),
                                               _simCpuTime( 0 )
{
   _gmv0.resize( ckt.numNodes() );
   _gmv1.resize( ckt.numNodes() );
   _gmv0x4.resize( ckt.numNodes() );
   _gmv1x4.resize( ckt.numNodes() );
#ifdef HT_PROFILE
   _numGateEvalsPerFrame.resize( 1 ); // to accommodate simulation
                                      // in identifying feedback loops
#endif
   std::fill( _gmv0.begin(), _gmv0.end(), ALL_ONES_64 ); // init to X:(1,0)
   std::fill( _gmv1.begin(), _gmv1.end(), 0 );
}


Evaluate::~Evaluate() {
   ;
}


void Evaluate::gmv0x4(int gid, int i, uint64_t v ) {
   assert( i >= 0 && i < 4 );
   _gmv0x4[gid].s[i] = v;
   //switch( i ) {
   //   case 0: { _gmv0x4[gid].s[0] = v; break; }
   //   case 1: { _gmv0x4[gid].s[1] = v; break; }
   //   case 2: { _gmv0x4[gid].s[2] = v; break; }
   //   case 3: { _gmv0x4[gid].s[3] = v; break; }
   //   default: { HLog << "unexpected i=" << i << std::endl; assert(0); }
   //}
}


void Evaluate::gmv1x4(int gid, int i, uint64_t v ) {
   assert( i >= 0 && i < 4 );
   _gmv1x4[gid].s[i] = v;
   //switch( i ) {
   //   case 0: { _gmv1x4[gid].s[0] = v; break; }
   //   case 1: { _gmv1x4[gid].s[1] = v; break; }
   //   case 2: { _gmv1x4[gid].s[2] = v; break; }
   //   case 3: { _gmv1x4[gid].s[3] = v; break; }
   //   default: { HLog << "unexpected i=" << i << std::endl; assert(0); }
   //}
}


void Evaluate::gmv0x4Dff(int gid, int i, uint64_t v ) {
   if( clockedDffIsUsed() ) {
      gmv0x4( gid+2, i, v ); // slave MUX
      gmv0x4( gid+3, i, v ); // master DE
      gmv0x4( gid+4, i, v ); // slave DE
   } else {
      gmv0x4( gid, i, v);
   }
}


void Evaluate::gmv1x4Dff(int gid, int i, uint64_t v ) {
   if( clockedDffIsUsed() ) {
      gmv1x4( gid+2, i, v ); // slave MUX
      gmv1x4( gid+3, i, v ); // master DE
      gmv1x4( gid+4, i, v ); // slave DE
   } else {
      gmv1x4( gid, i, v);
   }
}


uint64_t Evaluate::gmv0x4(int gid, int i) const {
   assert( i >= 0 && i < 4 );
   uint64_t val = _gmv0x4[gid].s[i];
   //switch( i ) {
   //   case 0: { val = _gmv0x4[gid].s[0]; break; }
   //   case 1: { val = _gmv0x4[gid].s[1]; break; }
   //   case 2: { val = _gmv0x4[gid].s[2]; break; }
   //   case 3: { val = _gmv0x4[gid].s[3]; break; }
   //   default: { HLog << "unexpected i=" << i << std::endl; assert(0); }
   //}
   return val;
}


uint64_t Evaluate::gmv1x4(int gid, int i) const {
   assert( i >= 0 && i < 4 );
   uint64_t val = _gmv1x4[gid].s[i];
   //switch( i ) {
   //   case 0: { val = _gmv1x4[gid].s[0]; break; }
   //   case 1: { val = _gmv1x4[gid].s[1]; break; }
   //   case 2: { val = _gmv1x4[gid].s[2]; break; }
   //   case 3: { val = _gmv1x4[gid].s[3]; break; }
   //   default: { HLog << "unexpected i=" << i << std::endl; assert(0); }
   //}
   return val;
}


void Evaluate::gmv0Dff(int gid, uint64_t v) {
   if( clockedDffIsUsed() ) {
      gmv0( gid+2, v ); // slave MUX
      gmv0( gid+3, v ); // master DE
      gmv0( gid+4, v ); // slave DE
   } else {
      gmv0( gid, v);
   }
}


void Evaluate::gmv1Dff(int gid, uint64_t v) {
   if( clockedDffIsUsed() ) {
      gmv1( gid+2, v ); // slave MUX
      gmv1( gid+3, v ); // master DE
      gmv1( gid+4, v ); // slave DE
   } else {
      gmv1( gid, v);
   }
}






void Evaluate::resetMeasurements() {

#ifdef HT_PROFILE
   _numGateEvals = 0;
   _numGateEvalsPerFrame.assign( numCycles()*numFramesPerCycle() +1, 0 );
   _numGateEvalsPerFrame.shrink_to_fit();
#endif
   _simSysTime = 0;
   _simCpuTime = 0;
}


void Evaluate::printSimulationStatistics() const {
   HLog << std::endl;
#ifdef HT_PROFILE
   HLog << TwoSpace
        << "Number of gate evaluations: " << numGateEvals() << "\n";
   unsigned long sum_evals = 0;
   for( unsigned f=0; f < _numGateEvalsPerFrame.size(); f++ ) {
      char buf[1024];
      sum_evals += _numGateEvalsPerFrame[f];
      sprintf(buf, "frame:%3u;  evals:%6lu", f, _numGateEvalsPerFrame[f]);
      HLog << TwoSpace << TwoSpace << buf << std::endl;
   }
   HLog << TwoSpace
        << "Summation of frame evals: " << sum_evals << "\n";
#endif
   HLog << TwoSpace
        << "elapsed Wall time: " << simSysTime() << "\n"
        << TwoSpace
        << "elapsed  CPU time: " << simCpuTime() << "\n"
        << std::endl;
   SimulationOptions & sim_opt = (htDB::instance())->simOptions();
   SimLocation sim_loc = sim_opt.simLocation();
   EvaluateCL& eval_cl = (htDB::instance())->evalCl();
   if( OCL_GPU == sim_loc || OCL_CPU == sim_loc ) {
      HLog << TwoSpace
           << "number of good sim enqueues: "
           << eval_cl.numGsimEnqueues() << "\n"
           << TwoSpace
           << "number of gmv copies from host to GPU: "
           << eval_cl.numGmvCopiesHost2Gpu() << "\n"
           << TwoSpace
           << "number of gmv copies from GPU to host: "
           << eval_cl.numGmvCopiesGpu2Host() << "\n"
           << TwoSpace
           << "Wall time for copying gmv from host to GPU: "
           << eval_cl.elapsedSysTimeGmvHost2Gpu() << "\n"
           << TwoSpace
           << " CPU time for copying gmv from host to GPU: "
           << eval_cl.elapsedCpuTimeGmvHost2Gpu() << "\n"
           << TwoSpace
           << "Wall time for copying gmv from GPU to host: "
           << eval_cl.elapsedSysTimeGmvGpu2Host() << "\n"
           << TwoSpace
           << " CPU time for copying gmv from GPU to host: "
           << eval_cl.elapsedCpuTimeGmvGpu2Host() << "\n"
           << std::endl;
   }
}


void Evaluate::bindToCkt( LevelizedNetlist & ckt ) {
   _cktPtr = &ckt;
   _gmv0.resize( ckt.numNodes() );
   _gmv1.resize( ckt.numNodes() );
   _gmv0x4.resize( ckt.numNodes() );
   _gmv1x4.resize( ckt.numNodes() );
   std::fill( _gmv0.begin(), _gmv0.end(), ALL_ONES_64 ); // init to X:(1,0)
   std::fill( _gmv1.begin(), _gmv1.end(), 0 );
}


void Evaluate::setGmv2val( int gid, int v, uint64_t mask ) {
   switch( v ) {
      case 0: {
         _gmv0[gid] &= ~mask;
         break;
      }
      case 1: {
         _gmv0[gid] |= mask;
         break;
      }
      default: {
         HLog << InfoError
              << "unexpected value in 2-value logic: " << v
              << std::endl;
         assert(0);
      }
   }
}


void Evaluate::setGmv4val( int gid, int v, uint64_t mask ) {
   switch( v ) {
      case 0: { // (0,0) '0'
         _gmv0[gid] &= ~mask;
         _gmv1[gid] &= ~mask;
         break;
      }
      case 1: { // (1,1) '1'
         _gmv0[gid] |= mask;
         _gmv1[gid] |= mask;
         break;
      }
      case 2: { // (1,0) 'X'
         _gmv0[gid] |= mask;
         _gmv1[gid] &= ~mask;
         break;
      }
      case 3: { // (0,1) 'Z'
         _gmv0[gid] &= ~mask;
         _gmv1[gid] |= mask;
         break;
      }
      default: {
         HLog << InfoError
              << "unexpected value in 4-value logic: " << v
              << std::endl;
         assert(0);
      }
   }
}



void Evaluate::storeForcePIAndInitFF(
                    int cycle,              // cycle number
                    int frame,
                    bool vector_mode_on,    // ocl_cpu vector mode
                    bool is_4val_logic,  // flag to use 4 or 2 value logic
                    bool io_per_cycle,      // fpi and mpo per cycle
                    bool last_cycle,        // last cycle within a pattern
                    std::vector<uint64_t>& bits_v0, // pattern bit stream
                    std::vector<uint64_t>& bits_v1,
                    std::vector< cl_ulong4 >& bits_v0_256,
                    std::vector< cl_ulong4 >& bits_v1_256 )
{
   // Save force_pi, init_ff
   if( vector_mode_on ) {
      if( 0==frame ) {// bits_v0/1_256 are cleared at frame 0 per cycle
         bits_v0_256.clear();
         bits_v1_256.clear();
      }
      if(( 0==cycle || io_per_cycle ) && 0==frame) {  // store force_pi
         for( int i=0; i < numDataPIs(); i++ ) {
            int g = constDataInputNodes()[i];
            bits_v0_256.push_back( gmv0x4(g) );
            bits_v1_256.push_back( gmv1x4(g) );
            //debugAssignStorePattern(
            //   "sav FPIv",g,cycle,frame,gmv0x4(g).s[0],gmv1x4(g).s[0]);
         }
      }
      if( 0==cycle && 0==frame ) { // store init_ff
         const int mul = clockedDffIsUsed() ? 5 : 1;
         const int offset = clockedDffIsUsed() ? 2 : 0; // 2: slave MUX
         int g = firstNodeFF() + offset;
         for( int i=0; i < numFFs(); i++, g +=mul ) {
            bits_v0_256.push_back( gmv0x4(g) );
            bits_v1_256.push_back( gmv1x4(g) );
            //debugAssignStorePattern(
            //   "sav IFFv",g,cycle,frame,gmv0x4(g).s[0],gmv1x4(g).s[0]);
         }
      }
   } else {
      if( 0==frame ) { // bits_v0 & bits_v1 are cleared at frame 0 per cycle
         bits_v0.clear();
         bits_v1.clear();
      }
      if( (0==cycle || io_per_cycle) && 0==frame ) {  // store force_pi
         for( int i=0; i < numDataPIs(); i++ ) {
            int g = constDataInputNodes()[i];
            bits_v0.push_back( gmv0(g) );
            if( is_4val_logic ) {
               bits_v1.push_back( gmv1(g) );
            }
            //debugAssignStorePattern("sav FPI",g,cycle,frame,gmv0(g),gmv1(g));
         }
      }
      if( 0==cycle && 0==frame) { // store init_ff
         const int mul = clockedDffIsUsed() ? 5 : 1;
         const int offset = clockedDffIsUsed() ? 2 : 0; // 2: slave MUX
         int g = firstNodeFF() + offset;
         for( int i=0; i < numFFs(); i++, g +=mul ) {
            bits_v0.push_back( gmv0(g) );
            if( is_4val_logic ) {
               bits_v1.push_back( gmv1(g) );
            }
            //debugAssignStorePattern("sav IFF",g,cycle,frame,gmv0(g),gmv1(g));
         }
      }
   }
}


void Evaluate::storeMeasurePOAndObserveFF(
                  int cycle,              // cycle number
                  int frame,              // frame number
                  bool vector_mode_on,    // ocl_cpu vector mode
                  bool is_4val_logic,  // flag to use 4 or 2 value logic
                  bool io_per_cycle,      // fpi and mpo per cycle
                  bool last_cycle,        // last cycle within a pattern
                  std::vector<uint64_t>& bits_v0, // pattern bit stream
                  std::vector<uint64_t>& bits_v1,
                  std::vector< cl_ulong4 >& bits_v0_256,
                  std::vector< cl_ulong4 >& bits_v1_256 )
{
   // Save measure_po and observe_ff
   if( vector_mode_on ) {
      // bits_v0[] and bits_v1[] are cleared upon storing force_pi and init_ff
      if((last_cycle || io_per_cycle) && 0==frame) { // store measure_po
         for( int o=0; o<numPOs(); o++ ) {
            int g = output(o);
            bits_v0_256.push_back( gmv0x4(g) );
            bits_v1_256.push_back( gmv1x4(g) );
            //debugAssignStorePattern(
            //   "sav MPOv",g,cycle,frame,gmv0x4(g).s[0],gmv1x4(g).s[0]);
         }
      }
      if(((clockedDffIsUsed()&& 2==frame) || (!clockedDffIsUsed()&& 0==frame))
         && last_cycle ) { // store o_ff on input of DFF (last pulse saved)
         const int mul = clockedDffIsUsed() ? 5 : 1;
         const int offset = clockedDffIsUsed() ? 2 : 0; // 2: slave MUX
         int g = firstNodeFF() + offset;
         for( int i=0; i < numFFs(); i++, g +=mul) {
            bits_v0_256.push_back( gmv0x4(g) );
            bits_v1_256.push_back( gmv1x4(g) );
            //debugAssignStorePattern(
            //   "sav OFFv",g,cycle,frame,gmv0x4(g).s[0],gmv1x4(g).s[0]);
         }
      }
   } else {
      // bits_v0[] and bits_v1[] are cleared upon storing force_pi and init_ff
      if((last_cycle || io_per_cycle) && 0==frame) { // store measure_po
         for( int o=0; o<numPOs(); o++ ) {
            int g = output(o);
            bits_v0.push_back( gmv0(g) );
            if( is_4val_logic ) {
               bits_v1.push_back( gmv1(g) );
            }
            //debugAssignStorePattern("sav MPO",g,cycle,frame,gmv0(g),gmv1(g));
         }
      }
      // store oFF on input of DFF (last pulse saved)
      if(((clockedDffIsUsed()&& 2==frame) || (!clockedDffIsUsed()&& 0==frame))
         && last_cycle ) {
         const int mul = clockedDffIsUsed() ? 5 : 1;
         const int offset = clockedDffIsUsed() ? 2 : 0; // 2: slave MUX
         int g = firstNodeFF() + offset;
         for( int i=0; i < numFFs(); i++, g +=mul) {
            bits_v0.push_back( gmv0(g) );
            if( is_4val_logic ) {
               bits_v1.push_back( gmv1(g) );
            }
            //debugAssignStorePattern("sav OFF",g,cycle,frame,gmv0(g),gmv1(g));
         }
      }
   }
}


// For each cycle, simulate combinational logic and DFF in one pass
// the order is level 1 to n, and now DFFs are at last level
void Evaluate::goodSim( int parallel,
                        int cycle,
                        int frame,
                        bool vector_mode_on,
                        bool is_4val_logic,
                        bool store_patterns,
                        bool io_per_cycle,
                        bool last_cycle,
                        std::vector< uint64_t >& bits_v0,
                        std::vector< uint64_t >& bits_v1,
                        std::vector< cl_ulong4 >& bits_v0_256,
                        std::vector< cl_ulong4 >& bits_v1_256 )
{
   SimulationOptions & sim_opt = (htDB::instance())->simOptions();
   SimLocation sim_loc = sim_opt.simLocation();
   EvaluateCL& eval_cl = (htDB::instance())->evalCl();
   cl_uint pfm_num = sim_opt.platformNum();
   cl_uint dev_num = sim_opt.deviceNum();

   if( store_patterns ) {  // store force_pi and init_ff
      storeForcePIAndInitFF( cycle, frame, vector_mode_on,
         is_4val_logic, io_per_cycle, last_cycle,
         bits_v0, bits_v1, bits_v0_256, bits_v1_256 );
   }

#ifdef HT_PROFILE
   int frame_id = cycle * numFramesPerCycle() + frame + 1;
#endif
   if( is_4val_logic ) {
      if( OCL_GPU == sim_loc || OCL_CPU == sim_loc ) {
         if( eval_cl.debugFlag() & EVAL_CL_DBG_PAT_CYCLE ) {
            HLog << "Cycle " << cycle
                 << ". Start running good sim on gpu ....."
                 << std::endl;
         }
         eval_cl.goodSim1CycleOnGpu( pfm_num, dev_num, ckt());
      } else {
         // use 4-value logic gate evaluation, non-event sim on host
         for( int g = ckt().firstNodeInLevel()[1]; g < numNodes(); g++) {
            uint64_t val0=0, val1=0;
            gateEval4val( g, val0, val1
#ifdef HT_PROFILE
                         ,frame_id
#endif
                        );
            gmv0( g, val0 );
            gmv1( g, val1 );
            // debug gateEval4valNonDivergent
            /*
            uint64_t v0_cmp=0, v1_cmp=0;
            gateEval4valNonDivergent( g, v0_cmp, v1_cmp);
            if( (val0 != v0_cmp) || (val1 != v1_cmp)) {
               HLog << "Gate: " << g <<" "<< ckt().gate(g).typeName() << "\n"
                    << "      " << numPred(g) << " inputs\n"
                    << "val0: " << mask64ToBinary( val0 )  << "\n"
                    << "      " << mask64ToBinary( v0_cmp ) << "\n"
                    << "val1: " << mask64ToBinary( val1 ) << "\n"
                    << "      " << mask64ToBinary( v1_cmp ) << "\n"
                    << std::endl;
            }
            */
         }
      }
   } else {
      // use 2-value logic gate evaluation
      for( int g = ckt().firstNodeInLevel()[1]; g < numNodes(); g++) {
         uint64_t val=0;
         gateEval2val( g, val
#ifdef HT_PROFILE
                      ,frame_id
#endif
                     );
         gmv0( g, val );
      }
   }

   // Copy node value data from gpu to host when sim_loc is OCL_GPU
   // No need to copy if sim_loc is OCL_CPU
   if( OCL_GPU == sim_loc ) {
      if( eval_cl.debugFlag() & EVAL_CL_DBG_GMV_COPY ) {
         HLog << "copy gmv from GPU to host .....\n" << std::endl;
      }
      int start_node = 0;
      int num_nodes = ckt().numNodes();
      if( 0==cycle || io_per_cycle || last_cycle ) {
         // only copy back when it is required
         // 0==cycle may not be required ????? to store init_ff as we set
         //   predecessors of FFs at the beginning.
         eval_cl.copyGmvFromGpuToHost( pfm_num,
                                       dev_num,
                                       0, // use gmv of group 0
                                       start_node,
                                       num_nodes );
      }
   }

   if( store_patterns ) { // store measure_po and observe_ff
      storeMeasurePOAndObserveFF( cycle, frame, vector_mode_on,
         is_4val_logic, io_per_cycle, last_cycle,
         bits_v0, bits_v1, bits_v0_256, bits_v1_256 );
   }
}


bool Evaluate::isInverterOfRemodeledDff( int gid ) const {
   return (clockedDffIsUsed() &&
           ckt().level(gid)==maxLevel() &&
           logic(gid)==T_NOT);
}


// Combined version of evaluate and update all kinds of gates
//   return the gid where gmv changed; -1 means gmv does not change
int  Evaluate::evalAndUpdateQGate4val( int g
#ifdef HT_PROFILE
                                      ,int frame_id
#endif
                                     )
{
   int gates_to_sim = ckt().combinedFlags()[(g<<2)+GATES_SIM_OFFSET];
   int cmp_offset = (5==gates_to_sim) ? 2 : 0;

   uint64_t old_gmv0 = gmv0( g + cmp_offset ); // can be slave MUX
   uint64_t old_gmv1 = gmv1( g + cmp_offset ); //   or the gate itself
   for( int i=0; i<gates_to_sim; i++) { // simulate 5 gates in a pass
      uint64_t val0=0, val1=0;
      gateEval4val( g+i, val0, val1
#ifdef HT_PROFILE
                   ,frame_id
#endif
                  );
      gmv0( g+i, val0);
      gmv1( g+i, val1);
   }
   if( gmv0(g+cmp_offset)!=old_gmv0 || gmv1(g+cmp_offset)!=old_gmv1 ) {
      return (g+cmp_offset);   // old and new gmvs are different
   } else {
      return -1;
   }
}


int  Evaluate::evalAndUpdateQGate2val( int g
#ifdef HT_PROFILE
                                      ,int frame_id
#endif
                                     )
{
   int gates_to_sim = ckt().combinedFlags()[(g<<2)+GATES_SIM_OFFSET];
   int cmp_offset = (5==gates_to_sim) ? 2 : 0;

   uint64_t old_gmv0 = gmv0( g + cmp_offset ); // can be slave MUX
                                               //   or the gate itself
   for( int i=0; i<gates_to_sim; i++) { // simulate 5 gates in a pass
      uint64_t val0=0;
      gateEval2val( g+i, val0
#ifdef HT_PROFILE
                   ,frame_id
#endif
                  );
      gmv0( g+i, val0);
   }
   if( gmv0(g+gates_to_sim)!=old_gmv0 ) { // Check if gmv of slave MUX changed
      return (g+cmp_offset);   // old and new gmvs are different
   } else {
      return -1;
   }
}


// Evaluate and update gmv of normal gate (including delay element).
//   TRUE: gmv changed
bool Evaluate::evalAndUpdateGate4val( int g
#ifdef HT_PROFILE
                                     ,int frame_id
#endif
                                    )
{
   uint64_t old_gmv0 = gmv0( g );
   uint64_t old_gmv1 = gmv1( g );
   uint64_t val0=0, val1=0;
   //assert( 1==ckt().combinedFlags()[(g<<2)+GATES_SIM_OFFSET] );
   gateEval4val( g, val0, val1
#ifdef HT_PROFILE
                ,frame_id
#endif
               );
   if( val0!=old_gmv0 || val1!=old_gmv1 ) {
      gmv0( g, val0);
      gmv1( g, val1);
      return true;
   } else {
      return false;
   }
}


bool Evaluate::evalAndUpdateGate2val( int g
#ifdef HT_PROFILE
                                     ,int frame_id
#endif
                                    )
{
   uint64_t old_gmv0 = gmv0( g );
   uint64_t val0=0;
   //assert( 1==ckt().combinedFlags()[(g<<2)+GATES_SIM_OFFSET] );
   gateEval2val( g, val0
#ifdef HT_PROFILE
                ,frame_id
#endif
               );
   if( val0!=old_gmv0) {
      gmv0( g, val0);
      return true;
   } else {
      return false;
   }
}


// Evaluate and update gmv of remodeled Dff. TRUE: gmv changed
bool Evaluate::evalAndUpdateRDFF4val( int g
#ifdef HT_PROFILE
                                     ,int frame_id
#endif
                                    )
{
   uint64_t old_gmv0 = gmv0( g+2 ); // slave MUX
   uint64_t old_gmv1 = gmv1( g+2 );

   // debug, print clock pin value which should be all 1's or all 0's.
#ifdef HT_DEBUG
/*
   int ck = predC0(g)[0];
   uint64_t ckx_mask = gmv0(ck) ^ gmv1(ck);
   if( ckx_mask ) {
      HLog << "### inv:" << g+1 << "  clk_pin:" << ck+1 << std::endl;
      HLog << "    " << nodeValues( ck ) << std::endl;
   }
   printAllNodeValues();
*/
#endif
   //assert( 5==ckt().combinedFlags()[(g<<2)+GATES_SIM_OFFSET] );
   for( int i=0; i<5; i++) { // simulate 5 gates in a pass
      uint64_t val0=0, val1=0;
      gateEval4val( g+i, val0, val1
#ifdef HT_PROFILE
                   ,frame_id
#endif
                  );
      gmv0( g+i, val0);
      gmv1( g+i, val1);
   }
   if( gmv0(g+2)!=old_gmv0 || gmv1(g+2)!=old_gmv1 ) {
      return true;   // old and new gmvs are different
   } else {
      return false;
   }
}


bool Evaluate::evalAndUpdateRDFF2val( int g
#ifdef HT_PROFILE
                                     ,int frame_id
#endif
                                    )
{
   uint64_t old_gmv0 = gmv0( g+2 ); // slave MUX
   //assert( 5==ckt().combinedFlags()[(g<<2)+GATES_SIM_OFFSET] );
   for( int i=0; i<5; i++) { // simulate 5 gates in a pass
      uint64_t val0=0;
      gateEval2val( g+i, val0
#ifdef HT_PROFILE
                   ,frame_id
#endif
                  );
      gmv0( g+i, val0);
   }
   if( gmv0(g+2)!=old_gmv0 ) { // Check if gmv of slave MUX changed
      return true;   // old and new gmvs are different
   } else {
      return false;
   }
}

///////////////////////////////////////////////////////////////////
//  Learning evaluation
//
//////////////////////////////////////////////////////////////////
/*void Learning::simLearning() {
   bool is_4val_logic=true, vector_mode_on=false, is_static_learning=false;
   initializeNodeValues( is_4val_logic, vector_mode_on, is_static_learning,NULL);
   for( int g=0; g < numNodes(); g++) {
      uint64_t val0=0, val1=0;
      gateEval4val( g, val0, val1
#ifdef HT_PROFILE
                    ,0
#endif
                  );
      gmv0( g, val0 );
      gmv1( g, val1 );
   }
}


void Learning::initializeNodeValues( bool is_4val_logic,
                                     bool vector_mode_on,
                                     bool is_static_learning,
                                     LearningManager* Learning_mgr )
{
   if( is_static_learning ) { // For static_learning sim, init background values
      if( is_4val_logic ) {
         // For 4-val logic system, initialize all nodes to X first,
         //   then set TIE0/TIE1 and insert events
         for( int g=0; g < numNodes(); g++) {
            if( vector_mode_on ) {
               for( int i=0; i<4; i++) {
                  gmv0x4( g, i, ALL_ONES_64 );
                  gmv1x4( g, i, 0 );
               }
            } else {
               gmv0( g, ALL_ONES_64 );
               gmv1( g, 0);
            }
         }
         for( int g = firstNodeTIE(); g < firstNodeTIE() + numTIEs(); g++) {
            uint64_t val0=0, val1=0;
            gateEval4val( g, val0, val1
#ifdef HT_PROFILE
                         ,0
#endif
                        );
            gmv0( g, val0);
            gmv1( g, val1);
         }
} */
// Comment: in event-driven implementation, it runs faster to use seperate
//   eval functions of evalAndUpdateRDFF4val() and evalAndUpdateGate4val()
//   than unified function of evalAndUpdateQGate4val(). As the second
//   implementation requires more memory accesses to obtain q gate offset
//   and #gates to simulate, where are hardcoded in 1st implementation.
void Evaluate::goodSimWithEvents( int parallel,
                        int cycle,
                        int frame,
                        bool is_3frame_sim,
                        bool is_4val_logic,
                        bool store_patterns,
                        bool io_per_cycle,
                        bool last_cycle,
                        std::vector< uint64_t >& bits_v0,
                        std::vector< uint64_t >& bits_v1,
                        EventManager* event_mgr )
{
   bool vector_mode_on = false; // declare vector_mode_on etc to
   std::vector< cl_ulong4 > bits_v0_256; // use the same routines
   std::vector< cl_ulong4 > bits_v1_256; // to store pattern information

   if( store_patterns ) {  // store force_pi and init_ff
      storeForcePIAndInitFF( cycle, frame, vector_mode_on,
         is_4val_logic, io_per_cycle, last_cycle,
         bits_v0, bits_v1, bits_v0_256, bits_v1_256 );
   }

#ifdef HT_PROFILE
   int frame_id = cycle * numFramesPerCycle() + frame + 1;
#endif
   if( is_4val_logic ) {
      // use 4-value logic gate evaluation
      int g = -1;
#ifdef HT_DEBUG
      HLog << "in cycle " <<cycle<<"  frame "<<frame<<std::endl;
      int num_flops_changed = 0;
      //if( 0==cycle && 2==frame ) {
      //   HLog << " START Evals:" << _numGateEvalsPerFrame[3] << std::endl;
      //   HLog << " popped events:" << std::endl;
      //}
      //HLog << "#### Pre cycle:" << cycle << " frame:" << frame << std::endl;
      //printAllNodeValues();
      //event_mgr->dumpEvents();
      //bool node_value_not_dumped = true; // keep for later debug
#endif
      while( (g=event_mgr->next( is_3frame_sim )) >= 0 ) { // events not empty
         //if( 0==cycle && 2==frame ) {
         //   HLog << "  " << g << " " << std::endl;
         //}
         /*
         int g_schedule = evalAndUpdateQGate4val( g );
         if( g_schedule > 0 ) {
            addSuccessorEvents( g_schedule, events);
         }
         */
         // HLog << "current gate:" << g+1 << std::endl;  // LL debug
#ifdef HT_DEBUG
         // for dumping node values up to certain level
         /*
         if( 16==level(g) && 0==cycle && 0==frame ) {
            if( node_value_not_dumped) {
               std::string fname = "m21.nodes.host";
               HLog << "### dump node values up to level " << level(g)-1
                    << " in file " << fname << std::endl;
               node_value_not_dumped = false;
               printAllNodeValues( 1, level(g)-1, fname);
            }
         }
         */
#endif
         if( isInverterOfRemodeledDff( g )) {
            if( evalAndUpdateRDFF4val( g
#ifdef HT_PROFILE
                                      ,frame_id
#endif
                                     ))
            {
               // HLog << "#### gmv changed" << std::endl; // LL debug
#ifdef HT_PROFILE
               num_flops_changed++;
#endif
               addSuccessorEvents( g+2, event_mgr ); // g+2: slave MUX
            }
         } else {
            if( evalAndUpdateGate4val( g
#ifdef HT_PROFILE
                                      ,frame_id
#endif
                                     ))
            {
               // HLog << "#### gmv changed" << std::endl; // LL debug
               addSuccessorEvents( g, event_mgr );
            }
         }
#ifdef HT_DEBUG
         //HLog << "### pop up node " << g+1 << std::endl;
         //event_mgr->dumpEvents(); // LL debug
#endif
      }
#ifdef HT_DEBUG
      HLog << "#### number of flops changed: " << num_flops_changed
           << std::endl;
      //HLog << "#### Post cycle:" << cycle << " frame:" << frame << std::endl;
      //printAllNodeValues( 1 );
#endif
   } else {
      // use 2-value logic gate evaluation
      int g = -1;
      while( (g=event_mgr->next( is_3frame_sim )) >= 0 ) { // events not empty
         /*
         int g_schedule = evalAndUpdateQGate2val( g );
         if( g_schedule > 0 ) {
            addSuccessorEvents( g_schedule, events);
         }
         */
         // HLog << "current gate:" << g+1 << std::endl;  // LL debug
         if( isInverterOfRemodeledDff( g )) {
            if( evalAndUpdateRDFF2val( g
#ifdef HT_PROFILE
                                      ,frame_id
#endif
                                     ))
            {
               // HLog << "#### gmv changed" << std::endl; // LL debug
               addSuccessorEvents( g+2, event_mgr ); // g+2: slave MUX
            }
         } else {
            if( evalAndUpdateGate2val( g
#ifdef HT_PROFILE
                                      ,frame_id
#endif
                                     ))
            {
               // HLog << "#### gmv changed" << std::endl; // LL debug
               addSuccessorEvents( g, event_mgr );
            }
         }
         // events.dumpEvents(); // LL debug
      }
   }
#ifdef HT_DEBUG
   //if( 0==cycle && 2==frame ) {
   //   HLog << " END Evals:" << _numGateEvalsPerFrame[3] << std::endl;
   //}
#endif

   if( store_patterns ) { // store measure_po and observe_ff
      storeMeasurePOAndObserveFF( cycle, frame, vector_mode_on,
         is_4val_logic, io_per_cycle, last_cycle,
         bits_v0, bits_v1, bits_v0_256, bits_v1_256 );
   }
}


void Evaluate::simImpactOfPinConstraints() {
   bool is_4val_logic=true, vector_mode_on=false, is_event_driven=false;
   initializeNodeValues( is_4val_logic, vector_mode_on, is_event_driven,NULL);
   for( int g=0; g < numNodes(); g++) {
      uint64_t val0=0, val1=0;
      gateEval4val( g, val0, val1
#ifdef HT_PROFILE
                    ,0
#endif
                  );
      gmv0( g, val0 );
      gmv1( g, val1 );
   }
}


void Evaluate::initializeNodeValues( bool is_4val_logic,
                                     bool vector_mode_on,
                                     bool is_event_driven,
                                     EventManager* event_mgr )
{
   if( is_event_driven ) { // For event-driven sim, init background values
      if( is_4val_logic ) {
         // For 4-val logic system, initialize all nodes to X first,
         //   then set TIE0/TIE1 and insert relevent events
         for( int g=0; g < numNodes(); g++) {
            if( vector_mode_on ) {
               for( int i=0; i<4; i++) {
                  gmv0x4( g, i, ALL_ONES_64 );
                  gmv1x4( g, i, 0 );
               }
            } else {
               gmv0( g, ALL_ONES_64 );
               gmv1( g, 0);
            }
         }
         for( int g = firstNodeTIE(); g < firstNodeTIE() + numTIEs(); g++) {
            uint64_t val0=0, val1=0;
            gateEval4val( g, val0, val1
#ifdef HT_PROFILE
                         ,0
#endif
                        );
            gmv0( g, val0);
            gmv1( g, val1);
            if( logic(g) != T_TIEX) { // NOT add events only when it is TIEX
               addSuccessorEvents( g, event_mgr );
            }
         }
      } else {
         // For 2-val logic system, do a one-pass simulation based on fixed
         //   input, TIE0/TIE1, and DFF values
         for( int g = firstNodePI(); g < firstNodePI()+numPIs(); g++ ) {
            gmv0( g, 0 );
         }
         for( int g = firstNodeTIE(); g < firstNodeTIE() + numTIEs(); g++) {
            // Only TIE0 and TIE1 are allowed for 2-value-logic
            assert( logic(g)==T_TIE0 || logic(g)==T_TIE1 );
            uint64_t val=0;
            gateEval2val( g, val
#ifdef HT_PROFILE
                         ,0
#endif
                        );
            gmv0( g, val );
         }
         for( int g = firstNodeFF(); g < firstNodeFF()+numFFs(); g++ ) {
            gmv0( g, 0 );
         }
         // As DFFs are now in last level,
         // simulate from tie gates to gate just before FFs in one pass
         //   to init gate values
         for( int g = numPIs() + numTIEs(); g < firstNodeFF(); g++) {
            uint64_t val=0;
            gateEval2val( g, val
#ifdef HT_PROFILE
                         ,0
#endif
                        );
            gmv0( g, val );
         }
      }
   } else {
      // Only TIE nodes need to be initialized for non-event simulation
      for( int g = firstNodeTIE(); g < firstNodeTIE() + numTIEs(); g++) {
         uint64_t val0=0, val1=0;
         gateEval4val( g, val0, val1
#ifdef HT_PROFILE
                      ,0
#endif
                     );
         if( vector_mode_on ) {
            for( int i=0; i<4; i++) {
               gmv0x4( g, i, val0);
               gmv1x4( g, i, val1);
            }
         } else {
            gmv0( g, val0 );
            gmv1( g, val1 );
         }
      }
   }
}


void Evaluate::addSuccessorEvents( int g, EventManager* event_mgr )
{
   for( int ix=0; ix < numSucc(g); ix++ ) {
      int suc = gate(g).succ(ix);  // successor node
      int lev = gate(suc).level(); // level of successor node
      // insertion ensures no node duplication in event queue
      // If current node is slave MUX of a clocked Dff, successor at the same
      //   level is ignored.
      // If successor is part of clocked Dff. Adjust gate id to the 1st
      //   inverter.
      if( ckt().level(g) != lev ) {
         /*
         int offset = (int)ckt().combinedFlags()[(suc<<2)+EVENT_ID_OFFSET];
         events.insert( suc+offset, lev);
         */
         /*
         if( clockedDffIsUsed() && suc >= firstNodeFF()) {
            if( logic(suc)==T_NOT ) {
               events.insert( suc, lev );
               //assert( 0==ckt().combinedFlags()[(suc<<2)+EVENT_ID_OFFSET]);
            } else if( logic(suc)==T_MUX_2 && logic(suc-1)==T_NOT) {
               events.insert( suc-1, lev ); // suc is master MUX
               //assert(-1==ckt().combinedFlags()[(suc<<2)+EVENT_ID_OFFSET]);
            } else if( logic(suc)==T_MUX_2 && logic(suc-2)==T_NOT) {
               events.insert( suc-2, lev ); // suc is slave MUX
               //assert(-2==ckt().combinedFlags()[(suc<<2)+EVENT_ID_OFFSET]);
            }
         } else {
            events.insert( suc, lev );
            //assert(0==ckt().combinedFlags()[(suc<<2)+EVENT_ID_OFFSET]);
         }
         */
         int adj_id = suc;
         if( clockedDffIsUsed() && suc >= firstNodeFF()) {
            // adjust to gate id of inverter in remodeled D flip flop
            adj_id -= (suc - firstNodeFF()) % 5;
         }
         event_mgr->insert( adj_id, lev );
      }
   }
}


void Evaluate::gateEval2val( int id,
                             uint64_t& value
#ifdef HT_PROFILE
                            ,int frame_id
#endif
                           )
{
#ifdef HT_PROFILE
   _numGateEvals++;
   _numGateEvalsPerFrame[frame_id]++;
#endif
   switch ( logic(id) ) {
      case T_INPUT:
      //case T_TIE1:
      //case T_TIE0:
      {
         value = gmv0( id );
         break;
      }
      case T_TIE1:
      {
         value = ALL_ONES_64;
         break;
      }
      case T_TIE0:
      {
         value = 0;
         break;
      }
      case T_OUTPUT:
      {
         value = gmv0( predC0(id)[0] );
         break;
      }
      case T_AND:
      {
         value = ALL_ONES_64;
         for( int i = 0; i < numPred(id); i++) {
            value &= gmv0( predC0(id)[i] );
         }
         break;
      }
      case T_NAND:
      {
         value = ALL_ONES_64;
         for( int i = 0; i < numPred(id); i++) {
            value &= gmv0( predC0(id)[i] );
         }
         value ^= ALL_ONES_64;
         break;
      }
      case T_OR:
      {
         value = 0;
         for( int i = 0; i < numPred(id); i++) {
            value |= gmv0( predC0(id)[i] );
         }
         break;
      }
      case T_NOR:
      {
         value = 0;
         for( int i = 0; i < numPred(id); i++) {
            value |= gmv0( predC0(id)[i] );
         }
         value ^= ALL_ONES_64;
         break;
      }
      case T_NOT:
      {
         value = gmv0( predC0(id)[0] ) ^ ALL_ONES_64;
         break;
      }
      case T_BUF:
      case T_DFF:
      case T_BUF_FB:
      {
         value = gmv0( predC0(id)[0] );
         break;
      }
      case T_XOR:
      {
         value = gmv0( predC0(id)[0] );
         for( int i = 1; i < numPred(id); i++) {
            value ^= gmv0( predC0(id)[i] );
         }
         break;
      }
      case T_XNOR:
      {
         value = gmv0( predC0(id)[0] );
         for( int i = 1; i < numPred(id); i++) {
            value ^= gmv0( predC0(id)[i] );
         }
         value ^= ALL_ONES_64;
         break;
      }
      case T_MUX_2: // Assume port (sel, d0, d1)
      {
         uint64_t sel = gmv0( predC0(id)[0] );
         uint64_t  d0 = gmv0( predC0(id)[1] );
         uint64_t  d1 = gmv0( predC0(id)[2] );
         value = (sel & d1) | ((~sel) & d0);
         break;
      }
      default:
      {
         HLog << InfoError
              << "gate type " <<  logic(id)
              << " is not supported in 2-value logic simulation";
         break;
      }
   }
   //if ((logic[node] == T_INPUT) || (gmv0[node] != value)) {
   //   gmv0[node] = value;
   //   EVENT->good_add_succ_event (node);  // add events for successor nodes
   //}
}


void Evaluate::gateEval4val( int id,
                             uint64_t& val0,
                             uint64_t& val1
#ifdef HT_PROFILE
                            ,int frame_id
#endif
                           )
{
   uint64_t v1;

#ifdef HT_PROFILE
   _numGateEvals++;
   _numGateEvalsPerFrame[frame_id]++;
#endif

   switch( logic(id) ) {
      case T_INPUT:
      {
         val0 = gmv0(id);
         val1 = gmv1(id);
         break;
      }
      case T_TIE1:  // (1,1)
      {
         val0 = ALL_ONES_64;
         val1 = ALL_ONES_64;
         break;
      }
      case T_TIE0:  // (0,0)
      {
         val0 = 0;
         val1 = 0;
         break;
      }
      case T_TIEX:  // (1,0)
      {
         val0 = ALL_ONES_64;
         val1 = 0;
         break;
      }
      case T_TIEZ:  // (0,1)
      {
         val0 = 0;
         val1 = ALL_ONES_64;
         break;
      }
      case T_OUTPUT:
      case T_DFF:
      case T_BUF:
      case T_BUF_FB:
      {
         val0 = gmv0( predC0(id)[0] );
         val1 = gmv1( predC0(id)[0] );
         break;
      }
      case T_BUFZ:
      {
         val0 = gmv0( predC0(id)[0] );
         val1 = gmv1( predC0(id)[0] );

         uint64_t xz_mask = gmv0(predC0(id)[0]) ^ gmv1(predC0(id)[0]);
         // set bits in xz_mask to X (1, 0)
         val0 |= xz_mask;
         val1 &= (~xz_mask);
         break;
      }
      case T_AND:
      {
         //val1 = val0 = ALL_ONES_64;
         val0 = gmv0( predC0(id)[0] );
         val1 = gmv1( predC0(id)[0] );
         for( int i = 1; i < numPred(id); i++) {
            val0 &= gmv0( predC0(id)[i] );
            val1 &= gmv1( predC0(id)[i] );
         }
         break;
      }
      case T_NAND:
      {
         //val1 = val0 = ALL_ONES_64;
         val0 = gmv0( predC0(id)[0] );
         val1 = gmv1( predC0(id)[0] );
         for( int i = 1; i < numPred(id); i++) {
            val0 &= gmv0( predC0(id)[i] );
            val1 &= gmv1( predC0(id)[i] );
         }
         v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      case T_OR:
      {
         //val1 = val0 = 0;
         val0 = gmv0( predC0(id)[0] );
         val1 = gmv1( predC0(id)[0] );
         for( int i = 1; i < numPred(id); i++) {
            val0 |= gmv0( predC0(id)[i] );
            val1 |= gmv1( predC0(id)[i] );
         }
         break;
      }
      case T_NOR:
      {
         //val1 = val0 = 0;
         val0 = gmv0( predC0(id)[0] );
         val1 = gmv1( predC0(id)[0] );
         for( int i = 1; i < numPred(id); i++) {
            val0 |= gmv0( predC0(id)[i] );
            val1 |= gmv1( predC0(id)[i] );
         }
         v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      case T_NOT:
      {
         val0 = gmv1( predC0(id)[0] ) ^ ALL_ONES_64;
         val1 = gmv0( predC0(id)[0] ) ^ ALL_ONES_64;
         break;
      }
      case T_XOR:
      {
         val0 = gmv0( predC0(id)[0] );
         val1 = gmv1( predC0(id)[0] );
         for( int i = 1; i < numPred(id); i++) {
            v1 = ALL_ONES_64 ^
               (((ALL_ONES_64 ^ gmv0( predC0(id)[i] )) & (ALL_ONES_64^val0)) |
                (gmv1( predC0(id)[i] ) & val1));
            val1 = ((ALL_ONES_64 ^ gmv0( predC0(id)[i] )) & val1) |
                    (gmv1( predC0(id)[i] ) & (ALL_ONES_64 ^ val0));
            val0 = v1;
         }
         break;
      }
      case T_XNOR:
      {
         val0 = gmv0( predC0(id)[0] );
         val1 = gmv1( predC0(id)[0] );
         for( int i = 1; i < numPred(id); i++) {
            v1 = ALL_ONES_64 ^
               (((ALL_ONES_64 ^ gmv0( predC0(id)[i] )) & (ALL_ONES_64^val0)) |
                (gmv1( predC0(id)[i] ) & val1));
            val1 = ((ALL_ONES_64^gmv0( predC0(id)[i] )) & val1) |
                    (gmv1( predC0(id)[i] ) & (ALL_ONES_64 ^ val0));
            val0 = v1;
         }
         v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      // Add more primitives to support industrial design
      case T_BUS: {  // LL: apply to parallel patterns
         val0 = 0;
         val1 = ALL_ONES_64;
         for( int i = 0; i < numPred(id); i++) {
            val0 |= gmv0( predC0(id)[i] );
            val1 &= gmv1( predC0(id)[i] );
         }
         // Changing Z bit to X
         uint64_t z_mask = (~val0) & val1; // Get bits with Z (0,1) values
         val0 |= z_mask;
         val1 &= (~z_mask);
         break;
      }
      case T_TRISTATE:  // assume in0 is EN; in1 is I
      case T_TRISTATE1: { // resistive NMOS switch
         val0 = ALL_ONES_64;
         val1 = 0;
         int gid_en = predC0(id)[0];
         int gid_i = predC0(id)[1];
         // e1: enable is 1; D passthrough
         uint64_t e1_mask = gmv0(gid_en) & gmv1(gid_en);
         val0 = gmv0( gid_i ) & e1_mask;
         val1 = gmv1( gid_i ) & e1_mask;

         // e0: enable 0; output Z (0,1)
         uint64_t e0_mask = ~(gmv0(gid_en) | gmv1(gid_en));
         val0 &= ~e0_mask;  // output Z (0,1) with enable 0
         val1 |= e0_mask;

/*
         // ex: enable X, output X (1,0) ; can init (val0, val1) to X instead
         uint64_t ex_mask = ~(e1_mask | e0_mask);
         val0 |= ex_mask;   // output X (1,0) with enable X
         val1 &= ~ex_mask;
*/
         break;
      }
      case T_MUX_2: // Assume port (sel, d0, d1)
      {
         // s1: selector is 1; s0: selector is 0; dq: d0==d1
         // start with Xs (1,0)
         val0 = ALL_ONES_64;
         val1 = 0;

         uint64_t val0_sel = gmv0(predC0(id)[0]);
         uint64_t val1_sel = gmv1(predC0(id)[0]);

         // assign d0 to output if sel is all 0's
         uint64_t  s0_mask = ~(val0_sel | val1_sel);
         if( ALL_ONES_64 == s0_mask ) {
            val0 = gmv0(predC0(id)[1]);
            val1 = gmv1(predC0(id)[1]);
            break;
         }
         // assign d1 to output if sel is all 1's
         uint64_t  s1_mask = val0_sel & val1_sel;
         if( ALL_ONES_64 == s1_mask ) {
            val0 = gmv0(predC0(id)[2]);
            val1 = gmv1(predC0(id)[2]);
            break;
         }

         uint64_t  val0_d0 = gmv0(predC0(id)[1]);
         uint64_t  val1_d0 = gmv1(predC0(id)[1]);
         uint64_t  val0_d1 = gmv0(predC0(id)[2]);
         uint64_t  val1_d1 = gmv1(predC0(id)[2]);
         // pass d0 for sel==0 and d0==d1
         uint64_t dq_mask = ~((val0_d0 ^ val0_d1) | (val1_d0 ^ val1_d1));
         uint64_t s0_dq_mask = s0_mask | dq_mask;
         val0 &= ((val0_d0 & s0_dq_mask) | ~s0_dq_mask);
         val1 |= (val1_d0 & s0_dq_mask);

         // pass d1 for sel==1
         //val0 &= ((gmv0(predC0(id)[2]) & s1_mask) | ~s1_mask);
         val0 &= ((val0_d1 & s1_mask) | ~s1_mask);
         val1 |= (val1_d1 & s1_mask);

         break;
      }
      case T_DFF_L: // assume port set, reset, clock and data
      {
         val0 = gmv0( predC0(id)[3] );  // always capture data
         val1 = gmv1( predC0(id)[3] );
         break;
      }
      case T_NAND_LATCH: // assume port set, reset, clock and data
      {
         uint64_t ck1_mask = gmv0(predC0(id)[2]) & gmv1(predC0(id)[2]);
         uint64_t ckx_mask = gmv0(predC0(id)[2]) ^ gmv1(predC0(id)[2]);
         //uint64_t ck0_mask = ~(gmv0(predC0(id)[2]) | gmv1(predC0(id)[2]));
#ifdef HT_DEBUG
         /*
         if( ckx_mask != 0 ) {
            HLog << InfoError
                 << "invalid enable signal for latch(" << id+1 << ")\n"
                 << nodeValues( predC0(id)[2] ) << std::endl;
            //printAllNodeValues();
            //assert( 0 );
         }
         */
#endif
         uint64_t in0 = gmv0(predC0(id)[3]);
         uint64_t in1 = gmv1(predC0(id)[3]);
         val0 = gmv0( id ); // previous value
         val1 = gmv1( id );

         // pass through data with ck1_mask
         val0 = (val0 & (~ck1_mask)) | (in0 & ck1_mask);
         val1 = (val1 & (~ck1_mask)) | (in1 & ck1_mask);

         // set output as X-(1,0) with ckx_mask
         val0 =  val0 | ckx_mask;
         val1 =  val1 & ~(ckx_mask);

         // remaining bits of ck0_mask are kept

         break;
      }
      /*
      case T_MUX_2:  // LL: not parallel behaviour in 1st if branch with selector (1,1)
      {
         if (gmv0( predC0(id)[2] ) && gmv1( predC0(id)[2] )) {
            val0 = gmv0( predC0(id)[1] );
            val1 = gmv1( predC0(id)[1] );
         } else if (!gmv0( predC0(id)[2] ) && !gmv1( predC0(id)[2] )) {
            val0 = gmv0( predC0(id)[0] );
            val1 = gmv1( predC0(id)[0] );
         } else if (gmv0( predC0(id)[0] ) == gmv0( predC0(id)[1] ) &&
                    gmv1( predC0(id)[0] ) == gmv1( predC0(id)[1] )) {
            val0 = gmv0( predC0(id)[0] );
            val1 = gmv1( predC0(id)[0] );
         } else {
            val0 = ALL_ONES_64;
            val1 = 0;
         }
         break;
      }
      case T_BUS: // LL: not apply to parallel patterns,
      {
         val0 = 0;
         val1 = ALL_ONES_64;
         for( int i = 0; i < numPred(id); i++) {
            val0 |= gmv0( predC0(id)[i] );
            val1 &= gmv1( predC0(id)[i] );
         }
         if (val1 && !val0) {  // change Z output to X; LL: not apply on bit level
            val0 = ALL_ONES_64;
            val1 = 0;
         }
         break;
      }
      case T_BUS_GOHIGH: // LL : save as T_BUS
      {
         val0 = 0;
         val1 = ALL_ONES_64;
         for( int i = 0; i < numPred(id); i++) {
            val0 |= gmv0( predC0(id)[i] );
            val1 &= gmv1( predC0(id)[i] );
         }
         if (val1 && !val0) {  // change Z output to 1
            val0 = ALL_ONES_64;
            val1 = ALL_ONES_64;
         }
         break;
      }
      case T_BUS_GOLOW: // LL : save as T_BUS
      {
         val0 = 0;
         val1 = ALL_ONES_64;
         for( int i = 0; i < numPred(id); i++) {
            val0 |= gmv0( predC0(id)[i] );
            val1 &= gmv1( predC0(id)[i] );
         }
         if (val1 && !val0) {  // change Z output to 0
            val0 = 0;
            val1 = 0;
         }
      }
      break;
      case T_BUSZ:  // LL: apply to parallel patterns
      {
         val0 = 0;
         val1 = ALL_ONES_64;
         for( int i = 0; i < numPred(id); i++) {
            val0 |= gmv0( predC0(id)[i] );
            val1 &= gmv1( predC0(id)[i] );
         }
      }
      break;
      case T_TRISTATE:
      {
         if (!gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
            val0 = gmv0( predC0(id)[0] );
            val1 = gmv1( predC0(id)[0] );
         } else {
            if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] )) {
               val0 = 0;
               val1 = ALL_ONES_64;
            } else {
               val0 = ALL_ONES_64;
               val1 = 0;
            }
         }
      }
      break;
      case T_TRISTATE1:
      {
         if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] )) {
            val0 = gmv0( predC0(id)[0] );
            val1 = gmv1( predC0(id)[0] );
         } else {
            if (!gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
               val0 = 0;
               val1 = ALL_ONES_64;
            } else {
               val0 = ALL_ONES_64;
               val1 = 0;
            }
         }
      }
      break;
      case T_TRISTATEINV:
      {
         if (!gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
            val0 = gmv1( predC0(id)[0] ) ^ ALL_ONES_64;
            val1 = gmv0( predC0(id)[0] ) ^ ALL_ONES_64;
         } else {
            if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] )) {
               val0 = 0;
               val1 = ALL_ONES_64;
            } else {
               val0 = ALL_ONES_64;
               val1 = 0;
            }
         }
      }
      break;
      case T_DFF_C:
      {
         if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] )) {
            val0 = gmv0( predC0(id)[0] );
            val1 = gmv1( predC0(id)[0] );
         } else {
            if (!gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
               val0 = 0;
               val1 = 0;
            } else {
               val0 = ALL_ONES_64;
               val1 = 0;
            }
         }
      }
      break;
      case T_DFF_P:
      {
         if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] )) {
            val0 = gmv0( predC0(id)[0] );
            val1 = gmv1( predC0(id)[0] );
         } else {
            if (!gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
               val0 = ALL_ONES_64;
               val1 = ALL_ONES_64;
            } else {
               val0 = ALL_ONES_64;
               val1 = 0;
            }
         }
      }
      break;
      case T_DFF_CP:
      {
         if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] ) &&
             gmv0( predC0(id)[2] ) && gmv1( predC0(id)[2] )) {
            val0 = gmv0( predC0(id)[0] );
            val1 = gmv1( predC0(id)[0] );
         } else {
            if ((gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) ||
                (gmv0( predC0(id)[2] ) && !gmv1( predC0(id)[2] )) ||
                (!gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] ) &&
                 !gmv0( predC0(id)[2] ) && !gmv1( predC0(id)[2] ))) {
               val0 = ALL_ONES_64;
               val1 = 0;
            } else {
               if (!gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
                  val0 = 0;
                  val1 = 0;
               } else {
                  val0 = ALL_ONES_64;
                  val1 = ALL_ONES_64;
               }
            }
         }
      }
      break;
      case T_CMOS:
      {
         if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] ) ||
             !gmv0( predC0(id)[2] ) && !gmv1( predC0(id)[2] )) {
            val0 = gmv0( predC0(id)[0] );
            val1 = gmv1( predC0(id)[0] );
         } else {
            if (!gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )
                && gmv0( predC0(id)[2] ) && gmv1( predC0(id)[2] )) {
               val0 = 0;
               val1 = ALL_ONES_64;
            } else {
               val0 = ALL_ONES_64;
               val1 = 0;
            }
         }
      }
      break;
      case T_DFF_L:
      {
         if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] )) {
            val0 = gmv0( predC0(id)[0] );
            val1 = gmv1( predC0(id)[0] );
         } else {
            if (gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
               if (gmv0( predC0(id)[0] ) == gmv0(id) &&
                   gmv1( predC0(id)[0] ) == gmv1(id)) {
                  val0 = gmv0(id);
                  val1 = gmv1(id);
               } else {
                  val0 = ALL_ONES_64;
                  val1 = 0;
               }
            } else {
               val0 = gmv0(id);
               val1 = gmv1(id);
            }
         }
      }
      break;
      case T_DFF_LC:
      {
         if (gmv0( predC0(id)[2] ) && gmv1( predC0(id)[2] )) {
            if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] )) {
               val0 = gmv0( predC0(id)[0] );
               val1 = gmv1( predC0(id)[0] );
            } else {
               if (gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
                  if (gmv0( predC0(id)[0] ) == gmv0(id) &&
                      gmv1( predC0(id)[0] ) == gmv1(id)) {
                     val0 = gmv0(id);
                     val1 = gmv1(id);
                  } else {
                     val0 = ALL_ONES_64;
                     val1 = 0;
                  }
               } else {
                  val0 = gmv0(id);
                  val1 = gmv1(id);
               }
            }            } else {
            if (!gmv0( predC0(id)[2] ) && !gmv1( predC0(id)[2] )) {
               val0 = val1 = 0;
            } else {
               val0 = ALL_ONES_64;
               val1 = 0;
            }
         }
      }
      break;
      case T_DFF_LP:
      {
         if (gmv0( predC0(id)[2] ) && gmv1( predC0(id)[2] )) {
            if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] )) {
               val0 = gmv0( predC0(id)[0] );
               val1 = gmv1( predC0(id)[0] );
            } else {
               if (gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
                  if (gmv0( predC0(id)[0] ) == gmv0(id) &&
                      gmv1( predC0(id)[0] ) == gmv1(id)) {
                     val0 = gmv0(id);
                     val1 = gmv1(id);
                  } else {
                     val0 = ALL_ONES_64;
                     val1 = 0;
                  }
               } else {
                  val0 = gmv0(id);
                  val1 = gmv1(id);
               }
            }            } else {
            if (!gmv0( predC0(id)[2] ) && !gmv1( predC0(id)[2] )) {
               val0 = val1 = ALL_ONES_64;
            } else {
               val0 = ALL_ONES_64;
               val1 = 0;
            }
         }
      }
      break;
      case T_DFF_LCP:
      {
         if (gmv0( predC0(id)[2] ) && gmv1( predC0(id)[2] ) &&
             gmv0( predC0(id)[3] ) && gmv1( predC0(id)[3] )) {
            if (gmv0( predC0(id)[1] ) && gmv1( predC0(id)[1] )) {
               val0 = gmv0( predC0(id)[0] );
               val1 = gmv1( predC0(id)[0] );
            } else {
               if (gmv0( predC0(id)[1] ) && !gmv1( predC0(id)[1] )) {
                  if (gmv0( predC0(id)[0] ) == gmv0(id) &&
                      gmv1( predC0(id)[0] ) == gmv1(id)) {
                     val0 = gmv0(id);
                     val1 = gmv1(id);
                  } else {
                     val0 = ALL_ONES_64;
                     val1 = 0;
                  }
               } else {
                  val0 = gmv0(id);
                  val1 = gmv1(id);
               }
            }            } else {
            if ((gmv0( predC0(id)[2] ) && !gmv1( predC0(id)[2] )) ||
                (gmv0( predC0(id)[3] ) && !gmv1( predC0(id)[3] )) ||
                (!gmv0( predC0(id)[2] ) && !gmv1( predC0(id)[2] ) &&
                 !gmv0( predC0(id)[3] ) && !gmv1( predC0(id)[3] ))) {
               val0 = ALL_ONES_64;
               val1 = 0;
            } else {
               if (!gmv0( predC0(id)[2] ) && !gmv1( predC0(id)[2] )) {
                  val0 = val1 = 0;
               } else {
                  val1 = val0 = ALL_ONES_64;
               }
            }
         }
      }
      break;
      */
      default:
      {
         HLog << InfoError
              << "gate type " <<  logic(id)
              << " is not supported in 4-value logic simulation"
              << std::endl;
         break;
      }
   }
}


void Evaluate::gateEval4valNonDivergent( int id,
                                         uint64_t& val0,
                                         uint64_t& val1
#ifdef HT_PROFILE
                                        ,int frame_id
#endif
                                       )
{
#ifdef HT_PROFILE
   _numGateEvals++;
   _numGateEvalsPerFrame[frame_id]++;
#endif

   const int NUM_GATE_TYPES = 12;
   // side input offset for multi-input gates
   const int tableOfSideInOffset[4][3] = { {0, 0, 0},  // 1-input gate
                                     {1, 0, 0},  // 2-input gate
                                     {1, 2, 0},  // 3-input gate
                                     {1, 2, 3}   // 4-input gate
                                   };

   // If offset is 0, use 0xff as pseudo input for AND, NAND
   //                 use 0x00 as pseudo input for OR, NOR, XOR, XNOR
   const int tablePseudoIn[NUM_GATE_TYPES][4] = {
      {-1, -1, -1, -1}, // JUNK,           /* 0 */
      { 0,  0,  0,  0}, // T_INPUT,        /* 1 */
      { 0,  0,  0,  0}, // T_OUTPUT,       /* 2 */
      { 0,  2,  2,  2}, // T_XOR,          /* 3 */
      { 0,  2,  2,  2}, // T_XNOR,         /* 4 */
      { 0,  0,  0,  0}, // T_DFF,          /* 5 */
      { 1,  2,  2,  2}, // T_AND,          /* 6 */
      { 1,  2,  2,  2}, // T_NAND,         /* 7 */
      { 0,  2,  2,  2}, // T_OR,           /* 8 */
      { 0,  2,  2,  2}, // T_NOR,          /* 9 */
      { 0,  0,  0,  0}, // T_NOT,          /* 10 */
      { 0,  0,  0,  0}  // T_BUF,          /* 11 */
   };
   // which entry in array input_values[] to retrieve for AND operation
   const int tableAnd[NUM_GATE_TYPES] = {
      -1, // JUNK,           /* 0 */
      1,  // T_INPUT,        /* 1 */
      1,  // T_OUTPUT,       /* 2 */
      1,  // T_XOR,          /* 3 */
      1,  // T_XNOR,         /* 4 */
      1,  // T_DFF,          /* 5 */
      2,  // T_AND,          /* 6 */
      2,  // T_NAND,         /* 7 */
      1,  // T_OR,           /* 8 */
      1,  // T_NOR,          /* 9 */
      1,  // T_NOT,          /* 10 */
      1   // T_BUF,          /* 11 */
   };
   // which entry in array input_values[] to retrieve for OR operation
   const int tableOr[NUM_GATE_TYPES] = {
      -1, // JUNK,           /* 0 */
      0,  // T_INPUT,        /* 1 */
      0,  // T_OUTPUT,       /* 2 */
      0,  // T_XOR,          /* 3 */
      0,  // T_XNOR,         /* 4 */
      0,  // T_DFF,          /* 5 */
      0,  // T_AND,          /* 6 */
      0,  // T_NAND,         /* 7 */
      2,  // T_OR,           /* 8 */
      2,  // T_NOR,          /* 9 */
      0,  // T_NOT,          /* 10 */
      0   // T_BUF,          /* 11 */
   };
   // which entry in array input_values[] to retrieve for XOR operation
   const int tableXor[NUM_GATE_TYPES] = {
      -1, // JUNK,           /* 0 */
      0,  // T_INPUT,        /* 1 */
      0,  // T_OUTPUT,       /* 2 */
      2,  // T_XOR,          /* 3 */
      2,  // T_XNOR,         /* 4 */
      0,  // T_DFF,          /* 5 */
      0,  // T_AND,          /* 6 */
      0,  // T_NAND,         /* 7 */
      0,  // T_OR,           /* 8 */
      0,  // T_NOR,          /* 9 */
      0,  // T_NOT,          /* 10 */
      0   // T_BUF,          /* 11 */
   };
   // which entry in array input_values[] to retrieve for NOT operation
   const int tableInv[NUM_GATE_TYPES] = {
      -1, // JUNK,           /* 0 */
      0,  // T_INPUT,        /* 1 */
      0,  // T_OUTPUT,       /* 2 */
      0,  // T_XOR,          /* 3 */
      1,  // T_XNOR,         /* 4 */
      0,  // T_DFF,          /* 5 */
      0,  // T_AND,          /* 6 */
      1,  // T_NAND,         /* 7 */
      0,  // T_OR,           /* 8 */
      1,  // T_NOR,          /* 9 */
      1,  // T_NOT,          /* 10 */
      0   // T_BUF,          /* 11 */
   };

   int gid = id; // current gate id
   int numInputs = numPred( gid ); // predFirst[ gid+1 ] - predFirst[ gid ];

   int gType = (int)logic( gid );
   int gidIn1 = predC0(gid)[0]; //preds[predFirst[gid]]; // 1st input gid

   val0 = gmv0( gidIn1 );
   val1 = gmv1( gidIn1 );

   bool debug = false; // (3356==gid);
   if( debug ) {
      HLog << "  val0: " << mask64ToBinary( val0 ) << std::endl;
      HLog << "  val1: " << mask64ToBinary( val1 ) << std::endl;
   }

   uint64_t input_values[3][2] = {{ALL_ZEROS, ALL_ZEROS},
                                  {ALL_ONES_64, ALL_ONES_64},
                                  {ALL_ZEROS, ALL_ZEROS}
                                 };

   //
   // iterate through the other 3 side inputs
   uint64_t in0 = 0;
   uint64_t in1 = 0;
   uint64_t v0 = 0;
   for (int s=0; s<3; s++ ) {
      int offset = tableOfSideInOffset[ numInputs-1 ][ s ];
      int gidSide = predC0(gid)[offset]; // preds[ predFirst[gid] + offset ];
      uint64_t val0s = gmv0( gidSide );
      uint64_t val1s = gmv1( gidSide );

      // update input_values[2] to the right value;
      // when offset==0, 0xFF for AND/NAND, 0x00 for OR/NOR/XOR/XNOR
      input_values[2][0] = val0s;
      input_values[2][1] = val1s;

      input_values[2][0] = input_values[ tablePseudoIn[gType][offset]][0];
      input_values[2][1] = input_values[ tablePseudoIn[gType][offset]][1];

      // AND
      in0 = input_values[ tableAnd[ gType ]][0];
      in1 = input_values[ tableAnd[ gType ]][1];
      val0 &= in0;
      val1 &= in1;
      if( debug ) {
         HLog << "  s = " << s << std::endl;
         HLog << "    in0.AND: " << mask64ToBinary( in0 ) << std::endl;
         HLog << "    in1.AND: " << mask64ToBinary( in1 ) << std::endl;
      }
      // OR
      in0 = input_values[ tableOr[ gType ]][0];
      in1 = input_values[ tableOr[ gType ]][1];
      val0 |= in0;
      val1 |= in1;
      if( debug ) {
         HLog << "    in0.OR : " << mask64ToBinary( in0 ) << std::endl;
         HLog << "    in1.OR : " << mask64ToBinary( in1 ) << std::endl;
      }

      // XOR
      // for XOR gate, draw K-map on 0-1-X-Z with Z row/column as don't cares
      //  out0 =  (in0' * val0' +  in1 & val1)'
      //  out1 =   in0' * val1  +  in1 & val0'
      in0 = input_values[ tableXor[ gType ]][0];
      in1 = input_values[ tableXor[ gType ]][1];
      v0 = ~ (((~in0) & (~val0)) | ( in1 & val1));
      val1 = ((~in0) & val1) | (in1 & (~val0));
      val0 = v0;
      if( debug ) {
         HLog << "    in0.XOR: " << mask64ToBinary( in0 ) << std::endl;
         HLog << "    in1.XOR: " << mask64ToBinary( in1 ) << std::endl;
      }
   }
   // bitwise NOT; what if we have X, ??? This is a problem ???
   //   solved by storing both and retrieve it later
   // save non-inverted (val0, val1) in input_values[0] first
   input_values[0][0] = val0;
   input_values[0][1] = val1;
   // Invert if applicable
   v0 = val0;
   val0 = ~val1;
   val1 =  ~v0;
   // save the inverted (val0, val1) value in input_values[1]
   input_values[1][0] = val0;
   input_values[1][1] = val1;
   // retrieve the right (val0, val1)
   val0 = input_values[ tableInv[ gType ]][0];
   val1 = input_values[ tableInv[ gType ]][1];

   if( debug ) {
      HLog << "  in0.INV: " << mask64ToBinary( in0 ) << std::endl;
      HLog << "  in1.INV: " << mask64ToBinary( in1 ) << std::endl;
   }
}


void Evaluate::assignBitParallelStimuliInVectorMode( bool external_pattern,
                                                     int start_in_256,
                                                     int parallel_in_256,
                                                     int cycle,
                                                     int frame,
                                                     bool io_op_per_cycle
                                                   )
{
   assert( start_in_256 >= 0 );
   assert( parallel_in_256 > 0 && parallel_in_256 <= 256 );
   assert( cycle >= 0 );
   int passes = parallel_in_256 / 64;
   if( (parallel_in_256 % 64) > 0 ) { passes += 1; }
   int start = start_in_256;
   for( int p=0; p < passes; p++ ) {
      // compute parallel in 64
      int parallel = parallel_in_256 % 64;
      if( 0==parallel || (p+1)<passes ) {
         parallel = 64; // if not last pass or last pass is 64 patterns
      }
      PatternSet& pset = (htDB::instance())->externalPatternSet();
      std::vector<uint64_t> vec0;
      std::vector<uint64_t> vec1;
      bool force_pi_is_on = false;
      std::vector<std::string> clk_pins;
      if( external_pattern) {
         pset.getPatternStimuli(
            start, parallel, cycle, vec0, vec1, force_pi_is_on, clk_pins );
      }
      if(((force_pi_is_on && external_pattern) || // external pat
         ((0==cycle || io_op_per_cycle ) && !external_pattern)) && // rand pat
         0==frame ) {
         // update gmv on host
         for( int i=0; i < numDataPIs(); i++) {
            int g = constDataInputNodes()[i];
            uint64_t val0 = 0, val1 = 0;
            if( external_pattern ) {
               val0 = vec0[i];
               val1 = vec1[i];
            } else {
               val0 = ((htDB::instance())->randomGen()).mt64Rand();
               val0 &= MASK_UPTO_64[parallel];
               val1 = val0;
            }
            gmv0x4( g, p, val0 );
            gmv1x4( g, p, val1 );
            //debugAssignStorePattern("force PIv", g, cycle, frame,val0,val1);
         }
      }
      // Assign clock inputs at each frame of each frame if there are clocks
      for( int i=0; i < numClockPIs(); i++ ) {
         uint64_t val0 = 0, val1 = 0;
         int g = constClockInputNodes()[i];
         // clock assume off_state at frame 0 and 2
         int v = (1==frame) ? (1 - clockOffState(g)) : clockOffState(g);
         if( v ) {
            val0 = ALL_ONES_64;
            val1 = ALL_ONES_64;
         }
         //debugAssignStorePattern("force clock.v", g, cycle, frame,val0,val1);
         gmv0x4( g, p, val0 );
         gmv1x4( g, p, val1 );
      }
      // Init FF only at frame 0 of cycle 0
      if( 0==cycle && 0==frame ) {
         int offset = force_pi_is_on ? numDataPIs() : 0;
         const int mul = clockedDffIsUsed() ? 5 : 1; // gate to sim per DFF
         int g = firstNodeFF(); // gid of 1st gate in DFF
         for( int b=0 ; b < numFFs(); b++, g+=mul) {
            uint64_t val0 = 0, val1 = 0;
            if( external_pattern ) {
               assert( vec0.size() == (unsigned)offset+numFFs() );
               val0 = vec0[offset+b];
               val1 = vec1[offset+b];
            } else {
               val0 = ((htDB::instance())->randomGen()).mt64Rand();
               val0 &= MASK_UPTO_64[parallel];
               val1 = val0;
            }
            gmv0x4Dff( g, p, val0 );
            gmv1x4Dff( g, p, val1 );
            //debugAssignStorePattern("init FFv", g, cycle, frame,val0,val1);
         }
      }
      // TODO, how to extend the usage of clk_pins
      start += parallel; // these two numbers are w.r.t 64-bit boundary
   }
}


bool Evaluate::oldAndNewValuesDiffer( int gid,
                                      uint64_t val0,
                                      uint64_t val1,
                                      bool is_4val_logic )
{
   bool res = (gmv0(gid)!=val0 || (gmv1(gid)!=val1 && is_4val_logic));
   return res;
}


void Evaluate::debugAssignStorePattern( const std::string msg,
                                        int gid,
                                        int cycle,
                                        int frame,
                                        uint64_t val0,
                                        uint64_t val1 )
{
   HLog << "#### " << msg << " gid=" << gid+1
        << " cycle=" << cycle << "  frame=" << frame
        << std::endl;
   HLog << "val0: " << mask64ToBinary( val0) << std::endl;
   HLog << "val1: " << mask64ToBinary( val1) << std::endl;
}


void Evaluate::assignBitParallelStimuli( bool external_pattern,
                                         int start,
                                         int parallel,
                                         int cycle,
                                         int frame,
                                         bool io_op_per_cycle,
                                         bool is_4val_logic,
                                         bool is_event_driven,
                                         EventManager* event_mgr
                                       )
{
   assert( start >= 0 && cycle >= 0 && frame >= 0);
   assert( parallel > 0 && parallel <= 64 );

   int start_node = 0; // for copy gmv to GPU
   int num_nodes = 0;  // for copy gmv to GPU

   PatternSet& pset = (htDB::instance())->externalPatternSet();
   std::vector<uint64_t> vec0;
   std::vector<uint64_t> vec1;
   bool force_pi_is_on = false;
   std::vector<std::string> clk_pins;
   if( external_pattern ) {
      pset.getPatternStimuli( start, parallel, cycle, vec0, vec1,
                              force_pi_is_on, clk_pins );
   }
   // Force PI is done at frame 0 of cycle 0 or each cycle if io_op_per_cycle
   if(((force_pi_is_on && external_pattern) ||  // external pat
      ((0==cycle || io_op_per_cycle) && !external_pattern)) // random pat
      && 0==frame ) {
      // update start_node and num_nodes for copying gmv to GPU
      start_node = 0;
      num_nodes += numPIs();
      for( int i=0; i < numDataPIs(); i++) {
         int g = constDataInputNodes()[i];
         uint64_t val0 = 0, val1 = 0;
         if( external_pattern ) {
            val0 = vec0[i];
            val1 = vec1[i];
         } else {
            val0 = ((htDB::instance())->randomGen()).mt64Rand();
            val0 &= MASK_UPTO_64[parallel];
            val1 = val0;
         }
         if( is_event_driven &&
            oldAndNewValuesDiffer( g, val0, val1, is_4val_logic)) {
            addSuccessorEvents( g, event_mgr );
         }
         //debugAssignStorePattern("force PI", g, cycle, frame,val0,val1);
         gmv0( g, val0 );
         gmv1( g, val1 );
      }
   }
   // Assign clock inputs at each frame of each frame if there are clocks
   for( int i=0; i < numClockPIs(); i++ ) {
      uint64_t val0 = 0, val1 = 0;
      int g = constClockInputNodes()[i];
      // clock assume off_state at frame 0 and 2
      int v = (1==frame) ? (1 - clockOffState(g)) : clockOffState(g);
      if( v ) {
         val0 = ALL_ONES_64;
         val1 = ALL_ONES_64;
      }
      if( is_event_driven &&
         oldAndNewValuesDiffer( g, val0, val1, is_4val_logic)) {
         addSuccessorEvents( g, event_mgr );
      }
      //debugAssignStorePattern("force clock", g, cycle, frame,val0,val1);
      gmv0( g, val0 );
      gmv1( g, val1 );
   }
   // Init FF only at frame 0 of cycle 0
   if( 0==cycle && 0==frame ) {
      num_nodes = ckt().numNodes(); // update num_nodes to all nodes
                                    // as input of DFF is everywhere
      const int offset = force_pi_is_on ? numDataPIs() : 0;
      const int mul = clockedDffIsUsed() ? 5 : 1; // gate to simulate per DFF
      int g = firstNodeFF(); // gid of 1st gate in DFF
      for( int b=0 ; b < numFFs(); b++, g+=mul) {
         uint64_t val0 = 0, val1 = 0;
         if( external_pattern ) {
            assert( vec0.size() == (unsigned)offset+numFFs() );
            val0 = vec0[offset+b];
            val1 = vec1[offset+b];
         } else {
            val0 = ((htDB::instance())->randomGen()).mt64Rand();
            val0 &= MASK_UPTO_64[parallel];
            val1 = val0;
         }
         //debugAssignStorePattern("init FF", g, cycle, frame,val0,val1);
         if( is_event_driven ) {
            int g_adj = clockedDffIsUsed() ? (g+2) : g; // g+2 is slave MUX
            if( oldAndNewValuesDiffer( g_adj, val0, val1, is_4val_logic)) {
               addSuccessorEvents( g_adj, event_mgr );
            }
            // Insert FF node g to event queue if the value of init_ff
            //   is different than its data input
            int gid_din = clockedDffIsUsed() ? // gid of data input
                          predC0(g+1)[1] : // d0 input of master MUX
                          predC0(g)[0];    // input of DE
            if( oldAndNewValuesDiffer( gid_din, val0, val1, is_4val_logic)) {
               event_mgr->insert(g, ckt().level(g));
            }
         }
         gmv0Dff( g, val0 ); // assign gmv to Dff; can be multiple gates
         gmv1Dff( g, val1 );
      }
   }
   if( is_event_driven) {
      // Start simulating events from level 1
      event_mgr->curLevel( 1 );
   }
   // TODO, how to extend the usage of clk_pins
   // copy node value data from host to GPU when sim_loc is OCL_GPU
   SimLocation sim_loc = (htDB::instance())->simOptions().simLocation();
   EvaluateCL& eval_cl = (htDB::instance())->evalCl();
   // Only copy gmv when sim_loc is GPU; for CPU there is no need to copy.
   if( OCL_GPU == sim_loc ) {
      if( eval_cl.debugFlag() & EVAL_CL_DBG_GMV_COPY) {
         HLog << "copy gmv from host to gpu ....." << std::endl;
      }
      SimulationOptions & sim_opt = (htDB::instance())->simOptions();
      cl_uint pfm_num = sim_opt.platformNum();
      cl_uint dev_num = sim_opt.deviceNum();
      if( num_nodes > 0 ) {
         // Only copy nodes of required range when it is needed
         eval_cl.copyGmvFromHostToGpu( pfm_num,
                                       dev_num,
                                       0, // use gmv of group 0
                                       start_node,
                                       num_nodes );
      }
   }
}


void Evaluate::getSimCyclesAndReseedRandomGen( bool external_pattern ) {
   // Obtain number of pattern cycles
   _numCycles = 0;
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   if( external_pattern ) {
      _numCycles =
         (int)(htDB::instance())->externalPatternSet().cyclesPerPattern();
   } else { // RANDOM pattern source
      _numCycles = ((htDB::instance())->randomPatterns()).sequentialDepth();
      if( ckt().isCombinationalCkt() && _numCycles>1 ) {
         HLog << InfoWarn
              << "design '" << cktOnHost.iscasCktFile()
              << "' is combinational circuit."
              << "Sequential depth (cycle) is changed to 1 automatically"
              << std::endl;
         _numCycles = 1; // overwrite sequential depth with 1
      }
      // Reseed random generator upon each call of "simulate_patterns"
      int seed = ((htDB::instance())->randomPatterns()).seed();
      int real_seed = (0==seed) ? time(NULL) : seed;
      ((htDB::instance())->randomGen()).seedMt64Rand(unsigned(real_seed));
   }
}


bool Evaluate::simulatePatterns( bool external_pattern,
                                 bool store_patterns )
{
   // Retrieve simulator options
   SimulationOptions & sim_opt = (htDB::instance())->simOptions();
   const bool is_3frame_sim = (3==numFramesPerCycle());
   const bool is_4val_logic = sim_opt.fourValueLogic();
   const bool io_per_cycle = sim_opt.ioOpPerCycle();
   const bool is_event_driven = sim_opt.isEventDriven();
   const LogicSimMethod lsim_type = sim_opt.logicSimMethod();
   EvaluateCL& eval_cl = (htDB::instance())->evalCl();
   const SimLocation sim_loc = sim_opt.simLocation();
   const bool vector_mode_on = sim_opt.vectorModeOn();
   const int num_parallel_psets = sim_opt.numParallelPsetsOnGpu();

   // Compute sim cycles and reseed random number generator
   getSimCyclesAndReseedRandomGen( external_pattern );
   cl_uint pfm_num = sim_opt.platformNum();
   cl_uint dev_num = sim_opt.deviceNum();
   if( OCL_GPU == sim_loc || OCL_CPU==sim_loc ) {
      eval_cl.resetOclMeasurements();
      if( !eval_cl.setupSimulationOnOclDevice( pfm_num, dev_num, ckt())) {
         return false;
      }
      if(1==num_parallel_psets) { // For overlaping kernel with memcpy
         if( false==eval_cl.setupPatternDataOnHostAndGpu( pfm_num )) {
            return false;
         }
      }
   }

   // reset timer and counter for gate evaluations
   resetMeasurements();
   // get start time
   std::chrono::system_clock::time_point
      start_t = std::chrono::system_clock::now();
   std::clock_t start_cpu_t = std::clock();

   // declare event queue for event-driven simulation
   // To test brute force event manager
   // BruteForceEventManager event_manager( numLevels(), numNodes());
   // To test mimic-gpu event manager
   MimicGpuEventManager event_mgr0( numNodes(), ckt().firstNodeInLevel());
   // To test flag-based event manager
   FlagBasedEventManager event_mgr1( numNodes(), ckt().firstNodeInLevel());

   // Get the pointer of event manager
   EventManager *event_mgr = NULL;
   switch( lsim_type ) {
      case EVENT_FLAGS_BASED: {
         event_mgr = &event_mgr1;
         break;
      }
      case EVENT_QUEUE_BASED: {
         event_mgr = &event_mgr0;
         break;
      }
      default: break;
   }
   // Initialize node value for either event or non-event driven simulation
   //    on host
   if( HOST == sim_loc ) {
      initializeNodeValues( is_4val_logic, vector_mode_on,
                            is_event_driven, event_mgr);
   }

   int start=0;
   int end = external_pattern ?
      (htDB::instance())->externalPatternSet().numPatterns() :
      (htDB::instance())->randomPatterns().number();

   HLog << InfoNote
        << "simulating " << end << " "
        << (external_pattern ? "external":"random") << " patterns ..... "
        << std::endl;
   sim_opt.printOptions();

   // For overlaping kernel with memcpy
   if( OCL_GPU==sim_loc && 1==num_parallel_psets) {
      if( !eval_cl.simulatePatternsWithOverlap( pfm_num, dev_num, start, end,
         external_pattern, store_patterns, numCycles(), io_per_cycle,
         lsim_type) ) {
         return false;
      }
      start = end + 1; // Skip the following while loop in this scenario
   }

   const int max_width = vector_mode_on ? 256 : 64;
   // 'end' is the actual ending pattern index + 1
   while( start < end ) {
      //int parallel = (end-start)<63 ? (end-start) : 64;
      // The old one seems to have a boundary problem
      int parallel = (end-start) < max_width ? (end-start) : max_width;

      // bits_v0 and bits_v1 store parallel pattern bitstream data
      std::vector< std::vector< uint64_t > > bits_v0( numCycles() );
      std::vector< std::vector< uint64_t > > bits_v1( numCycles() );
      std::vector< std::vector< cl_ulong4 > > bits_v0_256( numCycles() );
      std::vector< std::vector< cl_ulong4 > > bits_v1_256( numCycles() );

      // simulate patterns in parallel cycle by cycle
      if( is_event_driven ) {
         if( vector_mode_on ) {
            HLog << InfoError
                 << "event driven simulation is not supported for vector mode"
                 << std::endl;
            return false;
         }
         for( int cycle=0; cycle < numCycles(); cycle++) {
            for( int frame=0; frame < numFramesPerCycle(); frame++ ) {
               assignBitParallelStimuli( external_pattern,
                                         start,
                                         parallel,
                                         cycle,
                                         frame,
                                         io_per_cycle,
                                         is_4val_logic,
                                         is_event_driven,
                                         event_mgr );

               bool last_cycle = (cycle == (numCycles()-1));
               goodSimWithEvents( parallel,
                                  cycle,
                                  frame,
                                  is_3frame_sim,
                                  is_4val_logic,
                                  store_patterns,
                                  io_per_cycle,
                                  last_cycle,
                                  bits_v0[cycle],
                                  bits_v1[cycle],
                                  event_mgr );
               //HLog << "######### cycle " << cycle << "#####\n";
               //printAllNodeValues();
            }
         }
      } else {
         for( int cycle=0; cycle < numCycles(); cycle++) {
            for( int frame=0; frame < numFramesPerCycle(); frame++ ) {
               if( vector_mode_on ) {
                  assignBitParallelStimuliInVectorMode( external_pattern,
                                                        start,
                                                        parallel,
                                                        cycle,
                                                        frame,
                                                        io_per_cycle );
               } else {
                  assignBitParallelStimuli( external_pattern,
                                            start,
                                            parallel,
                                            cycle,
                                            frame,
                                            io_per_cycle,
                                            is_4val_logic,
                                            is_event_driven,
                                            event_mgr );

               }

               bool last_cycle = (cycle == (numCycles()-1));
               goodSim( parallel,
                        cycle,
                        frame,
                        vector_mode_on,
                        is_4val_logic,
                        store_patterns,
                        io_per_cycle,
                        last_cycle,
                        bits_v0[cycle],
                        bits_v1[cycle],
                        bits_v0_256[cycle],
                        bits_v1_256[cycle] );
               //HLog << "################ cycle " << cycle << "#####\n";
               //printAllNodeValues();
            }
         }
      }

      if( store_patterns ) { // store pattern in internal pset if applicable
         if( vector_mode_on ) {
            for( int b=0; b<parallel; b++ ) {
               int p = b / 64; // compute word position within vector
                               // 0-63:0, 64-127:1, 128-191:2, 192-255:3
               //if( (b % 64)==0 ) { p += 1; }
               uint64_t mask = MASK_1HOT_64[ b%64 ]; // bit mask within word
               PatternSet& pset = (htDB::instance())->internalPatternSet();
               pset.addPattern( bits_v0_256,bits_v1_256,p,mask,io_per_cycle);
            }
         } else {
            uint64_t mask = 1;
            for( int b=0; b<parallel; b++ ) {
               PatternSet& pset = (htDB::instance())->internalPatternSet();
               if( is_4val_logic ) {
                  pset.addPattern( bits_v0, bits_v1, mask, io_per_cycle );
               } else {
                  pset.addPattern( bits_v0, bits_v0, mask, io_per_cycle );
               }
               mask <<= 1;
            }
         }
      }
      start += parallel; // update 'start' pattern number
   }

   // get end time
   std::chrono::system_clock::time_point
      end_t = std::chrono::system_clock::now();
   std::clock_t end_cpu_t = std::clock();
   _simSysTime = elapsedSysTimeInSeconds(start_t, end_t);
   _simCpuTime = elapsedCpuTimeInSeconds(start_cpu_t, end_cpu_t);
   printSimulationStatistics();

   return true;
}


std::string Evaluate::nodeValue(int g, uint64_t mask) const {
   std::string val("");
   if( gmv0(g) & mask ) {
      if( gmv1(g) & mask ) {
         val = "1";
      } else {
         val = "X";
      }
   } else {
      if( gmv1(g) & mask ) {
         val = "Z";
      } else {
         val = "0";
      }
   }
   return val;
}


std::string Evaluate::nodeValues( int g ) const {
   std::string str("");
   std::string val("");
   for( int i=0; i < 64; i++ ) {
      uint64_t mask = MASK_1HOT_64[i];
      if( gmv0(g) & mask ) {
         if( gmv1(g) & mask ) {
            val = "1";
         } else {
            val = "X";
         }
      } else {
         if( gmv1(g) & mask ) {
            val = "Z";
         } else {
            val = "0";
         }
      }
      str = val + str;
   }
   return str;
}


void Evaluate::printNode(int g, uint64_t mask) const {
   std::string val("");
   if( gmv0(g) & mask ) {
      if( gmv1(g) & mask ) {
         val = "1";
      } else {
         val = "X";
      }
   } else {
      if( gmv1(g) & mask ) {
         val = "Z";
      } else {
         val = "0";
      }
   }
   char buf[1024];
   sprintf(buf, "(%4d:%s)", g+1, val.c_str());
   HLog << buf;
}


void Evaluate::printNode(int g, uint64_t mask, std::ofstream& ofs) const {
   std::string val("");
   if( gmv0(g) & mask ) {
      if( gmv1(g) & mask ) {
         val = "1";
      } else {
         val = "X";
      }
   } else {
      if( gmv1(g) & mask ) {
         val = "Z";
      } else {
         val = "0";
      }
   }
   char buf[1024];
   sprintf(buf, "(%4d:%s)", g+1, val.c_str());
   ofs << buf;
}


void Evaluate::printAllNodeValues() const {
   HLog << "Dump internal node values:\n";
   for( int g=0; g < numNodes(); g++ ) {
      HLog << g+1 << ":" << ckt().gate(g).typeName();
      if( predC0(g).size() ) {
         HLog << " p(";
         for( unsigned p=0; p < predC0(g).size(); p++ ) {
            HLog << " " << (predC0(g)[p] + 1);
         }
         HLog << " )";
      }
      HLog << std::endl;
      HLog << nodeValues( g ) << std::endl;
   }
   HLog << std::endl;
}


void Evaluate::printAllNodeValues(uint64_t mask ) const {
   for( int g=0; g < numNodes(); g++ ) {
      printNode( g, mask);
      HLog << " ";
      HLog << g+1 << ":" << ckt().gate(g).typeName();
      if( predC0(g).size() ) {
         HLog << " p(";
         for( unsigned p=0; p < predC0(g).size(); p++ ) {
            HLog << " " << (predC0(g)[p] + 1);
         }
         HLog << " )";
      }
      HLog << std::endl;
   }
   HLog << std::endl;
}


void Evaluate::printAllNodeValues( uint64_t mask,
                                   int up_to_lev,
                                   const std::string& fn ) const
{

   std::ofstream ofs( fn );
   if( !(ofs.is_open()) ) {
      HLog << InfoError
           << "failed to open file '" << fn << " for write"
           << std::endl;
      return;
   }
   for( int g=0; g < numNodes(); g++ ) {
      if( level(g) > up_to_lev ) {
         break;
      }
      printNode( g, mask, ofs);
      ofs << " ";
      ofs << g+1 << ":" << ckt().gate(g).typeName();
      if( predC0(g).size() ) {
         ofs << " p(";
         for( unsigned p=0; p < predC0(g).size(); p++ ) {
            ofs << " " << (predC0(g)[p] + 1);
         }
         ofs << " )";
      }
      ofs << std::endl;
   }
   ofs << std::endl;
   ofs.close();
}


void Evaluate::printInputs(uint64_t mask) const {
   char buf[1024];
   //sprintf( buf, "Input values on mask %" PRIu64, mask);
   sprintf( buf, "Input values on mask %jx", mask);
   HLog << buf << std::endl;
   for( int g=firstNodePI(); g < firstNodePI()+numPIs(); g++) {
      printNode( g, mask);
   }
}


void Evaluate::printDffs(uint64_t mask) const {
   char buf[1024];
   //sprintf( buf, "DFF values on mask %" PRIu64, mask);
   sprintf( buf, "DFF values on mask %jx", mask);
   HLog << buf << std::endl;
   for( int g=firstNodeFF(); g < firstNodeFF()+numFFs(); g++) {
      printNode( g, mask);
   }
}
