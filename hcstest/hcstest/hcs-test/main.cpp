/////////////////////////////////////////////////////////////////
//                        main.cpp
//  Entry point for hcs-test.exe. It includes command option
//  parsing, tcl command registration, and tclsh invocation etc.
/////////////////////////////////////////////////////////////////

#include <tcl.h>
#include <string>
#include <string.h>
#include "app-common/utility.h"
#include "app-common/command.h"
#include "HtCommand.h"
#include "OpenCLInfo.h"
#include "HtSetting.h"
#include "Design.h"

// Forward declaration of functions used in main()
int Tcl_AppInit( Tcl_Interp *interp);

static bool processArgcArgvInMain( int argc, char** argv,
                              std::string& batchFile,
                              std::string& logFile,
                              bool& logFileOverwrite );
static void printToolBanner();


// Main function
int main( int argc, char* argv[]) {


   std::string batch_file("");
   const std::string defaultLogFile("HCS-TEST.log");
   std::string log_file( defaultLogFile.c_str() );
   bool replace = false;
   if( false==processArgcArgvInMain(
              argc, argv, batch_file, log_file, replace )) {
      return 1;
   }

   std::string exec_path( executablePath(argv[0]) );
   int pos = exec_path.rfind('/'); // remove executable name from path
   (appCommonDB::instance())->execPath( exec_path.substr(0, pos+1) );
   (appCommonDB::instance())->logFileName( log_file );

   if( defaultLogFile != log_file &&
       fileExists( log_file.c_str() ) &&
       false==replace ) {
      std::cout << InfoError
                << "File \"" << log_file << "\" exists. "
                << "Please use \"-replace\" to overwrite it.\n";
      return 1;
   }

   // Setup tee stream and log file ofstream
   std::ofstream ofs_log( log_file.c_str() );
   TeeDevice tee_device( std::cout, ofs_log );
   TeeStream tee_stream( tee_device );
   (appCommonDB::instance())->teeStream( tee_stream );
   (appCommonDB::instance())->logOfstream( ofs_log );

   // Log tool invocation
   for( int i=0; i<argc; i++ ) {
      HLog << argv[i] << " ";
   }
   HLog << "\n";

   // Print tool banner
   printToolBanner();

   // Log tcl script info
   if( batch_file.size() ) {
      HLog << InfoComment
           << "Tcl script file was set to '"
           << batch_file << "'\n";
   }

   // Log user specified log file
   HLog << InfoComment
        << "Log file was set to '"
        << log_file << "'\n";
   HLog.flush();

   // Query OpenCLInfo here
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   ocl_info.query();

   // Fake argc and argv for Tcl_Main()
   int tcl_argc = (batch_file == "") ? 1 : 2;
   char *tcl_argv[2];
   tcl_argv[0] = argv[0];
   // Use vecotr< char > to guarantee auto deletion
   std::vector< char > buffer( batch_file.size() + 1);
   tcl_argv[1] = &(buffer[0]);
   strcpy( tcl_argv[1], batch_file.c_str());

   // Tcl_Main() is called
   Tcl_Main( tcl_argc, tcl_argv, Tcl_AppInit);

   // Close tee stream and log file stream
   tee_stream.close();
   ofs_log.close();
   return 0;
}


// Tcl_AppInit is called from Tcl_Main
// after the Tcl interpreter has been created
// and before the script file
// or interactive command loop is entered
int Tcl_AppInit( Tcl_Interp *interp) {
   if( Tcl_Init( interp ) == TCL_ERROR) {
      return TCL_ERROR;
   }

   // Register application-specific commands
   registerSharedTclCommands( interp ); // in app-common
   registerHtTclCommands( interp );

   // Define the start-up filename. This file is read in
   // case the program is run interactively.
   // Tcl_SetVar( interp, "tcl_rcFileName", "~/.mytcl", TCL_GLOBAL_ONLY);
   // Tcl_SetVar( interp, "tcl_rcFileName", "~/.tclshrc", TCL_GLOBAL_ONLY);

   // Turn off tclreadline and use rlwrap instead
   /*
   std::string tclshrc_file = (appCommonDB::instance())->execPath();
   tclshrc_file += "scripts/tclreadline.tclshrc";
   if( !fileExists( tclshrc_file )) {
       std::cout << InfoError << "Tclshrc file '" << tclshrc_file
                 << "' cannot be located.\n";
       return 1;
   }
   Tcl_SetVar( interp, "tcl_rcFileName", tclshrc_file.c_str(), TCL_GLOBAL_ONLY);
   */

   // Set tcl interactive prompt
   /*
      proc prompt {} {
         puts -nonewline "\[[pwd]\]% "
         flush stdout
      }
      set ::tcl_prompt1 prompt
      If you want to do this from C, you can Tcl_Eval it.
   */
   std::string commands("");
   commands += "proc prompt1 {} {\n";
   commands += "  puts -nonewline \"HTest> \"\n";
   commands += "  flush stdout\n";
   commands += "}\n";
   commands += "proc prompt2 {} {\n";
   commands += "  puts -nonewline \"> \"\n";
   commands += "  flush stdout\n";
   commands += "}\n";
   commands += "set ::tcl_prompt1 prompt1\n";
   commands += "set ::tcl_prompt2 prompt2\n";

   Tcl_Eval( interp, commands.c_str());

   // Test of Tcl_Invoke
   // Tcl_Invoke( interp, "set", "foo", "$xyz [foo] {", NULL);

   // Redirect tcl_stdout, to be done
   /*
   FILE *fp_log = fopen("myfile.log", "w");
   //Tcl_Interp *interp = Tcl_CreateInterp();
   Tcl_Channel myChannel = Tcl_MakeFileChannel(fp_log, TCL_WRITABLE);
   Tcl_RegisterChannel(myChannel);
   Tcl_SetStdChannel(myChannel, TCL_STDOUT);
   */
   return TCL_OK;
}


static bool processArgcArgvInMain( int argc, char** argv,
                              std::string& batchFile,
                              std::string& logFile,
                              bool& logFileOverwrite )
{
   po::options_description options_description("Options:");
   options_description.add_options()
      ("help,h", "produce help message")
      ("file,f", po::value<std::string>(), "specify tcl script file")
      ("log,l", po::value<std::string>(), "specifiy log file")
      ("replace,r", po::bool_switch()->default_value(false),
                      "enable log file overwrite")
      //("pos1", po::value<int>(), "pos1 value")
      //("pos2", po::value<int>(), "pos2 value")
      ;

   // Disable positional options by not adding options in it
   po::positional_options_description pos_options;
   //pos_options.add("pos1", 1);
   //pos_options.add("pos2", 1);

   po::variables_map variables_map;

   try {
      //po::store( po::parse_command_line(argc, argv, options_description),
      //           variables_map);
      po::store(po::command_line_parser(argc, argv)
                .options( options_description )
                .positional( pos_options)
                .run(), variables_map);

      if( variables_map.count("help")) {
         // Usage of positional options has to be print manually
         //   such as "  hcs-test [ options ] <pos1> <pos2>\n\n"
         //   std::cout << options_description << "\n";
         std::cout << "Program usage:\n"
                   << "  hcs-test [ options ]\n\n"
                   << options_description;
         return false;
      }
      // Update log file name; default name is "hcs-test.log"
      if( variables_map.count("log")) {
         logFile = variables_map["log"].as<std::string>();
      }
      // Update script file name
      if( variables_map.count("file")) {
         batchFile = variables_map["file"].as<std::string>();
      }
      logFileOverwrite = variables_map["replace"].as<bool>();
      // There must be an easy way to handle the relationship between the
      // option "help" and other required options
      // Yes, the magic is putting the po::notify after "help" option check
      po::notify( variables_map );

   } catch(std::exception& e) {
      std::cerr << InfoError << e.what() << "\n";
      return false;

   } catch(...) {
      std::cerr << InfoError << "Unknown error!" << "\n";
      return false;
   }

   return true;
}


// Print tool banner with c++ iostream
static void printToolBanner() {
   // Get hostname
   char hostname[1024];
   gethostname( hostname, 1000 );
   struct sysinfo s_info;
   sysinfo( &s_info);

   // Get size of memory(ram) and swap
   size_t total_ram = s_info.totalram;
   size_t total_swap = s_info.totalswap;
   unsigned mem_unit = s_info.mem_unit;
   unsigned scale = 1024 * 1024 / mem_unit;

   size_t total_ram_mb = total_ram / scale;
   size_t total_swap_mb = total_swap / scale;

   char sysinfo_str[1024];
   sprintf( sysinfo_str, "Host: %s (%lu MB Memory, %lu MB Swap)",
            hostname, total_ram_mb, total_swap_mb );

   // __TIMESTAMP__ represents last modification time of current source file
   //   use __DATE__ and __TIME__ instead.
   HLog << "//                 HCS Test 0.1a\n"
        << "//       Built on " << __DATE__ << "  " <<  __TIME__ << "\n"
        << "//  @Shantou University, Shantou, Guangdong, China\n"
        << "//  The software is provided as is with no warranty.\n"
        << "//                  2017 - 2050\n"
        << "//\n"
        << "//  " << sysinfo_str <<  "\n";


   // Print current system time
   HLog << InfoComment
        << "Current time " << currentDateTime( SECOND_PRECISION ) << "\n";

   HLog.flush(); // Forcing a flush
}
