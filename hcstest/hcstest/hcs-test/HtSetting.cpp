/////////////////////////////////////////////////////////////////
//                    HtSetting.cpp
/////////////////////////////////////////////////////////////////
#include "HtSetting.h"

HcsTestSetting::HcsTestSetting() : _patSource( PAT_SRC_EXTERNAL ),
                                   _storePatterns( false )
{
   ;
}


HcsTestSetting::~HcsTestSetting() {
   ;
}


void SimulationOptions::printOptions() const {
   unsigned pfm_id = platformNum();
   unsigned dev_id = deviceNum();
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & platform = ocl_info.platform( pfm_id );
   OpenCLDevice& cl_dev = platform.device(dev_id);

   HLog << TwoSpace << "location: " << simLocationStr() << std::endl;
   if( OCL_GPU==simLocation()) {
      HLog << FourSpace
           << "platform_id: " << pfm_id
           << "; device_id: " << dev_id << std::endl;
      HLog << FourSpace
           << "device name: " << cl_dev.deviceName() << std::endl;
      OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
      OpenCLDevice& device = ocl_info.platform(pfm_id).device(dev_id);
      HLog << FourSpace
           << "global work size: ("
           << device.globalWorkSizes(0) << ":"
           << device.globalWorkSizes(1) << ":"
           << device.globalWorkSizes(2) << ")"
           << "; local work size: ("
           << device.localWorkSizes(0) << ":"
           << device.localWorkSizes(1) << ":"
           << device.localWorkSizes(2) << ")"
           << std::endl;
      HLog << FourSpace
           << "circuit cl_mem: " << cl_dev.cktClMemTypeName() << std::endl;
   }
   char event_msg[1024];
   switch( logicSimMethod() ) {
      case EVENT_FLAGS_BASED: {
         sprintf(event_msg, "event-driven simulation (flag-based)");
         break;
      }
      case EVENT_QUEUE_BASED: {
         sprintf(event_msg, "event-driven simulation (queue-based)");
         break;
      }
      default: {
         sprintf(event_msg, "in-order simulation");
         break;
      }
   }
   if( HOST==simLocation()) {
      HLog << TwoSpace
           << (fourValueLogic() ? "4-value logic":"2-value logic")
           << " and " <<  event_msg
           << std::endl;
   } else if( OCL_GPU==simLocation()) {
      HLog << TwoSpace
           << "4-value logic" << " and " << event_msg
           << std::endl;
   }
   HLog << TwoSpace
        << (ioOpPerCycle() ?
           "force_pi and measure_po are applied on every cycle":
           "force_pi is applied on cycle 0, and measure_po on last cycle")
        << std::endl;
}


bool SimulationOptions::verifyAndSetOclPlatformDevice( SimLocation sim_loc,
                                                       int pfm_id,
                                                       int dev_id )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   if( !ocl_info.checkPlatformIdRange( pfm_id ) ) {
      return false;
   }
   OpenCLPlatform& platform = ocl_info.platform((cl_uint)pfm_id);
   if( !platform.checkDeviceIdRange( dev_id )) {
      return false;
   }
   OpenCLDevice& device = platform.device((cl_uint)dev_id);
   if((CL_DEVICE_TYPE_GPU==device.deviceType() && sim_loc!=OCL_GPU) ||
      (CL_DEVICE_TYPE_CPU==device.deviceType() && sim_loc!=OCL_CPU) ) {
      HLog << InfoError
           << "the device type does not match "
           << device.deviceTypeName( device.deviceType()) << "."
           << std::endl;
      return false;
   }
   if( NULL == device.queue() ) {
      HLog << InfoError
           << "please create_ocl_queue first on the targeted device"
           << std::endl;
      return false;
   }
   _pfmId = (unsigned)pfm_id;
   _devId = (unsigned)dev_id;
   return true;
}
