#include "defines.h"
#include "Common.h"
#include "Macro.cl"


//---------------------------------------------------------------------
//    auxiliary functions for computing flattened global id
//---------------------------------------------------------------------
unsigned compute_flattened_global_id() {
   unsigned flatten_id = get_global_id(0);
   unsigned multiplicand = get_global_size(0);
   unsigned work_dims = get_work_dim();   // number of dimensions in use
   for( unsigned dim = 1; dim < work_dims; dim++ ) {
      //flatten_id += get_global_id( dim ) * multiplicand;
      flatten_id = mad24((unsigned)get_global_id( dim ),
                         (unsigned)multiplicand,
                         (unsigned)flatten_id );
      multiplicand = multiplicand * get_global_size( dim );
   }
   return flatten_id;
}


unsigned compute_adjusted_flatten_global_id( unsigned base_node_id )
{
   unsigned flatten_id = compute_flattened_global_id();
   return (base_node_id + flatten_id);
}


////

// Debug function to print all 32 bits of unsigned integer
void print_unsigned_32_bits( unsigned val )
{
   unsigned mask = MSB_MASK_32;
   for( int i=0; i<32; i++, mask >>= 1) {
      if( val & mask ) {
         printf("1");
      } else {
         printf("0");
      }
   }
}


// Debug function to print one bit of gmv value
void print_1_bit( ulong mask, ulong val0, ulong val1 )
{
   if( val0 & mask ) {
      if( val1 & mask ) {
         printf("1");
      } else {
         printf("X");
      }
   } else {
      if( val1 & mask ) {
         printf("Z");
      } else {
         printf("0");
      }
   }
}


// Debug function to print 64 bits of gmv value
void print_64_bits( ulong val0, ulong val1 )
{
   ulong mask = 0x8000000000000000;
   for( int i=0; i < 64; i++, mask >>= 1) {
      print_1_bit( mask, val0, val1);
   }
   printf("\n");
}


int gmv_changed( ulong old_gmv0, ulong old_gmv1,
                 ulong new_gmv0, ulong new_gmv1 )
{
   //if( (old_gmv0 != new_gmv0) || (old_gmv1 != new_gmv1)) {
   //   return 1;
   //} else {
   //   return 0;
   //}
   return ((old_gmv0 ^ new_gmv0) || (old_gmv1 ^ new_gmv1));
}


//////////////////////////////////////////////////////////////////////////
//  1.Make sure there is no memory out of bound access
//    otherwise it may cause CL_OUT_OF_RESOURCES in clEnqueue* function
//  2.Adjust global_work_size and local_work_size when enqueue kernel
//    such that global_id is within expected range
//  3.global work size must be divisible by local work size
//////////////////////////////////////////////////////////////////////////
__kernel void kernel_init_node_img( __write_only image2d_t desNodeImg,
                                    __global const int *buf_types,
                                    __global const int *buf_levels,
                                    __global const int *buf_pred_1st,
                                    __global const int *buf_succ_1st,
                                    const int shift,      // get img (x,y) by
                                    const unsigned mask,  //   shift and mask
                                    const unsigned base_node_id,
                                    const unsigned padding_bound )
{
   //uint work_dims = get_work_dim();     // number of dimensions in use
   //size_t gw_size = get_global_size(0); // global work size for dimen 0
   //size_t lw_size = get_local_size(0);  //  local work size for dimen 0
   size_t flatten_id = get_global_id(0);
   size_t multiplicand = get_global_size(0);
   uint work_dims = get_work_dim();   // number of dimensions in use
   for( uint dim = 1; dim < work_dims; dim++ ) {
      flatten_id += get_global_id( dim ) * multiplicand;
      multiplicand = multiplicand * get_global_size( dim );
   }
   size_t gid = base_node_id + flatten_id;
   //size_t gid_offset0 = get_global_offset(0); // global offset for dimen 0
   //size_t gid = get_global_id(0); // global id in dimen 0 with offset in mind


   if( gid > (size_t)padding_bound) return;
   // Compute gate index
   uint index = (uint)gid;

   // Group type, level, pred_1st, and succ_1st
   // into one pixel for node image of CL_SIGNED_INT32
   int4 pixel_node = (int4)( buf_types[index],
                             buf_levels[index],
                             buf_pred_1st[index],
                             buf_succ_1st[index] );

   // Assume image width is power of 2, '/' and '%' can be converted by
   //   compiler to shift and mask operations; otherwise, do it manually
   uint y = index >> shift;    // y: index / image_width
   uint x = index & mask;      // x: index % image_width

   write_imagei( desNodeImg, (int2)(x, y), pixel_node);
}


__kernel void kernel_init_conn_img_ushort( __write_only image2d_t desConnImg,
                                    __global const ushort *buf_preds_h16,
                                    __global const ushort *buf_preds_l16,
                                    __global const ushort *buf_succs_h16,
                                    __global const ushort *buf_succs_l16,
                                    const int shift,      // get img (x,y) by
                                    const unsigned mask,  //   shift and mask
                                    const unsigned base_node_id,
                                    const unsigned padding_bound )
{
   //uint work_dims = get_work_dim();     // number of dimensions in use
   //size_t gw_size = get_global_size(0); // global work size for dimen 0
   //size_t lw_size = get_local_size(0);  //  local work size for dimen 0
   size_t flatten_id = get_global_id(0);
   size_t multiplicand = get_global_size(0);
   uint work_dims = get_work_dim();   // number of dimensions in use
   for( uint dim = 1; dim < work_dims; dim++ ) {
      flatten_id += get_global_id( dim ) * multiplicand;
      multiplicand = multiplicand * get_global_size( dim );
   }
   size_t gid = base_node_id + flatten_id;
   //size_t gid_offset = get_global_offset(0); // global offset for dimen 0
   //size_t gid = get_global_id(0); // global id in dim 0 with offset in mind

   if( gid > (size_t)padding_bound) return;
   // Compute gate index
   uint index = (uint)gid;

   // Group preds, succs into one pixel for conn image of CL_UNSIGNED_INT16
   //
   uint ux = (uint)(buf_preds_h16[index]);        // upper 16-bit of preds[]
   uint uy = (uint)(buf_preds_l16[index]);        // lower 16-bit of preds[]
   uint uz = (uint)(buf_succs_h16[index]);        // upper 16-bit of succs[]
   uint uw = (uint)(buf_succs_l16[index]);        // lower 16-bit of succs[]
   uint4 pixel_conn = (uint4)(ux, uy, uz, uw);
   /*
   uint4 pixel_conn = (uint4)(buf_preds_h16[index], // upper 16-bit of preds[]
                              buf_preds_l16[index], // lower 16-bit of preds[]
                              buf_succs_h16[index], // upper 16-bit of succs[]
                              buf_succs_l16[index]);// lower 16-bit of succs[]
   */
   // Assume image width is power of 2, '/' and '%' can be converted by
   //   compiler to shift and mask operations; otherwise, do it manually
   uint y = index >> shift;    // y: index / image_width
   uint x = index & mask;      // x: index % image_width

   write_imageui( desConnImg, (int2)(x, y), pixel_conn);
}


__kernel void kernel_init_conn_img( __write_only image2d_t desConnImg,
                                    __global const int *buf_preds,
                                    __global const int *buf_succs,
                                    const int shift,      // get img (x,y) by
                                    const unsigned mask,  //   shift and mask
                                    const unsigned base_node_id,
                                    const unsigned padding_bound )
{
   //uint work_dims = get_work_dim();     // number of dimensions in use
   //size_t gw_size = get_global_size(0); // global work size for dimen 0
   //size_t lw_size = get_local_size(0);  //  local work size for dimen 0
   size_t flatten_id = get_global_id(0);
   size_t multiplicand = get_global_size(0);
   uint work_dims = get_work_dim();   // number of dimensions in use
   for( uint dim = 1; dim < work_dims; dim++ ) {
      flatten_id += get_global_id( dim ) * multiplicand;
      multiplicand = multiplicand * get_global_size( dim );
   }
   size_t gid = base_node_id + flatten_id;
   //size_t gid_offset = get_global_offset(0); // global offset for dimen 0
   //size_t gid = get_global_id(0); // global id in dim 0 with offset in mind

   if( gid > (size_t)padding_bound ) return;
   // Compute gate index
   uint index = (uint)(gid);

   // Group preds, succs into one pixel for conn image of CL_UNSIGNED_INT16
   uint ux = (uint)(buf_preds[index] >> 16);        // upper 16-bit of preds[]
   uint uy = (uint)(buf_preds[index] & 0x0000ffff); // lower 16-bit of preds[]
   uint uz = (uint)(buf_succs[index] >> 16);        // upper 16-bit of succs[]
   uint uw = (uint)(buf_succs[index] & 0x0000ffff); // lower 16-bit of succs[]
   uint4 pixel_conn = (uint4)(ux, uy, uz, uw);

   // Assume image width is power of 2, '/' and '%' can be converted by
   //   compiler to shift and mask operations; otherwise, do it manually
   uint y = index >> shift;    // y: index / image_width
   uint x = index & mask;      // x: index % image_width

   write_imageui( desConnImg, (int2)(x, y), pixel_conn);
}

/*
__kernel void kernel_init_ckt_image( __write_only image2d_t desNodeImg,
                                     __write_only image2d_t desConnImg,
                                     __global const int *buf_types,
                                     __global const int *buf_levels,
                                     __global const int *buf_pred_1st,
                                     __global const int *buf_succ_1st,
                                     __global const int *buf_preds,
                                     __global const int *buf_succs,
                                     const int shift,      // get img (x,y) by
                                     const unsigned mask ) //   shift and mask
{

   //__local uint masks[16] = { // maximum image width is 16k or 2^14
   //   0x0001, 0x0003, 0x0007, 0x000f,
   //   0x001f, 0x003f, 0x007f, 0x00ff,
   //   0x01ff, 0x03ff, 0x07ff, 0x0fff,
   //   0x1fff, 0x3fff, 0x7fff, 0xffff
   //};

   uint work_dims = get_work_dim();     // number of dimensions in use
   size_t gw_size = get_global_size(0); // global work size for dimen 0
   size_t lw_size = get_local_size(0);  //  local work size for dimen 0
   size_t gid_offset = get_global_offset(0); // global offset for dimen 0
   size_t gid = get_global_id(0);       // global id in dimension 0

   // Compute gate index
   uint index = (uint)(gid_offset + gid);

   // Group type, level, pred_1st, and succ_1st
   // into one pixel for node image of CL_SIGNED_INT32
   int4 pixel_node = (int4)( buf_types[index],
                             buf_levels[index],
                             buf_pred_1st[index],
                             buf_succ_1st[index] );

   // Group preds, succs into one pixel for conn image of CL_UNSIGNED_INT16
   uint ux = (uint)(buf_preds[index] >> 16);        // upper 16-bit of preds[]
   uint uy = (uint)(buf_preds[index] & 0x0000ffff); // lower 16-bit of preds[]
   uint uz = (uint)(buf_succs[index] >> 16);        // upper 16-bit of succs[]
   uint uw = (uint)(buf_succs[index] & 0x0000ffff); // lower 16-bit of succs[]
   uint4 pixel_conn = (ux, uy, uz, uw);

   // Assume image width is power of 2, '/' and '%' can be converted by
   //   compiler to shift and mask operations; otherwise, do it manually
   uint x = index >> shift;    // index / image_size.x;
   uint y = index & mask;      // index % image_size.x;

   write_imagei ( desNodeImg, (int2)(x, y), pixel_node);
   write_imageui( desConnImg, (int2)(x, y), pixel_conn);

}
*/

///////////////////////////////////////////////////////////////////////////////
//
//    Read circuit image to verify its correctness and completeness
//
///////////////////////////////////////////////////////////////////////////////

__kernel void kernel_read_ckt_image( __read_only image2d_t srcNodeImg,
                                     __read_only image2d_t srcConnImg,
                                     __global int *readback_types,
                                     __global int *readback_levels,
                                     __global int *readback_pred_1st,
                                     __global int *readback_succ_1st,
                                     __global int *readback_preds,
                                     __global int *readback_succs,
                                     const int shift,      // get img (x,y) by
                                     const unsigned mask,  //   shift and mask
                                     const unsigned base_node_id,
                                     const unsigned padding_bound )
                                     // sampler_t sampler )
{
   // Declare sampler to be used for image read
   sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |
                       CLK_ADDRESS_CLAMP_TO_EDGE |
                       CLK_FILTER_NEAREST;

   size_t flatten_id = get_global_id(0);
   size_t multiplicand = get_global_size(0);
   uint work_dims = get_work_dim();   // number of dimensions in use
   for( uint dim = 1; dim < work_dims; dim++ ) {
      flatten_id += get_global_id( dim ) * multiplicand;
      multiplicand = multiplicand * get_global_size( dim );
   }
   size_t gid = base_node_id + flatten_id;
   //size_t gid_offset = get_global_offset(0); // global offset for dimen 0
   //size_t gid = get_global_id(0); // global id in dimen 0 with offset in mind

   if( gid > (size_t)padding_bound) return;
   // Compute gate index
   uint index = (uint)gid;

   // Assume image_size.x is power of 2, '/' and '%' can be converted by
   //   compiler to shift and mask operations; otherwise, do it manually
   uint y = index >> shift;         // y: index / image_width
   uint x = index & mask;           // x: index % image_width

   int4 pixel_node = read_imagei( srcNodeImg, sampler, (int2)(x, y) );
   int type = pixel_node.x;
   int level = pixel_node.y;
   int pred_1st = pixel_node.z;
   int succ_1st = pixel_node.w;

   readback_types[index] = type;
   readback_levels[index] = level;
   readback_pred_1st[index] = pred_1st;
   readback_succ_1st[index] = succ_1st;

   // Obtain pred_1st and succ_1st of next gate by reading next node pixel
   uint next_i = index + 1;
   uint ny = next_i >> shift;
   uint nx = next_i & mask;
   int4 pixel_node_n = read_imagei( srcNodeImg, sampler, (int2)(nx, ny) );
   int pred_1st_n = pixel_node_n.z; // pred_1st of next gate
   int succ_1st_n = pixel_node_n.w; // succ_1st of next gate

   // access preds and succs of current gate
   for( int i=pred_1st; i<pred_1st_n; i++ ) {
      uint py = i >> shift;
      uint px = i & mask;
      uint4 pixel_conn = read_imageui( srcConnImg, sampler, (int2)(px, py) );
      int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
      // (int) upsample( pixel_conn.x, pixel_conn.y ); // from ushort to uint
      readback_preds[i] = pred_id;
   }
   //
   for( int i=succ_1st; i<succ_1st_n; i++ ) {
      uint sy = i >> shift;
      uint sx = i & mask;
      uint4 pixel_conn = read_imageui( srcConnImg, sampler, (int2)(sx, sy) );
      int succ_id =  (int)((pixel_conn.z << 16) + pixel_conn.w);
      // (int) upsample( pixel_conn.z, pixel_conn.w ); // from ushort to uint
      readback_succs[i] = succ_id;
   }
   //
}

///////////////////////////////////////////////////////////////////////////////
//
//    Evaluate gates in parallel
//
///////////////////////////////////////////////////////////////////////////////
__kernel void kernel_gsim_img(   __read_only image2d_t srcNodeImg,
                                 __read_only image2d_t srcConnImg,
                                 __global ulong *gmv0,
                                 __global ulong *gmv1,
                                 const int shift,      // get img (x,y) by
                                 const unsigned mask,  //   shift and mask
                                 const unsigned base_node_id,
                                 const unsigned padding_bound )
{
   // Declare sampler to be used for image read
   sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE |
                       CLK_ADDRESS_CLAMP_TO_EDGE |
                       CLK_FILTER_NEAREST;

   size_t flatten_id = get_global_id(0);
   size_t multiplicand = get_global_size(0);
   uint work_dims = get_work_dim();   // number of dimensions in use
   for( uint dim = 1; dim < work_dims; dim++ ) {
      flatten_id += get_global_id( dim ) * multiplicand;
      multiplicand = multiplicand * get_global_size( dim );
   }
   size_t gid = base_node_id + flatten_id;
   //size_t gid_offset = get_global_offset(0); // global offset for dimen 0
   //size_t gid = get_global_id(0); // global id in dim 0 with offset in mind
   if( gid > (size_t)padding_bound) return;

   // Compute gate index, node id 'nid'
   uint nid = (uint)gid;

   // Assume image_size.x is power of 2, '/' and '%' can be converted by
   //   compiler to shift and mask operations; otherwise, do it manually
   uint y = nid >> shift;         // y: index / image_width
   uint x = nid & mask;           // x: index % image_width

   int4 pixel_node = read_imagei( srcNodeImg, sampler, (int2)(x, y) );
   int type = pixel_node.x;
   int pred_1st = pixel_node.z;

   // Obtain pred_1st and succ_1st of next gate by reading next node pixel
   uint next_nid = nid + 1;
   uint ny = next_nid >> shift;
   uint nx = next_nid & mask;
   int4 pixel_node_n = read_imagei( srcNodeImg, sampler, (int2)(nx, ny) );
   int pred_1st_n = pixel_node_n.z; // pred_1st of next gate

   // access preds and succs of current gate
   /*
   for( int i=pred_1st; i<pred_1st_n; i++ ) {
      uint py = i >> shift;
      uint px = i & mask;
      uint4 pixel_conn = read_imageui( srcConnImg, sampler, (int2)(px, py) );
      int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
   }
   */
   ulong val0=0, val1=0;
   switch( type ) {
      case T_INPUT:
      {
         val0 = gmv0[nid];
         val1 = gmv1[nid];
         break;
      }
      case T_TIE1:  // (1,1)
      {
         val0 = ALL_ONES_64;
         val1 = ALL_ONES_64;
         break;
      }
      case T_TIE0:  // (0,0)
      {
         val0 = 0;
         val1 = 0;
         break;
      }
      case T_TIEX:  // (1,0)
      {
         val0 = ALL_ONES_64;
         val1 = 0;
         break;
      }
      case T_TIEZ:  // (0,1)
      {
         val0 = 0;
         val1 = ALL_ONES_64;
         break;
      }
      case T_OUTPUT:
      {
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 = gmv0[pred_id];
            val1 = gmv1[pred_id];
         }
         break;
      }
      case T_DFF:
      {
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 = gmv0[pred_id];
            val1 = gmv1[pred_id];
         }
         break;
      }
      case T_BUF:
      {
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 = gmv0[pred_id];
            val1 = gmv1[pred_id];
         }
         //val0 = gmv0( predC0(id)[0] );
         //val1 = gmv1( predC0(id)[0] );
         break;
      }
      case T_AND:
      {
         val1 = val0 = ALL_ONES_64;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 &= gmv0[pred_id];
            val1 &= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 &= gmv0( predC0(id)[i] );
         //   val1 &= gmv1( predC0(id)[i] );
         //}
         break;
      }
      case T_NAND:
      {
         val1 = val0 = ALL_ONES_64;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 &= gmv0[pred_id];
            val1 &= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 &= gmv0( predC0(id)[i] );
         //   val1 &= gmv1( predC0(id)[i] );
         //}
         ulong v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      case T_OR:
      {
         val1 = val0 = 0;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 |= gmv0[pred_id];
            val1 |= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 |= gmv0( predC0(id)[i] );
         //   val1 |= gmv1( predC0(id)[i] );
         //}
         break;
      }
      case T_NOR:
      {
         val1 = val0 = 0;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 |= gmv0[pred_id];
            val1 |= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 |= gmv0( predC0(id)[i] );
         //   val1 |= gmv1( predC0(id)[i] );
         //}
         ulong v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      case T_NOT:
      {
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 = gmv1[pred_id] ^ ALL_ONES_64;
            val1 = gmv0[pred_id] ^ ALL_ONES_64;
         }
         //val0 = gmv1( predC0(id)[0] ) ^ ALL_ONES_64;
         //val1 = gmv0( predC0(id)[0] ) ^ ALL_ONES_64;
         break;
      }
      case T_XOR:
      {
         // for 1st input on pred_1st
         for( int i=pred_1st; i < pred_1st + 1; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 = gmv1[pred_id];
            val1 = gmv0[pred_id];
         }
         // for inputs from pred_1st + 1
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            ulong v1 = ALL_ONES_64 ^
               (((ALL_ONES_64^gmv0[pred_id]) & (ALL_ONES_64 ^ val0)) |
                (gmv1[pred_id] & val1));
            val1 = ((ALL_ONES_64 ^ gmv0[pred_id]) & val1) |
               (gmv1[pred_id] & (ALL_ONES_64 ^ val0));
            val0 = v1;
         }
         //val0 = gmv0( predC0(id)[0] );
         //val1 = gmv1( predC0(id)[0] );
         //for( int i = 1; i < numPred(id); i++) {
         //   v1 = ALL_ONES_64 ^
         //      (((ALL_ONES_64^gmv0( predC0(id)[i] )) & (ALL_ONES_64^val0))|
         //         (gmv1( predC0(id)[i] ) & val1));
         //   val1 = ((ALL_ONES_64 ^ gmv0( predC0(id)[i] )) & val1) |
         //           (gmv1( predC0(id)[i] ) & (ALL_ONES_64 ^ val0));
         //   val0 = v1;
         //}
         break;
      }
      case T_XNOR:
      {
         // for 1st input on pred_1st
         for( int i=pred_1st; i < pred_1st + 1; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            val0 = gmv1[pred_id];
            val1 = gmv0[pred_id];
         }
         // for inputs from pred_1st + 1
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {
            uint py = i >> shift;
            uint px = i & mask;
            uint4 pixel_conn =
               read_imageui( srcConnImg, sampler, (int2)(px, py) );
            int pred_id = (int)((pixel_conn.x << 16) + pixel_conn.y);
            ulong v1 = ALL_ONES_64 ^
               (((ALL_ONES_64^gmv0[pred_id]) & (ALL_ONES_64 ^ val0)) |
                (gmv1[pred_id] & val1));
            val1 = ((ALL_ONES_64 ^ gmv0[pred_id]) & val1) |
               (gmv1[pred_id] & (ALL_ONES_64 ^ val0));
            val0 = v1;
         }
         //val0 = gmv0( predC0(id)[0] );
         //val1 = gmv1( predC0(id)[0] );
         //for( int i = 1; i < numPred(id); i++) {
         //   v1 = ALL_ONES_64 ^
         //    (((ALL_ONES_64 ^ gmv0(predC0(id)[i])) & (ALL_ONES_64^val0)) |
         //    (gmv1( predC0(id)[i] ) & val1));
         //   val1 = ((ALL_ONES_64^gmv0( predC0(id)[i] )) & val1) |
         //           (gmv1( predC0(id)[i] ) & (ALL_ONES_64 ^ val0));
         //   val0 = v1;
         //}
         ulong v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      default:
      {
         //HLog << InfoError
         //     << "gate type " <<  logic(id)
         //     << " is not supported in 4-value logic simulation";
         break;
      }
   }
   // update gate value (gmv0, gmv1)
   gmv0[nid] = val0;
   gmv1[nid] = val1;
}


__kernel void kernel_gsim_buf( __global const int *buf_types,
                               __global const int *buf_levels,
                               __global const int *buf_pred_1st,
                               __global const int *buf_succ_1st,
                               __global const int *buf_preds,
                               __global const int *buf_succs,
                               const unsigned first_ff_node,
                               const int clocked_dff_is_used,
                               __global ulong *gmv0,
                               __global ulong *gmv1,
                               const unsigned base_node_id,
                               const unsigned padding_bound )
{
   GSIM_BUF_TEMPLATE( ulong, OCL_GATE_EVAL,
                      buf_types,buf_levels,buf_pred_1st, buf_succ_1st,
                      buf_preds,buf_succs,first_ff_node, clocked_dff_is_used,
                      gmv0, gmv1, base_node_id, padding_bound );
   /*
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   ulong val0=0, val1=0;
   if( clocked_dff_is_used && gid >= first_ff_node ) {
      unsigned ui = gid - first_ff_node;
      unsigned gid_ff = first_ff_node + (ui << 2) + ui;
      for( int i=0; i < 5; i++, gid_ff++ ) { // simulate 5 gates per remodel
         OCL_GATE_EVAL( gid_ff, ulong, buf_pred_1st, buf_preds, buf_types,
                        gmv0, gmv1, val0, val1 );
         gmv0[gid_ff] = val0;
         gmv1[gid_ff] = val1;
      }
   } else {
      OCL_GATE_EVAL( gid, ulong, buf_pred_1st, buf_preds, buf_types,
                     gmv0, gmv1, val0, val1 );
      // update gate value (gmv0, gmv1)
      gmv0[gid] = val0;
      gmv1[gid] = val1;
   }
   */
}


/////////////////////////////////////////////////////////////////////
// simulate in vectors to make use of OpenCL CPU vector capability
/////////////////////////////////////////////////////////////////////
__kernel void kernel_gsim_buf_ulong4( __global const int *buf_types,
                                   __global const int *buf_levels,
                                   __global const int *buf_pred_1st,
                                   __global const int *buf_succ_1st,
                                   __global const int *buf_preds,
                                   __global const int *buf_succs,
                                   const unsigned first_ff_node,
                                   const int clocked_dff_is_used,
                                   __global ulong4 *gmv0,
                                   __global ulong4 *gmv1,
                                   const unsigned base_node_id,
                                   const unsigned padding_bound )
{
   GSIM_BUF_TEMPLATE( ulong4, OCL_GATE_EVAL_VEC,
                      buf_types,buf_levels,buf_pred_1st,buf_succ_1st,
                      buf_preds,buf_succs,first_ff_node,clocked_dff_is_used,
                      gmv0, gmv1, base_node_id, padding_bound );
   /*
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;
   // val0 = 0; IS equivalent to { val0.w=0; val0.x=0; val0.y=0; val0.z=0;}
   ulong4 val0=0, val1=0;
   if( clocked_dff_is_used && gid >= first_ff_node ) {
      unsigned ui = gid - first_ff_node;
      unsigned gid_ff = first_ff_node + (ui << 2) + ui;
      for( int i=0; i < 5; i++, gid_ff++ ) { // simulate 5 gates per remodel
         OCL_GATE_EVAL( gid_ff, ulong4, buf_pred_1st, buf_preds, buf_types,
                        gmv0, gmv1, val0, val1 );
         gmv0[gid_ff] = val0;
         gmv1[gid_ff] = val1;
      }
   } else {
      OCL_GATE_EVAL( gid, ulong4, buf_pred_1st, buf_preds, buf_types,
                     gmv0, gmv1, val0, val1 );
      // update gate value (gmv0, gmv1)
      gmv0[gid] = val0;
      gmv1[gid] = val1;
   }
   */
}


//////////////////////////////////////////////////////////////////////////
//         Event-driven good simulation kernel with ulong
//////////////////////////////////////////////////////////////////////////
__kernel void kernel_gsim_buf_event(
                 __global const int *buf_types,
                 __global const int *buf_levels,
                 __global const int *buf_pred_1st,
                 __global const int *buf_succ_1st,
                 __global const int *buf_preds,
                 __global const int *buf_succs,
                 __global const int *buf_q_offsets,
                 __global       int *buf_q_sizes,
                 __global  unsigned *buf_q_e_flags,
                 __global ulong *gmv0,
                 __global ulong *gmv1,
                 const int num_levels,
                 const unsigned first_ff_node,
                 const int clocked_dff_is_used,
                 const int curr_lev,
                 const unsigned base_node_id,
                 const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   int index = buf_q_offsets[ curr_lev ] + gid; // no race here
   /*unsigned rid = buf_q_e_flags[ index ] & 0x7FFFFFFF;*/ // real gate id
   unsigned rid = atomic_or( &(buf_q_e_flags[index]), 0 );
   rid = rid & 0x7FFFFFFF;

   atomic_dec( &(buf_q_sizes[curr_lev]) ); // decrement queue size of curr_lev

   /* buf_q_e_flags[rid] &= 0x7FFFFFFF; */ // clear in-queue flag of 'rid'
   atomic_and( &(buf_q_e_flags[rid]), 0x7FFFFFFF );

   ulong val0=0, val1=0;
   ulong old_val0=0, old_val1=0;
   if( clocked_dff_is_used && rid >= first_ff_node ) {
      ulong new_val0=0, new_val1=0;
      unsigned rid_smux = rid + 2;
      old_val0 = gmv0[ rid_smux ]; // gmv of slave MUX
      old_val1 = gmv1[ rid_smux ];
      unsigned rid_ff = rid;
      for( int i=0; i < 5; i++, rid_ff++ ) { // simulate 5 gates per remodel
         OCL_GATE_EVAL( rid_ff, ulong, buf_pred_1st, buf_preds, buf_types,
                        gmv0, gmv1, val0, val1 );
         gmv0[ rid_ff ] = val0;
         gmv1[ rid_ff ] = val1;
         if( 2==i ) {
            new_val0 = val0;
            new_val1 = val1;
         }
      }
      // Gmv of D flip flop changed
      if( gmv_changed( old_val0, old_val1, new_val0, new_val1 )) {
         ADD_SUCCESSOR_EVENTS( rid_smux, buf_levels, buf_succ_1st,
            buf_succs, buf_q_e_flags, buf_q_offsets, buf_q_sizes,
            num_levels, first_ff_node, clocked_dff_is_used, curr_lev );
      }
   } else {
      OCL_GATE_EVAL( rid, ulong, buf_pred_1st, buf_preds, buf_types,
                     gmv0, gmv1, val0, val1 );
      // update gate value (gmv0, gmv1)
      old_val0 = gmv0[ rid ];
      old_val1 = gmv1[ rid ];
      gmv0[ rid ] = val0;
      gmv1[ rid ] = val1;
      if( gmv_changed( old_val0, old_val1, val0, val1 )) {
         ADD_SUCCESSOR_EVENTS( rid, buf_levels, buf_succ_1st,
            buf_succs, buf_q_e_flags, buf_q_offsets, buf_q_sizes,
            num_levels, first_ff_node, clocked_dff_is_used, curr_lev );
      }
   }
}


//////////////////////////////////////////////////////////////////////////
//        Flag-based event-driven good simulation kernel with ulong
//////////////////////////////////////////////////////////////////////////
__kernel void kernel_gsim_buf_event_flag(
                 __global const int *buf_types,
                 __global const int *buf_levels,
                 __global const int *buf_pred_1st,
                 __global const int *buf_succ_1st,
                 __global const int *buf_preds,
                 __global const int *buf_succs,
                 __global       int *buf_q_sizes,
                 __global      char *buf_flags,
                 __global ulong *gmv0,
                 __global ulong *gmv1,
                 const int num_levels,
                 const unsigned first_ff_node,
                 const int clocked_dff_is_used,
                 const int curr_lev,
                 const unsigned base_node_id,
                 const unsigned padding_bound )
{
   if( 0==buf_q_sizes[curr_lev] ) {
      return; // exit if no events in current level
   }
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) {
      return; // gid is over upperbound
   }
   if( 0==buf_flags[gid] ) {
      return; // node gid is not in queue
   }

   buf_flags[gid] = 0; // clear in-queue flag of 'gid'
                       // clear buf_q_sizes when all events in curr_lev
                       //   are evaluated
   ulong val0=0, val1=0;
   ulong old_val0=0, old_val1=0;
   if( clocked_dff_is_used && gid >= first_ff_node ) {
      ulong new_val0=0, new_val1=0;
      unsigned gid_smux = gid + 2;
      old_val0 = gmv0[ gid_smux ]; // gmv of slave MUX
      old_val1 = gmv1[ gid_smux ];
      unsigned gid_ff = gid;
      for( int i=0; i < 5; i++, gid_ff++ ) { // simulate 5 gates per remodel
         OCL_GATE_EVAL( gid_ff, ulong, buf_pred_1st, buf_preds, buf_types,
                        gmv0, gmv1, val0, val1 );
         gmv0[ gid_ff ] = val0;
         gmv1[ gid_ff ] = val1;
         if( 2==i ) {
            new_val0 = val0;
            new_val1 = val1;
         }
      }
      // Gmv of D flip flop changed
      if( gmv_changed( old_val0, old_val1, new_val0, new_val1 )) {
         ADD_SUCCESSOR_EVENTS_FLAG( gid_smux, buf_levels, buf_succ_1st,
            buf_succs, buf_flags, buf_q_sizes, first_ff_node,
            clocked_dff_is_used, curr_lev );
      }
   } else {
      OCL_GATE_EVAL( gid, ulong, buf_pred_1st, buf_preds, buf_types,
                     gmv0, gmv1, val0, val1 );
      // update gate value (gmv0, gmv1)
      old_val0 = gmv0[ gid ];
      old_val1 = gmv1[ gid ];
      gmv0[ gid ] = val0;
      gmv1[ gid ] = val1;
      if( gmv_changed( old_val0, old_val1, val0, val1 )) {
         ADD_SUCCESSOR_EVENTS_FLAG( gid, buf_levels, buf_succ_1st,
            buf_succs, buf_flags, buf_q_sizes, first_ff_node,
            clocked_dff_is_used, curr_lev );
      }
   }
}


// Good sim kernel with non divergent code path
#define NUM_GATE_TYPES  12
// side input offset for multi-input gates
__constant int tableOfSideInOffset[4][3] = { {0, 0, 0},  // 1-input gate
                                  {1, 0, 0},  // 2-input gate
                                  {1, 2, 0},  // 3-input gate
                                  {1, 2, 3}   // 4-input gate
                                };

// If offset is 0, use 0xff as pseudo input for AND, NAND
//                 use 0x00 as pseudo input for OR, NOR, XOR, XNOR
__constant int tablePseudoIn[NUM_GATE_TYPES][4] = {
   {-1, -1, -1, -1}, // JUNK,           /* 0 */
   { 0,  0,  0,  0}, // T_INPUT,        /* 1 */
   { 0,  0,  0,  0}, // T_OUTPUT,       /* 2 */
   { 0,  2,  2,  2}, // T_XOR,          /* 3 */
   { 0,  2,  2,  2}, // T_XNOR,         /* 4 */
   { 0,  0,  0,  0}, // T_DFF,          /* 5 */
   { 1,  2,  2,  2}, // T_AND,          /* 6 */
   { 1,  2,  2,  2}, // T_NAND,         /* 7 */
   { 0,  2,  2,  2}, // T_OR,           /* 8 */
   { 0,  2,  2,  2}, // T_NOR,          /* 9 */
   { 0,  0,  0,  0}, // T_NOT,          /* 10 */
   { 0,  0,  0,  0}  // T_BUF,          /* 11 */
};
// which entry in array input_values[] to retrieve for AND operation
__constant int tableAnd[NUM_GATE_TYPES] = {
   -1, // JUNK,           /* 0 */
   1,  // T_INPUT,        /* 1 */
   1,  // T_OUTPUT,       /* 2 */
   1,  // T_XOR,          /* 3 */
   1,  // T_XNOR,         /* 4 */
   1,  // T_DFF,          /* 5 */
   2,  // T_AND,          /* 6 */
   2,  // T_NAND,         /* 7 */
   1,  // T_OR,           /* 8 */
   1,  // T_NOR,          /* 9 */
   1,  // T_NOT,          /* 10 */
   1   // T_BUF,          /* 11 */
};
// which entry in array input_values[] to retrieve for OR operation
__constant int tableOr[NUM_GATE_TYPES] = {
   -1, // JUNK,           /* 0 */
   0,  // T_INPUT,        /* 1 */
   0,  // T_OUTPUT,       /* 2 */
   0,  // T_XOR,          /* 3 */
   0,  // T_XNOR,         /* 4 */
   0,  // T_DFF,          /* 5 */
   0,  // T_AND,          /* 6 */
   0,  // T_NAND,         /* 7 */
   2,  // T_OR,           /* 8 */
   2,  // T_NOR,          /* 9 */
   0,  // T_NOT,          /* 10 */
   0   // T_BUF,          /* 11 */
};
// which entry in array input_values[] to retrieve for XOR operation
__constant int tableXor[NUM_GATE_TYPES] = {
   -1, // JUNK,           /* 0 */
   0,  // T_INPUT,        /* 1 */
   0,  // T_OUTPUT,       /* 2 */
   2,  // T_XOR,          /* 3 */
   2,  // T_XNOR,         /* 4 */
   0,  // T_DFF,          /* 5 */
   0,  // T_AND,          /* 6 */
   0,  // T_NAND,         /* 7 */
   0,  // T_OR,           /* 8 */
   0,  // T_NOR,          /* 9 */
   0,  // T_NOT,          /* 10 */
   0   // T_BUF,          /* 11 */
};
// which entry in array input_values[] to retrieve for NOT operation
__constant int tableInv[NUM_GATE_TYPES] = {
   -1, // JUNK,           /* 0 */
   0,  // T_INPUT,        /* 1 */
   0,  // T_OUTPUT,       /* 2 */
   0,  // T_XOR,          /* 3 */
   1,  // T_XNOR,         /* 4 */
   0,  // T_DFF,          /* 5 */
   0,  // T_AND,          /* 6 */
   1,  // T_NAND,         /* 7 */
   0,  // T_OR,           /* 8 */
   1,  // T_NOR,          /* 9 */
   1,  // T_NOT,          /* 10 */
   0   // T_BUF,          /* 11 */
};

__kernel void kernel_gsim_buf_non_divergent(
                                   __global const int *buf_types,
                                   __global const int *buf_levels,
                                   __global const int *buf_pred_1st,
                                   __global const int *buf_succ_1st,
                                   __global const int *buf_preds,
                                   __global const int *buf_succs,
                                   const unsigned first_ff_node,
                                   const int clocked_dff_is_used,
                                   __global ulong *gmv0,
                                   __global ulong *gmv1,
                                   const unsigned base_node_id,
                                   const unsigned padding_bound,
                                   const int max_inputs,
                                   const int and_is_on,
                                   const int or_is_on,
                                   const int xor_is_on,
                                   const int inv_is_on )
{
   size_t flatten_id = get_global_id(0);
   size_t multiplicand = get_global_size(0);
   uint work_dims = get_work_dim();   // number of dimensions in use
   for( int dim = 1; dim < work_dims; dim++ ) {
      flatten_id += get_global_id( dim ) * multiplicand;
      multiplicand = multiplicand * get_global_size( dim );
   }
   size_t gid = base_node_id + flatten_id;
   //size_t gid_offset = get_global_offset(0); // global offset for dimen 0
   //size_t gid = get_global_id(0); // global id in dim 0 with offset in mind
   if( gid > (size_t)padding_bound) return;

   int pred_1st = buf_pred_1st[gid];
   int pred_1st_n = buf_pred_1st[gid+1]; // pred_1st of next gate
   int gType = buf_types[gid];

   ulong val0=0, val1=0;
   int numInputs = pred_1st_n - pred_1st;

   int gidIn1 = buf_preds[ pred_1st ]; // 1st input gid

   val0 = gmv0[ gidIn1 ];
   val1 = gmv1[ gidIn1 ];

   ulong input_values[3][2] = {{ALL_ZEROS, ALL_ZEROS},
                               {ALL_ONES_64, ALL_ONES_64},
                               {ALL_ZEROS, ALL_ZEROS}
                              };

   //
   // iterate through the other 3 side inputs
   ulong in0 = 0;
   ulong in1 = 0;
   ulong v0 = 0;
   //for (int s=0; s<3; s++ ) {
   for (int s=0; s < (max_inputs-1); s++ ) {
      int offset = tableOfSideInOffset[ numInputs-1 ][ s ];
      int gidSide = buf_preds[ pred_1st + offset ];
      ulong val0s = gmv0[ gidSide ];
      ulong val1s = gmv1[ gidSide ];

      // update input_values[2] to the right value;
      // when offset==0, 0xFF for AND/NAND, 0x00 for OR/NOR/XOR/XNOR
      input_values[2][0] = val0s;
      input_values[2][1] = val1s;

      input_values[2][0] = input_values[ tablePseudoIn[gType][offset]][0];
      input_values[2][1] = input_values[ tablePseudoIn[gType][offset]][1];

      // AND
      if( and_is_on ) {
         in0 = input_values[ tableAnd[ gType ]][0];
         in1 = input_values[ tableAnd[ gType ]][1];
         val0 &= in0;
         val1 &= in1;
      }
      // OR
      if( or_is_on ) {
         in0 = input_values[ tableOr[ gType ]][0];
         in1 = input_values[ tableOr[ gType ]][1];
         val0 |= in0;
         val1 |= in1;
      }
      // XOR
      // for XOR gate, draw K-map on 0-1-X-Z with Z row/column as don't cares
      //  out0 =  (in0' * val0' +  in1 & val1)'
      //  out1 =   in0' * val1  +  in1 & val0'
      if( xor_is_on ) {
         in0 = input_values[ tableXor[ gType ]][0];
         in1 = input_values[ tableXor[ gType ]][1];
         v0 = ~ (((~in0) & (~val0)) | ( in1 & val1));
         val1 = ((~in0) & val1) | (in1 & (~val0));
         val0 = v0;
      }
   }
   // bitwise NOT; what if we have X, ??? This is a problem ???
   //   solved by storing both and retrieve it later
   // save non-inverted (val0, val1) in input_values[0] first
   if( inv_is_on ) {
      input_values[0][0] = val0;
      input_values[0][1] = val1;
      // computed the Inverted value
      v0 = val0;
      val0 = ~val1;
      val1 =  ~v0;
      // save the inverted (val0, val1) value in input_values[1]
      input_values[1][0] = val0;
      input_values[1][1] = val1;
      // retrieve the right (val0, val1)
      val0 = input_values[ tableInv[ gType ]][0];
      val1 = input_values[ tableInv[ gType ]][1];
   }
   // update gate value (gmv0, gmv1)
   gmv0[gid] = val0;
   gmv1[gid] = val1;
}


///////////////////////////////////////////////////////////////////////
//  Initialize x background and queue status
///////////////////////////////////////////////////////////////////////
__kernel void kernel_init_x_background( __global ulong *gmv0,
                                        __global ulong *gmv1,
                                        const unsigned base_node_id, //--
                                        const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;
   gmv0[ gid ] = ALL_ONES_64;
   gmv1[ gid ] = 0;
}


///////////////////////////////////////////////////////////////////////
//  Initialize TIE gates, this is one time job per design
///////////////////////////////////////////////////////////////////////
__kernel void kernel_init_tie_gates(  __global const int *buf_types,
                                      __global const int *buf_levels,
                                      __global const int *buf_pred_1st,
                                      __global const int *buf_succ_1st,
                                      __global const int *buf_preds,
                                      __global const int *buf_succs,
                                      __global const int *buf_q_offsets,
                                      __global       int *buf_q_sizes,
                                      __global  unsigned *buf_q_e_flags,
                                      __global ulong *gmv0,
                                      __global ulong *gmv1,
                                      const int num_levels,
                                      const unsigned first_ff_node,
                                      const int clocked_dff_is_used,
                                      const int is_event_driven,
                                      const unsigned base_node_id, //--
                                      const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   ulong val0=0, val1=0;
   OCL_GATE_EVAL( gid, ulong, buf_pred_1st, buf_preds, buf_types,
                  gmv0, gmv1, val0, val1 );

   int gmv_need_update = 1;
   if( is_event_driven ) {
      ulong old_gmv0 = gmv0[ gid ];
      ulong old_gmv1 = gmv1[ gid ];
      gmv_need_update = gmv_changed( old_gmv0, old_gmv1, val0, val1);
      if( gmv_need_update ) {
         int curr_lev = 0;
         ADD_SUCCESSOR_EVENTS( gid, buf_levels, buf_succ_1st,
            buf_succs, buf_q_e_flags, buf_q_offsets, buf_q_sizes,
            num_levels, first_ff_node, clocked_dff_is_used, curr_lev );
      }
   }
   if( gmv_need_update ) { // avoid unnecessary mem-write
      gmv0[ gid ] = val0;
      gmv1[ gid ] = val1;
   }
}


__kernel void kernel_init_tie_gates_flag( __global const int *buf_types,
                                          __global const int *buf_levels,
                                          __global const int *buf_pred_1st,
                                          __global const int *buf_succ_1st,
                                          __global const int *buf_preds,
                                          __global const int *buf_succs,
                                          __global       int *buf_q_sizes,
                                          __global      char *buf_flags,
                                          __global ulong *gmv0,
                                          __global ulong *gmv1,
                                          const unsigned first_ff_node,
                                          const int clocked_dff_is_used,
                                          const int is_event_driven,
                                          const unsigned base_node_id, //--
                                          const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   ulong val0=0, val1=0;
   OCL_GATE_EVAL( gid, ulong, buf_pred_1st, buf_preds, buf_types,
                  gmv0, gmv1, val0, val1 );

   int gmv_need_update = 1;
   if( is_event_driven ) {
      ulong old_gmv0 = gmv0[ gid ];
      ulong old_gmv1 = gmv1[ gid ];
      gmv_need_update = gmv_changed( old_gmv0, old_gmv1, val0, val1);
      if( gmv_need_update ) {
         int curr_lev = 0;
         ADD_SUCCESSOR_EVENTS_FLAG( gid, buf_levels, buf_succ_1st,
            buf_succs, buf_flags, buf_q_sizes, first_ff_node,
            clocked_dff_is_used, curr_lev );
      }
   }
   if( gmv_need_update ) { // avoid unnecessary mem-write
      gmv0[ gid ] = val0;
      gmv1[ gid ] = val1;
   }
}


////////////////////////////////////////////////////////////////////////
//   Assign input stimulus to gmv per cycle
//   when cycle==0,
//     assign both force_pi and init_ff, and padding_bound is the sum
//     of numPIs() and numFFs()
//   when cycle > 0,
//     assign only force_pi if io_per_cycle is true, and padding_bound
//     is numPIs()
////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////
//   This is for simplified design without clock pin, outdated,
__kernel void kernel_assign_stimulus( __global ulong *gmv0,
                                      __global ulong *gmv1,
                                      __global const ulong *pat_in_gmv0,
                                      __global const ulong *pat_in_gmv1,
                                      const unsigned first_nid, // pi or ff
                                      const unsigned pat_in_offset,
                                      const unsigned base_node_id,
                                      const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   size_t gid_pat_in = (size_t)pat_in_offset + gid - first_nid;
   gmv0[ gid ] = pat_in_gmv0[ gid_pat_in ];
   gmv1[ gid ] = pat_in_gmv1[ gid_pat_in ];
}


__kernel void kernel_assign_data_pi(  __global const int *buf_types,
                                      __global const int *buf_levels,
                                      __global const int *buf_pred_1st,
                                      __global const int *buf_succ_1st,
                                      __global const int *buf_preds,
                                      __global const int *buf_succs,
                                      __global const int *buf_q_offsets,
                                      __global       int *buf_q_sizes,
                                      __global  unsigned *buf_q_e_flags,
                                      __global const int *data_input_nodes,
                                      __global ulong *gmv0,
                                      __global ulong *gmv1,
                                      const int num_levels,
                                      const unsigned first_ff_node,
                                      const int clocked_dff_is_used,
                                      const int is_event_driven, //--
                                      __global const ulong *pat_in_gmv0,
                                      __global const ulong *pat_in_gmv1,
                                      const unsigned pat_in_offset,
                                      const unsigned base_node_id,
                                      const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   unsigned gid_pat_in = pat_in_offset + gid;
   int input_gid = data_input_nodes[ gid ];
   //printf("***forth*****  gid %u, input_gid:%d\n", gid, input_gid);
   ulong new_gmv0 = pat_in_gmv0[ gid_pat_in ];
   ulong new_gmv1 = pat_in_gmv1[ gid_pat_in ];
   int gmv_need_update = 1;
   if( is_event_driven ) {
      ulong old_gmv0 = gmv0[ input_gid ];
      ulong old_gmv1 = gmv1[ input_gid ];
      gmv_need_update = gmv_changed( old_gmv0, old_gmv1, new_gmv0, new_gmv1);
      //printf("*******  gid %u, gmv_need_update %d\n", gid, gmv_need_update);
      if( gmv_need_update ) {
         int curr_lev = 0;
         ADD_SUCCESSOR_EVENTS( input_gid, buf_levels, buf_succ_1st,
            buf_succs, buf_q_e_flags, buf_q_offsets, buf_q_sizes,
            num_levels, first_ff_node, clocked_dff_is_used, curr_lev );
      }
   }
   if( gmv_need_update ) { // avoid unnecessary mem-write
      gmv0[ input_gid ] = new_gmv0;
      gmv1[ input_gid ] = new_gmv1;
   }
}


__kernel void kernel_assign_data_pi_flag( __global const int *buf_types,
                                          __global const int *buf_levels,
                                          __global const int *buf_pred_1st,
                                          __global const int *buf_succ_1st,
                                          __global const int *buf_preds,
                                          __global const int *buf_succs,
                                          __global       int *buf_q_sizes,
                                          __global      char *buf_flags,
                                          __global const int *data_input_nodes,
                                          __global ulong *gmv0,
                                          __global ulong *gmv1,
                                          const unsigned first_ff_node,
                                          const int clocked_dff_is_used,
                                          const int is_event_driven, //--
                                          __global const ulong *pat_in_gmv0,
                                          __global const ulong *pat_in_gmv1,
                                          const unsigned pat_in_offset,
                                          const unsigned base_node_id,
                                          const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   unsigned gid_pat_in = pat_in_offset + gid;
   int input_gid = data_input_nodes[ gid ];
   ulong new_gmv0 = pat_in_gmv0[ gid_pat_in ];
   ulong new_gmv1 = pat_in_gmv1[ gid_pat_in ];
   int gmv_need_update = 1;
   if( is_event_driven ) {
      ulong old_gmv0 = gmv0[ input_gid ];
      ulong old_gmv1 = gmv1[ input_gid ];
      gmv_need_update = gmv_changed( old_gmv0, old_gmv1, new_gmv0, new_gmv1);
      if( gmv_need_update ) {
         int curr_lev = 0;
         ADD_SUCCESSOR_EVENTS_FLAG( input_gid, buf_levels, buf_succ_1st,
            buf_succs, buf_flags, buf_q_sizes, first_ff_node,
            clocked_dff_is_used, curr_lev );
      }
   }
   if( gmv_need_update ) { // avoid unnecessary mem-write
      gmv0[ input_gid ] = new_gmv0;
      gmv1[ input_gid ] = new_gmv1;
   }
}


__kernel void kernel_assign_clock_pi( __global const int *buf_types,
                                      __global const int *buf_levels,
                                      __global const int *buf_pred_1st,
                                      __global const int *buf_succ_1st,
                                      __global const int *buf_preds,
                                      __global const int *buf_succs,
                                      __global const int *buf_q_offsets,
                                      __global       int *buf_q_sizes,
                                      __global  unsigned *buf_q_e_flags,
                                      __global const int *clk_inputs_offstates,
                                      __global ulong *gmv0,
                                      __global ulong *gmv1,
                                      const int num_levels,
                                      const unsigned first_ff_node,
                                      const int clocked_dff_is_used,
                                      const int is_event_driven, //--
                                      const int clock_is_on,
                                      const unsigned base_node_id,
                                      const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;
   unsigned ui = gid << 1;  // equivalent to (gid x 2)
   int clk_gid = clk_inputs_offstates[ ui ];
   int off_state = clk_inputs_offstates[ ui + 1 ];
   ulong val = 0;
   if((1==clock_is_on && 0==off_state) || (0==clock_is_on && 1==off_state)) {
      val = ALL_ONES_64;
   }
   int gmv_need_update = 1;
   if( is_event_driven ) {
      ulong old_gmv0 = gmv0[ clk_gid ];
      ulong old_gmv1 = gmv1[ clk_gid ];
      gmv_need_update = gmv_changed( old_gmv0, old_gmv1, val, val);
      if( gmv_need_update ) {
         int curr_lev = 0;
         ADD_SUCCESSOR_EVENTS( clk_gid, buf_levels, buf_succ_1st,
            buf_succs, buf_q_e_flags, buf_q_offsets, buf_q_sizes,
            num_levels, first_ff_node, clocked_dff_is_used, curr_lev );
      }
   }
   if( gmv_need_update ) { // avoid unnecessary mem-write
      gmv0[ clk_gid ] = val;
      gmv1[ clk_gid ] = val;
   }
}


__kernel void kernel_assign_cpi_flag(__global const int *buf_types,
                                     __global const int *buf_levels,
                                     __global const int *buf_pred_1st,
                                     __global const int *buf_succ_1st,
                                     __global const int *buf_preds,
                                     __global const int *buf_succs,
                                     __global       int *buf_q_sizes,
                                     __global      char *buf_flags,
                                     __global const int *clk_inputs_offstates,
                                     __global ulong *gmv0,
                                     __global ulong *gmv1,
                                     const unsigned first_ff_node,
                                     const int clocked_dff_is_used,
                                     const int is_event_driven, //--
                                     const int clock_is_on,
                                     const unsigned base_node_id,
                                     const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;
   unsigned ui = gid << 1;  // equivalent to (gid x 2)
   int clk_gid = clk_inputs_offstates[ ui ];
   int off_state = clk_inputs_offstates[ ui + 1 ];
   ulong val = 0;
   if((1==clock_is_on && 0==off_state) || (0==clock_is_on && 1==off_state)) {
      val = ALL_ONES_64;
   }
   int gmv_need_update = 1;
   if( is_event_driven ) {
      ulong old_gmv0 = gmv0[ clk_gid ];
      ulong old_gmv1 = gmv1[ clk_gid ];
      gmv_need_update = gmv_changed( old_gmv0, old_gmv1, val, val);
      if( gmv_need_update ) {
         int curr_lev = 0;
         ADD_SUCCESSOR_EVENTS_FLAG( clk_gid, buf_levels, buf_succ_1st,
            buf_succs, buf_flags, buf_q_sizes, first_ff_node,
            clocked_dff_is_used, curr_lev );
      }
   }
   if( gmv_need_update ) { // avoid unnecessary mem-write
      gmv0[ clk_gid ] = val;
      gmv1[ clk_gid ] = val;
   }
}


__kernel void kernel_assign_init_ff(  __global const int *buf_types,
                                      __global const int *buf_levels,
                                      __global const int *buf_pred_1st,
                                      __global const int *buf_succ_1st,
                                      __global const int *buf_preds,
                                      __global const int *buf_succs,
                                      __global const int *buf_q_offsets,
                                      __global       int *buf_q_sizes,
                                      __global  unsigned *buf_q_e_flags,
                                      __global ulong *gmv0,
                                      __global ulong *gmv1,
                                      const int num_levels,
                                      const unsigned first_ff_node,
                                      const int clocked_dff_is_used,
                                      const int is_event_driven, //--
                                      __global const ulong *pat_in_gmv0,
                                      __global const ulong *pat_in_gmv1,
                                      const unsigned pat_in_offset,
                                      const unsigned base_node_id,
                                      const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   unsigned gid_pat_in = pat_in_offset + gid;
   unsigned gid_dff = clocked_dff_is_used ?
                      first_ff_node + (gid << 2) + gid : // + (gid x 5)
                      first_ff_node + gid;
   ulong new_gmv0 = pat_in_gmv0[ gid_pat_in ];
   ulong new_gmv1 = pat_in_gmv1[ gid_pat_in ];
   int gmv_need_update = 1;
   if( is_event_driven ) {
      unsigned gid_cmp = clocked_dff_is_used ? (gid_dff + 2) : gid_dff;
      ulong old_gmv0 = gmv0[ gid_cmp ];
      ulong old_gmv1 = gmv1[ gid_cmp ];
      gmv_need_update = gmv_changed( old_gmv0, old_gmv1, new_gmv0, new_gmv1);
      if( gmv_need_update ) {
         int curr_lev = num_levels - 1;
         ADD_SUCCESSOR_EVENTS( gid_cmp, buf_levels, buf_succ_1st,
            buf_succs, buf_q_e_flags, buf_q_offsets, buf_q_sizes,
            num_levels, first_ff_node, clocked_dff_is_used, curr_lev );
      }
      // Add dff to event queue if the new value and d-input value differs
      // din is either d1 of master MUX or input of DE
      unsigned gid_din = clocked_dff_is_used ?
                         buf_preds[ buf_pred_1st[ gid_dff + 1]+ 1] :
                         buf_preds[ buf_pred_1st[ gid_dff ]];
      if( gmv_changed( gmv0[gid_din], gmv1[gid_din], new_gmv0, new_gmv1)) {
         // insert gid_dff into event queue
         unsigned old_flag = atomic_or( &(buf_q_e_flags[gid_dff]), 0);
         //if( !(buf_q_e_flags[ gid_dff ] & 0x80000000)) {
         if( 0==(old_flag & 0x80000000) ) {
            //buf_q_e_flags[ gid_dff ] |= 0x80000000; // no race here
            atomic_or( &(buf_q_e_flags[ gid_dff ]), 0x80000000);
            int lev = num_levels - 1;
            //int old_size = atomic_inc( buf_q_sizes + lev); // to avoid race
            int old_size = atomic_inc( &(buf_q_sizes[lev])); // to avoid race
            int offset = buf_q_offsets[ lev ];
            //unsigned q_flag = buf_q_e_flags[ offset + old_size] & 0x80000000;
            atomic_and( &(buf_q_e_flags[ offset + old_size]), 0x80000000);
            //buf_q_e_flags[ offset + old_size ] = q_flag + gid_dff;
            atomic_add( &(buf_q_e_flags[ offset + old_size]), gid_dff);
         }
      }
   }
   // assign value of init_ff
   if( gmv_need_update ) { // avoid unnecessary mem-write
      if( clocked_dff_is_used ) {
         gmv0[ gid_dff + 2 ] = new_gmv0;  // slave MUX
         gmv1[ gid_dff + 2 ] = new_gmv1;
         gmv0[ gid_dff + 3 ] = new_gmv0;  // master DE
         gmv1[ gid_dff + 3 ] = new_gmv1;
         gmv0[ gid_dff + 4 ] = new_gmv0;  // slave DE
         gmv1[ gid_dff + 4 ] = new_gmv1;
      } else {
         gmv0[ gid_dff ] = new_gmv0;
         gmv1[ gid_dff ] = new_gmv1;
      }
   }
}


__kernel void kernel_assign_init_ff_flag(__global const int *buf_types,
                                         __global const int *buf_levels,
                                         __global const int *buf_pred_1st,
                                         __global const int *buf_succ_1st,
                                         __global const int *buf_preds,
                                         __global const int *buf_succs,
                                         __global       int *buf_q_sizes,
                                         __global      char *buf_flags,
                                         __global ulong *gmv0,
                                         __global ulong *gmv1,
                                         const int num_levels,
                                         const unsigned first_ff_node,
                                         const int clocked_dff_is_used,
                                         const int is_event_driven, //--
                                         __global const ulong *pat_in_gmv0,
                                         __global const ulong *pat_in_gmv1,
                                         const unsigned pat_in_offset,
                                         const unsigned base_node_id,
                                         const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   unsigned gid_pat_in = pat_in_offset + gid;
   unsigned gid_dff = clocked_dff_is_used ?
                      first_ff_node + (gid << 2) + gid : // + (gid x 5)
                      first_ff_node + gid;
   ulong new_gmv0 = pat_in_gmv0[ gid_pat_in ];
   ulong new_gmv1 = pat_in_gmv1[ gid_pat_in ];
   int gmv_need_update = 1;
   if( is_event_driven ) {
      unsigned gid_cmp = clocked_dff_is_used ? (gid_dff + 2) : gid_dff;
      ulong old_gmv0 = gmv0[ gid_cmp ];
      ulong old_gmv1 = gmv1[ gid_cmp ];
      gmv_need_update = gmv_changed( old_gmv0, old_gmv1, new_gmv0, new_gmv1);
      int curr_lev = num_levels - 1;
      if( gmv_need_update ) {
         ADD_SUCCESSOR_EVENTS_FLAG( gid_cmp, buf_levels, buf_succ_1st,
            buf_succs, buf_flags, buf_q_sizes, first_ff_node,
            clocked_dff_is_used, curr_lev );
      }
      // Add dff to event queue if the new value and d-input value differs
      // din is either d1 of master MUX or input of DE
      unsigned gid_din = clocked_dff_is_used ?
                         buf_preds[ buf_pred_1st[ gid_dff + 1]+ 1] :
                         buf_preds[ buf_pred_1st[ gid_dff ]];
      if( gmv_changed( gmv0[gid_din], gmv1[gid_din], new_gmv0, new_gmv1)) {
         // set in_queue flag
         buf_flags[ gid_dff ] = 1;
         // best to be set only once. TBD how to optimize it or use
         //   atomic_or( &(buf_q_sizes[ curr_lev ]), 0x1);
         buf_q_sizes[ curr_lev ] = 1;
      }
   }
   // assign value of init_ff
   if( gmv_need_update ) { // avoid unnecessary mem-write
      if( clocked_dff_is_used ) {
         gmv0[ gid_dff + 2 ] = new_gmv0;  // slave MUX
         gmv1[ gid_dff + 2 ] = new_gmv1;
         gmv0[ gid_dff + 3 ] = new_gmv0;  // master DE
         gmv1[ gid_dff + 3 ] = new_gmv1;
         gmv0[ gid_dff + 4 ] = new_gmv0;  // slave DE
         gmv1[ gid_dff + 4 ] = new_gmv1;
      } else {
         gmv0[ gid_dff ] = new_gmv0;
         gmv1[ gid_dff ] = new_gmv1;
      }
   }
}


////////////////////////////////////////////////////////////////////////
//   Assign init_ff to gmv when cycle==0
//   when cycle==0,
//     assign both force_pi and init_ff, and padding_bound is the sum
//     of numPIs() and numFFs()
//   when cycle > 0,
//     assign only force_pi if io_per_cycle is true, and padding_bound

////////////////////////////////////////////////////////////////////////
//   Assign init_ff to gmv when cycle==0
//   when cycle==0,
//     assign both force_pi and init_ff, and padding_bound is the sum
//     of numPIs() and numFFs()
//   when cycle > 0,
//     assign only force_pi if io_per_cycle is true, and padding_bound
//     is numPIs()
////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////
//   Copy output response from gmv per cycle
//   when last_cycle,
//     copy measure_po if io_per_cycle is true,
//   when not last_cycle,
//     copy measure_po if io_per_cycle is true,
////////////////////////////////////////////////////////////////////////
__kernel void kernel_save_outputs( __global const int *output_nodes,
                                   __global const ulong *gmv0,
                                   __global const ulong *gmv1,
                                   __global ulong *pat_out_gmv0,
                                   __global ulong *pat_out_gmv1,
                                   const unsigned pat_out_offset,
                                   const unsigned base_node_id,
                                   const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   unsigned gid_pat_out = pat_out_offset + gid;
   int gout = output_nodes[ gid ];
   pat_out_gmv0[ gid_pat_out ] = gmv0[ gout ];
   pat_out_gmv1[ gid_pat_out ] = gmv1[ gout ];
}


//////////////////////////////////////////////////////////////////////
//  Make sure clock pulse is simulated in last cycle
//////////////////////////////////////////////////////////////////////
__kernel void kernel_save_obs_ffs( const unsigned first_ff_node,
                                   const int clocked_dff_is_used,
                                   __global const ulong *gmv0,
                                   __global const ulong *gmv1,
                                   __global ulong *pat_out_gmv0,
                                   __global ulong *pat_out_gmv1,
                                   const unsigned pat_out_offset,
                                   const unsigned base_node_id,
                                   const unsigned padding_bound )
{
   unsigned gid = compute_adjusted_flatten_global_id( base_node_id );
   if( gid > padding_bound) return;

   unsigned gid_pat_out = pat_out_offset + gid;
   unsigned gid_ff = clocked_dff_is_used ?        // + (gid x 5) + 2
                     first_ff_node + (gid << 2) + gid + 2: //  slave MUX
                     first_ff_node + gid; //  Delay element

   pat_out_gmv0[ gid_pat_out ] = gmv0[ gid_ff ];
   pat_out_gmv1[ gid_pat_out ] = gmv1[ gid_ff ];
}
