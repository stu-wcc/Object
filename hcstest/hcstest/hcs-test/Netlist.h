//////////////////////////////////////////////////////////////////
//                        Netlist.h
//
//  Class and function definition for circuit class in host cpu
//
//////////////////////////////////////////////////////////////////

#ifndef _NETLIST_H
#define _NETLIST_H

#include <vector>
#include <string>
#include <utility>
#include <ostream>
#include <initializer_list>
#include "Common.h"

#define   GATE_OPTIMIZED_AWAY    0x00000001
#define   GATE_POLARITY          0x00000002
#define   GATE_REMODELED         0x00000004
#define   NEW_GATE_PER_REMODEL   0x00000008

#define   GATE_TYPE_OFFSET   0
#define   GATES_SIM_OFFSET   1
#define   EVENT_ID_OFFSET    2


//////////////////////////////////////////////////////////////////
//      Class definition of reduced gate for netlist optimization
//////////////////////////////////////////////////////////////////
class ReducedGate {
   public:
      ReducedGate( GType t,
                   const std::vector< int >& preds_in,
                   const std::vector< int >& succs_in
                 ): _type( t ),
                    _preds( preds_in ),
                    _succs( succs_in )
      { ; }
      ReducedGate( const ReducedGate & rg
                 ): _type( rg.type() ),
                    _preds( rg.preds() ),
                    _succs( rg.succs() )
      { ; }
      ~ReducedGate() { ; }

      GType type() const { return _type; }
      bool isDFlipFlop() const {
         return (type()==T_DFF_L || type()==T_NAND_LATCH); // dumped by Hans
      }
      bool isDelayElement() const {
         return type()==T_DFF;
      }
      const std::vector<int> & preds() const { return _preds; }
      const std::vector<int> & succs() const { return _succs; }

      std::vector<int> & predsEdit() { return _preds; }
      std::vector<int> & succsEdit() { return _succs; }

      void preds( const std::vector<int> & new_vals ) { _preds = new_vals; }
      void succs( const std::vector<int> & new_vals ) { _succs = new_vals; }

      void pred( size_t pos, int val ) {
         assert( pos >= 0 && pos < _preds.size() );
         _preds[pos] = val;
      }

   private:
      GType _type;
      std::vector< int > _preds;
      std::vector< int > _succs;
};


//////////////////////////////////////////////////////////////////
//      Class definition of each gate
//////////////////////////////////////////////////////////////////
class Gate {

   public:

      Gate();
      ~Gate();
      int id() const { return _id; }
      void id( int n ) { _id = n; }

      static std::string typeToName( GType t );
      static std::string typeToVlog( GType t );
      std::string typeName() const { return typeToName(type()); }
      //const char* pinName() const { return _pinName.c_str(); }
      std::string pinName() const { return _pinName; }
      //std::string traceToPinName(int id) const;
      //std::string tracePinName() const; // { return traceToPinName( id() ); }

      void pinName( const std::string & name ) { _pinName = name; }

      GType type() const { return _type; }
      void type( GType t ) { _type = t; }

      bool isPrimaryInput() const {
         return (T_INPUT == _type);
      }
      bool isTieGate() const {
         return (T_TIE1 == _type) ||
                (T_TIE0 == _type) ||
                (T_TIEX == _type) ||
                (T_TIEZ == _type);
      }
      bool isDffOrLatch() const {
         return (T_DFF == _type) ||
                (T_DFF_L == _type) ||
                (T_DFF_LCP == _type) ||
                (T_DFF_LC == _type) ||
                (T_DFF_LP == _type) ||
                (T_DFF_CP == _type) ||
                (T_DFF_C == _type) ||
                (T_DFF_P == _type) ||
                (T_NAND_LATCH == _type) ||
                (T_NOR_LATCH == _type);
      }
      bool isPseudoInput() const {
         return isPrimaryInput() || isTieGate() || isDffOrLatch();
      }
      bool isIndustrialDffOrLatch() const {
         return (T_DFF_L == _type) ||
                (T_DFF_LCP == _type) ||
                (T_DFF_LC == _type) ||
                (T_DFF_LP == _type) ||
                (T_DFF_CP == _type) ||
                (T_DFF_C == _type) ||
                (T_DFF_P == _type) ||
                (T_NAND_LATCH == _type) ||
                (T_NOR_LATCH == _type);
      }

      int level() const { return _level; }
      void level( int l ) { _level = l; }

      int numPred() const { return _numPred; }
      void numPred( int n ) { _numPred = n; }

      int numSucc() const { return _numSucc; }
      void numSucc( int n ) { _numSucc = n; }

      int predC0( int i ) const { return _predC0[i]; }
      const std::vector< int >& predC0() const { return _predC0; }
      void predC0( const std::vector< int >& v ) { _predC0 = v; }

      int predC1( int i ) const { return _predC1[i]; }
      const std::vector< int >& predC1() const { return _predC1; }
      void predC1( const std::vector< int >& v ) { _predC1 = v; }

      int succ( int i ) const { return _succ[i]; }
      const std::vector< int >& succ()   const { return _succ; }
      void succ( const std::vector< int >& v ) { _succ = v; }

      int obs() const { return _obs; }
      void obs( int n ) { _obs = n; }

      int c0() const { return _c0; }
      void c0( int n ) { _c0 = n; }

      int c1() const { return _c1; }
      void c1( int n ) { _c1 = n; }

      unsigned flag() const { return _flag; }
      void flag( unsigned v ) { _flag = v; }

      const std::vector<int> & idmap() const { return _mapIds; }
      void idmap( const std::vector<int>& ids ) { _mapIds = ids; }

   private:
      int _id;
      std::string _pinName;
      GType _type;
      int _level;
      int _numPred;
      std::vector< int > _predC0;
      std::vector< int > _predC1;
      int _numSucc;
      std::vector< int > _succ;
      int _obs;  // observability value
      int _c0;   // controlability 0 value
      int _c1;   // controlability 1 value
      unsigned _flag; // GATE_OPTIMIZED_AWAY  0x00000001
                      // GATE_POLARITY        0x00000002  // vs _mapId
                      // GATE_REMODELED       0x00000004
                      // NEW_GATE_PER_REMODEL 0x00000008
      std::vector< int > _mapIds;
                       // For original netlist, _mapIds
                       //   if the gate is kept
                       //   if the gate is optimized away
                       //   if the gate is remodeled
                       // For optimized netlist, _mapIds
                       //   if the gate is kept
                       //   if the gate is new gate per remodel

};


//////////////////////////////////////////////////////////////////
//      Class definition of levelized circuit
//////////////////////////////////////////////////////////////////
class LevelizedNetlist {
   friend class Evaluate;

   public:
      LevelizedNetlist();
      ~LevelizedNetlist();

      void clear();

      bool readLevelizedCircuit( const std::string& cktFile );

      // This assumes that netlist is levelized already
      void computeCircuitStatistics();

      bool readPinnameMapping( const std::string& nameFile );

      int numNodes() const { return _numNodes; }
      void numNodes( int num ) { _numNodes = num; }

      int junk() const { return _junk; }
      void junk( int num ) { _junk = num; }

      int numFramesPerCycle() const {
         return clockedDffIsUsed() ? 3 : 1;
      }
      const std::vector<int>& constDataInputNodes() const {
         return _dataInputNodes;
      }
      const std::vector<int>& constClockInputNodes() const {
         return _clockInputNodes;
      }
      const std::map<int, int>& constClockOffStates() const {
         return _clockIdAndOffstates;
      }
      int clockOffState( int gid ) const {
         const auto it = _clockIdAndOffstates.find( gid );
         assert( it != _clockIdAndOffstates.end());
         return it->second;
      }
      int numPIs() const { return _numPIs; }
      int numDataPIs() const { return (int)_dataInputNodes.size(); }
      int numClockPIs() const { return (int)_clockInputNodes.size(); }
      int numClocks() const { return (int)_clockIdAndOffstates.size(); }
      int numTIEs() const { return _numTIEs; }
      int numPOs() const { return _numPOs; }
      int numDEs() const { return _numDEs; }
      int numRDFFs() const { return _numRDFFs; }
      int numFBBUFs() const { return _numFBBUFs; }
      int numLatches() const { return _numLatches; }
      int numFFs() const {
         return clockedDffIsUsed() ? numRDFFs() : numDEs();
      }
      int firstNodePI() const { return _firstNodePI; }
      int firstNodeTIE() const { return _firstNodeTIE; }
      int firstNodeDE() const { return _firstNodeDE; }
      int firstNodeRDFF() const { return _firstNodeRDFF; }
      int firstNodeFBBUF() const { return _firstNodeFBBUF; }
      int firstNodeLatch() const { return _firstNodeLatch; }
      const std::map<int, int> & clockIdAndOffstates() const {
         return _clockIdAndOffstates;
      }
      int firstNodeFF() const {
         return clockedDffIsUsed() ? firstNodeRDFF() : firstNodeDE();
      }
      int output( int g ) const { return _outputNodes[g]; }

      int level( int g ) const { return _levels[g]; }
      int maxLevel() const { return _numLevels-1; }
      int numLevels() const { return _numLevels; }
      int maxLevelWidth() const { return _maxLevelWidth; }

      const std::vector<int> & numNodesPerLevel() {
         return _numNodesInLevel;
      }
      const std::vector<int> & numNodesPerType() {
         return _numNodesPerType;
      }
      const std::vector<int> & firstNodeInLevel() {
         return _firstNodeInLevel;
      }
      int maxInputsInLevel(int l) const { return _maxInputsInLevel[l]; }
      int andIsOnInLevel(int l) const { return _andIsOnInLevel[l]; }
      int orIsOnInLevel(int l) const { return _orIsOnInLevel[l]; }
      int xorIsOnInLevel(int l) const { return _xorIsOnInLevel[l]; }
      int invIsOnInLevel(int l) const { return _invIsOnInLevel[l]; }

      bool addClocks( const std::vector<int>& clk_ids, int off_state );
      void deleteClocks();
      void makeCopyOfNetlistTopology( std::vector<ReducedGate>& to_nodes);

      bool isHitecLevelConvention() const { return _isHitecLevelConvention; }
      bool clockedDffIsUsed() const { return _remodeledDFlipflopDetected; }
      bool isSequentialCkt() const { return (_numDEs>0); }
      bool isCombinationalCkt() const { return (0==_numDEs); }

      void printCktStatistics( bool verbose ) const;

      // levelization routine starting from inputs and flip-flops
      bool levelizeFromInputsAndFfs( std::ostream& os );

      // Special treatment on industrial level netlist
      void processAlienLevelNetlist( std::ostream& os );

      // Experiment on the effect of level rebalancing
      //   align level size to multiply of 'align' except level 0
      void dumpRebalancedLevelNetlist( std::ostream& os, int align );

      // Dump circuit, for debug
      void dumpLevelizedCircuit( std::string fname, bool test_ocl ) const;
      void dumpSimplifiedNetlistTopology( std::ostream& os ) const;

      // Dump add clock circuit
      bool dumpClockedCircuit( std::string filename, int clock_edges )const;

      void dumpLevelizedCircuitWithGroupedTypes( std::ostream& os ) const;

      // To convert ISCAS bench to Fastscan consumable Verilog netlist
      void dumpFscanVerilogNetlist( std::ostream& os, std::string& fn) const;

      // Duplicate circuit multiple times to mimic larger design
      void dumpMultipliedBenchCircuit( std::ostream& os, int mul );
      void dumpMultipliedLevelCircuit( std::ostream& os, int mul );

      // Dump pin name mapping, for debug
      void dumpPinnameMapping( std::string fname ) const;

      void appendGate( const Gate& gat ) { _nodes.push_back( gat ); }

      const Gate & constGate( int id ) const { return _nodes[id]; }
      Gate & gate( int id ) { return _nodes[id]; }

      //-------------------------------------------------------------
      // OpenCL-related APIs
      void populateOclVectors(); // create vectors for OpenCL kernels

      void dumpOclSimEvents(int cur_lev) const;
      void dumpSuccsSucc1st() const;

      // vector of event count per level
      std::vector<int>& qSizes() { return _qSizes; }
      // vector of q flags per gate
      std::vector<char>& qFlags() { return _qFlags; }
      // vector of index offset of event queue for each level
      std::vector<int>& qOffsets() { return _qOffsets; }
      // vector of event queue
      std::vector<unsigned>& qEventsFlags() { return _qEventsFlags; }
      // vector of data input ids (for force_pi)
      std::vector<int>& dataInputNodes() { return _dataInputNodes; }
      // vector of clock inputs and offsets
      std::vector<int>& clockInputsAndOffstates() {
         return _clockInputsAndOffstates;
      }
      // vector of output ids
      std::vector<int>& outputNodes() { return _outputNodes; }
      // vector of logic types of each gate
      std::vector<int>& types() { return _types; }
      // vector of combined flags
      std::vector<char>& combinedFlags() { return _combinedFlags; }
      // vector of levels of each gate
      std::vector<int>& levels() { return _levels; }
      // vector of predesssor gate ids
      std::vector<int>& preds() { return _preds; }
      // vector of first index in _preds[]
      std::vector<int>& predFirst() { return _predFirst; }
      // vector of successor gate ids
      std::vector<int>& succs() { return _succs; }
      // vector of first index of _succs[]
      std::vector<int>& succFirst() { return _succFirst; }

   protected:
      std::string tracePinName( int id );
      GType logic( int id ) const { return _nodes[id].type(); }
      int numPred( int id ) const { return _nodes[id].numPred(); }
      const std::vector< int > & predC0( int id ) const {
         return _nodes[id].predC0();
      }
      const std::vector< int > & predC1( int id ) const {
         return _nodes[id].predC1();
      }

   private:
      std::vector< Gate > _nodes;           // original node list
      // The following information can be derived from _nodes
      bool _isHitecLevelConvention;
      bool _remodeledDFlipflopDetected;
      int _numNodes;
      int _junk;
      int _numPIs;         // # of PIs including clock pins
      int _numTIEs;        // # of TIE nodes
      int _numPOs;         // # of POs
      int _numDEs;         // # of delay elements
      int _numRDFFs;       // # of remodeled D flip flops
      int _numFBBUFs;      // # of feedback buffers
      int _numLatches;      // # of latches
      int _firstNodePI;    // first gid of PI including clock pins
      int _firstNodeTIE;   // first gid of TIE nodes
      int _firstNodeDE;    // first gid of delay element
      int _firstNodeRDFF;  // first gid of remodeled Dff
      int _firstNodeFBBUF; // first gid of feedback buffer
      int _firstNodeLatch; // first gid of latch
      int _numLevels;
      int _maxLevelWidth;
      std::vector< int > _numNodesPerType;  // num nodes for each gate type
      std::vector< int > _numNodesInLevel;  // number of nodes per level
      std::vector< int > _firstNodeInLevel; // first node id on each level
      std::vector< int > _maxInputsInLevel; // max inputs per level
      std::vector< int > _andIsOnInLevel;   // AND used per level
      std::vector< int > _orIsOnInLevel;    // OR used per level
      std::vector< int > _xorIsOnInLevel;   // XOR used per level
      std::vector< int > _invIsOnInLevel;   // INV used per level

      std::map<int, int> _clockIdAndOffstates;
      std::vector< int > _clockInputNodes;
      std::vector< int > _dataInputNodes; // used in force_pi
      std::vector< int > _outputNodes;    // vector of output node ids
      // Data used for whole netlist on OpenCL kernels
      // For gate 'g', its attributes and topology can be accessed as follows:
      //   gate type    :  _types[g]
      //   level        :  _levels[g]
      //   num of preds :  _predFirst[g+1] - _predFirst[g]
      //                   if the two values are the same, num of preds is 0.
      //   predecessors :  _preds[ _predFirst[g] ] to
      //                   _preds[ _predFirst[g+1] - 1 ]
      //   num of succs :  _succFirst[g+1] - _succFirst[g]
      //                   if the two values are the same, num of succs is 0.
      //   successors   :  _succs[ _succFirst[g] ] to
      //                   _succs[ _succFirst[g+1] - 1 ]
      //
      std::vector< int > _clockInputsAndOffstates; // clock id, off-state
      std::vector< unsigned > _qEventsFlags; // 1-dimen event queue and flags
      std::vector< int > _qOffsets;       // queue index offset for each level
      std::vector< int > _qSizes;         // queue sizes for each level
      std::vector< char> _qFlags;         // indicate if gate is in queue

      std::vector< char > _combinedFlags; // 4 x netlist_size
                                     // combined fields of types[],
                                     //   gates_to_sim[] and event_offset[]
      std::vector< int > _types;     // vector of logic types of each gate
      std::vector< int > _levels;    // vector of levels of each gate
      std::vector< int > _preds;     // vector of predesssor gate ids
      std::vector< int > _predFirst; // vector of first index in _preds[]
      std::vector< int > _succs;     // vector of successor gate ids
      std::vector< int > _succFirst; // vector of first index of _succs[]
};

#endif
