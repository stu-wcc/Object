/////////////////////////////////////////////////////////////////
//                      IdealCircuit.h
//
//    Class and function definition for making an ideal circuit.
//    It gernerates .lev file.
//
////////////////////////////////////////////////////////////////

#ifndef _MAKEIDEALCIRCUIT_H
#define _MAKEIDEALCIRUCIT_H

#include <ostream>
#include <vector>

class IdealCircuit {

   public:
      IdealCircuit() { ; };
      ~IdealCircuit() { ; };

      // Write out levelized circuit
      bool writeLevCircuit
         ( std::ostream & os,           // output file stream
           std::vector<int>& lev_sizes,  // Gate count at each level
           std::vector<int>& firstnodes, // First node id at each level
           int num_nodes,  // number of nodes in circuit
           int num_levels, // number of levels in circuit
           std::vector<int>& blk_widths,  // Width / level count of each block
           bool isSequential ) const; // Is sequential or combinational?

      // Print individual node in levelized format
      void printNodeInLevFormat( int id,
                                 int type,
                                 int level,
                                 int numPred,
                                 std::vector<int>& predC0,
                                 std::vector<int>& predC1,
                                 int numSucc,
                                 std::vector<int>& succ,
                                 int obs,
                                 int C0,
                                 int C1,
                                 std::ostream &ofs ) const;

      // Obtain predecessor list for a gate when current level and previous
      //   level have the same gate count.
      std::vector<int> rectPred( int startId, // start id of current level
                                 int id,      // target gate id
                                 int lev_size) const;

      // Obtain successor list for a gate when current level and next level
      //   have the same gate count.
      std::vector<int> rectSucc( int startId, // start id of current level
                                 int id,      // target gate id
                                 int lev_size) const;

      // Obtain predecessor list for a gate when its previous level
      //   is higher the current level
      std::vector<int> triPred( int startId, // start id of current level
                                int id,      // target gate id
                                int preId,   // start id of previous level
                                int lev_size, // gate count of current level
                                int preLevSize) const; // of previous level

      // Obtain successor list for a gate when its next level has fewer
      //   gate count than the current level
      std::vector<int> triSucc( int startId, // start id of current level
                                int id,      // target gate id
                                int nextId,  // start id of next level
                                int lev_size, // gate count of current level
                                int nextLevSize) const; // of next level

      bool customizeCircuit(std::ostream & os,
                            std::vector<int> line_lens,
                            bool seq) const;

      bool ordinaryCircuit( std::ostream & os,
                            int num_inputs,
                            int num_levels,
                            int decrement,
                            int keep ,
                            bool seq) const;

   protected:
      IdealCircuit ( IdealCircuit& );

   private:


};

#endif
