//////////////////////////////////////////////////////////////////
//                        Event.cpp
//
//           Function definition for Event class
//
//////////////////////////////////////////////////////////////////

#include "Event.h"
#include <algorithm>
#include <assert.h>
#include "HtSetting.h"
#include "app-common/utility.h"


BruteForceEventManager::BruteForceEventManager( int num_levels, int num_nodes )
   : _curLevel( -1 ) {
   init( num_levels, num_nodes );
}


BruteForceEventManager::BruteForceEventManager() : _curLevel( -1 ) {
   ;
}


BruteForceEventManager::~BruteForceEventManager() {
   ;
}


void BruteForceEventManager::init( int num_levels, int num_nodes ) {

   _events.resize( num_levels );
   _inQueue.resize( num_nodes );
   std::fill( _inQueue.begin(), _inQueue.end(), false );
   _curLevel = -1;
}


void BruteForceEventManager::clear() {
   for( unsigned lev=0; lev < _events.size(); lev++ ) {
      _events[lev].clear();
   }
   std::fill( _inQueue.begin(), _inQueue.end(), false );
   _curLevel = -1;
}


void BruteForceEventManager::insert( int g, int level ) {
   assert( g >= 0 && g < (int)_inQueue.size() ); // verify 0 <= g < num_nodes
   if( !inQueue(g) ) { // not in queue
      _inQueue[g] = true;
      _events[level].push_back( g );
   }
}


// When handling 3-frame, it does not stop until queues at all level
//   are empty
int BruteForceEventManager::next( bool is_3frame ) {
   if( _events[ curLevel() ].empty() ) {
      if( (numLevels()-1)==curLevel() ) { // Now DFFs are in last level
         curLevel( -1 );
      } else {
         int level = curLevel()+1;
         while( level < numLevels() ) {
            if( !(_events[ level ].empty()) ) {
               curLevel( level );
               break;
            }
            level += 1;
         }
         if( numLevels()==level ) { // all levels are empty after
            curLevel( -1 );         //   the above WHILE loop
         }
      }
   }
   if( curLevel() == -1 && is_3frame ) {
      // loop back to locate non-empty queue
      for( int l=0; l < numLevels(); l++ ) {
         if( !(_events[l].empty())) {
            curLevel( l );
            break;
         }
      }
   }
   if( curLevel() == -1 ) {
      return -1;
   }
   assert( _events[ curLevel() ].size() );
   int g = _events[ curLevel() ].back(); // get the last element
   _events[ curLevel() ].pop_back();     // pop the last element
   _inQueue[g] = false; // clear the flag
   return g;
}


void BruteForceEventManager::dumpEvents() const {
   HLog << "Current level: " << _curLevel << std::endl;
   for( unsigned l=0; l<_events.size(); l++ ) {
      HLog << "Level " << l << ":";
      for( unsigned e=0; e<_events[l].size(); e++ ) {
         HLog << " " << _events[l][e]+1;
      }
      HLog << std::endl;
   }
}

//--------------------------------------------------------------------------
//   Mimic event queue handling on GPU
//     it is more efficient than the brute force event manager
//     For s38417 with 1500 random patterns and 6 cycles, run time improves
//     from 1.81s to 1.21s, non-event is 0.48s
//--------------------------------------------------------------------------

MimicGpuEventManager::MimicGpuEventManager(
   int num_nodes, const std::vector<int>& firstNodePerLevel )
   : _numLevels( (int)firstNodePerLevel.size() ),
     _curLevel( -1 )
{
   init( num_nodes, firstNodePerLevel );
}


MimicGpuEventManager::MimicGpuEventManager() : _numLevels( 0 ),
                       _curLevel( -1 )
{
   ;
}


MimicGpuEventManager::~MimicGpuEventManager() {
   ;
}


void MimicGpuEventManager::init( int num_nodes,
                     const std::vector<int>& firstNodePerLevel )
{
   _events.resize( num_nodes );
   _events.shrink_to_fit();

   _queueSizes.resize( numLevels() );
   _queueSizes.shrink_to_fit();

   _queueOffsets = firstNodePerLevel;
   _queueOffsets.shrink_to_fit();
}


void MimicGpuEventManager::clear() {
   std::fill( _queueSizes.begin(), _queueSizes.end(), 0 );
   std::fill( _events.begin(), _events.end(), 0);
   _curLevel = -1;
}


void MimicGpuEventManager::insert( int g, int level ) {
   assert( g >= 0 && g < (int)_events.size() ); // verify 0 <= g < num_nodes
   if((_events[g] & MASK_1HOT_32[31])==0) {  // not in queue
      _events[g] |= MASK_1HOT_32[31];        // set MSB flag bit
      int p = _queueOffsets[ level ] + _queueSizes[ level ];
      _queueSizes[ level ] += 1;
      _events[p] = (_events[p] & MASK_1HOT_32[31]) | (unsigned)g;
   }
}


int MimicGpuEventManager::next( bool is_3frame ) {
   if( _queueSizes[ curLevel() ]==0 ) {
      if( (numLevels()-1)==curLevel() ) { // Now DFFs are in last level
         curLevel( -1 );
      } else {
         int level = curLevel()+1;

         //HLog << "next()level: "<< level         // LL debug
         //     << "  qSize: "<<_queueSizes[level]
         //     << std::endl;
         //dumpEvents();                           // LL debug
         while( level < numLevels() ) {
            if( _queueSizes[ level ] ) {
               curLevel( level );
               break;
            }
            level += 1;
         }
         if( numLevels()==level ) { // all levels are empty after
            curLevel( -1 );         //   the above WHILE loop
         }
      }
   }
   if( curLevel() == -1 && is_3frame ) {
      // loop back to locate non-empty queue
      for( int l=0; l < numLevels(); l++ ) {
         if( _queueSizes[l] ) {
            curLevel( l );
            break;
         }
      }
#ifdef HT_DEBUG
      if( curLevel() != -1 ) {  // LL debug
         HLog << "### loop back to level " << curLevel() << std::endl;
      }
#endif
   }
   if( curLevel() == -1 ) {
      return -1;
   }
   assert( _queueSizes[ curLevel() ] );

   int p = _queueOffsets[ curLevel() ] + _queueSizes[ curLevel() ] - 1 ;
   int g = (int)(_events[p] & MASK_UPTO_32[31]) ; // get the last element
   _queueSizes[ curLevel() ] -= 1;     // pop the last element
   _events[g] &= MASK_UPTO_32[31];     // clear the flag
   return g;
}


bool MimicGpuEventManager::inQueue( int g ) const {
   return ((_events[g] & MASK_1HOT_32[31])!=0);
}


void MimicGpuEventManager::dumpEvents() const {
   HLog << "Current level: " << _curLevel << std::endl;
   for( int l=0; l<numLevels(); l++ ) {
      HLog << "Level " << l << ":";
      for( int e=0; e<_queueSizes[l]; e++ ) {
         HLog << " " << (_events[ _queueOffsets[l]+e ] & MASK_UPTO_32[31])+1;
      }
      HLog << std::endl;
   }
}


///////////////////////////////////////////////////////////////////
//  Flag-based event manager
///////////////////////////////////////////////////////////////////
FlagBasedEventManager::FlagBasedEventManager(
   int num_nodes, const std::vector<int>& firstNodePerLevel )
   : _numNodes( num_nodes ),
     _numLevels( (int)firstNodePerLevel.size() ),
     _curLevel( -1 ),
     _curNode( 0 )
{
   init( num_nodes, firstNodePerLevel );
}


FlagBasedEventManager::FlagBasedEventManager() : _numLevels( 0 ),
                       _curLevel( -1 )
{
   ;
}


FlagBasedEventManager::~FlagBasedEventManager() {
   ;
}


void FlagBasedEventManager::init( int num_nodes,
                     const std::vector<int>& firstNodePerLevel )
{
   _flags.resize( num_nodes );
   _flags.shrink_to_fit();

   _queueSizes.resize( numLevels() );
   _queueSizes.shrink_to_fit();

   _queueOffsets = firstNodePerLevel;
   _queueOffsets.push_back( num_nodes ); // to store upper bound of last level
   _queueOffsets.shrink_to_fit();

   clear();
}


void FlagBasedEventManager::clear() {
   std::fill( _queueSizes.begin(), _queueSizes.end(), 0 );
   std::fill( _flags.begin(), _flags.end(), 0);
   _curLevel = -1;
}


void FlagBasedEventManager::insert( int g, int level ) {
   assert( g >= 0 && g < _numNodes ); // verify 0 <= g < num_nodes
   _flags[ g ] = 1;
   _queueSizes[ level ] = 1;
}


int FlagBasedEventManager::next( bool is_3frame ) {
   if( _queueSizes[ curLevel() ]==0 ) {
      if( (numLevels()-1)==curLevel() ) { // Now DFFs are in last level
         curLevel( -1 );
      } else {
         int level = curLevel()+1;

         while( level < numLevels() ) {
            if( _queueSizes[ level ] ) {
               curLevel( level );
               break;
            }
            level += 1;
         }
         if( numLevels()==level ) { // all levels are empty after
            curLevel( -1 );         //   the above WHILE loop
         }
      }
   }
   if( curLevel() == -1 && is_3frame ) {
      // loop back to locate non-empty queue
      for( int l=0; l < numLevels(); l++ ) {
         if( _queueSizes[l] ) {
            curLevel( l );
            break;
         }
      }
#ifdef HT_DEBUG
      if( curLevel() != -1 ) {  // LL debug
         HLog << "### loop back to level " << curLevel() << std::endl;
      }
#endif
   }
   if( curLevel() == -1 ) {
      return -1;
   }
   assert( _queueSizes[ curLevel() ] );


   while( 0 == _flags[_curNode] ) {
      _curNode++;
   } // out of loop when _flags[_curNode]=1
   int g = _curNode; // store the node to return
   assert( _curNode < _queueOffsets[ curLevel()+1] );
   _flags[ g ] = 0; // clear in-queue flag
   // update _queueSizes[ curLevel() ]
   _curNode++;
   while( 0 == _flags[_curNode] && _curNode < _queueOffsets[curLevel()+1] ) {
      _curNode++;
   }
   if( _queueOffsets[curLevel()+1] == _curNode) {
      _queueSizes[ curLevel() ] = 0; // no event in current level
      //_curLevel++; // increment current level
   }

   return g;
}


bool FlagBasedEventManager::inQueue( int g ) const {
   return ( 1==_flags[g] );
}


void FlagBasedEventManager::dumpEvents() const {
   HLog << "Current level: " << _curLevel << std::endl;
   for( int l=0; l<numLevels(); l++ ) {
      HLog << "Level " << l << ":";
      int start = _queueOffsets[l];
      int   end = _queueOffsets[l+1];
      //int end = ((numLevels()-1)==l) ? _numNodes : _queueOffsets[l+1];
      if( _queueSizes[l] ) {
         for( int g=start; g < end; g++ ) {
            if( _flags[g] ) {
               HLog << " " << g+1;
            }
         }
      }
      HLog << std::endl;
   }
}
