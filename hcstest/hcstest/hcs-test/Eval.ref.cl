
__kernel void kernel_gsim_ckt_buf( __global const int *buf_types,
                                   __global const int *buf_levels,
                                   __global const int *buf_pred_1st,
                                   __global const int *buf_succ_1st,
                                   __global const int *buf_preds,
                                   __global const int *buf_succs,
                                   __global ulong *gmv0,
                                   __global ulong *gmv1,
                                   const unsigned base_node_id,
                                   const unsigned padding_bound )
{
   size_t flatten_id = get_global_id(0);
   size_t multiplicand = get_global_size(0);
   uint work_dims = get_work_dim();   // number of dimensions in use
   for( uint dim = 1; dim < work_dims; dim++ ) {
      flatten_id += get_global_id( dim ) * multiplicand;
      multiplicand = multiplicand * get_global_size( dim );
   }
   size_t gid = base_node_id + flatten_id;
   //size_t gid_offset = get_global_offset(0); // global offset for dimen 0
   //size_t gid = get_global_id(0); // global id in dim 0 with offset in mind
   if( gid > (size_t)padding_bound) return;
   OCL_GATE_EVAL( gid, ulong , buf_pred_1st, buf_preds, buf_types,
                  gmv0, gmv1, val0, val1 );

   // Compute gate index, node id 'nid'
   uint nid = (uint)gid;

   int pred_1st = buf_pred_1st[nid];
   int pred_1st_n = buf_pred_1st[nid+1]; // pred_1st of next gate
   int type = buf_types[nid];

   ulong val0=0, val1=0;
   switch( type ) {
      case T_INPUT:
      {
         val0 = gmv0[nid];
         val1 = gmv1[nid];
         break;
      }
      case T_TIE1:  // (1,1)
      {
         val0 = ALL_ONES_64;
         val1 = ALL_ONES_64;
         break;
      }
      case T_TIE0:  // (0,0)
      {
         val0 = 0;
         val1 = 0;
         break;
      }
      case T_TIEX:  // (1,0)
      {
         val0 = ALL_ONES_64;
         val1 = 0;
         break;
      }
      case T_TIEZ:  // (0,1)
      {
         val0 = 0;
         val1 = ALL_ONES_64;
         break;
      }
      case T_OUTPUT:
      case T_DFF:
      case T_BUF:
      {
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 = gmv0[pred_id];
            val1 = gmv1[pred_id];
         }
         //val0 = gmv0( predC0(id)[0] );
         //val1 = gmv1( predC0(id)[0] );
         break;
      }
      case T_AND:
      {
         val1 = val0 = ALL_ONES_64;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 &= gmv0[pred_id];
            val1 &= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 &= gmv0( predC0(id)[i] );
         //   val1 &= gmv1( predC0(id)[i] );
         //}
         break;
      }
      case T_NAND:
      {
         val1 = val0 = ALL_ONES_64;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 &= gmv0[pred_id];
            val1 &= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 &= gmv0( predC0(id)[i] );
         //   val1 &= gmv1( predC0(id)[i] );
         //}
         ulong v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      case T_OR:
      {
         val1 = val0 = 0;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 |= gmv0[pred_id];
            val1 |= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 |= gmv0( predC0(id)[i] );
         //   val1 |= gmv1( predC0(id)[i] );
         //}
         break;
      }
      case T_NOR:
      {
         val1 = val0 = 0;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 |= gmv0[pred_id];
            val1 |= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 |= gmv0( predC0(id)[i] );
         //   val1 |= gmv1( predC0(id)[i] );
         //}
         ulong v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      case T_NOT:
      {
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 = gmv1[pred_id] ^ ALL_ONES_64;
            val1 = gmv0[pred_id] ^ ALL_ONES_64;
         }
         //val0 = gmv1( predC0(id)[0] ) ^ ALL_ONES_64;
         //val1 = gmv0( predC0(id)[0] ) ^ ALL_ONES_64;
         break;
      }
      case T_XOR:
      {
         // for 1st input on pred_1st
         for( int i=pred_1st; i < pred_1st + 1; i++ ) {
            int pred_id = buf_preds[i];
            val0 = gmv1[pred_id];
            val1 = gmv0[pred_id];
         }
         // for inputs from pred_1st + 1
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            ulong v1 = ALL_ONES_64 ^
               (((ALL_ONES_64^gmv0[pred_id]) & (ALL_ONES_64 ^ val0)) |
                (gmv1[pred_id] & val1));
            val1 = ((ALL_ONES_64 ^ gmv0[pred_id]) & val1) |
               (gmv1[pred_id] & (ALL_ONES_64 ^ val0));
            val0 = v1;
         }
         //val0 = gmv0( predC0(id)[0] );
         //val1 = gmv1( predC0(id)[0] );
         //for( int i = 1; i < numPred(id); i++) {
         //   v1 = ALL_ONES_64 ^
         //      (((ALL_ONES_64^gmv0( predC0(id)[i] )) & (ALL_ONES_64^val0))|
         //         (gmv1( predC0(id)[i] ) & val1));
         //   val1 = ((ALL_ONES_64 ^ gmv0( predC0(id)[i] )) & val1) |
         //           (gmv1( predC0(id)[i] ) & (ALL_ONES_64 ^ val0));
         //   val0 = v1;
         //}
         break;
      }
      case T_XNOR:
      {
         // for 1st input on pred_1st
         for( int i=pred_1st; i < pred_1st + 1; i++ ) {
            int pred_id = buf_preds[i];
            val0 = gmv1[pred_id];
            val1 = gmv0[pred_id];
         }
         // for inputs from pred_1st + 1
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            ulong v1 = ALL_ONES_64 ^
               (((ALL_ONES_64^gmv0[pred_id]) & (ALL_ONES_64 ^ val0)) |
                (gmv1[pred_id] & val1));
            val1 = ((ALL_ONES_64 ^ gmv0[pred_id]) & val1) |
               (gmv1[pred_id] & (ALL_ONES_64 ^ val0));
            val0 = v1;
         }
         //val0 = gmv0( predC0(id)[0] );
         //val1 = gmv1( predC0(id)[0] );
         //for( int i = 1; i < numPred(id); i++) {
         //   v1 = ALL_ONES_64 ^
         //    (((ALL_ONES_64 ^ gmv0(predC0(id)[i])) & (ALL_ONES_64^val0)) |
         //    (gmv1( predC0(id)[i] ) & val1));
         //   val1 = ((ALL_ONES_64^gmv0( predC0(id)[i] )) & val1) |
         //           (gmv1( predC0(id)[i] ) & (ALL_ONES_64 ^ val0));
         //   val0 = v1;
         //}
         ulong v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      // Add more primitives to support industrial design
      case T_BUS:
      case T_BUSZ: {  // LL: apply to parallel patterns
         val0 = 0;
         val1 = ALL_ONES_64;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 |= gmv0[pred_id];
            val1 &= gmv1[pred_id];
         }
         // Changing Z bit to X
         ulong z_mask = (~val0) & val1; // Get bits with Z (0,1) values
         val0 |= z_mask;
         val1 &= (~z_mask);
         break;
      }
      case T_TRISTATE1: { // assume in0 is D; in1 is EN
         // e1: enable is 1; D passthrough
         ulong e1_mask = gmv0[ buf_preds[ pred_1st+1]] &
                         gmv1[ buf_preds[ pred_1st+1]];
         val0 = gmv0[ buf_preds[pred_1st] ] & e1_mask;
         val1 = gmv1[ buf_preds[pred_1st] ] & e1_mask;

         // e0: enable 0; output Z (0,1)
         ulong e0_mask = ~(gmv0[ buf_preds[ pred_1st+1]] |
                           gmv1[ buf_preds[ pred_1st+1]] );
         val0 &= ~e0_mask;  // output Z (0,1) with enable 0
         val1 |= e0_mask;

         // ex: enable X, output X (1,0)
         ulong ex_mask = ~(e1_mask | e0_mask);
         val0 |= ex_mask;   // output X (1,0) with enable X
         val1 &= ~ex_mask;

         break;
      }
      case T_MUX_2: // Assume port (sel, d0, d1)
      {
         // s1: selector is 1; s0: selector is 0; dq: d0==d1
         // start with Xs (1,0)
         val0 = ALL_ONES_64;
         val1 = 0;

         // pass d0 for sel==0 and d0==d1
         ulong s0_mask = ~(gmv0[ buf_preds[ pred_1st]] |
                           gmv1[ buf_preds[ pred_1st]] );
         ulong dq_mask = ~((gmv0[ buf_preds[ pred_1st+1]] ^
                            gmv0[ buf_preds[ pred_1st+2]] ) |
                           (gmv1[ buf_preds[ pred_1st+1]] ^
                            gmv1[ buf_preds[ pred_1st+2]] ));
         ulong s0_dq_mask = s0_mask | dq_mask;
         val0 &= ((gmv0[ buf_preds[ pred_1st+1]] & s0_dq_mask) | ~s0_dq_mask);
         val1 |= (gmv1[ buf_preds[ pred_1st+1]] & s0_dq_mask);

         // pass d1 for sel==1
         ulong s1_mask = gmv0[ buf_preds[pred_1st]] &
                         gmv1[ buf_preds[pred_1st]] ;
         val0 &= ((gmv0[ buf_preds[ pred_1st+2]] & s1_mask) | ~s1_mask);
         val1 |= (gmv1[ buf_preds[ pred_1st+2]] & s1_mask);

         break;
      }
      case T_DFF_L: // assume port set, reset, clock and data
      {
         val0 = gmv0[ buf_preds[pred_1st+3] ]; // always capture data
         val1 = gmv1[ buf_preds[pred_1st+3] ];
         break;
      }
      default:
      {
         //HLog << InfoError
         //     << "gate type " <<  logic(id)
         //     << " is not supported in 4-value logic simulation";
         break;
      }
   }
   // update gate value (gmv0, gmv1)
   gmv0[nid] = val0;
   gmv1[nid] = val1;
   //
}

/////////////////////////////////////////////////////////////////////
// simulate in vectors to make use of OpenCL CPU vector capability
/////////////////////////////////////////////////////////////////////
__kernel void kernel_gsim_ulong4_ckt_buf( __global const int *buf_types,
                                   __global const int *buf_levels,
                                   __global const int *buf_pred_1st,
                                   __global const int *buf_succ_1st,
                                   __global const int *buf_preds,
                                   __global const int *buf_succs,
                                   __global ulong4 *gmv0,
                                   __global ulong4 *gmv1,
                                   const unsigned base_node_id,
                                   const unsigned padding_bound )
{
   size_t flatten_id = get_global_id(0);
   size_t multiplicand = get_global_size(0);
   uint work_dims = get_work_dim();   // number of dimensions in use
   for( uint dim = 1; dim < work_dims; dim++ ) {
      flatten_id += get_global_id( dim ) * multiplicand;
      multiplicand = multiplicand * get_global_size( dim );
   }
   size_t gid = base_node_id + flatten_id;
   if( gid > (size_t)padding_bound) return;

   // Compute gate index, node id 'nid'
   uint nid = (uint)gid;

   int pred_1st = buf_pred_1st[nid];
   int pred_1st_n = buf_pred_1st[nid+1]; // pred_1st of next gate
   int type = buf_types[nid];

   // val0 = 0; IS equivalent to
   //   val0.w = 0;
   //   val0.x = 0;
   //   val0.y = 0;
   //   val0.z = 0;
   ulong4 val0=0, val1=0;
   switch( type ) {
      case T_INPUT:
      {
         val0 = gmv0[nid];
         val1 = gmv1[nid];
         break;
      }
      case T_TIE1:  // (1,1)
      {
         val0 = ALL_ONES_64;
         val1 = ALL_ONES_64;
         break;
      }
      case T_TIE0:  // (0,0)
      {
         val0 = 0;
         val1 = 0;
         break;
      }
      case T_TIEX:  // (1,0)
      {
         val0 = ALL_ONES_64;
         val1 = 0;
         break;
      }
      case T_TIEZ:  // (0,1)
      {
         val0 = 0;
         val1 = ALL_ONES_64;
         break;
      }
      case T_OUTPUT:
      case T_DFF:
      case T_BUF:
      {
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 = gmv0[pred_id];
            val1 = gmv1[pred_id];
         }
         //val0 = gmv0( predC0(id)[0] );
         //val1 = gmv1( predC0(id)[0] );
         break;
      }
      case T_AND:
      {
         val1 = val0 = ALL_ONES_64;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 &= gmv0[pred_id];
            val1 &= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 &= gmv0( predC0(id)[i] );
         //   val1 &= gmv1( predC0(id)[i] );
         //}
         break;
      }
      case T_NAND:
      {
         val1 = val0 = ALL_ONES_64;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 &= gmv0[pred_id];
            val1 &= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 &= gmv0( predC0(id)[i] );
         //   val1 &= gmv1( predC0(id)[i] );
         //}
         ulong4 v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      case T_OR:
      {
         val1 = val0 = 0;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 |= gmv0[pred_id];
            val1 |= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 |= gmv0( predC0(id)[i] );
         //   val1 |= gmv1( predC0(id)[i] );
         //}
         break;
      }
      case T_NOR:
      {
         val1 = val0 = 0;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 |= gmv0[pred_id];
            val1 |= gmv1[pred_id];
         }
         //for( int i = 0; i < numPred(id); i++) {
         //   val0 |= gmv0( predC0(id)[i] );
         //   val1 |= gmv1( predC0(id)[i] );
         //}
         ulong4 v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      case T_NOT:
      {
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 = gmv1[pred_id] ^ ALL_ONES_64;
            val1 = gmv0[pred_id] ^ ALL_ONES_64;
         }
         //val0 = gmv1( predC0(id)[0] ) ^ ALL_ONES_64;
         //val1 = gmv0( predC0(id)[0] ) ^ ALL_ONES_64;
         break;
      }
      case T_XOR:
      {
         // for 1st input on pred_1st
         for( int i=pred_1st; i < pred_1st + 1; i++ ) {
            int pred_id = buf_preds[i];
            val0 = gmv1[pred_id];
            val1 = gmv0[pred_id];
         }
         // for inputs from pred_1st + 1
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            ulong4 v1 = ALL_ONES_64 ^
               (((ALL_ONES_64^gmv0[pred_id]) & (ALL_ONES_64 ^ val0)) |
                (gmv1[pred_id] & val1));
            val1 = ((ALL_ONES_64 ^ gmv0[pred_id]) & val1) |
               (gmv1[pred_id] & (ALL_ONES_64 ^ val0));
            val0 = v1;
         }
         //val0 = gmv0( predC0(id)[0] );
         //val1 = gmv1( predC0(id)[0] );
         //for( int i = 1; i < numPred(id); i++) {
         //   v1 = ALL_ONES_64 ^
         //      (((ALL_ONES_64^gmv0( predC0(id)[i] )) & (ALL_ONES_64^val0))|
         //         (gmv1( predC0(id)[i] ) & val1));
         //   val1 = ((ALL_ONES_64 ^ gmv0( predC0(id)[i] )) & val1) |
         //           (gmv1( predC0(id)[i] ) & (ALL_ONES_64 ^ val0));
         //   val0 = v1;
         //}
         break;
      }
      case T_XNOR:
      {
         // for 1st input on pred_1st
         for( int i=pred_1st; i < pred_1st + 1; i++ ) {
            int pred_id = buf_preds[i];
            val0 = gmv1[pred_id];
            val1 = gmv0[pred_id];
         }
         // for inputs from pred_1st + 1
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            ulong4 v1 = ALL_ONES_64 ^
               (((ALL_ONES_64^gmv0[pred_id]) & (ALL_ONES_64 ^ val0)) |
                (gmv1[pred_id] & val1));
            val1 = ((ALL_ONES_64 ^ gmv0[pred_id]) & val1) |
               (gmv1[pred_id] & (ALL_ONES_64 ^ val0));
            val0 = v1;
         }
         //val0 = gmv0( predC0(id)[0] );
         //val1 = gmv1( predC0(id)[0] );
         //for( int i = 1; i < numPred(id); i++) {
         //   v1 = ALL_ONES_64 ^
         //    (((ALL_ONES_64 ^ gmv0(predC0(id)[i])) & (ALL_ONES_64^val0)) |
         //    (gmv1( predC0(id)[i] ) & val1));
         //   val1 = ((ALL_ONES_64^gmv0( predC0(id)[i] )) & val1) |
         //           (gmv1( predC0(id)[i] ) & (ALL_ONES_64 ^ val0));
         //   val0 = v1;
         //}
         ulong4 v1 = val0;
         val0 = ALL_ONES_64 ^ val1;
         val1 = ALL_ONES_64 ^ v1;
         break;
      }
      // Add more primitives to support industrial design
      case T_BUS:
      case T_BUSZ: {  // LL: apply to parallel patterns
         val0 = 0;
         val1 = ALL_ONES_64;
         for( int i=pred_1st; i<pred_1st_n; i++ ) {
            int pred_id = buf_preds[i];
            val0 |= gmv0[pred_id];
            val1 &= gmv1[pred_id];
         }
         // Changing Z bit to X
         ulong4 z_mask = (~val0) & val1; // Get bits with Z (0,1) values
         val0 |= z_mask;
         val1 &= (~z_mask);
         break;
      }
      case T_TRISTATE1: { // assume in0 is D; in1 is EN
         // e1: enable is 1; D passthrough
         ulong4 e1_mask = gmv0[ buf_preds[ pred_1st+1]] &
                         gmv1[ buf_preds[ pred_1st+1]];
         val0 = gmv0[ buf_preds[pred_1st] ] & e1_mask;
         val1 = gmv1[ buf_preds[pred_1st] ] & e1_mask;

         // e0: enable 0; output Z (0,1)
         ulong4 e0_mask = ~(gmv0[ buf_preds[ pred_1st+1]] |
                           gmv1[ buf_preds[ pred_1st+1]] );
         val0 &= ~e0_mask;  // output Z (0,1) with enable 0
         val1 |= e0_mask;

         // ex: enable X, output X (1,0)
         ulong4 ex_mask = ~(e1_mask | e0_mask);
         val0 |= ex_mask;   // output X (1,0) with enable X
         val1 &= ~ex_mask;

         break;
      }
      case T_MUX_2: // Assume port (sel, d0, d1)
      {
         // s1: selector is 1; s0: selector is 0; dq: d0==d1
         // start with Xs (1,0)
         val0 = ALL_ONES_64;
         val1 = 0;

         // pass d0 for sel==0 and d0==d1
         ulong4 s0_mask = ~(gmv0[ buf_preds[ pred_1st]] |
                           gmv1[ buf_preds[ pred_1st]] );
         ulong4 dq_mask = ~((gmv0[ buf_preds[ pred_1st+1]] ^
                            gmv0[ buf_preds[ pred_1st+2]] ) |
                           (gmv1[ buf_preds[ pred_1st+1]] ^
                            gmv1[ buf_preds[ pred_1st+2]] ));
         ulong4 s0_dq_mask = s0_mask | dq_mask;
         val0 &= ((gmv0[ buf_preds[ pred_1st+1]] & s0_dq_mask) | ~s0_dq_mask);
         val1 |= (gmv1[ buf_preds[ pred_1st+1]] & s0_dq_mask);

         // pass d1 for sel==1
         ulong4 s1_mask = gmv0[ buf_preds[pred_1st]] &
                         gmv1[ buf_preds[pred_1st]] ;
         val0 &= ((gmv0[ buf_preds[ pred_1st+2]] & s1_mask) | ~s1_mask);
         val1 |= (gmv1[ buf_preds[ pred_1st+2]] & s1_mask);

         break;
      }
      case T_DFF_L: // assume port set, reset, clock and data
      {
         val0 = gmv0[ buf_preds[pred_1st+3] ]; // always capture data
         val1 = gmv1[ buf_preds[pred_1st+3] ];
         break;
      }
      default:
      {
         //HLog << InfoError
         //     << "gate type " <<  logic(id)
         //     << " is not supported in 4-value logic simulation";
         break;
      }
   }
   // update gate value (gmv0, gmv1)
   gmv0[nid] = val0;
   gmv1[nid] = val1;
   //
}
