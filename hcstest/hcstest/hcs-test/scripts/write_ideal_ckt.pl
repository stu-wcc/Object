#!/usr/bin/perl
#
# To write 2-input AND, OR or XOR gate netlist for experiment
#
#
my $usage = "Usage: write_ideal_ckt.pl <num_of_inputs/dffs>\n" .
            "          <num_of_levels>\n" .
            "          <bench_file>\n" .
            "          <--bench | --level>\n" .
            "          <--sequential | --combinational>\n" .
            "          <--and | --and_or | --and_xor | --and_or_xor_6 >\n";

my %valid_ckt_formats = ("--bench" => 0,
                         "--level" => 0 );

my %valid_ckt_types = ( "--sequential" => 0,
                        "--combinational" => 0 );

my %valid_netlist_types = ( "--and" => 0,
                            "--and_or" => 0,
                            "--and_xor" => 0,
                            "--and_or_xor_6" => 0 );

# check number of arguments
die "$usage"
   if( $#ARGV != 5 );

my $num_inputs = $ARGV[0];
my $num_levels = $ARGV[1];
my $bench_file = $ARGV[2];
my $circuit_format = $ARGV[3];
my $circuit_type = $ARGV[4];
my $netlist_type = $ARGV[5];

# check if file format is valid
die "Invalid circuit format: $circuit_format\n$usage\n"
   if( !exists( $valid_ckt_formats{ $circuit_format } ));

# check if circuit type is valid
die "Invalid circuit type: $circuit_type\n$usage\n"
   if( !exists( $valid_ckt_types{ $circuit_type } ));

# check if netlist type is valid
die "Invalid netlist type: $netlist_type\n$usage\n"
   if( !exists( $valid_netlist_types{ $netlist_type } ));

open BENCH, ">$bench_file" or die "Can't open file for write: $!";


### print BENCH file
if( $circuit_format eq "--bench" ) {
   if( $circuit_type eq "--combinational" ) {
      # Print inputs
      for( my $n=0; $n < $num_inputs; $n++ ) {
         print BENCH "INPUT( G$n\_0 )\n";
      }
      print BENCH "\n";

      # Print outnputs
      my $max_level = $num_levels - 1;
      for( my $n=0; $n < $num_inputs; $n++ ) {
         print BENCH "OUTPUT( G$n\_$max_level )\n";
      }
      print BENCH "\n";
   } elsif( $circuit_type eq "--sequential" ) {
      # Print DFFs
      my $max_level = $num_levels - 1;
      for( my $n=0; $n < $num_inputs; $n++ ) {
         print BENCH "G$n\_0 = DFF( G$n\_$max_level )\n";
      }
      print BENCH "\n";
   }

   # Print AND gate netlist
   for( my $l=1; $l < $num_levels; $l++ ) {
      for( my $n=0; $n < $num_inputs; $n++ ) {
         my $pl = $l - 1; # previous level number
         my $sn = ($n + 1) % $num_inputs; # second input id
         my $n_mod_2 = $n % 2;
         my $n_mod_6 = $n % 6;

         if( $netlist_type eq "--and" ) {
            print BENCH "G$n\_$l = AND( G$n\_$pl, G$sn\_$pl )\n";
         } elsif( $netlist_type eq "--and_or" ) {
            if( 0 == $n_mod_2 ) {
               print BENCH "G$n\_$l = AND( G$n\_$pl, G$sn\_$pl )\n";
            } elsif( 1 == $n_mod_2 ) {
               print BENCH "G$n\_$l =  OR( G$n\_$pl, G$sn\_$pl )\n";
            }
         } elsif( $netlist_type eq "--and_xor" ) {
            if( 0 == $n_mod_2 ) {
               print BENCH "G$n\_$l = AND( G$n\_$pl, G$sn\_$pl )\n";
            } elsif( 1 == $n_mod_2 ) {
               print BENCH "G$n\_$l = XOR( G$n\_$pl, G$sn\_$pl )\n";
            }
         } elsif( $netlist_type eq "--and_or_xor_6" ) {
            if( 0 == $n_mod_6 ) {
               print BENCH "G$n\_$l =  AND( G$n\_$pl, G$sn\_$pl )\n";
            } elsif( 1 == $n_mod_6 ) {
               print BENCH "G$n\_$l = NAND( G$n\_$pl, G$sn\_$pl )\n";
            } elsif( 2 == $n_mod_6 ) {
               print BENCH "G$n\_$l =   OR( G$n\_$pl, G$sn\_$pl )\n";
            } elsif( 3 == $n_mod_6 ) {
               print BENCH "G$n\_$l =  NOR( G$n\_$pl, G$sn\_$pl )\n";
            } elsif( 4 == $n_mod_6 ) {
               print BENCH "G$n\_$l =  XOR( G$n\_$pl, G$sn\_$pl )\n";
            } elsif( 5 == $n_mod_6 ) {
               print BENCH "G$n\_$l = XNOR( G$n\_$pl, G$sn\_$pl )\n";
            }
         }
      }
   }
### print LEVEL file
} elsif( $circuit_format eq "--level" ) {
   my $num_nodes = $num_inputs * $num_levels;
   my $adj_num_nodes = $num_nodes + 1;
   # print # of nodes
   print BENCH "$adj_num_nodes\n";
   # print junk number
   print BENCH "10\n";
   # print nodes at level 0
   for( my $n=0; $n < $num_inputs; $n++ ) {
      my $gid = $n+1;
      my $succ_id1 = $gid - 1 + $num_inputs;
      if( 1 == $gid ) {
         $succ_id1 = $succ_id1 + $num_inputs;
      }
      my $succ_id2 = $gid + $num_inputs;
      my $pred_id = $gid + ($num_inputs * ($num_levels - 1));
      my $lev = 0;
      my $obs = $num_levels - 1;
      my $c1 = 0;
      my $c0 = 0;
      if( $circuit_type eq "--combinational" ) {
         # print INPUT
         print BENCH "$gid 1 $lev 0 2 $succ_id1 $succ_id2 $obs ; $c0 $c1\n";
      } elsif( $circuit_type eq "--sequential" ) {
         # print DFF
         print BENCH "$gid 5 $lev 1 $pred_id $pred_id 2 $succ_id1 $succ_id2 $obs ; $c0 $c1\n";
      }
   }
   # print nodes on intermediate levels
   for( my $l=1; $l < $num_levels-1; $l++ ) {
      for( my $g=0; $g < $num_inputs; $g++ ) {
         my $gid = $g + 1 + $l * $num_inputs;
         my $succ_id1 = $gid - 1 + $num_inputs;
         if( (1 + $l * $num_inputs) == $gid ) {
            $succ_id1 = $succ_id1 + $num_inputs;
         }
         my $succ_id2 = $gid + $num_inputs;
         my $pred_id1 = $gid - $num_inputs;
         my $pred_id2 = $pred_id1 + 1;
         if( 0==($pred_id1 % $num_inputs) ) {
            $pred_id2 = $pred_id2 - $num_inputs;
         }
         my $lev = $l * 5;
         my $obs = $num_levels - $l - 1;
         my $c1 = $l;
         my $c0 = $l;
         # AND:6 NAND:7 OR:8 NOR:9 XOR:3 XNOR:4
         if( ($circuit_type eq "--combinational") && ($num_levels-2==$l)) {
            print BENCH "$gid 6 $lev 2 $pred_id1 $pred_id2 $pred_id1 $pred_id2 1 $succ_id2 $obs ; $c0 $c1\n";
         } else {
            print BENCH "$gid 6 $lev 2 $pred_id1 $pred_id2 $pred_id1 $pred_id2 2 $succ_id1 $succ_id2 $obs ; $c0 $c1\n";
         }
      }
   }
   # print nodes on last level
   for( my $n=0; $n < $num_inputs; $n++ ) {
      my $l = $num_levels - 1;
      my $lev = $l * 5;
      my $gid = $n+1 + $l * $num_inputs;
      my $succ_id = $n + 1; # for DFF
      my $pred_id1 = $gid - $num_inputs;
      my $pred_id2 = $pred_id1 + 1;
      if( 0==($pred_id1 % $num_inputs) ) {
         $pred_id2 = $pred_id2 - $num_inputs;
      }
      my $obs = $num_levels - $l - 1;
      my $c1 = $l;
      my $c0 = $l;
      if( $circuit_type eq "--combinational" ) {
         # print OUTPUT
         print BENCH "$gid 2 $lev 1 $pred_id1 $pred_id1 0 $obs O $c0 $c1\n";
      } elsif( $circuit_type eq "--sequential" ) {
         # AND:6 NAND:7 OR:8 NOR:9 XOR:3 XNOR:4
         print BENCH "$gid 6 $lev 2 $pred_id1 $pred_id2 $pred_id1 $pred_id2 1 $succ_id $obs ; $c0 $c1\n";
      }
   }
}

close BENCH;




