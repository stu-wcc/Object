#################################################################
#                       prompt.tcl
#  Tcl commands to customize prompt for tcl shell. Commands
#  are hardcoded in main.cpp instead of sourcing this file.
#
#################################################################
proc prompt1 {} {
  puts -nonewline \"HTest> \"
  flush stdout
}
proc prompt2 {} {
  puts -nonewline \"> \"
  flush stdout
}
set ::tcl_prompt1 prompt1
set ::tcl_prompt2 prompt2

