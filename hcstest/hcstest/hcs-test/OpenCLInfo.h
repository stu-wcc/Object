//////////////////////////////////////////////////////////////////
//                        OpenCLInfo.h
//    Class to store OpenCL hardware information such as
//    platforms, devices and various parameters
//////////////////////////////////////////////////////////////////

#ifndef _OPENCL_INFO
#define _OPENCL_INFO

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>
#include <assert.h>

#if defined(_WIN32)
#include <malloc.h> // needed for alloca
#endif // _WIN32

#if defined(linux) || defined(__APPLE__) || defined(__MACOSX)
# include <alloca.h>
#endif // linux

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#define CL_PRINT_OK 0
#define CL_PRINT_ERROR 1

#define STATUS_OK 0
#define STATUS_ERROR 1

// Convert ocl function error code into readable string
const char * oclErrorString( cl_int err_code );

// Check ocl function return code and print corresponding message
bool oclFunctionFailed( cl_int err_code, const std::string & msg );

// Remember to extend the size of _kernels vector once adding a new item
typedef enum {
   KERNEL_INIT_NODE_IMG,
   KERNEL_INIT_CONN_IMG,
   KERNEL_TEST_CKT_IMAGE,
   KERNEL_GSIM_IMG,
   KERNEL_GSIM_BUF,
   KERNEL_GSIM_BUF_EVENT,
   KERNEL_GSIM_BUF_EVENT_FLAG,
   KERNEL_INIT_X_BACKGROUND,
   KERNEL_INIT_TIE_GATES,
   KERNEL_INIT_TIE_GATES_FLAG,
   KERNEL_ASSIGN_STIMULUS,
   KERNEL_ASSIGN_DATA_PI,
   KERNEL_ASSIGN_DATA_PI_FLAG,
   KERNEL_ASSIGN_CLOCK_PI,
   KERNEL_ASSIGN_CLOCK_PI_FLAG,
   KERNEL_ASSIGN_INIT_FF,
   KERNEL_ASSIGN_INIT_FF_FLAG,
   KERNEL_SAVE_OUTPUTS,
   KERNEL_SAVE_OBS_FFS,
   LAST_CL_KERNEL
} OclKernelId;


// Remember to extend the size of _clMemsCkt vector once adding a new item
typedef enum {
   CL_BUF_Q_EVENTS_FLAGS,
   CL_BUF_Q_OFFSETS,
   CL_BUF_Q_SIZES,
   CL_BUF_Q_FLAGS,
   CL_BUF_DATA_INPUTS,
   CL_BUF_CLOCKS_AND_OFFSTATES,
   CL_BUF_OUTPUTS,
   CL_BUF_TYPES,
   CL_BUF_LEVELS,
   CL_BUF_PREDS,
   CL_BUF_PRED1ST,
   CL_BUF_SUCCS,
   CL_BUF_SUCC1ST,
   CL_BUF_TYPES_RDBK,
   CL_BUF_LEVELS_RDBK,
   CL_BUF_PREDS_RDBK,
   CL_BUF_PRED1ST_RDBK,
   CL_BUF_SUCCS_RDBK,
   CL_BUF_SUCC1ST_RDBK,
   // CL_BUF_GMV0, // moved to seperate vectors to allow multiple copies
   // CL_BUF_GMV1,
   CL_IMG_NODE,
   CL_IMG_CONN,
   LAST_CL_MEM_CKT
} OclMemoryIdCkt;


// To indicate what cl_mem is used to store circuit on GPU/Cl device
typedef enum {
   CKT_BUFFER,
   CKT_IMAGE,
} CktClMemType;


class OpenCLInfo;
class OpenCLPlatform;
class OpenCLDevice;


class OpenCLInfo {
   // friend class EvaluateCL;
   public:
      OpenCLInfo();
      ~OpenCLInfo();
      bool notQueried() const { return !_isQueried; }
      bool query();
      bool display( bool summary );


      bool checkContextAndQueue( cl_uint pfm_id, cl_uint dev_id );
      bool createContext( int pfm_id );
      bool releaseContext( int pfm_id );

      bool createCmdQueue( int pfm_id, int dev_id,
                           cl_command_queue_properties properties );
      bool releaseCmdQueue( int pfm_id, int dev_id );

      bool checkPlatformIdRange( int pfm_id ) const;
      cl_uint numPlatforms() const { return _numPlatforms; }
      OpenCLPlatform & platform( cl_uint i ) { return _platforms[i]; }
      const OpenCLPlatform & platformConst( cl_uint i ) const {
         return _platforms[i];
      }

      // release Ocl resources at the top level
      bool releaseOclResources( bool for_delete_ckt );
      void printProperties( bool summary ) const;

      // functions relevant global work size and local work size
      void printUserPreferences();
      bool setUserPreferences( std::vector<int> gw_size,
                               std::vector<int> lw_size,
                               CktClMemType type,
                               bool print_only );
      bool setUserPreferences( int pfm_id,
                               std::vector<int> gw_size,
                               std::vector<int> lw_size,
                               CktClMemType type,
                               bool print_only );
      bool setUserPreferences( int pfm_id,
                               int dev_id,
                               std::vector<int> gw_size,
                               std::vector<int> lw_size,
                               CktClMemType type,
                               bool print_only );

   private:
      bool _isQueried;
      cl_uint _numPlatforms; // total number of platforms
      std::vector< cl_platform_id >  _platformIds; // vector of platform ids
      std::vector< OpenCLPlatform > _platforms; // vector of platforms

};


class OpenCLPlatform {
   // friend class EvaluateCL;
   public:
      OpenCLPlatform( cl_platform_id id );
      ~OpenCLPlatform();

      // release platform-related ocl resources
      bool releaseOclMemForPatternData();
      bool releaseOclResources( bool for_delete_ckt );

      void printProperties( bool summary ) const;
      bool display( bool summary ); // Print all properties
      // Print single platform property; 0:OK, 1:ERROR
      int print( cl_platform_info info );

      // Get platform info in terms of string, 0:success, 1:failure
      int getPlatformInfo( cl_platform_info info, std::string& val );

      void printUserPreferences();

      bool createContext();
      bool releaseContext();

      cl_context context() { return _context; }

      // Use reference. So it can be modified directly
      cl_program& program() { return _program; }

      cl_kernel& kernel( OclKernelId id ) { return _kernels[(int)id]; }
      cl_mem& clMemCkt( OclMemoryIdCkt id ) { return _clMemsCkt[(int)id]; }

      cl_mem& clMemGmv0( cl_uint grp ) { return _clMemsGmv0[ grp ]; }
      cl_mem& clMemGmv1( cl_uint grp ) { return _clMemsGmv1[ grp ]; }

      cl_mem& clMemPatInGmv0( cl_uint g ) { return _clMemsPatInGmv0[ g ]; }
      cl_mem& clMemPatInGmv1( cl_uint g ) { return _clMemsPatInGmv1[ g ]; }

      cl_mem& clMemPatOutGmv0( cl_uint g ) { return _clMemsPatOutGmv0[ g ];}
      cl_mem& clMemPatOutGmv1( cl_uint g ) { return _clMemsPatOutGmv1[ g ];}

      bool checkDeviceIdRange( int dev_id ) const;
      cl_uint numDevices() const { return _numDevices; }
      OpenCLDevice & device( int i ) { return _devices[i]; }
      cl_device_id * ptrToDeviceId( int i) { return &(_deviceIds[i]); }
      const OpenCLDevice& deviceConst( int i ) const {
         return _devices[i];
      }

      const std::string& infoName(cl_platform_info info) const {
         return _infoName[info];
      }

      bool cktPfmResourcesCreated() const { return _cktPfmResourcesCreated;}
      void cktPfmResourcesCreated(bool v) { _cktPfmResourcesCreated = v;}

      // Retrieve relevant platform info
      const std::string platformProfile() const {return _platformProfile;}
      const std::string platformVersion() const {return _platformVersion;}
      const std::string platformVendor() const {return _platformVendor;}
      const std::string platformExtensions() const {
         return _platformExtensions;
      }

   protected:
      bool query();

   private:
      // flag to indicate if the ocl platform is queried
      bool _isQueried;

      // flag to indicate of ocl mem objects and kernels created for a circuit
      bool _cktPfmResourcesCreated;

      // context associated with platform
      cl_context _context;

      // ocl program created and then built for all devices on the context
      cl_program _program;

      // kernels created out of ocl program
      std::vector< cl_kernel > _kernels;

      // cl memory objects for circuit; created out of context
      std::vector< cl_mem > _clMemsCkt;

      // cl memory objects for gmv values
      unsigned _maxGmvCopiesOnGPU;
      std::vector< cl_mem > _clMemsGmv0;
      std::vector< cl_mem > _clMemsGmv1;

      // cl memory for pattern data can hold multiple of 64 patterns
      unsigned _maxPatternCopies; // maximal pattern copies on GPU
      std::vector< cl_mem > _clMemsPatInGmv0;
      std::vector< cl_mem > _clMemsPatInGmv1;
      std::vector< cl_mem > _clMemsPatOutGmv0;
      std::vector< cl_mem > _clMemsPatOutGmv1;

      // platform id for the class
      cl_platform_id _platformId;

      // number of devices in a platform
      cl_uint _numDevices;

      // vector of device ids
      std::vector< cl_device_id > _deviceIds;

      // vector of devices
      std::vector< OpenCLDevice > _devices;

      // platform profile
      std::string _platformProfile;
      // platform version
      std::string _platformVersion;
      // platform vendor
      std::string _platformVendor;
      // platform extension
      std::string _platformExtensions;


      // map to translate CL_PLATFORM enums to string
      static std::map< cl_platform_info, std::string > _infoName;

};

///
// Simple trait class used to wrap base types.
//
template <typename T>
class ArrayType {
   public:
      static bool isChar() { return false; }
};

///
// Specialized for the char (i.e. null terminated string case).
//
template<>
class ArrayType<char> {
   public:
      static bool isChar() { return true; }
};

class OpenCLDevice {
   // friend class EvaluateCL;
   public:
      OpenCLDevice( OpenCLPlatform* parent, cl_device_id id );
      ~OpenCLDevice();

      // Query OpenCL device info/property
      bool query();

      // release device-related ocl resources
      bool releaseOclResources( bool for_delete_ckt );

      // create command queue for the device
      bool createCmdQueue( cl_context context,
                           cl_command_queue_properties properties );
      // release command queue for the device
      bool releaseCmdQueue();

      cl_command_queue queue() { return _clQueue; }
      cl_command_queue ioQueue() { return _ioQueue; }
      cl_device_id  deviceId() { return _clDeviceId; }


      // Print bitfield type
      const std::string printSpacer(int count) const ;
      void printFpConfig(
              cl_device_fp_config _singleFpConfig) const;
      void printGlobalMemCacheType(
              cl_device_mem_cache_type _globalMemCache) const;
      void printLocalMemType(
              cl_device_local_mem_type _localMemType ) const;
      void printExecCapabilities(
              cl_device_exec_capabilities _executionCapabilities ) const;
      void printQueueProperties(
              cl_command_queue_properties _queueProperties) const;

      template< typename T >
      void appendBitfield(T info,
                          T value,
                          std::string name,
                          std::string & str);

      bool display( bool summary ); // Print all device properties
      void printProperties( bool summary ) const;

      const std::string deviceTypeName( cl_device_type type ) const;
      const std::string& infoName( cl_device_info info ) const {
         return _infoName[info];
      }
      const std::string& globalMemoryCacheType( cl_uint type ) const {
         return _globalMemoryCacheType[type];
      }
      const std::string& localMemoryType( cl_uint type ) const {
         return _localMemoryType[type];
      }
      const std::string& fpConfigType( cl_bitfield type ) const {
         return _fpConfigType[type];
      }
      const std::string& queuePropertiesType( cl_bitfield type ) const {
         return _queuePropertiesType[type];
      }
      const std::string& execCapabilitiesType( cl_bitfield type ) const {
         return _execCapabilitiesType[type];
      }
      // Get device info
      // 0: success; 1: failure
      template< typename T >
      int getDeviceInfo( cl_device_info info, std::vector< T >& val );

      // Print single device property; 0:OK, 1:ERROR
      template< typename T >
      int display( cl_device_info name, std::string str, bool isArray=false );

      // User-specified parameters
      cl_uint globalWorkSizes( unsigned dim ) const {
         return _globalWorkSizes[ dim ];
      }
      void globalWorkSizes( const std::vector<int>& v) {
         _maxGlobalWorkSize = 1;
         for( cl_uint dim=0; dim < maxWorkItemsDimens(); dim++ ) {
            if( -1 != v[dim] ) { // skip if the value is -1
               int min = std::min( v[dim], (int)maxWorkItemSize(dim));
               _globalWorkSizes[dim] = min;
            }
            _maxGlobalWorkSize *= _globalWorkSizes[dim];
         }
      }
      cl_uint localWorkSizes( unsigned dim ) const {
         return _localWorkSizes[ dim ];
      }
      void localWorkSizes( const std::vector<int>& v ) {
         for( cl_uint dim=0; dim < maxWorkItemsDimens(); dim++ ) {
            if( -1 != v[dim] ) { // skip if the value is -1
               int min = std::min( v[dim], (int)maxWorkGroupSize());
               _localWorkSizes[dim] = min;
            }
         }
      }
      CktClMemType cktClMemType() const { return _cktClMemType; }
      std::string cktClMemTypeName() const;
      void cktClMemType( CktClMemType t ) { _cktClMemType = t; }

      // Computed parameters
      cl_uint maxGlobalWorkSize() const { return _maxGlobalWorkSize; }

      // Check if intended work size setting is valid
      bool validWorkSizeSetting( const std::vector<int>& gw_size,
                                 const std::vector<int>& lw_size ) const;
      void printUserPreferences() const;

      bool cktDevResourcesCreated() const { return _cktDevResourcesCreated; }
      void cktDevResourcesCreated(bool v) { _cktDevResourcesCreated = v; }

      // Retrieve relevant device info/properties
      cl_device_type  deviceType() const { return _deviceType; }
      const std::string deviceName() const { return _deviceName; }
      const std::string deviceVendor() const { return _deviceVendor; }
      const std::string driverVersion() const { return _driverVersion; }
      const std::string deviceProfile() const { return _deviceProfile; }
      const std::string deviceVersion() const { return _deviceVersion; }
      const std::string deviceOpenclCVersion() const {
         return _deviceOpenclCVersion;
      }
      cl_uint deviceVendorId () const { return _deviceVendorId; }
      cl_uint maxComputeUnits() const { return _maxComputeUnits; }
      cl_uint maxWorkItemsDimens() const { return _maxWorkItemsDimens; }
      size_t  maxWorkItemSize( cl_uint dimen ) const {
         assert( dimen < maxWorkItemsDimens() );
         return _maxWorkItemSizes[dimen];
      }
      cl_uint maxWorkGroupSize() const { return _maxWorkGroupSize; }
      // size_t
      cl_uint preferredVectorWidthChar() const {
         return _preferredVectorWidthChar;
      }
      cl_uint preferredVectorWidthShort() const {
         return _preferredVectorWidthShort;
      }
      cl_uint preferredVectorWidthInt() const {
         return _preferredVectorWidthInt;
      }
      cl_uint preferredVectorWidthLong() const {
         return _preferredVectorWidthLong;
      }
      cl_uint preferredVectorWidthFloat() const {
         return _preferredVectorWidthFloat;
      }
      cl_uint preferredVectorWidthDouble() const {
         return _preferredVectorWidthDouble;
      }
      cl_uint preferredVectorWidthHalf() const {
         return _preferredVectorWidthHalf;
      }
      cl_uint nativeVectorWidthChar() const { return _nativeVectorWidthChar; }
      cl_uint nativeVectorWidthShort() const{ return _nativeVectorWidthShort;}
      cl_uint nativeVectorWidthInt() const { return _nativeVectorWidthInt; }
      cl_uint nativeVectorWidthLong() const { return _nativeVectorWidthLong; }
      cl_uint nativeVectorWidthFloat() const{ return _nativeVectorWidthFloat;}
      cl_uint nativeVectorWidthDouble() const {
         return _nativeVectorWidthDouble;
      }
      cl_uint nativeVectorWidthHalf() const { return _nativeVectorWidthHalf; }
      cl_uint maxClockFrequency() const { return _maxClockFrequency; }
      cl_uint addressBits() const { return _addressBits; }
      cl_ulong maxMemAllocSize() const { return _maxMemAllocSize; }
      cl_bool imageSupport() const { return _imageSupport; }
      cl_uint maxReadImageArgs() const { return _maxReadImageArgs; }
      cl_uint maxWriteImageArgs() const { return _maxWriteImageArgs; }
      size_t image2dMaxWidth() const { return _image2dMaxWidth; }
      size_t image2dMaxHeight() const { return _image2dMaxHeight; }
      size_t image3dMaxWidth() const { return _image3dMaxWidth; }
      size_t image3dMaxHeight() const { return _image3dMaxHeight; }
      size_t image3dMaxDepth() const { return _image3dMaxDepth; }
      size_t imageMaxBufferSize() const { return _imageMaxBufferSize; }
      size_t imageMaxArraySize() const { return _imageMaxArraySize; }
      cl_uint maxSamplers() const { return _maxSamplers; }
      size_t maxParameterSize() const { return _maxParameterSize; }
      cl_uint memBaseAddrAlign() const { return _memBaseAddrAlign; }
      cl_uint minDataTypeAlignSize() const { return _minDataTypeAlignSize; }
      cl_device_fp_config singleFpConfig() const { return _singleFpConfig; }
      cl_device_mem_cache_type globalMemCacheType() const {
         return _globalMemCacheType;
      }
      cl_uint globalMemCachelineSize() const {return _globalMemCachelineSize;}
      cl_ulong globalMemCacheSize() const { return _globalMemCacheSize; }
      cl_ulong globalMemSize() const { return _globalMemSize; }
      cl_ulong maxConstantBufferSize() const { return _maxConstantBufferSize;}
      cl_uint maxConstantArgs() const { return _maxConstantArgs; }
      cl_device_local_mem_type localMemType() const { return _localMemType; }
      cl_ulong localMemSize() const { return _localMemSize; }
      cl_bool errorCorrectionSupport() const {return _errorCorrectionSupport;}
      cl_bool hostUnifiedMemory() const { return _hostUnifiedMemory; }
      size_t profilingTimerResolution() const {
         return _profilingTimerResolution;
      }
      cl_bool endianLittle() const { return _endianLittle; }
      cl_bool available() const { return _available; }
      cl_bool compilerAvailable() const { return _compilerAvailable; }
      const std::string deviceExtensions() const { return _deviceExtensions; }
      cl_command_queue_properties queueProperties() const {
         return _queueProperties;
      }
      cl_platform_id devicePlatform() const { return _devicePlatform; }
      cl_device_exec_capabilities executionCapablities() const {
         return _executionCapabilities;
      }

   protected:
      OpenCLPlatform* platform() { return _platform; }

   private:
      OpenCLPlatform* _platform;   // reference to parent platform
      cl_device_id  _clDeviceId;   // device id for the device
      cl_command_queue _clQueue;   // command queue for the device
      cl_command_queue _ioQueue;   // command queue for data transfer

      // flag to indicate of ocl data created for a circuit
      bool _cktDevResourcesCreated;

      // User-specified or computed parameters
      std::vector< cl_uint > _globalWorkSizes;
      std::vector< cl_uint > _localWorkSizes;
      cl_uint _maxGlobalWorkSize; // multiply _maxWorkItemSizes across dimen
      CktClMemType _cktClMemType;


      // Selected device properties
      cl_device_type _deviceType;
      std::string _deviceName;
      std::string _deviceVendor;
      std::string _driverVersion;
      std::string _deviceProfile;
      std::string _deviceVersion;
      std::string _deviceOpenclCVersion;
      cl_uint _deviceVendorId;
      cl_uint _maxComputeUnits;
      cl_uint _maxWorkItemsDimens;
      std::vector< size_t > _maxWorkItemSizes;
      cl_uint _maxWorkGroupSize;
      //std::vector< size_t > _maxWorkGropSize;
      cl_uint _preferredVectorWidthChar;
      cl_uint _preferredVectorWidthShort;
      cl_uint _preferredVectorWidthInt;
      cl_uint _preferredVectorWidthLong;
      cl_uint _preferredVectorWidthFloat;
      cl_uint _preferredVectorWidthDouble;
      cl_uint _preferredVectorWidthHalf;
      cl_uint _nativeVectorWidthChar;
      cl_uint _nativeVectorWidthShort;
      cl_uint _nativeVectorWidthInt;
      cl_uint _nativeVectorWidthLong;
      cl_uint _nativeVectorWidthFloat;
      cl_uint _nativeVectorWidthDouble;
      cl_uint _nativeVectorWidthHalf;
      cl_uint _maxClockFrequency;
      cl_uint _addressBits;
      cl_ulong _maxMemAllocSize;
      cl_bool _imageSupport;
      cl_uint _maxReadImageArgs;
      cl_uint _maxWriteImageArgs;
      size_t _image2dMaxWidth;
      size_t _image2dMaxHeight;
      size_t _image3dMaxWidth;
      size_t _image3dMaxHeight;
      size_t _image3dMaxDepth;

      cl_uint _minDataTypeAlignSize;
      cl_device_fp_config _singleFpConfig;
      cl_device_mem_cache_type _globalMemCacheType;
      cl_uint _globalMemCachelineSize;
      cl_ulong _globalMemCacheSize;
      size_t _imageMaxBufferSize;
      size_t _imageMaxArraySize;
      cl_uint _maxSamplers;
      size_t _maxParameterSize;
      cl_uint _memBaseAddrAlign;
      cl_ulong _globalMemSize;
      cl_ulong _maxConstantBufferSize;
      cl_uint _maxConstantArgs;
      cl_device_local_mem_type _localMemType;
      cl_ulong _localMemSize;

      cl_bool _errorCorrectionSupport;
      cl_bool _hostUnifiedMemory;

      size_t  _profilingTimerResolution;
      cl_bool _endianLittle;
      cl_bool _available;
      cl_bool _compilerAvailable;
      cl_device_exec_capabilities _executionCapabilities;
      cl_platform_id _devicePlatform;
      std::string _deviceExtensions;
      cl_command_queue_properties _queueProperties;

      // map cl_device_info to string
      static std::map< cl_device_info, std::string > _infoName;
      static std::map< cl_uint, std::string > _localMemoryType;
      static std::map< cl_uint, std::string > _globalMemoryCacheType;
      static std::map< cl_bitfield, std::string > _fpConfigType;
      static std::map< cl_bitfield, std::string > _queuePropertiesType;
      static std::map< cl_bitfield, std::string > _execCapabilitiesType;

};

#endif
