//////////////////////////////////////////////////////////////////
//
//                        PatternOnHost.h
//
//  Definition for pattern related classes
//
//////////////////////////////////////////////////////////////////

#ifndef _PATTERN_ON_HOST
#define _PATTERN_ON_HOST

#include <boost/dynamic_bitset.hpp>
#include "rapidjson/error/en.h"
#include "rapidjson/filereadstream.h"
#include "rapidjson/filewritestream.h"
#include "rapidjson/schema.h"
#include "rapidjson/stringbuffer.h"
#include "rapidjson/prettywriter.h"
#include "app-common/AppCommonSetting.h"

#ifdef __APPLE__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif


///////////////////////////////////////////////////////////
//       class to define pattern value per cycle
//  2 bits of (V0, V1) are used to code 4 values.
//    0: (0, 0)
//    Z: (0, 1)
//    X: (1, 0)
//    1: (1, 1)
///////////////////////////////////////////////////////////
class PatternCycle {
   public:
      PatternCycle( unsigned len_force_pi,
                    unsigned len_init_ff,
                    unsigned len_measure_po,
                    unsigned len_observe_ff,
                    const std::vector<std::string>& pulse_clk_pins );

      ~PatternCycle();

      void string2bitset( const std::string& str,
                          unsigned offset,
                          boost::dynamic_bitset<> & v0,
                          boost::dynamic_bitset<> & v1 );

      // Convert values obtained through parallel simulation into bitset
      void vector2bitset( uint64_t mask,
                          const std::vector< uint64_t >& s0,
                          const std::vector< uint64_t >& s1,
                          boost::dynamic_bitset<> & v0,
                          boost::dynamic_bitset<> & v1 );

      void vector2bitset( int pos,
                          uint64_t mask,
                          const std::vector< cl_ulong4 >& s0,
                          const std::vector< cl_ulong4 >& s1,
                          boost::dynamic_bitset<> & v0,
                          boost::dynamic_bitset<> & v1 );

      // Convert bitset to vectors that store input stimuli in parallel
      void bitset2vector( uint64_t mask,
                          std::vector<uint64_t> & vec0,
                          std::vector<uint64_t> & vec1 ) const;

      // Convert bitset to string of 0, 1 and X for easy printing
      void bitset2string( const boost::dynamic_bitset<> & v0,
                          const boost::dynamic_bitset<> & v1,
                          unsigned offset,
                          unsigned len,
                          std::string & str ) const;

      char getBitValue( unsigned n,
                        boost::dynamic_bitset<> v0,
                        boost::dynamic_bitset<> v1 ) const;

      void setBitValue( unsigned n,
                        boost::dynamic_bitset<> v0,
                        boost::dynamic_bitset<> v1,
                        char v );

      void setBitset( uint64_t mask,
                      const std::vector< uint64_t >& s0,
                      const std::vector< uint64_t >& s1 )
      {
         vector2bitset( mask, s0, s1, _v0, _v1);
      }

      void setBitset( int pos,
                      uint64_t mask,
                      const std::vector< cl_ulong4 >& s0,
                      const std::vector< cl_ulong4 >& s1 )
      {
         vector2bitset( pos, mask, s0, s1, _v0, _v1);
      }


      bool hasForcePI() const { return (_lenForcePI > 0); }

      void setForcePI( const std::string& str ) {
         string2bitset( str, _offsetFPI,  _v0, _v1 );
      }

      void setForcePI( unsigned n, char v ) {
         setBitValue( _offsetFPI + n, _v0, _v1, v );
      }

      void getForcePI( std::string& str ) const {
         bitset2string( _v0, _v1, _offsetFPI, _lenForcePI, str );
      }

      char getForcePI( unsigned n ) const {
         return getBitValue( _offsetFPI + n, _v0, _v1 );
      }


      bool hasMeasurePO() const { return (_lenMeasurePO > 0); }

      void setMeasurePO( const std::string& str ) {
         string2bitset( str, _offsetMPO, _v0, _v1);
      }

      void setMeasurePO( unsigned n, char v ) {
         setBitValue( _offsetMPO + n, _v0, _v1, v );
      }

      void getMeasurePO( std::string& str ) const {
         bitset2string( _v0, _v1, _offsetMPO, _lenMeasurePO, str );
      }

      char getMeasurePO( unsigned n ) const {
         return getBitValue( _offsetMPO + n, _v0, _v1 );
      }

      bool hasInitFF() const { return (_lenInitFF > 0); }

      void setInitFF( const std::string& str ) {
         string2bitset( str, _offsetIFF, _v0, _v1);
      }

      void setInitFF( unsigned n, char v ) {
         setBitValue( _offsetIFF + n, _v0, _v1, v );
      }

      void getInitFF( std::string& str ) const {
         bitset2string( _v0, _v1, _offsetIFF, _lenInitFF, str );
      }

      char getInitFF( unsigned n ) const {
         return getBitValue( _offsetIFF + n, _v0, _v1 );
      }

      bool hasObserveFF() const { return (_lenObserveFF > 0); }

      void setObserveFF( const std::string& str_observe_ff ) {
         string2bitset( str_observe_ff, _offsetOFF, _v0, _v1);
      }

      void setObserveFF( unsigned n, char v ) {
         setBitValue( _offsetOFF + n, _v0, _v1, v );
      }

      void getObserveFF( std::string& str ) const {
         bitset2string( _v0, _v1, _offsetOFF, _lenObserveFF, str );
      }

      char getObserveFF( unsigned n ) const {
         return getBitValue( _offsetOFF + n, _v0, _v1 );
      }

      bool hasPulseClk() const { return (_clkPinsToPulse.size()>0); }
      const std::vector< std::string > & clkPins() const {
         return _clkPinsToPulse;
      }

   private:
      boost::dynamic_bitset<> _v0; // value of all bitstreams in a cycle
      boost::dynamic_bitset<> _v1;
      unsigned _lenForcePI;
      unsigned _lenInitFF;
      unsigned _lenMeasurePO;
      unsigned _lenObserveFF;
      unsigned _offsetFPI;
      unsigned _offsetIFF;
      unsigned _offsetMPO;
      unsigned _offsetOFF;
      std::vector< std::string > _clkPinsToPulse;

};


class Pattern {
   public:
      Pattern();
      ~Pattern();

      void addCycle( const PatternCycle& c ) { _cycles.push_back( c ); }
      unsigned numCycles() const { return _cycles.size(); }
      const PatternCycle & cycle( unsigned c ) { return _cycles[c]; }

   private:
      std::vector< PatternCycle > _cycles;
};


//////////////////////////////////////////////////////////////////////
//    For now, fix the sequence of cycle timing template
//////////////////////////////////////////////////////////////////////
class CycleTimingTemplate {
   public:
      CycleTimingTemplate() :
         _timeForcePi( 0 ),
         _timeInitFF( 1 ),
         _timeMeasurePO( 2 ),
         _timePulseClk( 3 ),
         _timeObserveFF( 4 ) { ; }
      ~CycleTimingTemplate() { ;}
      unsigned timeForcePI() const { return _timeForcePi; }
      unsigned timeInitFF() const { return _timeInitFF; }
      unsigned timeMeasurePO() const { return _timeMeasurePO; }
      unsigned timePulseClk() const { return _timePulseClk; }
      unsigned timeObserveFF() const { return _timeObserveFF; }

   private:
      unsigned _timeForcePi;
      unsigned _timeInitFF;
      unsigned _timeMeasurePO;
      unsigned _timePulseClk;
      unsigned _timeObserveFF;
};


class PatternSet {
   public:

      PatternSet();
      ~PatternSet();

      // Read in patterns against schema
      bool readPatterns( const std::string& fname );
      // Write patterns in JSON container
      void writePatterns( std::ostream& os );
      // clear patterns
      void clearPatterns() { _patterns.clear(); }

      unsigned numPatterns() const { return _patterns.size(); }

      // For now, assume all patterns have the same sequential depth
      unsigned cyclesPerPattern() const { return _patterns[0].numCycles(); }
      bool isEmpty() const { return (_patterns.size()==0); }
      const std::string& version() const { return _version; }

      // Access design DB to get basic circuit properties
      int numDataPIs() const;
      int numPOs() const;
      int numFFs() const;
      bool isSequentialLogic() const;
      bool isCombinationalLogic() const;

      // Add a pattern with bitstream stored in (bits_v0_256, bits_v1_256),
      void addPattern( const std::vector<std::vector<cl_ulong4>>& bits_v0_256,
                       const std::vector<std::vector<cl_ulong4>>& bits_v1_256,
                       int pos, // word position within vector
                       uint64_t mask,
                       bool io_per_cycle );

      // Add a pattern with bitstream stored in (bits_v0, bits_v1),
      //   1-hot mask, for single-thread simulation on host
      void addPattern( const std::vector< std::vector<uint64_t> > & bits_v0,
                       const std::vector< std::vector<uint64_t> > & bits_v1,
                       uint64_t mask,
                       bool io_per_cycle );

      // Add a pattern with bitstream stored array *gmv0, *gmv1
      //   1-hot mask, for pattern data copied back from GPU
      void addPattern( const std::vector<uint64_t>& gmv0_in,
                       const std::vector<uint64_t>& gmv1_in,
                       const std::vector<uint64_t>& gmv0_out,
                       const std::vector<uint64_t>& gmv1_out,
                       uint64_t mask,
                       int n_cycles,
                       bool io_per_cycle );

      // Get parallel pattern stimuli for a given cycle and store it
      //   in (vec0, vec1)
      //   Assume that all parallel patterns has the same cycle behavior.
      void getPatternStimuli( int start,     // starting pattern number
                              int parallel,  // patterns simulated in parallel
                              int cycle,     // cycle number
                              // bitstream for force_pi or init_ff
                              std::vector<uint64_t>& vec0,
                              std::vector<uint64_t>& vec1,
                              // flag to indicate force_pi
                              bool & force_pi_is_on,
                              // pulsed clocks
                              std::vector<std::string>& clk_pins );



   protected:
      bool verifyAndLoadPatternFile( const std::string& fname );
      bool verifyPropertyInt( int ref_int,
                              int pat_int,
                              const std::string& property_name,
                              bool against_design = false );
      bool verifyPropertyString( const std::vector< std::string >& ref_str_vec,
                                 const std::string& pat_str,
                                 const std::string& property_name);
      bool verifyIndividualPattern( unsigned n, rapidjson::Document & d );
      void loadVerifiedPatterns( rapidjson::Document & d );
      const CycleTimingTemplate & cycleTiming() const { return _timing; }
      void addPattern( const Pattern& p ) { _patterns.push_back( p ); }

   private:
      PatternSet( PatternSet& pset ); // Disable copy constructor
      std::vector< Pattern > _patterns;
      CycleTimingTemplate _timing;
      std::string _version;
};

#endif
