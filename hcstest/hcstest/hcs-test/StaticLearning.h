////////////////////////////////////////////////////////////////////////
//                          StaticLearning.h
//
///////////////////////////////////////////////////////////////////////





//#ifndef _STATIC_LEARNING
//#define _STATIC_LEARNING

#include <stack>
#include <vector>

class StaticLearning {
   public:
      StaticLearning( int num_levels, int num_nodes );
      void init( int num_levels, int num_nodes );
      
      void clear()= 0;

      virtual void insert( int g, int level ) =0; // Insert gate into stack 

      virtual bool empty() const;
      virtual void dumpEvents() const ;

      virtual int curLevel() const ;
     GType logic( int id ) const { return ckt().gate(id).type(); }


};
//#endif
