#define GATHER_SUCCESSOR_GATES( GateId, \
                                Levels, \
                                Succ1st, \
                                Succs, \
                                QEFlags, \
                                QOffsets, \
                                QSizes, \
                                NumLevels, \
                                FirstNodeFF, \
                                ClockedDffIsUsed, \
                                CurrentLev ) \
{ \
   int succ_1st = Succ1st[ GateId ]; \
   int succ_1st_n = Succ1st[ GateId + 1 ]; \
   for( int i = succ_1st; i < succ_1st_n; i++) { \
      int sid = Succs[ i ]; \
      int succ_lev = Levels[ sid ]; \
      if( succ_lev != CurrentLev ) { \
         int adj_id = sid; \
         if( ClockedDffIsUsed && sid >= FirstNodeFF ) { \
            adj_id -= (sid - FirstNodeFF) % 5; \
         } \
         int old_size = atomic_inc( QSizes + NumLevels ); \
         QEFlags[ QOffsets[NumLevels] + old_size ] = adj_id; \
      } \
   } \
} \


#define ADD_SUCCESSOR_EVENTS( GateId, \
                              Levels, \
                              Succ1st, \
                              Succs, \
                              QEFlags, \
                              QOffsets, \
                              QSizes, \
                              NumLevels, \
                              FirstNodeFF, \
                              ClockedDffIsUsed, \
                              CurrentLev ) \
{ \
   int succ_1st = Succ1st[ GateId ]; \
   int succ_1st_n = Succ1st[ GateId + 1 ]; \
   for( int i = succ_1st; i < succ_1st_n; i++) { \
      int sid = Succs[ i ]; \
      int succ_lev = Levels[ sid ]; \
      if( succ_lev != CurrentLev ) { \
         int adj_id = sid; \
         if( ClockedDffIsUsed && sid >= FirstNodeFF ) { \
            adj_id -= (sid - FirstNodeFF) % 5; \
         } \
         /* update in-queue flag */ \
         unsigned old_flag = atomic_or( &(QEFlags[adj_id]), 0x80000000); \
         if( 0==(old_flag & 0x80000000)) { \
            int old_size = atomic_inc( &(QSizes[succ_lev]) ); \
            int offset = QOffsets[ succ_lev ]; \
            /* unsigned flag = QEFlags[offset + old_size] & 0x80000000;*/ \
            atomic_and( &(QEFlags[offset + old_size]), 0x80000000); \
            /* QEFlags[offset + old_size] = flag + (unsigned)adj_id;*/ \
            atomic_add( &(QEFlags[offset + old_size]), (unsigned)adj_id); \
            /* It can happen that offset + old_size == adj_id of another thread */ \
            /* causing race between mem-write and mem-read at the same address */ \
         } \
      } \
   } \
} \


/* Argument 'Levels' and 'CurrentLev' can be removed if DFF is primitive */
#define ADD_SUCCESSOR_EVENTS_FLAG( GateId, \
                                   Levels, \
                                   Succ1st, \
                                   Succs, \
                                   Flags, \
                                   QSizes, \
                                   FirstNodeFF, \
                                   ClockedDffIsUsed, \
                                   CurrentLev ) \
{ \
   /* printf("## inside ADD_SUCCESSOR_EVENTS_FLAG\n"); */ \
   int succ_1st = Succ1st[ GateId ]; \
   int succ_1st_n = Succ1st[ GateId + 1 ]; \
   for( int i = succ_1st; i < succ_1st_n; i++) { \
      int sid = Succs[ i ]; \
      int succ_lev = Levels[ sid ]; \
      /* do not add event if succ is in remodeled dff or TIE gate in lev 0*/ \
      if( succ_lev != CurrentLev && succ_lev != 0) { \
         int adj_id = sid; \
         if( ClockedDffIsUsed && sid >= FirstNodeFF ) { \
            adj_id -= (sid - FirstNodeFF) % 5; \
            /* printf("dff adjust id: %d added, gid:%d\n", adj_id+1, GateId+1);*/ \
         } \
         /* update in-queue flag */ \
         Flags[ adj_id ] = 1; \
         /* printf("adj_id %2d added L%d, %d\n", adj_id+1, succ_lev, Flags[adj_id]); */ \
         /* update Q sizes. 0: empty queue; 1: non-empty queue */ \
         /*   atomic_or( &(QSizes[succ_lev]), 0x1 ) */ \
         QSizes[ succ_lev ] = 1; \
      } \
   } \
} \


#define GSIM_BUF_TEMPLATE( GmvType, \
                           GateEvalFunction, \
                           Types,  \
                           Levels,  \
                           Pred1st,  \
                           Succ1st,  \
                           Preds,  \
                           Succs,  \
                           FirstFFNode,  \
                           ClockedDffIsUsed,  \
                           Gmv0,  \
                           Gmv1,  \
                           BaseNodeId,  \
                           PaddingBound )  \
{  \
   unsigned gid = compute_adjusted_flatten_global_id( BaseNodeId );  \
   if( gid > PaddingBound) return;  \
  \
   GmvType val0=0, val1=0;  \
/* uncomment to make segfault(i7-8650u) with vector mode disappear */ \
/* */ \
/*   return; */ \
/* */ \
/* uncomment to simplify segfault(i7-8650u) with vector mode simulation */ \
/* */ \
/*   Gmv0[gid] = val0;  */ \
/*   Gmv1[gid] = val1;  */ \
/*   return;  */ \
/* */ \
   if( ClockedDffIsUsed && gid >= FirstFFNode ) {  \
      unsigned ui = gid - FirstFFNode;  \
      unsigned gid_ff = FirstFFNode + (ui << 2) + ui;  \
      for( int i=0; i < 5; i++, gid_ff++ ) {  \
         GateEvalFunction( gid_ff, GmvType, Pred1st, Preds, Types,  \
                        Gmv0, Gmv1, val0, val1 );  \
         Gmv0[gid_ff] = val0;  \
         Gmv1[gid_ff] = val1;  \
      }  \
   } else {  \
      GateEvalFunction( gid, GmvType, Pred1st, Preds, Types,  \
                     Gmv0, Gmv1, val0, val1 );  \
      Gmv0[gid] = val0;  \
      Gmv1[gid] = val1;  \
   }  \
}  \



#define OCL_GATE_EVAL( Gate_Id,  \
                       GmvData, \
                       Pred1st, \
                       Preds, \
                       Types, \
                       Gmv0, \
                       Gmv1, \
                       Va_0, \
                       Va_1 ) \
{ \
   /* int pred_1st = Pred1st[Gate_Id]; */ \
   /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
   int type = Types[Gate_Id];  \
  \
   switch( type ) {  \
      case T_INPUT:  \
      {  \
         Va_0 = Gmv0[Gate_Id];  \
         Va_1 = Gmv1[Gate_Id];  \
         break;  \
      }  \
      case T_TIE1:  \
      {  \
         Va_0 = ALL_ONES_64;  \
         Va_1 = ALL_ONES_64;  \
         break;  \
      }  \
      case T_TIE0:  \
      {  \
         Va_0 = 0;  \
         Va_1 = 0;  \
         break;  \
      }  \
      case T_TIEX:  \
      {  \
         Va_0 = ALL_ONES_64;  \
         Va_1 = 0;  \
         break;  \
      }  \
      case T_TIEZ:  \
      {  \
         Va_0 = 0;  \
         Va_1 = ALL_ONES_64;  \
         break;  \
      }  \
      case T_OUTPUT:  \
      case T_DFF:  \
      case T_BUF:  \
      case T_BUF_FB: \
      {  \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         /*    int pred_id = Preds[i];*/  \
         /*    Va_0 = Gmv0[pred_id];*/  \
         /*    Va_1 = Gmv1[pred_id];*/  \
         /* }*/  \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id];  \
         Va_1 = Gmv1[pred_id];  \
         break;  \
      }  \
      case T_BUFZ: \
      { \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id];  \
         Va_1 = Gmv1[pred_id];  \
         GmvData xz_mask = Va_0 ^ Va_1; \
         /* set bits in xz_mask to X (1, 0) */ \
         Va_0 |= xz_mask; \
         Va_1 &= (~xz_mask); \
         break;  \
      } \
      case T_AND:  \
      {  \
         /* Va_1 = Va_0 = ALL_ONES_64;*/  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id]; \
         Va_1 = Gmv1[pred_id]; \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         for( int i=pred_1st+1; i<pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            Va_0 = Va_0 & Gmv0[pred_id];  \
            Va_1 = Va_1 & Gmv1[pred_id];  \
         }  \
         break;  \
      }  \
      case T_NAND:  \
      {  \
         /* Va_1 = Va_0 = ALL_ONES_64;*/  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id]; \
         Va_1 = Gmv1[pred_id]; \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         for( int i=pred_1st+1; i<pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            Va_0 &= Gmv0[pred_id];  \
            Va_1 &= Gmv1[pred_id];  \
         }  \
         GmvData v1 = Va_0;  \
         Va_0 = ALL_ONES_64 ^ Va_1;  \
         Va_1 = ALL_ONES_64 ^ v1;  \
         break;  \
      }  \
      case T_OR:  \
      {  \
         /* Va_1 = Va_0 = 0; */  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id]; \
         Va_1 = Gmv1[pred_id]; \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         for( int i=pred_1st+1; i<pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            Va_0 |= Gmv0[pred_id];  \
            Va_1 |= Gmv1[pred_id];  \
         }  \
         break;  \
      }  \
      case T_NOR:  \
      {  \
         /* Va_1 = Va_0 = 0; */ \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id]; \
         Va_1 = Gmv1[pred_id]; \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         for( int i=pred_1st+1; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Va_0 |= Gmv0[pred_id];  \
            Va_1 |= Gmv1[pred_id];  \
         }  \
         GmvData v1 = Va_0;  \
         Va_0 = ALL_ONES_64 ^ Va_1;  \
         Va_1 = ALL_ONES_64 ^ v1;  \
         break;  \
      }  \
      case T_NOT:  \
      {  \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         /*    int pred_id = Preds[i];*/  \
         /*    Va_0 = Gmv1[pred_id] ^ ALL_ONES_64;*/  \
         /*    Va_1 = Gmv0[pred_id] ^ ALL_ONES_64;*/  \
         /* }*/ \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_id = Preds[ pred_1st ];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1];*/  \
         Va_0 = Gmv1[pred_id] ^ ALL_ONES_64;  \
         Va_1 = Gmv0[pred_id] ^ ALL_ONES_64;  \
         break;  \
      }  \
      case T_XOR:  \
      {  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id];  \
         Va_1 = Gmv1[pred_id];  \
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            GmvData sval0 = Gmv0[ pred_id ]; \
            GmvData sval1 = Gmv1[ pred_id ]; \
            GmvData v1 = ALL_ONES_64 ^  \
               (((ALL_ONES_64 ^ sval0) & (ALL_ONES_64 ^ Va_0)) |  \
                ( sval1 & Va_1));  \
            Va_1 = ((ALL_ONES_64 ^ sval0) & Va_1) |  \
               (sval1 & (ALL_ONES_64 ^ Va_0));  \
            Va_0 = v1;  \
         }  \
         break;  \
      }  \
      case T_XNOR:  \
      {  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id];  \
         Va_1 = Gmv1[pred_id];  \
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            GmvData v1 = ALL_ONES_64 ^  \
               (((ALL_ONES_64^Gmv0[pred_id]) & (ALL_ONES_64 ^ Va_0)) |  \
                (Gmv1[pred_id] & Va_1));  \
            Va_1 = ((ALL_ONES_64 ^ Gmv0[pred_id]) & Va_1) |  \
               (Gmv1[pred_id] & (ALL_ONES_64 ^ Va_0));  \
            Va_0 = v1;  \
         }  \
         GmvData v1 = Va_0;  \
         Va_0 = ALL_ONES_64 ^ Va_1;  \
         Va_1 = ALL_ONES_64 ^ v1;  \
         break;  \
      }  \
      case T_BUS: {  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         Va_0 = 0;  \
         Va_1 = ALL_ONES_64;  \
         for( int i=pred_1st; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Va_0 |= Gmv0[pred_id];  \
            Va_1 &= Gmv1[pred_id];  \
         }  \
         GmvData z_mask = (~Va_0) & Va_1;  \
         Va_0 |= z_mask;  \
         Va_1 &= (~z_mask);  \
         break;  \
      }  \
      case T_TRISTATE: \
      case T_TRISTATE1: {  \
         Va_0 = ALL_ONES_64; /* init to Xs */ \
         Va_1 = 0; \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
         int gid_en = Preds[ pred_1st ]; \
         int gid_i = Preds[ pred_1st +1 ]; \
         /* en is 1; in passthrough */ \
         GmvData e1_mask = Gmv0[ gid_en ] & Gmv1[ gid_en];  \
         Va_0 = Gmv0[ gid_i ] & e1_mask;  \
         Va_1 = Gmv1[ gid_i ] & e1_mask;  \
         /* en is 0; output Z (0,1) */ \
         GmvData e0_mask = ~(Gmv0[ gid_en] | Gmv1[ gid_en] );  \
         Va_0 &= (~e0_mask);  \
         Va_1 |= e0_mask;  \
         /* remaining bits take X (initial values) */\
         break;  \
      }  \
      case T_MUX_2:  \
      {  \
         /* Va_0 = ALL_ONES_64;*/  \
         /* Va_1 = 0;*/  \
         /* GmvData s0_mask = ~(Gmv0[ Preds[ pred_1st]] |*/  \
         /*                  Gmv1[ Preds[ pred_1st]] );*/  \
         /* GmvData dq_mask = ~((Gmv0[ Preds[ pred_1st+1]] ^*/  \
         /*                    Gmv0[ Preds[ pred_1st+2]] ) |*/  \
         /*                   (Gmv1[ Preds[ pred_1st+1]] ^*/  \
         /*                    Gmv1[ Preds[ pred_1st+2]] ));*/  \
         /* GmvData s0_dq_mask = s0_mask | dq_mask;*/  \
         /* Va_0 &= ((Gmv0[ Preds[ pred_1st+1]] & s0_dq_mask) | ~s0_dq_mask);*/  \
         /* Va_1 |= (Gmv1[ Preds[ pred_1st+1]] & s0_dq_mask);*/  \
         /* GmvData s1_mask = Gmv0[ Preds[pred_1st]] &*/  \
         /*                Gmv1[ Preds[pred_1st]] ;*/  \
         /* Va_0 &= ((Gmv0[ Preds[ pred_1st+2]] & s1_mask) | ~s1_mask);*/  \
         /* Va_1 |= (Gmv1[ Preds[ pred_1st+2]] & s1_mask);*/  \
         /* break;*/  \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
         int gid_sel = Preds[ pred_1st ];  \
         /* int gid_d0 = 0; */ \
         /* int gid_d1 = 0; */ \
         int gid_d0 = Preds[ pred_1st+1 ];  \
         int gid_d1 = Preds[ pred_1st+2 ];  \
         GmvData val0_sel = Gmv0[ gid_sel ]; \
         GmvData val1_sel = Gmv1[ gid_sel ]; \
         /* assign d0 to output if sel is all 0's */ \
         /* As s0/1_mask can be vector type, use all() to determine */ \
         /*   all scalar components match all-1s. */ \
         GmvData  s0_mask = ~(val0_sel | val1_sel); \
         if( s0_mask == ALL_ONES_64 ) {  \
            /* gid_d0 = Preds[ pred_1st+1 ];*/  \
            Va_0 = Gmv0[ gid_d0 ]; \
            Va_1 = Gmv1[ gid_d0 ]; \
            break; \
         } \
         /* assign d1 to output if sel is all 1's */ \
         GmvData s1_mask = val0_sel & val1_sel; \
         /* vector comparison return -1 for true; scalar 1 for true */ \
         if( s1_mask == ALL_ONES_64 ) {  \
            /* gid_d1 = Preds[ pred_1st+2 ]; */  \
            Va_0 = Gmv0[ gid_d1 ]; \
            Va_1 = Gmv1[ gid_d1 ]; \
            break; \
         } \
         /* s1: selector is 1; s0: selector is 0; dq: d0==d1 */ \
         /* start with Xs (1,0) */ \
         Va_0 = ALL_ONES_64; \
         Va_1 = 0; \
         GmvData val0_d0 = Gmv0[ gid_d0 ]; \
         GmvData val1_d0 = Gmv1[ gid_d0 ]; \
         GmvData val0_d1 = Gmv0[ gid_d1 ]; \
         GmvData val1_d1 = Gmv1[ gid_d1 ]; \
         /* pass d0 for sel==0 and d0==d1 */ \
         GmvData dq_mask = ~((val0_d0 ^ val0_d1) | (val1_d0 ^ val1_d1)); \
         GmvData s0_dq_mask = s0_mask | dq_mask; \
         Va_0 = Va_0 & ((val0_d0 & s0_dq_mask) | ~s0_dq_mask); \
         Va_1 = Va_1 | (val1_d0 & s0_dq_mask); \
         /* pass d1 for sel==1 */ \
         Va_0 = Va_0 & ((val0_d1 & s1_mask) | ~s1_mask); \
         Va_1 = Va_1 | (val1_d1 & s1_mask); \
         break; \
      }  \
      case T_DFF_L:  \
      {  \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1];*/ \
         int gid_din = Preds[pred_1st+3];  \
         Va_0 = Gmv0[ gid_din ];  \
         Va_1 = Gmv1[ gid_din ];  \
         break;  \
      }  \
      /* assume port set, reset, clock and data */ \
      case T_NAND_LATCH: \
      { \
         int pred_1st = Pred1st[Gate_Id]; \
         int gid_clk = Preds[pred_1st+2]; \
         int gid_din = Preds[pred_1st+3]; \
         GmvData ck1_mask = Gmv0[ gid_clk ] & Gmv1[ gid_clk ]; \
         GmvData ckx_mask = Gmv0[ gid_clk ] ^ Gmv1[ gid_clk ]; \
         /* uint64_t ck0_mask = ~(Gmv0[ gid_clk ] | Gmv1[ gid_clk ]); */ \
         GmvData in0 = Gmv0[ gid_din ]; \
         GmvData in1 = Gmv1[ gid_din ]; \
         /* previous value */ \
         Va_0 = Gmv0[ Gate_Id ]; \
         Va_1 = Gmv1[ Gate_Id ]; \
         /* pass through data with ck1_mask */ \
         Va_0 = (Va_0 & (~ck1_mask)) | (in0 & ck1_mask); \
         Va_1 = (Va_1 & (~ck1_mask)) | (in1 & ck1_mask); \
         /* set output as X-(1,0) with ckx_mask */ \
         Va_0 = Va_0 | ckx_mask; \
         Va_1 = Va_1 & (~ckx_mask); \
         /* remaining bits of ck0_mask are kept */ \
         break; \
      } \
      default:  \
      {  \
         break;  \
      }  \
   }  \
} \


#define OCL_GATE_EVAL_VEC( Gate_Id,  \
                           GmvVecData, \
                           Pred1st, \
                           Preds, \
                           Types, \
                           Gmv0, \
                           Gmv1, \
                           Va_0, \
                           Va_1 ) \
{ \
   /* int pred_1st = Pred1st[Gate_Id]; */ \
   /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
   int type = Types[Gate_Id];  \
  \
   switch( type ) {  \
      case T_INPUT:  \
      {  \
         Va_0 = Gmv0[Gate_Id];  \
         Va_1 = Gmv1[Gate_Id];  \
         break;  \
      }  \
      case T_TIE1:  \
      {  \
         Va_0 = ALL_ONES_64;  \
         Va_1 = ALL_ONES_64;  \
         break;  \
      }  \
      case T_TIE0:  \
      {  \
         Va_0 = 0;  \
         Va_1 = 0;  \
         break;  \
      }  \
      case T_TIEX:  \
      {  \
         Va_0 = ALL_ONES_64;  \
         Va_1 = 0;  \
         break;  \
      }  \
      case T_TIEZ:  \
      {  \
         Va_0 = 0;  \
         Va_1 = ALL_ONES_64;  \
         break;  \
      }  \
      case T_OUTPUT:  \
      case T_DFF:  \
      case T_BUF:  \
      case T_BUF_FB: \
      {  \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         /*    int pred_id = Preds[i];*/  \
         /*    Va_0 = Gmv0[pred_id];*/  \
         /*    Va_1 = Gmv1[pred_id];*/  \
         /* }*/  \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id];  \
         Va_1 = Gmv1[pred_id];  \
         break;  \
      }  \
      case T_BUFZ: \
      { \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id];  \
         Va_1 = Gmv1[pred_id];  \
         GmvVecData xz_mask = Va_0 ^ Va_1; \
         /* set bits in xz_mask to X (1, 0) */ \
         Va_0 |= xz_mask; \
         Va_1 &= (~xz_mask); \
         break;  \
      } \
      case T_AND:  \
      {  \
         /* Va_1 = Va_0 = ALL_ONES_64;*/  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id]; \
         Va_1 = Gmv1[pred_id]; \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         for( int i=pred_1st+1; i<pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            Va_0 &= Gmv0[pred_id];  \
            Va_1 &= Gmv1[pred_id];  \
         }  \
         break;  \
      }  \
      case T_NAND:  \
      {  \
         /* Va_1 = Va_0 = ALL_ONES_64;*/  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id]; \
         Va_1 = Gmv1[pred_id]; \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         for( int i=pred_1st+1; i<pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            Va_0 &= Gmv0[pred_id];  \
            Va_1 &= Gmv1[pred_id];  \
         }  \
         GmvVecData v1 = Va_0;  \
         Va_0 = ALL_ONES_64 ^ Va_1;  \
         Va_1 = ALL_ONES_64 ^ v1;  \
         break;  \
      }  \
      case T_OR:  \
      {  \
         /* Va_1 = Va_0 = 0; */  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id]; \
         Va_1 = Gmv1[pred_id]; \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         for( int i=pred_1st+1; i<pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            Va_0 |= Gmv0[pred_id];  \
            Va_1 |= Gmv1[pred_id];  \
         }  \
         break;  \
      }  \
      case T_NOR:  \
      {  \
         /* Va_1 = Va_0 = 0; */ \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id]; \
         Va_1 = Gmv1[pred_id]; \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         for( int i=pred_1st+1; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Va_0 |= Gmv0[pred_id];  \
            Va_1 |= Gmv1[pred_id];  \
         }  \
         GmvVecData v1 = Va_0;  \
         Va_0 = ALL_ONES_64 ^ Va_1;  \
         Va_1 = ALL_ONES_64 ^ v1;  \
         break;  \
      }  \
      case T_NOT:  \
      {  \
         /* for( int i=pred_1st; i<pred_1st_n; i++ ) {*/  \
         /*    int pred_id = Preds[i];*/  \
         /*    Va_0 = Gmv1[pred_id] ^ ALL_ONES_64;*/  \
         /*    Va_1 = Gmv0[pred_id] ^ ALL_ONES_64;*/  \
         /* }*/ \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_id = Preds[ pred_1st ];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1];*/  \
         Va_0 = Gmv1[pred_id] ^ ALL_ONES_64;  \
         Va_1 = Gmv0[pred_id] ^ ALL_ONES_64;  \
         break;  \
      }  \
      case T_XOR:  \
      {  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id];  \
         Va_1 = Gmv1[pred_id];  \
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            GmvVecData v1 = ALL_ONES_64 ^  \
               (((ALL_ONES_64^Gmv0[pred_id]) & (ALL_ONES_64 ^ Va_0)) |  \
                (Gmv1[pred_id] & Va_1));  \
            Va_1 = ((ALL_ONES_64 ^ Gmv0[pred_id]) & Va_1) |  \
               (Gmv1[pred_id] & (ALL_ONES_64 ^ Va_0));  \
            Va_0 = v1;  \
         }  \
         break;  \
      }  \
      case T_XNOR:  \
      {  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         int pred_id = Preds[ pred_1st ];  \
         Va_0 = Gmv0[pred_id];  \
         Va_1 = Gmv1[pred_id];  \
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {  \
            pred_id = Preds[i];  \
            GmvVecData v1 = ALL_ONES_64 ^  \
               (((ALL_ONES_64^Gmv0[pred_id]) & (ALL_ONES_64 ^ Va_0)) |  \
                (Gmv1[pred_id] & Va_1));  \
            Va_1 = ((ALL_ONES_64 ^ Gmv0[pred_id]) & Va_1) |  \
               (Gmv1[pred_id] & (ALL_ONES_64 ^ Va_0));  \
            Va_0 = v1;  \
         }  \
         GmvVecData v1 = Va_0;  \
         Va_0 = ALL_ONES_64 ^ Va_1;  \
         Va_1 = ALL_ONES_64 ^ v1;  \
         break;  \
      }  \
      case T_BUS: {  \
         int pred_1st = Pred1st[Gate_Id];  \
         int pred_1st_n = Pred1st[Gate_Id+1];  \
         Va_0 = 0;  \
         Va_1 = ALL_ONES_64;  \
         for( int i=pred_1st; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Va_0 |= Gmv0[pred_id];  \
            Va_1 &= Gmv1[pred_id];  \
         }  \
         GmvVecData z_mask = (~Va_0) & Va_1;  \
         Va_0 |= z_mask;  \
         Va_1 &= (~z_mask);  \
         break;  \
      }  \
      case T_TRISTATE: \
      case T_TRISTATE1: {  \
         Va_0 = ALL_ONES_64; /* init to Xs */ \
         Va_1 = 0; \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
         int gid_en = Preds[ pred_1st ]; \
         int gid_i = Preds[ pred_1st +1 ]; \
         /* en is 1; in passthrough */ \
         GmvVecData e1_mask = Gmv0[ gid_en ] & Gmv1[ gid_en];  \
         Va_0 = Gmv0[ gid_i ] & e1_mask;  \
         Va_1 = Gmv1[ gid_i ] & e1_mask;  \
         /* en is 0; output Z (0,1) */ \
         GmvVecData e0_mask = ~(Gmv0[ gid_en] | Gmv1[ gid_en] );  \
         Va_0 &= (~e0_mask);  \
         Va_1 |= e0_mask;  \
         /* remaining bits take X (initial values).. */\
         break;  \
      }  \
      case T_MUX_2:  \
      {  \
         /* Va_0 = ALL_ONES_64;*/  \
         /* Va_1 = 0;*/  \
         /* GmvVecData s0_mask = ~(Gmv0[ Preds[ pred_1st]] |*/  \
         /*                  Gmv1[ Preds[ pred_1st]] );*/  \
         /* GmvVecData dq_mask = ~((Gmv0[ Preds[ pred_1st+1]] ^*/  \
         /*                    Gmv0[ Preds[ pred_1st+2]] ) |*/  \
         /*                   (Gmv1[ Preds[ pred_1st+1]] ^*/  \
         /*                    Gmv1[ Preds[ pred_1st+2]] ));*/  \
         /* GmvVecData s0_dq_mask = s0_mask | dq_mask;*/  \
         /* Va_0 &= ((Gmv0[ Preds[ pred_1st+1]] & s0_dq_mask) | ~s0_dq_mask);*/  \
         /* Va_1 |= (Gmv1[ Preds[ pred_1st+1]] & s0_dq_mask);*/  \
         /* GmvVecData s1_mask = Gmv0[ Preds[pred_1st]] &*/  \
         /*                Gmv1[ Preds[pred_1st]] ;*/  \
         /* Va_0 &= ((Gmv0[ Preds[ pred_1st+2]] & s1_mask) | ~s1_mask);*/  \
         /* Va_1 |= (Gmv1[ Preds[ pred_1st+2]] & s1_mask);*/  \
         /* break;*/  \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1]; */  \
         int gid_sel = Preds[ pred_1st ];  \
         /* int gid_d0 = 0; */ \
         /* int gid_d1 = 0; */ \
         int gid_d0 = Preds[ pred_1st+1 ];  \
         int gid_d1 = Preds[ pred_1st+2 ];  \
         GmvVecData val0_sel = Gmv0[ gid_sel ]; \
         GmvVecData val1_sel = Gmv1[ gid_sel ]; \
         /* assign d0 to output if sel is all 0's */ \
         /* As s0/1_mask can be vector type, use all() to determine */ \
         /*   all scalar components match all-1s. */ \
         GmvVecData  s0_mask = ~(val0_sel | val1_sel); \
         if( all( s0_mask == ALL_ONES_64)) {  \
            /* gid_d0 = Preds[ pred_1st+1 ];*/  \
            Va_0 = Gmv0[ gid_d0 ]; \
            Va_1 = Gmv1[ gid_d0 ]; \
            break; \
         } \
         /* assign d1 to output if sel is all 1's */ \
         GmvVecData s1_mask = val0_sel & val1_sel; \
         /* vector comparison return -1 for true; scalar 1 for true */ \
         if( all( s1_mask == ALL_ONES_64)) {  \
            /* gid_d1 = Preds[ pred_1st+2 ]; */  \
            Va_0 = Gmv0[ gid_d1 ]; \
            Va_1 = Gmv1[ gid_d1 ]; \
            break; \
         } \
         /* s1: selector is 1; s0: selector is 0; dq: d0==d1 */ \
         /* start with Xs (1,0) */ \
         Va_0 = ALL_ONES_64; \
         Va_1 = 0; \
         GmvVecData val0_d0 = Gmv0[ gid_d0 ]; \
         GmvVecData val1_d0 = Gmv1[ gid_d0 ]; \
         GmvVecData val0_d1 = Gmv0[ gid_d1 ]; \
         GmvVecData val1_d1 = Gmv1[ gid_d1 ]; \
         /* pass d0 for sel==0 and d0==d1 */ \
         GmvVecData dq_mask = ~((val0_d0 ^ val0_d1) | (val1_d0 ^ val1_d1)); \
         GmvVecData s0_dq_mask = s0_mask | dq_mask; \
         Va_0 = Va_0 & ((val0_d0 & s0_dq_mask) | ~s0_dq_mask); \
         Va_1 = Va_1 | (val1_d0 & s0_dq_mask); \
         /* pass d1 for sel==1 */ \
         Va_0 = Va_0 & ((val0_d1 & s1_mask) | ~s1_mask); \
         Va_1 = Va_1 | (val1_d1 & s1_mask); \
         break; \
      }  \
      case T_DFF_L:  \
      {  \
         int pred_1st = Pred1st[Gate_Id];  \
         /* int pred_1st_n = Pred1st[Gate_Id+1];*/ \
         int gid_din = Preds[pred_1st+3];  \
         Va_0 = Gmv0[ gid_din ];  \
         Va_1 = Gmv1[ gid_din ];  \
         break;  \
      }  \
      /* assume port set, reset, clock and data */ \
      case T_NAND_LATCH: \
      { \
         int pred_1st = Pred1st[Gate_Id]; \
         int gid_clk = Preds[pred_1st+2]; \
         int gid_din = Preds[pred_1st+3]; \
         GmvVecData ck1_mask = Gmv0[ gid_clk ] & Gmv1[ gid_clk ]; \
         GmvVecData ckx_mask = Gmv0[ gid_clk ] ^ Gmv1[ gid_clk ]; \
         /* uint64_t ck0_mask = ~(Gmv0[ gid_clk ] | Gmv1[ gid_clk ]); */ \
         GmvVecData in0 = Gmv0[ gid_din ]; \
         GmvVecData in1 = Gmv1[ gid_din ]; \
         /* previous value */ \
         Va_0 = Gmv0[ Gate_Id ]; \
         Va_1 = Gmv1[ Gate_Id ]; \
         /* pass through data with ck1_mask */ \
         Va_0 = (Va_0 & (~ck1_mask)) | (in0 & ck1_mask); \
         Va_1 = (Va_1 & (~ck1_mask)) | (in1 & ck1_mask); \
         /* set output as X-(1,0) with ckx_mask */ \
         Va_0 = Va_0 | ckx_mask; \
         Va_1 = Va_1 & (~ckx_mask); \
         /* remaining bits of ck0_mask are kept */ \
         break; \
      } \
      default:  \
      {  \
         break;  \
      }  \
   }  \
} \
