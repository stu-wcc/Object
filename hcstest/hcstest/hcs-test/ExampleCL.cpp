/////////////////////////////////////////////////////////////////
//                    ExampleCL.cpp
/////////////////////////////////////////////////////////////////
#include "ExampleCL.h"
#include "HtSetting.h"
#include "OpenCLInfo.h"

bool test_ocl_kernel_execution() {

   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   cl_context context = ocl_info.platform(0).context();

   size_t array_size = 100;
   int buf_size = (int)array_size; // use as parameter to kernel
   std::vector< cl_int > bufSrc( array_size );
   std::vector< cl_int > bufDes( array_size );
   std::vector< cl_int > result( array_size );
   // iniitialize buf
   for( size_t i=0; i<array_size; i++ ) {
      bufSrc[i] = i * 4;
      bufDes[i] = 0;
   }
   cl_mem buf_src = NULL;
   cl_mem buf_des = NULL;
   cl_int errNum = 0;
   size_t cl_buf_size = array_size * sizeof(cl_int);
   // int* host_buf_ptr = new int[ cl_buf_size / sizeof(int) ];
   buf_src = clCreateBuffer(context,
                            CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                            cl_buf_size,
                            &(bufSrc[0]),
                            &errNum );
   if( oclFunctionFailed(errNum, "fail to create buf_src.")) return false;

   buf_des = clCreateBuffer(context,
                            CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                            cl_buf_size,
                            &(bufDes[0]),
                            &errNum );
   if( oclFunctionFailed(errNum, "fail to create buf_des.")) return false;

   cl_int2 image_size;
   image_size.s[0] = 16; // image width
   image_size.s[1] = 16; // image height
   //cl_int image_width = 10;
   //cl_int image_height = 10;
   cl_uint pitch_alignment = 128; // 512;
   int intermediate_image_pitch =
      pitch_alignment * (1 + ((image_size.s[0] - 1) / pitch_alignment));
   HLog << "intermediate_image_pitch: " << intermediate_image_pitch
        << std::endl;
   // Declare cl image format
   cl_image_format clImageFormat;
   clImageFormat.image_channel_order = CL_RGBA;
   // other channel data type: CL_UNSIGNED_INT32, CL_UNORM_INT8;
   clImageFormat.image_channel_data_type = CL_SIGNED_INT32;

   // Declare cl image description
   cl_image_desc clImageDesc;
   clImageDesc.image_type = CL_MEM_OBJECT_IMAGE2D;
   clImageDesc.image_width = image_size.s[0];
   clImageDesc.image_height = image_size.s[1];
   //clImageDesc.image_depth = 1;
   //clImageDesc.image_array_size = 1;
   clImageDesc.image_row_pitch = intermediate_image_pitch * sizeof(cl_int4);
   clImageDesc.image_slice_pitch = 0;
   clImageDesc.num_mip_levels = 0;
   clImageDesc.num_samples = 0;
   clImageDesc.buffer = NULL;

   //clImageDesc.mem_object = cl_intermediate_buffer;
   //clImageDesc.image_slice_pitch = 0;
   //clImageDesc.num_mip_levels = 0;
   //clImageDesc.num_samples = 0;
   //clImageDesc.buffer = NULL;
   std::vector< cl_int4 > cl_image_io_buffer( image_size.s[0]*image_size.s[1]);

   cl_mem ckt_image = NULL;
   HLog << "Before create image, context: " << context
        << ", device:" << ocl_info.platform(0).device(0).deviceId()
        << std::endl;

   ckt_image = clCreateImage(context,
                            CL_MEM_READ_WRITE,
 // | CL_MEM_COPY_HOST_PTR, // | CL_MEM_HOST_NO_ACCESS,
                            &clImageFormat,
                            &clImageDesc,
                            &(cl_image_io_buffer[0]), // host_buffer_ptr,
                                  // cl_intermediate_buffer,//NULL has problem
                            &errNum);

   /*
   ckt_image = clCreateImage2D(context,
                               CL_MEM_READ_WRITE,
                               &clImageFormat,
                               (size_t)image_size.s[0],
                               (size_t)image_size.s[1],
                               intermediate_image_pitch * sizeof(cl_int4),
                               cl_image_io_buffer, // NULL,
                               &errNum );
   */
   if( oclFunctionFailed(errNum, "fail to create ckt_image.")) return false;

   // create cl sampler
   cl_sampler sampler = NULL;
   sampler = clCreateSampler(context,
                             CL_FALSE, // Non-normalized coordinates
                             CL_ADDRESS_CLAMP_TO_EDGE,
                             CL_FILTER_NEAREST,
                             &errNum);
   if( oclFunctionFailed(errNum, "fail to create sampler.")) return false;

   // Create OpenCL program from Example.cl kernel source
   cl_program program = NULL;
   std::string exec_path = (appCommonDB::instance())->execPath();
   std::string file_path = exec_path + "Example.cl";
   HLog << "CL file: " << file_path << std::endl;
   std::ifstream cl_file( file_path, std::ios::in);
   if( !cl_file.is_open() ) {
      HLog << "failed to open " << file_path << std::endl;
      return false;
   }
   std::ostringstream oss;
   oss << cl_file.rdbuf();
   std::string srcStdStr = oss.str();
   const char* srcStr = srcStdStr.c_str();
   //program = CreateProgram(context, device, "Evaluate.cl");
   program = clCreateProgramWithSource(context,
                                       1,
                                       (const char**)&srcStr,
                                       NULL,
                                       &errNum );
   if( oclFunctionFailed(errNum, "fail to create program.")) return false;

   cl_device_id device = ocl_info.platform(0).device(0).deviceId();
   // Build program on specified device
   // errNum = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
   // Build the program for all devices on the context
   errNum = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
   if( oclFunctionFailed(errNum, "fail to build program.")) {
      oclDumpProgramBuildLog( program, device );
      clReleaseProgram(program);
      return false;
   }

   cl_kernel kn_init_ckt = NULL;
   cl_kernel kn_read_ckt = NULL;

   kn_init_ckt = clCreateKernel( program, "kernel_init_ckt", &errNum );
   if( oclFunctionFailed(errNum,"fail to create kernel_init_ckt.")) {
      return false;
   }

   errNum  = clSetKernelArg(kn_init_ckt, 0, sizeof(cl_mem), &ckt_image);
   errNum |= clSetKernelArg(kn_init_ckt, 1, sizeof(cl_mem), &buf_src);
   errNum |= clSetKernelArg(kn_init_ckt, 2, sizeof(int), &buf_size);
   errNum |= clSetKernelArg(kn_init_ckt, 3, sizeof(cl_int2), &image_size);
   if( oclFunctionFailed(errNum,"fail to set args for kernel_init_ckt.")) {
      return false;
   }

   kn_read_ckt = clCreateKernel( program, "kernel_read_ckt", &errNum );
   if( oclFunctionFailed(errNum,"fail to create kernel_read_ckt.")) {
      return false;
   }

   errNum  = clSetKernelArg(kn_read_ckt, 0, sizeof(cl_mem), &ckt_image);
   errNum |= clSetKernelArg(kn_read_ckt, 1, sizeof(cl_sampler), &sampler);
   errNum |= clSetKernelArg(kn_read_ckt, 2, sizeof(cl_mem), &buf_des);
   errNum |= clSetKernelArg(kn_read_ckt, 3, sizeof(int), &buf_size);
   if( oclFunctionFailed(errNum,"fail to set args for kernel_read_ckt.")) {
      return false;
   }

   cl_kernel kn_buf_trans = NULL;

   kn_buf_trans = clCreateKernel( program, "kernel_buf_trans", &errNum );
   if( oclFunctionFailed(errNum,"fail to create kernel_buf_trans.")) {
      return false;
   }

   errNum |= clSetKernelArg(kn_buf_trans, 0, sizeof(cl_mem), &buf_src);
   errNum |= clSetKernelArg(kn_buf_trans, 1, sizeof(cl_mem), &buf_des);
   errNum |= clSetKernelArg(kn_buf_trans, 2, sizeof(int), &buf_size);
   if( oclFunctionFailed(errNum,"fail to set args for kernel_buf_trans.")) {
      return false;
   }

   //size_t globalWorkSize[1] = { array_size };
   size_t globalWorkSize[1] = { 128 };
   size_t localWorkSize[1] = { 1 };

   cl_command_queue queue = ocl_info.platform(0).device(0).queue();


   // get kernel work group size
   size_t kernel_work_group_size=0;;
   errNum = clGetKernelWorkGroupInfo(kn_init_ckt, device,
      CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t),
      &kernel_work_group_size, NULL);
   HLog << "kn_init_ckt, kernel_work_group_size: " << kernel_work_group_size
        << " status: " << oclErrorString(errNum)
        << std::endl;

   cl_event event_init_ckt;
   errNum = clEnqueueNDRangeKernel(
                      queue, kn_init_ckt, 1, NULL,
                      globalWorkSize, localWorkSize,
                      0, // 1, // num_events_in_wait_list
                      NULL, // &eventBufferToImage, cl_event* event_wait_list
                      &event_init_ckt );
   if( oclFunctionFailed(errNum,"fail to enqueue kernel_init_ckt.")) {
      return false;
   }

   /*
   errNum = clEnqueueNDRangeKernel(
                      queue, kn_read_ckt, 1, NULL,
                      globalWorkSize, localWorkSize,
                      0, // 1, // num_events_in_wait_list
                      NULL, // &event_init_ckt, // cl_event* event_wait_list
                      NULL);
   */
   errNum = clEnqueueNDRangeKernel(
                      queue, kn_read_ckt, 1, NULL,
                      globalWorkSize, localWorkSize,
                      1, // num_events_in_wait_list
                      &event_init_ckt, // cl_event* event_wait_list
                      NULL);
   if( oclFunctionFailed(errNum,"fail to enqueue kernel_read_ckt.")) {
      return false;
   }
   /*
   cl_event event_buf_trans;
   errNum = clEnqueueNDRangeKernel(
                      queue, kn_buf_trans, 1, NULL,
                      globalWorkSize, localWorkSize,
                      0, // 1, // num_events_in_wait_list
                      NULL, // &eventBufferToImage, cl_event* event_wait_list
                      &event_buf_trans );
   if( oclFunctionFailed(errNum,"fail to enqueue kernel_buf_trans.")) {
      return false;
   }
   */
   // Read the output buffer back to the Host
   errNum = clEnqueueReadBuffer( queue, buf_des, CL_TRUE,
                                 0, array_size * sizeof(cl_int), &(result[0]),
                                 0, NULL, NULL);
   if( oclFunctionFailed(errNum,"fail to enqueue read-buffer.")) {
      return false;
   }

   // Output the result buffer
   for( size_t i=0; i<array_size; i++ ) {
      HLog << result[i] << " ";
   }
   HLog << std::endl;
   HLog << "Test OpenCL successfully." << std::endl;
   return true;
}

///
//  Cleanup any created OpenCL resources
/*
void Cleanup(cl_context context, cl_command_queue commandQueue,
             cl_program program, cl_kernel kernel, cl_mem memObjects[3])
{
    for (int i = 0; i < 3; i++)
    {
        if (memObjects[i] != 0)
            clReleaseMemObject(memObjects[i]);
    }
    if (commandQueue != 0)
        clReleaseCommandQueue(commandQueue);

    if (kernel != 0)
        clReleaseKernel(kernel);

    if (program != 0)
        clReleaseProgram(program);

    if (context != 0)
        clReleaseContext(context);

}
*/
