//////////////////////////////////////////////////////////////////
//                        Event.h
//
//  Class and function definition for event class used in
//    event-driven simulation. It is designed as a timing-wheel
//    per circuit level.
//
//////////////////////////////////////////////////////////////////

#ifndef _EVENT_H
#define _EVENT_H

#include <queue>
#include <vector>

// Virutal base class for event queue handling
class EventManager {
   public:
      // EventManager() { ; }
      // ~EventManager() { ; }

      virtual void clear() =0;

      virtual int next( bool is_3frame ) =0; // Get gate id of next event;
                                             // -1 means empty queue
      virtual void insert( int g, int level ) =0; // Insert gate into event Q

      virtual bool empty() const =0;
      virtual void dumpEvents() const =0;

      virtual int curLevel() const =0;
      virtual void curLevel( int v ) =0;

};


class BruteForceEventManager : public EventManager {
   public:
      BruteForceEventManager( int num_levels, int num_nodes );
      BruteForceEventManager();
      ~BruteForceEventManager();

      void init( int num_levels, int num_nodes );
      void clear();

      int next( bool is_3frame ); // Get gate id of next event;
                                  // -1 means empty queue
      void insert( int g, int level ); // Insert a gate into event list

      bool empty() const { return curLevel()==-1; }
      void dumpEvents() const;

      int curLevel() const { return _curLevel; }
      void curLevel( int v ) { _curLevel = v; }

   protected:
      BruteForceEventManager( const BruteForceEventManager & );

      bool inQueue( int g ) const { return _inQueue[g]; }
      int numLevels() const { return (int)_events.size(); }

   private:
      std::vector< std::vector<int> > _events; // one event queue per level
      std::vector< bool > _inQueue; // To indicate if one node is in queue.
                                    // Its size is equal to num_of_nodes.
      int _curLevel; // -1: all event queues are empty
                     // >=0: lowest level where event exists
                     //   _curLevel goes through 1, 2, .. n then 0
};


////////////////////////////////////////////////////////////////////////
//     Host implementation that mimic event queue handling on GPU
////////////////////////////////////////////////////////////////////////
class MimicGpuEventManager : public EventManager {
   public:
      MimicGpuEventManager( int num_nodes,
                            const std::vector<int>& firstNodePerLevel );
      MimicGpuEventManager();
      ~MimicGpuEventManager();

      void init( int num_nodes, const std::vector<int>& firstNodePerLevel );
      void clear();

      int next( bool is_3frame ); // Get gate id of next event;
                                  // -1 means empty queue
      void insert( int g, int level ); // Insert a gate into event list

      bool empty() const { return curLevel()==-1; }
      void dumpEvents() const;

      int curLevel() const { return _curLevel; }
      void curLevel( int v ) { _curLevel = v; }

   protected:
      MimicGpuEventManager( const MimicGpuEventManager & );

      inline bool inQueue( int g ) const;
      int numLevels() const { return _numLevels; }

   private:
      // One-dimensional event queue for all levels
      //  * its size is the same of netlist size
      //  * event queue for level 'l' starts at firstNodeInLevel(l)
      //  * MSB bit of each node 'n' indicate if the node is in queue
      std::vector< unsigned > _events;
      std::vector< int > _queueOffsets; // offset in _events for each level
      std::vector< int > _queueSizes;   // queue size for each level
      int _numLevels;
      int _curLevel; // -1: all event queues are empty (for 3-frame sim only)
                     // >=0: lowest level where event exists
                     //   _curLevel goes through 1, 2, .. n then 0
};


////////////////////////////////////////////////////////////////////////
//     Host implementation that is based on flag array
////////////////////////////////////////////////////////////////////////
class FlagBasedEventManager : public EventManager {
   public:
      FlagBasedEventManager( int num_nodes,
                            const std::vector<int>& firstNodePerLevel );
      FlagBasedEventManager();
      ~FlagBasedEventManager();

      void init( int num_nodes, const std::vector<int>& firstNodePerLevel );
      void clear();

      int next( bool is_3frame ); // Get gate id of next event;
                                  // -1 means empty queue
      void insert( int g, int level ); // Insert a gate into event list

      bool empty() const { return curLevel()==-1; }
      void dumpEvents() const;

      int curLevel() const { return _curLevel; }
      void curLevel( int l ) {
         _curLevel = l;
         _curNode = (-1==l) ? 0 : _queueOffsets[l];
      }

   protected:
      FlagBasedEventManager( const FlagBasedEventManager & );

      inline bool inQueue( int g ) const;
      int numLevels() const { return _numLevels; }

   private:
      // One-dimensional event queue for all levels
      //  * its size is the same of netlist size
      //  * event queue for level 'l' starts at firstNodeInLevel(l)
      //  * MSB bit of each node 'n' indicate if the node is in queue
      std::vector< char > _flags; // 1: in queue; 0: NOT in queue
      std::vector< int > _queueOffsets; // offset in _flags for each level
      std::vector< int > _queueSizes;   // queue size for each level
      int _numNodes;
      int _numLevels;
      int _curLevel; // -1: all event queues are empty (for 3-frame sim only)
                     // >=0: lowest level where event exists
                     //   _curLevel goes through 1, 2, .. n then 0
      int _curNode;
};
#endif
