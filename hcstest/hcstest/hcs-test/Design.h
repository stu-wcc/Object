//////////////////////////////////////////////////////////////////
//                        Design.h
//
//  Class and function definition for 'Design' class in host cpu
//
//////////////////////////////////////////////////////////////////

#ifndef _DESIGN_H
#define _DESIGN_H
#include "CircuitOnHost.h"
#include "PatternOnHost.h"

class Design {
   public:
      Design() { ; }
      ~Design(){ ; }

      CircuitOnHost & cktOnHost() { return _cktOnHost; }
      LevelizedNetlist & simNetlist() { return _cktOnHost.simNetlist(); }

   private:

      CircuitOnHost _cktOnHost;
};

#endif
