//////////////////////////////////////////////////////////////////
//                        Common.h
//
//  Define common data types used in host and OpenCL code base
//
//////////////////////////////////////////////////////////////////

#ifndef _COMMON_H
#define _COMMON_H

#define MSB_MASK_32 0x80000000

typedef enum {
   PAT_SRC_EXTERNAL,
   PAT_SRC_RANDOM
} PatternSource;


typedef enum {
   HOST,
   OCL_GPU,
   OCL_CPU
} SimLocation;


typedef enum {
   IN_ORDER,               // 0
   EVENT_QUEUE_BASED,      // 1
   EVENT_FLAGS_BASED       // 2
} LogicSimMethod;


typedef enum {
   JUNK,           /* 0 */
   T_INPUT,        /* 1 */
   T_OUTPUT,       /* 2 */
   T_XOR,          /* 3 */
   T_XNOR,         /* 4 */
   T_DFF,          /* 5 */
   T_AND,          /* 6 */
   T_NAND,         /* 7 */
   T_OR,           /* 8 */
   T_NOR,          /* 9 */
   T_NOT,          /* 10 */
   T_BUF,          /* 11 */
   T_TIE1,         /* 12 */
   T_TIE0,         /* 13 */
   T_TIEX,         /* 14 */
   T_TIEZ,         /* 15 */
   T_MUX_2,        /* 16 */
   T_BUS,          /* 17 */
   T_BUS_GOHIGH,   /* 18 */
   T_BUS_GOLOW,    /* 19 */
   T_BUFZ,         /* 20 */ // change BUSZ to BUFZ
   T_TRISTATE,     /* 21 */
   T_TRISTATEINV,  /* 22 */
   T_TRISTATE1,    /* 23 */
   T_DFF_L,        /* 24 */
   T_DFF_LCP,      /* 25 */
   T_DFF_LC,       /* 26 */
   T_DFF_LP,       /* 27 */
   T_DFF_CP,       /* 28 */
   T_DFF_C,        /* 29 */
   T_DFF_P,        /* 30 */
   T_NAND_LATCH,   /* 31 */
   T_NOR_LATCH,    /* 32 */
   T_CMOS,         /* 33 */
   T_NMOS,         /* 34 */
   T_PMOS,         /* 35 */
   T_NOTIF1,       /* 36 */
   T_NOTIF0,       /* 37 */
   T_BUFIF1,       /* 38 */
   T_BUFIF0,       /* 39 */
   T_D_LATCH,      /* 40 */
   T_BUF_FB,       /* 41 */
   T_LAST_GTYPE
} GType;

#endif
