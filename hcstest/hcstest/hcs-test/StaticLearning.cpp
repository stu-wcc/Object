

#include "StaticLearning.h"
#include <algorithm>
#include "HtSetting.h"
#include "app-common/utility.h"


StaticLearningManager::StaticLearningManager( int num_levels, int num_nodes ) {
   init( num_levels, num_nodes );
} 
StaticLearningManager::~StaticLearningManager() {
   ;
}

void StaticLearningManager::init( int num_levels, int num_nodes ) {

   _events.resize( num_levels );
   _inStack.resize( num_nodes );
   std::fill( _inStack.begin(), _inStack.end(), false );
   _curLevel = -1;
}

void StaticLearningManager::clear() {
   for( unsigned lev=0; lev < _events.size(); lev++ ) {
      _events[lev].clear();
   }
   std::fill( _inStack.begin(), _inStack.end(), false );
}

/*using namespace std;
   int main(){
      stack<int> g_stack;//creat g_stack
      for(int i=0;i < num_nodes; i++){
         g_stack.push(i);
      }
      //if(!s.empty()){
      //   cout<<"The stack is not empty"<<endl;
      //}
      //cout<<"The stack size "<<s.size()<<endl;
      while(!g_stack.empty()){
         cout<<" "<<g_stack.top();
         g_stack.pop();
         //gate_evaluation
      }
      cout<<endl;
      if(g_stack.empty()){
         cout<<"The stack is empty"<<endl;
      }
      return 0;
   }*/

void StaticLearning::learn(int num_nodes, int _numLevel,int id)
{
   int numInputs = numPred( gid );
   for (ix = 0; ix < num_nodes; ix++)
   {
      //g = num_nodes[ix];
      if (g.numInputs() == 1){
        continue;
      }

      switch ( logic(id)){
         case T_AND:
	 case T_NOR:
	    learn_node(numLevels(), ix, 1);
	    if (g.numSucc() > 1){
	       learn_node(numLevels(), ix, 0);
	    }
	    break;
	 case T_OR:
	 case T_NAND:
	    learn_node(numLevels(), ix, 0);
	    if (g.numSucc() > 1){
	       learn_node(numLevels(), ix, 1);
	    }
	 break;
	 default:
	    if (g.numSucc() > 1){
	       learn_node(numLevels(), ix, 0);
	       learn_node(numLevels(), ix, 1);
	    }
      }
}
     clear(g_stack);


////////////////////////////////////////////////////////////////
//                        Level value
//      Evaluate  circuit in forward and backward .
//	Set changed flag .Push the evaluated gate to stack.
//	Schedule next events.
/////////////////////////////////////////////////////////////////



void StaticLearning::leval(register GATEPTR g)
{
   register int i, j;
   register level iValue, v1;
   int num_x;
   logic iGateType;

// forward  evaluation //
   reset(gat.changed);
   pInListGates = g.inList();
   if (is_free(g)){
      return(0);
   }
   gateEval4val(g, iValue, iGateType, i);
   if (iValue == g._numSucc()){
	
      if (iValue != X) {  //no event//
         set(g.changed());
      }
      return(0);
   }
   if (g._numSucc() == X){
      g._numSucc() = iValue;	   	
      push(g_stack, g);
      set(g.changed());
      schedule__numSucc(g);
      return;
   }

			

	// backward implication //

	/*switch (logic(id))
	{
	case T_AND:
	case T_NAND:
	case T_OR:
	case T_NOR:
		v1 = (g.type() == AND || g.type() == NOR) ? ONE : ZERO;
		if (g._numSucc() == v1)
		{
					}
		else
		{
			for (i = num_x = 0; i< g.numInputs; i++)
				if (pInListGates[i]._numSucc() == X)
				{
					num_x++; j = i;
				}
			if (num_x == 1)
			{
				//pInListGates[j]._numSucc = gateEval4val[g.type()][g._numSucc()];
				set(g.changed());
				push(g_stack, pInListGates[j]);
				schedule_input(g, j);
				return;
			}
		}
		break;

		}

