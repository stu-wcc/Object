/////////////////////////////////////////////////////////////////
//                        CircuitOnHost.cpp
//
//  function definition for circuit class in host cpu
//
//////////////////////////////////////////////////////////////////

#include "app-common/utility.h"
#include "HtSetting.h"
#include "CircuitOnHost.h"
#include "Evaluate.h"
#include <assert.h>
#include <algorithm>
#include <climits>

CircuitOnHost::CircuitOnHost() : _iscasCktFile( "" ),
                                 _levelizedCktFile( "" ),
                                 _pinnameMappingFile ( "" ),
                                 _cktLoaded( false ),
                                 _useOptimizedNetlist( false ) {
   ;
}


CircuitOnHost::~CircuitOnHost() {
   clear();
}


void CircuitOnHost::clear() {
   _iscasCktFile = "";
   _levelizedCktFile = "";
   _pinnameMappingFile = "";
   _cktLoaded = false;
   _useOptimizedNetlist = false;

   _initialCkt.clear();
   _optimizedCkt.clear();
}


void CircuitOnHost::printCktStatistics( bool verbose ) const {
   HLog << InfoNote << "pre-optimization circuit:" << std::endl;
   _initialCkt.printCktStatistics( verbose );
   HLog << InfoNote << "post-optimization circuit:" << std::endl;
   _optimizedCkt.printCktStatistics( verbose );
}


void CircuitOnHost::printSysTimeInSeconds(
        std::chrono::system_clock::time_point& start,
        std::chrono::system_clock::time_point& end,
        const std::string& msg )
{
   HLog << TwoSpace << TwoSpace
        << "elapsed time in seconds (" << msg << "): "
        <<  elapsedSysTimeInSeconds(start,end)
        << std::endl;
}


bool CircuitOnHost::levelizedCktFile( const std::string& file ) {
   std::string extension = getFileExtension( file );
   if( "lev" != extension ) {
      HLog << InfoError
           << " only .lev extension is supported for level file"
           << std::endl;
      return false;
   }
   _levelizedCktFile = file;
   return true;
}


// Move DE nodes to the last level and OUTPUTS to 2nd-to-last
//   Store the updated netlist in _optimizedCkt
void CircuitOnHost::modifyHitecLevelNetlist() {
   assert( circuitLoaded() );
   assert( initialCkt().isHitecLevelConvention() );
   HLog << InfoNote
        << "HITEC level convention detected. Modifying level convention."
        << std::endl;
   // Place DFF at the last level and outputs at 2nd-to-last level
   std::vector<int> levels1( initialCkt().levels() );
   std::vector<int> levels2;
   std::vector<int> out_nodes( initialCkt().outputNodes() );
   std::vector<int> de_nodes;
   for( int i=0; i < initialCkt().numFFs(); i++ ){
      int g = initialCkt().firstNodeFF() + i;
      de_nodes.push_back( g );
   }

   std::vector<ReducedGate> dummy_nodes;
   adjustLevelsWithDEsAndOutputs( dummy_nodes,
                                  levels1, levels2, out_nodes, de_nodes);

   // compute idmap_post2pre and idmap_pre2post
   std::vector<int> idmap_post2pre;
   std::vector<int> idmap_pre2post( initialCkt().numNodes(), -1 );
   for( int i=0; i<initialCkt().numPIs(); i++) {
      int g = initialCkt().firstNodePI() + i;
      idmap_post2pre.push_back( g );    // input nodes
   }
   for( int i=0; i<initialCkt().numTIEs(); i++) {
      int g=initialCkt().firstNodeTIE() + i;
      idmap_post2pre.push_back( g );    // tie nodes
   }
   int sid = initialCkt().numPIs() +
             initialCkt().numTIEs() +
             initialCkt().numFFs();
   for( int g=sid; g<initialCkt().numNodes(); g++ ) {
      if( initialCkt().gate(g).type() != T_OUTPUT ) {
         idmap_post2pre.push_back( g ); // non-output combinational nodes
      }
   }
   for( int g=sid; g<initialCkt().numNodes(); g++ ) {
      if( initialCkt().gate(g).type() == T_OUTPUT ) {
         idmap_post2pre.push_back( g ); // output nodes
      }
   }
   for( int i=0; i<initialCkt().numFFs(); i++) {
      int g = initialCkt().firstNodeFF() + i;
      idmap_post2pre.push_back( g );    // DE nodes
   }
   assert( (int)idmap_post2pre.size()==initialCkt().numNodes() );
   for( size_t i=0; i< idmap_pre2post.size(); i++ ) {
      idmap_pre2post[ idmap_post2pre[i] ] = (int)i;
   }
   // update idmap[] for each gate in _initialCkt
   for( int i=0; i<initialCkt().numNodes(); i++) {
      initialCkt().gate(i).idmap( { idmap_pre2post[i] });
   }
   // populate _optimizedCkt
   for( size_t n=0; n < idmap_post2pre.size(); n++ ) {
      Gate gat( initialCkt().gate( idmap_post2pre[n] ));

      gat.level( levels2[ idmap_post2pre[n] ] ); // adjust level
      std::vector<int> predC0_in;
      for( size_t p=0; p<gat.predC0().size(); p++ ) {
         predC0_in.push_back( idmap_pre2post[ gat.predC0()[p] ]);
      }
      gat.predC0( predC0_in );
      std::vector<int> predC1_in( gat.predC1() );
      for( size_t p=0; p<gat.predC1().size(); p++ ) {
         predC1_in.push_back( idmap_pre2post[ gat.predC1()[p] ]);
      }
      gat.predC1( predC1_in );
      std::vector<int> succs_in;
      for( size_t s=0; s<gat.succ().size(); s++ ) {
         succs_in.push_back( idmap_pre2post[ gat.succ()[s] ]);
      }
      gat.succ( succs_in );
      optimizedCkt().appendGate( gat );
   }
   optimizedCkt().computeCircuitStatistics();
   optimizedCkt().populateOclVectors();
   // update idmap[] for each gate in _optimizedCkt
   for( int i=0; i<optimizedCkt().numNodes(); i++) {
      optimizedCkt().gate(i).idmap( { idmap_post2pre[i] });
   }
   std::string mod_lev_file( levelizedCktFile() );
   // Replace ".lev" with NULL
   mod_lev_file.replace( mod_lev_file.find_last_of("."), 4, "");
   mod_lev_file = mod_lev_file + ".mod.lev";
   optimizedCkt().dumpLevelizedCircuit( mod_lev_file, /* test_ocl=*/ false);
   useOptimizedNetlist( true );
}


void CircuitOnHost::optimizeAndRemodelNetlist( bool skip_buf_inv_opt,
                                               bool print_debug_info )
{
   HLog << InfoNote
        << "start optimizing and remodeling netlist ......."
        << std::endl;
   // Copy netlist topology to tempNodes
   HLog << InfoNote
        << "retrieve topology of original netlist ......" << std::endl;
   std::vector< ReducedGate > tempNodes;
   _initialCkt.makeCopyOfNetlistTopology( tempNodes );
   if( print_debug_info ) {
      HLog << "\n #### original copied netlist ##### " << std::endl;
      dumpNetlistOfReducedGates( tempNodes );
   }
   // keep copy of original netlist for debugging
   std::vector< ReducedGate > origNodes;
   _initialCkt.makeCopyOfNetlistTopology( origNodes );

   // Initialize mapping data from original netlist to optimized netlist
   const size_t pre_opt_netlist_size = tempNodes.size();
   std::vector< unsigned > flags( pre_opt_netlist_size, 0 );
   std::vector< int > temp_vec;
   std::vector< std::vector<int> > idmap( pre_opt_netlist_size, temp_vec);

   // Identify strongly connected components
   HLog << InfoNote
        << "identify feedback regions in circuit ......" << std::endl;
   std::vector< std::vector<int>> loops, fbbufs_per_scc, latches_per_scc;
   auto t1 = std::chrono::system_clock::now(); // measure run time
   identifyFeedbackRegions(tempNodes, loops, fbbufs_per_scc, latches_per_scc);
   auto t2 = std::chrono::system_clock::now(); // measure run time
   printSysTimeInSeconds(t1, t2, "loop identification");

   // Derive buffers and inverters to be optimized away
   HLog << InfoNote
        << "perform buffer/inverter optimization ......" << std::endl;
   t1 = std::chrono::system_clock::now(); // measure run time
   optimizeAwayBuffersAndInverters(tempNodes, flags, idmap, skip_buf_inv_opt);
   t2 = std::chrono::system_clock::now(); // measure run time
   printSysTimeInSeconds(t1, t2, "buf/inv optimiation");

   if( print_debug_info ) {
      HLog << "\n #### post buffer/inverter opt netlist ##### " << std::endl;
      dumpNetlistOfReducedGates( tempNodes, flags, idmap );
   }
   // Replace D latches with mux/delay-element model
   if( 0 ) { // There is no need to remodel D latch
      HLog << InfoNote << "remodel D latches ......" << std::endl;
      remodelDLatches( pre_opt_netlist_size, tempNodes, flags, idmap );
      if( print_debug_info ) {
         HLog << "\n #### post D-latch-remodel netlist ##### " << std::endl;
         dumpNetlistOfReducedGates( tempNodes, flags, idmap );
      }
   }
   // Replace D flip flops with 5-gate master-slave model
   //   TBD, can be replaced with 3-gate or just 2-gate model
   HLog << InfoNote << "remodel D flip-flops ......" << std::endl;
   t1 = std::chrono::system_clock::now(); // measure run time
   remodelDFlipFlops( pre_opt_netlist_size, tempNodes, flags, idmap );
   t2 = std::chrono::system_clock::now(); // measure run time
   printSysTimeInSeconds(t1, t2, "D flip-flop remodel");
   if( print_debug_info ) {
      HLog << "\n #### post D-flipflop-remodel netlist ##### " << std::endl;
      dumpNetlistOfReducedGates( tempNodes, flags, idmap );
   }

   // Scan tempNodes and create a copy of optimizedNodes without levelization
   HLog << InfoNote << "initialize optimized netlist ......" << std::endl;
   std::vector< ReducedGate > optimizedNodes;
   t1 = std::chrono::system_clock::now(); // measure run time
   initOptimizedNodes( pre_opt_netlist_size, tempNodes, optimizedNodes,
                       flags, idmap );
   t2 = std::chrono::system_clock::now(); // measure run time
   printSysTimeInSeconds(t1, t2, "initialize optimized netlist");
   if( print_debug_info ) {
      HLog << "\n #### init opt netlist ##### " << std::endl;
      dumpNetlistOfReducedGates( optimizedNodes );
      HLog << "\n #### updated flags and idmap " << std::endl;
      dumpNetlistOfReducedGates( origNodes, flags, idmap );
   }

   // Levelize optimized netlist
   HLog << InfoNote << "levelize optimized netlist ......" << std::endl;
   std::vector< ReducedGate > postLevelOptNodes;
   std::vector< int > levels1, levels2, levels_post_opt;
   t1 = std::chrono::system_clock::now(); // measure run time
   levelizeOptimizedNodes( pre_opt_netlist_size, optimizedNodes,
                           postLevelOptNodes, idmap, levels1, levels2,
                           levels_post_opt );
   t2 = std::chrono::system_clock::now(); // measure run time
   printSysTimeInSeconds(t1, t2, "levelize optimized netlist");
   if( print_debug_info ) {
      HLog << "\n #### Post opt netlist #####" << std::endl;
      dumpNetlistOfReducedGates( postLevelOptNodes, levels_post_opt );
      HLog << "\n #### updated flags and idmap " << std::endl;
      dumpNetlistOfReducedGates( origNodes, flags, idmap );
   }

   // populate _optNodes of vector<Gate>
   HLog << InfoNote << "construct optimized netlist ......" << std::endl;
#ifdef HT_DEBUG
   //dumpNetlistOfReducedGates( postLevelOptNodes, levels_post_opt );
#endif
   t1 = std::chrono::system_clock::now(); // measure run time
   populateOptNodesForSimulation( postLevelOptNodes,
                                  levels_post_opt, flags, idmap );
   t2 = std::chrono::system_clock::now(); // measure run time
   printSysTimeInSeconds(t1, t2, "construct optimized netlist");
}


std::vector<int> CircuitOnHost::unionOfTwoIntSets(
                                   const std::vector<int>& set1,
                                   const std::vector<int>& set2,
                                   int id_erased_in_set2 )
{
   std::vector<int> first( set1 );
   std::vector<int> second( set2 );
   // remove cgat from buff_pred.succs()
   std::vector<int>::iterator it;
   it = std::find( second.begin(), second.end(), id_erased_in_set2);
   assert( it != second.end());
   second.erase( it );
   // sort first and second
   std::sort( first.begin(), first.end() );
   std::sort( second.begin(), second.end() );
   // merge the two sets
   std::vector<int> v( first.size() + second.size() );
   it = std::set_union( first.begin(), first.end(),
                        second.begin(), second.end(),
                        v.begin() );
   v.resize( it - v.begin());
   return v;
}


void CircuitOnHost::identifyFeedbackRegions(
                       std::vector<ReducedGate> & cktnodes,
                       std::vector< std::vector<int> > & loops,
                       std::vector< std::vector<int> > & fbbufs_per_scc,
                       std::vector< std::vector<int> > & latches_per_scc)
{
   // test_boost_graph_lib(); // example to test usage of boost graph library
   assert( loops.size()==0 &&
           fbbufs_per_scc.size()==0 &&
           latches_per_scc.size()==0 );
   // build directed graph out of netlist topology
   d_graph_t graph;
   std::vector<vertex_t> vertices;
   for( size_t i=0; i < cktnodes.size(); i++) {
      vertices.push_back(boost::add_vertex( graph )); // construct vertices
   }
   for( size_t i=0; i < cktnodes.size(); i++) {
      const ReducedGate & node = cktnodes[i];
      for( size_t s=0; s < node.succs().size(); s++) {
         int sink = node.succs()[s];
         // break the edge intentionly when sink is TIE gate or DFF_L
         switch( cktnodes[sink].type()) {
            case T_TIE0:
            case T_TIE1:
            case T_TIEX:
            case T_TIEZ:
            case T_DFF_L: {
               break;
            }
            default: {
               boost::add_edge(vertices.at(i), vertices.at(sink), graph);
            }
         }
      }
   }
   GraphUtils::strongConnectedComponents( graph );

   int num_sccs = graph[boost::graph_bundle].number_of_components;
   std::vector< std::vector<int> > sccs( num_sccs );
   // collect nodes per strong component
   auto nodes = boost::vertices( graph );
   for( auto it = nodes.first; it != nodes.second; ++it) {
      sccs[ graph[*it].component ].push_back( *it );
   }
   for( size_t c=0; c < sccs.size(); c++ ) {
      const size_t c_size = sccs[c].size();
      if( c_size > 1 ) {
         loops.push_back( sccs[c] );
      }
   }
   HLog << "\nTotal Components: "
        << graph[boost::graph_bundle].number_of_components
        << "\nTotal Loops: " << loops.size()
        << std::endl;
#ifdef HT_DEBUG
   HLog << InfoNote
        << "dump SCC components to file 'components'" << std::endl;
   std::ofstream ofs1( "components" );
#endif
   for( size_t c=0; c < sccs.size(); c++ ) {
      const size_t c_size = sccs[c].size();
      if( c_size > 1 ) {
#ifdef HT_DEBUG
         ofs1 << "Component " << c << " (" << c_size <<  "): "
              << std::endl;
#endif
         std::vector< int > fbufs;
         std::vector< int > latches;
         for( size_t e=0; e < c_size; e++ ) {
            int gid = sccs[c][e];
#ifdef HT_DEBUG
            ofs1 << " " << gid+1;
#endif
            if( cktnodes[ gid ].type() == T_BUF_FB ) {
               fbufs.push_back( gid );
            } else if( cktnodes[ gid ].type() == T_NAND_LATCH) {
               latches.push_back( gid );
            }
         }
         fbbufs_per_scc.push_back( fbufs );
         latches_per_scc.push_back( latches );
#ifdef HT_DEBUG
         ofs1 << std::endl;
         ofs1 << "Component " << c << ": "
              << "fb_bufs(" << fbufs.size() << ") "
              << "latches(" << latches.size() << ")"
              << std::endl;
         ofs1 << "fb_bufs:";
         for( const auto i : fbufs ) { ofs1 << " " << i+1; }
         ofs1 << std::endl;
         ofs1 << "latches:";
         for( const auto i : latches ) { ofs1 << " " << i+1; }
         ofs1 << std::endl;
#endif
      } else {
         if( 1 != c_size ) {
            HLog << InfoError
                 << "Component " << c << " (" << c_size << ")"
                 << std::endl;
            assert(0);
         }
      }
   }
#ifdef HT_DEBUG
   ofs1.close();
#endif
   Evaluate teval( initialCkt() );
   teval.simImpactOfPinConstraints();
#ifdef HT_DEBUG
   HLog << InfoNote
        << "dump Constrained Nodes to file 'constrained_nodes'"
        << std::endl;
   std::ofstream os( "constrained_nodes" );
   for( size_t g=0; g < cktnodes.size(); g++) {
      if((teval.gmv0(g)==0 && teval.gmv1(g)==0) ||
         (teval.gmv0(g)==ALL_ONES_64 && teval.gmv1(g)==ALL_ONES_64) ) {
         if( (teval.type(g)!=T_TIE0) && (teval.type(g)!=T_TIE1) ) {
            std::string v = (teval.gmv0(g)==0) ? "0" : "1";
            os << g+1 << " : " << v << std::endl;
            //HLog << g+1 << " : " << v << std::endl;
         }
      }
   }
   os.close();
#endif
   //assert(0);
}


void CircuitOnHost::optimizeAwayBuffersAndInverters(
                       std::vector< ReducedGate >& copied_nodes,
                       std::vector< unsigned >& flags,
                       std::vector< std::vector<int> > & id_map,
                       bool skip_buf_inv_optimization )
{
   std::vector< int > levels;
   std::vector< int > levelized_order;
   std::vector< int > output_nodes;
   std::vector< int > seq_nodes;
   int offset_per_dff_remodel = -1;

   computeLevelOfOptimizedNodes( copied_nodes, offset_per_dff_remodel,
                                 levels, levelized_order,
                                 output_nodes,seq_nodes);

#ifdef HT_DEBUG
   // print levelized order for debug
   //for( size_t n=0; n < levelized_order.size(); n++ ) {
   //   HLog << levelized_order[n]+1 << std::endl;
   //}
#endif
   if( skip_buf_inv_optimization ) {
      HLog << InfoNote
           << "buffer/inverter optimization is skipped ....."
           << std::endl;
      return;
   }

   for( size_t n=0; n < copied_nodes.size(); n++ ) {
      //ReducedGate& cgat = copied_nodes[n]; // cgat: current gate
      const int cid = levelized_order[n];
      ReducedGate& cgat = copied_nodes[ cid ];
      if( cgat.type()==T_BUF ) {
         int buf_pred_id = cgat.preds()[0];
         ReducedGate& buf_pred = copied_nodes[buf_pred_id];
         // Replace pred of buff successor with buf_pred_id
         for( size_t s=0; s < cgat.succs().size(); s++) {
            ReducedGate& succ = copied_nodes[ cgat.succs()[s] ];
            for( size_t p=0; p < succ.preds().size(); p++) {
               if( succ.preds()[p]==cid ) {
                  // replace preds[p] with buf_pred_id
                  succ.pred( p, buf_pred_id );
                  // break; is not used in case buf is fed to a gate
                  //   more than once.
               }
            }
         }
         // Union succs of buf_pred_id with those of buf
         if( buf_pred.succs().size() == 1) {
            buf_pred.succs( cgat.succs() );
         } else { // buf_pred has fanouts
            std::vector<int> v =
               unionOfTwoIntSets( cgat.succs(), buf_pred.succs(), cid );
            buf_pred.succs( v );
         }

         // mark buffer as optimized away and update id_map
         flags[cid] |= GATE_OPTIMIZED_AWAY;
         id_map[cid].push_back( buf_pred_id );
      }
      if( cgat.type()==T_NOT &&
          !(flags[cid] & GATE_OPTIMIZED_AWAY) && // gate not optimized away yet
          cgat.succs().size()==1 &&
          copied_nodes[cgat.succs()[0]].type()==T_NOT ) {
         int inv_pred_id = cgat.preds()[0];
         int inv_succ_id = cgat.succs()[0];
         ReducedGate& inv_pred = copied_nodes[inv_pred_id];
         ReducedGate& inv_succ = copied_nodes[inv_succ_id];
         // Replace pred of inv_succ successor with inv_pred_id
         for( size_t s=0; s < inv_succ.succs().size(); s++) {
            ReducedGate& succ = copied_nodes[ inv_succ.succs()[s] ];
            for( size_t p=0; p < succ.preds().size(); p++) {
               if( int(inv_succ_id)==succ.preds()[p] ) {
                 // replace p with inv_pred_id
                 succ.pred( p, inv_pred_id );
               }
            }
         }
         // Union succs of inv_pred
         if( inv_pred.succs().size() == 1) {
            inv_pred.succs( inv_succ.succs() );
         } else { // inv_pred has fanouts
            std::vector<int> v =
               unionOfTwoIntSets( inv_succ.succs(), inv_pred.succs(), cid );
            inv_pred.succs( v );
         }
         // mark inverter pair as optimized away and update id_map
         flags[cid] |= GATE_OPTIMIZED_AWAY;
         flags[cid] |= GATE_POLARITY;
         flags[inv_succ_id] |= GATE_OPTIMIZED_AWAY;
         id_map[cid].push_back( inv_pred_id );
         id_map[inv_succ_id].push_back( inv_pred_id );
      }
   }
}


void CircuitOnHost::remodelDLatches(
                       size_t original_netlist_size,
                       std::vector<ReducedGate> & copied_nodes,
                       std::vector< unsigned > & flags,
                       std::vector< std::vector<int> > & id_map )
{
   for( size_t n=0; n < original_netlist_size; n++ ) {
      ReducedGate& cgat = copied_nodes[n]; // cgat: current gate
      if( cgat.type()==T_D_LATCH ) { // assume port enable and data
         int id_de = (int)copied_nodes.size();
         int id_mux = id_de + 1;
         int id_din    = cgat.preds()[1];
         int id_enable = cgat.preds()[0];

         // Construct DE
         appendGateToNetlist( copied_nodes,
                              T_DFF,
                              { id_mux },
                              { id_mux } );
         // Construct MUX (sel, d0, d1)
         std::vector<int> succs_new( cgat.succs() );
         succs_new.push_back( id_de ); // succs is set union of original
                                      // succs() of d-latch plus the delay
                                      // element
         appendGateToNetlist( copied_nodes,
                              T_MUX_2,
                              { id_enable, id_de, id_din },
                              succs_new );

         // Update succs of each pred of the original D latch
         for( size_t p=0; p < cgat.preds().size(); p++) {
            ReducedGate& pgat = copied_nodes[ cgat.preds()[p] ];
            std::vector<int> succs_in = pgat.succs();
            std::replace( succs_in.begin(), succs_in.end(), (int)n, id_mux);
            pgat.succs( succs_in );
         }
         // Update preds of each succ of the original D latch
         for( size_t s=0; s < cgat.succs().size(); s++) {
            ReducedGate& sgat = copied_nodes[ cgat.succs()[s] ];
            std::vector<int> preds_in = sgat.preds();
            std::replace( preds_in.begin(), preds_in.end(), (int)n, id_mux);
            sgat.preds( preds_in );
         }
         // Mark gate and linkage
         flags[n] |= GATE_REMODELED;
         id_map[n].assign( {id_de, id_mux} );
         // Update idmap that refered current D latch by opted-away bufs/invs
         for( size_t id=0; id < original_netlist_size; id++ ) {
            std::vector<int> & vals = id_map[id];
            for( size_t k=0; k < vals.size(); k++ ) {
               if( vals[k]==(int)n ) { // n is the id for current D latch
                  vals[k] = id_mux;
               }
            }
         }
      }
   }
}


void CircuitOnHost::remodelDFlipFlops(
                       size_t original_netlist_size,
                       std::vector<ReducedGate> & copied_nodes,
                       std::vector< unsigned > & flags,
                       std::vector< std::vector<int> > & id_map )
{
   bool pure_delay_element_detected = false;
   bool d_flipflop_detected = false;
   bool d_latch_detected = false;
   for( size_t n=0; n < original_netlist_size; n++ ) {
      // Please note the reference of cgat will be corrupted once
      //   copied_nodes[] get expanded. Then copied_nodes[n] should be
      //   used directly. !!!!!!!
      ReducedGate& cgat = copied_nodes[n]; // cgat: current gate
      if( cgat.type()==T_DFF ) {
         pure_delay_element_detected = true;
      }
      if( cgat.type()==T_D_LATCH ) {
         d_latch_detected = true;
      }
      if( cgat.type()==T_DFF_L ) { // assume port set, reset, clock and data
         d_flipflop_detected = true;
         int id_inv        = (int)copied_nodes.size();
         int id_mux_master = id_inv + 1;
         int id_mux_slave  = id_inv + 2;
         int id_de_master  = id_inv + 3;
         int id_de_slave   = id_inv + 4;
         int id_din        = cgat.preds()[3];
         int id_clk        = cgat.preds()[2];
         int id_reset      = cgat.preds()[1];
         int id_set        = cgat.preds()[0];

         // Construct inverter
         appendGateToNetlist( copied_nodes,
                              T_NOT,
                              { id_clk },
                              { id_mux_master } );
         // Construct master MUX (sel, d0, d1)
         appendGateToNetlist( copied_nodes,
                              T_MUX_2,
                              { id_inv, id_de_master, id_din },
                              { id_de_master, id_mux_slave } );
         // Construct slave MUX (sel, d0, d1)
         std::vector<int> succs_new( copied_nodes[n].succs() );
         succs_new.push_back( id_de_slave ); // succs is set union of original
                                      // succs() of d flip-flop plus the slave
                                      // delay element
         appendGateToNetlist( copied_nodes,
                              T_MUX_2,
                              { id_clk, id_de_slave, id_mux_master },
                              succs_new );
         // Construct DE master
         appendGateToNetlist( copied_nodes,
                              T_DFF,
                              { id_mux_master },
                              { id_mux_master } );
         // Construct DE slave
         appendGateToNetlist( copied_nodes,
                              T_DFF,
                              { id_mux_slave },
                              { id_mux_slave } );
         // Update succs of each pred of the original D flip flop
         for( size_t p=0; p < copied_nodes[n].preds().size(); p++) {
            int id_pred = copied_nodes[n].preds()[p];
            ReducedGate& pgat = copied_nodes[ id_pred ];
            std::vector<int> & succs_in = pgat.succsEdit();
            if( id_pred == id_din ) {
               std::replace( succs_in.begin(), succs_in.end(),
                             (int)n, id_mux_master );
            } else if( id_pred == id_clk ) {
               // replace successor 'n' with id_mux_slave and id_inv
               std::replace( succs_in.begin(), succs_in.end(),
                             (int)n, id_mux_slave );
               succs_in.push_back( id_inv );
            } else if( id_pred == id_set || id_pred == id_reset ) {
               succs_in.erase(
                  std::find( succs_in.begin(), succs_in.end(), (int)n ));
            }
         }
         // Update preds of each succ of the original D flip flop
         for( size_t s=0; s < copied_nodes[n].succs().size(); s++) {
            ReducedGate& sgat = copied_nodes[ copied_nodes[n].succs()[s] ];
            std::vector<int> & preds_in = sgat.predsEdit();
            std::replace( preds_in.begin(), preds_in.end(),
                          (int)n, id_mux_slave);
         }
         // Mark gate and linkage
         flags[n] |= GATE_REMODELED;
         id_map[n].assign(
           {id_inv, id_mux_master, id_mux_slave, id_de_master, id_de_slave});
         // Update idmap that refered D flip-flop by opted-away bufs/invs
         if( 0 ) { // skip this as buf/inv optimization is not performed
         for( size_t id=0; id < original_netlist_size; id++ ) {
            std::vector<int> & vals = id_map[id];
            for( size_t k=0; k < vals.size(); k++ ) {
               if( vals[k]==(int)n ) { // n is the id for D flip flop
                  vals[k] = id_mux_slave;
               }
            }
         }
         }
      }
   }
   if( pure_delay_element_detected && d_flipflop_detected ) {
      HLog << InfoError
           << "it is not allowed to combine D flip-flops with delay elements"
           << std::endl;
      assert(0);
   }
   if( d_latch_detected ) {
      HLog << InfoError
           << "D latch is not supported for now"
           << std::endl;
      assert(0);
   }
}


// Usage: declare pred_lst/succ_lst first and then pass in.
//    int pred_lst[] = { 1, 2 ,3 };
//    int succ_lst[] = { a, b };
//
// It is possible to directly pass pred_lst and succ_lst as
//   initializer_list as defined in C++11.
void CircuitOnHost::appendGateToNetlist(
                       std::vector<ReducedGate> & copied_nodes,
                       GType gtype,
                       int * pred_lst,
                       int * succ_lst )
{
   std::vector<int> preds_in( pred_lst,
                              pred_lst + sizeof(pred_lst)/sizeof(int) );
   std::vector<int> succs_in( succ_lst,
                              succ_lst + sizeof(succ_lst)/sizeof(int) );
   ReducedGate rgat( gtype, preds_in, succs_in );
   copied_nodes.push_back( rgat );
}

/*
void CircuitOnHost::appendGateToNetlist(
                       std::vector<ReducedGate> & copied_nodes,
                       GType gtype,
                       std::initializer_list<int> pred_lst,
                       std::initializer_list<int> succ_lst )
{
   std::vector<int> preds_in( pred_lst );
   std::vector<int> succs_in( succ_lst );
   ReducedGate rgat( gtype, preds_in, succs_in );
   copied_nodes.push_back( rgat );
}
*/

void CircuitOnHost::appendGateToNetlist(
                       std::vector<ReducedGate> & copied_nodes,
                       GType gtype,
                       const std::vector<int> & preds_in,
                       const std::vector<int> & succs_in )
{
   ReducedGate rgat( gtype, preds_in, succs_in );
   copied_nodes.push_back( rgat );
}


void CircuitOnHost::initOptimizedNodes(
                       size_t original_netlist_size,
                       const std::vector<ReducedGate> & temp_nodes,
                       std::vector<ReducedGate> & optimized_nodes,
                       const std::vector< unsigned > & flags,
                       std::vector< std::vector<int> > & id_map )
{
   assert( optimized_nodes.size()==0 );
   // Create index map from temp_nodes to optimized_nodes.
   std::vector<int> indexmap_t2s;  // map implemented as a vector
   size_t cur_sid = 0; // current id of optimized_nodes
   for( size_t n=0; n < temp_nodes.size(); n++ ) {
      bool is_node_to_be_removed = (n < original_netlist_size) ?
         (flags[n]& GATE_OPTIMIZED_AWAY || flags[n]& GATE_REMODELED) : false;
      if( is_node_to_be_removed ) {
         indexmap_t2s.push_back( -1 );
      } else {
         indexmap_t2s.push_back( cur_sid );
         cur_sid++;
      }
   }
   // Update id_map and let it point to index of optimized_nodes
   for( size_t n=0; n < original_netlist_size; n++) {
      if( id_map[n].size() ) { // the node is either opted-away or remodeled
         for( size_t i=0; i < id_map[n].size(); i++ ) {
            id_map[n][i] = indexmap_t2s[ id_map[n][i] ];
         }
      } else { // the node is kept and now store its index at opt-netlist
         id_map[n].push_back( indexmap_t2s[n] );
      }
   }
   // Now populate vector of optimized_nodes
   for( size_t n=0; n < temp_nodes.size(); n++ ) {
      bool is_node_to_be_removed = (n < original_netlist_size) ?
         (flags[n]& GATE_OPTIMIZED_AWAY || flags[n]& GATE_REMODELED) : false;
      if( !(is_node_to_be_removed)) {
         const ReducedGate& tgat = temp_nodes[n];
         GType type = tgat.type();
         std::vector<int> preds_in;
         std::vector<int> succs_in;
         for( size_t p=0; p < tgat.preds().size(); p++ ) {
            preds_in.push_back( indexmap_t2s[tgat.preds()[p]] );
         }
         for( size_t s=0; s < tgat.succs().size(); s++ ) {
            succs_in.push_back( indexmap_t2s[tgat.succs()[s]] );
         }
         ReducedGate sgat( type, preds_in, succs_in );
         optimized_nodes.push_back( sgat );
#ifdef HT_DEBUG
         //if( (n+1)==18975245 ) {
         //   HLog << "##### hit node 18975245 "
         //        << "new id: " << optimized_nodes.size() << std::endl;
         //}
#endif
      }
   }
}


// The assumption is all new nodes per D flip-flop remodeling are appended
//   to the end of input netlist
int CircuitOnHost::obtainOffsetOfRemodelGatesOfDFlipflop(
                      const std::vector<ReducedGate> & reduced_nodes,
                      const std::vector< std::vector<int>> & id_map )
{
   int offset = -1;
   for( size_t i=0; i < id_map.size(); i++ ) {
      if( id_map[i].size()==5 &&
          reduced_nodes[id_map[i][3]].isDelayElement() &&
          reduced_nodes[id_map[i][4]].isDelayElement() ) {
         // This determines the gate is a D flip-flop
         offset = id_map[i][0]; // the gate id of inverter
         break;
      }
   }
   return offset;
}


void CircuitOnHost::levelizeOptimizedNodes(
                       size_t original_netlist_size,
                       const std::vector<ReducedGate> & pre_level_optnodes,
                       std::vector<ReducedGate>       & post_level_optnodes,
                       std::vector< std::vector<int>> & id_map,
                       std::vector< int >             & levels1,
                       std::vector< int >             & levels2,
                       std::vector< int >             & levels_post_opt )
{
   assert( post_level_optnodes.size()==0 &&
           levels1.size()==0 &&
           levels2.size()==0 &&
           levels_post_opt.size()==0 );
   std::vector< int > levelized_order;
   std::vector< int > output_nodes;
   std::vector< int > seq_nodes;

   int offset = obtainOffsetOfRemodelGatesOfDFlipflop( pre_level_optnodes,
                                                       id_map );
   computeLevelOfOptimizedNodes( pre_level_optnodes, offset,
                                 levels1, levelized_order,
                                 output_nodes, seq_nodes);

   // Adjust levels1 to levels2 (with DE at last level and output 2nd to last
   adjustLevelsWithDEsAndOutputs( pre_level_optnodes,
                                  levels1, levels2, output_nodes, seq_nodes );
   // Within the same level group node of the same type together
   //   and create index map from pre_level_optnodes to post_level_optnodes

#ifdef HT_DEBUG
   //HLog << "############## level adjusted" << std::endl;
#endif
   // Create post_level_optnodes and update id_map
   generatePostLevelOptNodesWithTypeGrouping( original_netlist_size,
                                              pre_level_optnodes,
                                              levels2,
                                              post_level_optnodes,
                                              levels_post_opt,
                                              id_map );
}



void CircuitOnHost::computeLevelOfOptimizedNodes(
                       const std::vector<ReducedGate> & pre_level_optnodes,
                       int offset_per_dff_remodel,
                       std::vector< int > & level,
                       std::vector< int > & levelized_order,
                       std::vector< int > & output_nodes,
                       std::vector< int > & seq_nodes )
{
   assert( output_nodes.size()==0 &&
           seq_nodes.size()==0 &&
           levelized_order.size()==0 );
   // Obtain starting nodes of level 0 and output nodes
   std::vector< int > input_nodes;
   //std::vector< int > output_nodes;
   std::vector< int > tie_nodes;
   //std::vector< int > seq_nodes;

   const int pos = (-1 == offset_per_dff_remodel) ?
                   (int)pre_level_optnodes.size() : offset_per_dff_remodel;
   for( int n=0; n < pos; n++) {
      const ReducedGate& gat = pre_level_optnodes[ n ];
      switch( gat.type() ) {
         case T_INPUT: {
            input_nodes.push_back( n );
            levelized_order.push_back( n );
            break;
         }
         case T_OUTPUT: {
            output_nodes.push_back( n );
            break;
         }
         case T_TIE1:
         case T_TIE0:
         case T_TIEX:
         case T_TIEZ: {
            tie_nodes.push_back( n );
            levelized_order.push_back( n );
            break;
         }
         case T_DFF:
         case T_DFF_L:
         case T_D_LATCH:
         //case T_NAND_LATCH: // dumped by Hans // do not make it last
         case T_BUF_FB: {
            seq_nodes.push_back( n );
            levelized_order.push_back( n );
            break;
         }
#ifdef HT_DEBUG
         case T_NAND_LATCH: {
            // special treatment for taking care of M21 loops due to latch
            //   should be removed later.
            if( (n+1)==18975245 || (n+1)==18535694) {
               HLog << "##### hit node " << n+1 << std::endl;
               seq_nodes.push_back( n );
               levelized_order.push_back( n );
            }
            break;
         }
#endif
         default: {
            break;
         }
      }
   }
   // Push in new gates per D flip-flop remodeling
   for( int n=pos; n < (int)pre_level_optnodes.size(); n++) {
      seq_nodes.push_back( n );
      levelized_order.push_back( n );
   }

   if( (input_nodes.size()+tie_nodes.size()) == 0 ) {
      HLog << InfoWarn
           << "The circuit has no input and tie node."
           << std::endl;
   }

   std::vector< int > start_points( input_nodes );
   start_points.insert( start_points.end(),
                        tie_nodes.begin(), tie_nodes.end());
   start_points.insert( start_points.end(),
                        seq_nodes.begin(), seq_nodes.end());

   std::vector<int> visited( pre_level_optnodes.size(), 0 );
   level.assign( pre_level_optnodes.size(), -1 );

   // in_edges store preds of each node; a pred is removed once it is visited
   std::vector< std::vector<int>> in_edges;
   for( size_t i=0; i < pre_level_optnodes.size(); i++) {
      in_edges.push_back( pre_level_optnodes[i].preds());
   }
   // init Queue with PIs, TIEs, DEs or gates per DFF remodel
   std::queue<int> queue;
   for( size_t s=0; s < start_points.size(); s++ ) {
      level[ start_points[s] ] = 0; // assign start points with lev 0
      in_edges[ start_points[s] ].clear(); //   and clear its in-edges
      queue.push( start_points[s] );
   }
   while( !queue.empty() ) {
      int cid = queue.front(); // current gate id
      queue.pop();
      const ReducedGate& gat = pre_level_optnodes[cid];
      for( size_t s=0; s < gat.succs().size(); s++) {
         int sid = (gat.succs())[ s ];
         const ReducedGate& sgat = pre_level_optnodes[sid];
         if( 0==level[sid] ) {
            continue; // DONOT loop over level 0 node
         }
         assert( sid < (int)pre_level_optnodes.size() );
         typedef std::vector<int>::iterator ITER;
         ITER it= std::find( in_edges[sid].begin(), in_edges[sid].end(), cid);
         while( it != in_edges[sid].end() ) {
            in_edges[sid].erase( it );
            it = std::find( in_edges[sid].begin(), in_edges[sid].end(), cid);
         }

         if( in_edges[sid].size()==0 ) {
            queue.push( sid );
            // compute new_level as all predecessors are touched
            int new_l = 0;
            for( size_t p=0; p < sgat.preds().size(); p++) {
               int pid = sgat.preds()[p];
               if( new_l < level[pid])  { new_l = level[pid]; }
            }
            level[sid] = new_l + 1;
            levelized_order.push_back( sid );
         }
      }
   }
   int num_loop_nodes = 0;
   for( size_t g=0; g < level.size(); g++ ) {
      if( -1==level[g] ) { // Check if node is in a loop or fed by loop node
         num_loop_nodes++;
      }
      // check how many are D latches
   }
   if( num_loop_nodes ) {
      HLog << InfoNote << num_loop_nodes << " nodes are loop-related. "
           << std::endl;
   }
#ifdef HT_DEBUG
   int max_level = *std::max_element( level.begin(), level.end() );
   HLog << InfoNote
        << "#### Max level per levelization (" << max_level << ")"
        << std::endl;
#endif
}


void CircuitOnHost::adjustLevelsWithDEsAndOutputs(
                       const std::vector<ReducedGate> & pre_level_optnodes,
                       const std::vector< int > & level1,
                       std::vector< int > & level2,
                       const std::vector< int > & output_nodes,
                       const std::vector< int > & seq_nodes )
{
   int max_level = *std::max_element( level1.begin(),level1.end() );
   //int max_level = level1[It];

   level2.insert ( level2.end(), level1.begin(), level1.end() );
   for (size_t n=0; n < seq_nodes.size(); n++) {
      level2 [ seq_nodes[n] ] = max_level+1;
#ifdef HT_DEBUG
      // special treatment for M21 re-levelization
      //    should be refactored later
      if( pre_level_optnodes.size()) {
         int gid = seq_nodes[n];
         const ReducedGate& gat = pre_level_optnodes[ gid ];
         if( gat.type() == T_BUF_FB) {
            level2[ gid ] =  level1[ (gat.preds())[0] ] + 1;
            HLog  << gid+1 << " ## fb_buf 0 to " << level2[gid] << std::endl;
         } else if( gat.type() == T_NAND_LATCH) {
            std::vector<int> in_levs;
            for( size_t p=0; p < gat.preds().size(); p++) {
               in_levs.push_back( level1[ (gat.preds())[p] ]);
            }
            int lmax = *std::max_element( in_levs.begin(), in_levs.end());
            level2[ gid ] = lmax + 1;
            HLog << gid+1 << " ## latch 0 to " << level2[gid] << std::endl;
         }
      }
#endif
   }
   for (size_t n=0; n < output_nodes.size(); n++) {
      level2 [ output_nodes[n] ] = max_level;
   }
}



void CircuitOnHost::generatePostLevelOptNodesWithTypeGrouping(
                      size_t original_netlist_size,
                      const std::vector< ReducedGate > & pre_level_optnodes,
                      const std::vector< int > & levels2,
                      std::vector< ReducedGate > & post_level_optnodes,
                      std::vector< int > & levels_post_opt,
                      std::vector< std::vector<int> > & id_map )
{
   int max_level = *std::max_element( levels2.begin(),levels2.end() );
   HLog << InfoNote
        << "max level of post opt netlist " << max_level << std::endl;
   // idmap_pre2post[] is index map from pre_level_nodes to post_level_nodes
   std::vector<int> idmap_pre2post( pre_level_optnodes.size(),  -1 );
   std::vector<int> idmap_post2pre( pre_level_optnodes.size(),  -1 );
   size_t base = 0;
   for( int lev=0; lev <= max_level; lev++ ) {
      // allow for maximum 6-input gate
      int max_inputs = 6;
      std::vector<int> empty_vec(0, 0);
      std::vector< std::vector<int> > cnt_in(max_inputs, empty_vec);
      int n_types = (int) T_LAST_GTYPE;
      // Model as 3-d arrary [gate_type][num_inputs][gate_count]
      std::vector< std::vector< std::vector<int>>> group_gates(n_types,cnt_in);
      for ( size_t n=0; n<pre_level_optnodes.size(); n++ ) {
         if( levels2[n] != lev ) {
            continue; // Skip gates whose level is not the target level
         }
         const ReducedGate& cgat = pre_level_optnodes[n];
         switch ( cgat.type() ) {
            case T_OUTPUT:
            case T_NOT:
            case T_BUF:
            case T_BUFZ:
            case T_INPUT: // 0 inputs
            case T_DFF:
            case T_BUF_FB:
            case T_TIE1:
            case T_TIE0:
            case T_TIEX:
            case T_TIEZ:
            case T_TRISTATE: // 2 inputs
            case T_TRISTATE1: // 2 inputs
            case T_MUX_2: // 3 inputs
            case T_NAND_LATCH: // 4 inputs
            {
               // all the above types either has only one scenario of input
               //   count( such as 0 or 1 input ) or gate evaluation does not
               //   depend on input values( such as TIE gates )
               group_gates[(int)cgat.type()][0].push_back( n );
               break;
            }
            case T_XOR:
            case T_XNOR:
            case T_AND:
            case T_NAND:
            case T_OR:
            case T_NOR:
            case T_BUS:
            {
               // input count can vary for the above gate types.
               size_t num_preds = cgat.preds().size();
               group_gates[(int)cgat.type()][num_preds-1].push_back( n );
               break;
            }
            default:
            {
               HLog << InfoError
                    << "unexpected gate type '"
                    << Gate::typeToName( cgat.type() ) << "'"
                    << " on gate " << n
                    << std::endl;
               return;
            }
         }
      }
      // gate id per pre_level_optnodes[] in current level
      std::vector< int > gid_in_lev;
      if( 0==lev ) { // special treatment at level 0
         std::vector< int > lev0types ({ (int)T_INPUT,
                                         (int)T_TIE1,
                                         (int)T_TIE0,
                                         (int)T_TIEZ,
                                         (int)T_TIEX });
         for( size_t t=0; t < lev0types.size(); t++) {
            int type = lev0types[t];
            for( size_t i=0; i< group_gates[type][0].size(); i++) {
               gid_in_lev.push_back( group_gates[type][0][i] );
            }
         }
      }
      else if( max_level==lev &&
         ( group_gates[(int)T_DFF][0].size() ||
           group_gates[(int)T_BUF_FB][0].size()) ) {
         // Add feedback buffer first
         gid_in_lev = group_gates[(int)T_BUF_FB][0];
         // Append Latch after feedback buffer
         gid_in_lev.insert( gid_in_lev.end(),
                            group_gates[(int)T_NAND_LATCH][0].begin(),
                            group_gates[(int)T_NAND_LATCH][0].end() );
         // Append flip flop after latch.
         //   For now, we do not allow combination of DFF remodel and pure
         //   delay elements
         if( group_gates[(int)T_NOT][0].size() ) { // there is DFF remodel;
            // The 1st inverter is the starting pos of gates per DFF remodel
            int start_gid = group_gates[(int)T_NOT][0][0];
            assert( 0==(((int)pre_level_optnodes.size()-start_gid)%5) );
            for( int g=start_gid; g < (int)pre_level_optnodes.size(); g++ ) {
               gid_in_lev.push_back( g );
            }
         } else {
            gid_in_lev.insert( gid_in_lev.end(),
                               group_gates[(int)T_DFF][0].begin(),
                               group_gates[(int)T_DFF][0].end() );
         }
      }
      else {
         // Handle gate type with fixed input size
         std::vector< int > leading_types ({ (int)T_NOT,
                                             (int)T_BUF,
                                             (int)T_BUFZ,
                                             (int)T_TRISTATE,
                                             (int)T_TRISTATE1,
                                             (int)T_MUX_2,
                                             (int)T_BUF_FB,
                                             (int)T_NAND_LATCH });
         for( size_t t=0; t < leading_types.size(); t++) {
            int type = leading_types[t];
            for( size_t i=0; i< group_gates[type][0].size(); i++) {
               gid_in_lev.push_back( group_gates[type][0][i] );
            }
         }
         // Handle gate type with varied input size
         std::vector< int > middle_types ({(int)T_XOR,
                                           (int)T_XNOR,
                                           (int)T_AND,
                                           (int)T_NAND,
                                           (int)T_OR,
                                           (int)T_NOR,
                                           (int)T_BUS });
         for( size_t t=0; t < middle_types.size(); t++) {
            int type = middle_types[t];
            for( int ins=0; ins < max_inputs; ins++ ) {
               for( size_t i=0; i< group_gates[type][ins].size(); i++) {
                  gid_in_lev.push_back( group_gates[type][ins][i] );
               }
            }
         }
         // Outputs are put to the end of 2nd to last level
         for( size_t i=0; i< group_gates[(int)T_OUTPUT][0].size(); i++) {
            gid_in_lev.push_back( group_gates[(int)T_OUTPUT][0][i] );
         }
      }
      // gid_in_lev has relative position of gate in current level
      for( size_t p=0; p < gid_in_lev.size(); p++ ) {
         idmap_pre2post[ gid_in_lev[p] ] = (int)(base + p);
         idmap_post2pre[ base + p ] = gid_in_lev[p];
      }
      base += gid_in_lev.size();
   }

   // Generate post_level_optnodes[] based on idmap_pre2post & idmap_post2pre
   for( size_t g=0; g < idmap_post2pre.size(); g++ ) {
      const ReducedGate & preGat = pre_level_optnodes[ idmap_post2pre[g] ];
      GType type = preGat.type();
      std::vector<int> preds_in;
      for( size_t p=0; p < preGat.preds().size(); p++ ) {
         preds_in.push_back( idmap_pre2post[preGat.preds()[p]] );
      }
      std::vector<int> succs_in;
      for( size_t s=0; s < preGat.succs().size(); s++ ) {
         succs_in.push_back( idmap_pre2post[preGat.succs()[s]] );
      }
      ReducedGate postGat( type, preds_in, succs_in );
      post_level_optnodes.push_back( postGat );
      // Populate levels that correspond to index of post opt netlist
      levels_post_opt.push_back( levels2[ idmap_post2pre[g]] );
   }
   // Update id_map of original netlist
   for( size_t g=0; g < original_netlist_size; g++ ) {
      std::vector< int > vals_in;
      for( size_t p=0; p < id_map[g].size(); p++ ) {
         vals_in.push_back( idmap_pre2post[ id_map[g][p] ]);
      }
      id_map[g] = vals_in;
   }
}


void CircuitOnHost::populateOptNodesForSimulation(
                       const std::vector<ReducedGate> & post_level_optnodes,
                       const std::vector< int >       & levels_post_opt,
                       const std::vector< unsigned>   & flags,
                       const std::vector< std::vector<int>> & id_map )
{
   assert( initialCkt().numNodes() == (int)flags.size() &&
           initialCkt().numNodes() == (int)id_map.size() );

   for( size_t n=0; n < post_level_optnodes.size(); n++ ) {
      const ReducedGate & rg = post_level_optnodes[n];
      Gate gat;
      gat.type( rg.type() );
      gat.level( levels_post_opt[n] );
      gat.numPred( (int)rg.preds().size() );
      gat.predC0( rg.preds() );
      gat.predC1( rg.preds() );
      gat.numSucc( (int)rg.succs().size() );
      gat.succ( rg.succs() );
      // Can be computed later
      gat.obs( 0 );
      gat.c0( 0 );
      gat.c1( 0 );
      // _optNodes.push_back( gat );
      optimizedCkt().appendGate( gat );
   }

   // compute _flags and _mapIds between initialCkt and optimizedCkt
   for( int g=0; g < initialCkt().numNodes(); g++ ) {
      initialCkt().gate(g).flag( flags[g] );
      initialCkt().gate(g).idmap( id_map[g] );
      if( initialCkt().gate(g).flag() & GATE_REMODELED) {
         for( size_t i=0; i < initialCkt().gate(g).idmap().size(); i++) {
            // oid: gate id in optimized netlist
            int oid = initialCkt().gate(g).idmap()[i];
            optimizedCkt().gate(oid).flag( NEW_GATE_PER_REMODEL );
            optimizedCkt().gate(oid).idmap( {g} );
         }
      } else if( !(initialCkt().gate(g).flag() & GATE_OPTIMIZED_AWAY)) {
         assert( initialCkt().gate(g).idmap().size()==1 );
         int oid = initialCkt().gate(g).idmap()[0];
         optimizedCkt().gate(oid).idmap( {g} );
      }
   }
   optimizedCkt().computeCircuitStatistics();
   optimizedCkt().populateOclVectors();

   // Dump optimized level netlist
   std::string opt_lev_file( levelizedCktFile() );
   // Replace ".lev" with NULL
   opt_lev_file.replace( opt_lev_file.find_last_of("."), 4, "");
   opt_lev_file = opt_lev_file + ".opt.lev";
   optimizedCkt().dumpLevelizedCircuit( opt_lev_file, /* test_ocl=*/ false);

   // Dump idmap
   std::string id_map_file = opt_lev_file + ".opt.idmap";
   std::ofstream ofs( id_map_file );
   ofs << "Optimized --> Original" << std::endl;
   for( int g=0; g < optimizedCkt().numNodes(); g++ ) {
      ofs << "N" << g+1 << ":";
      for( unsigned m=0; m < optimizedCkt().gate(g).idmap().size(); m++) {
         ofs << " " << (optimizedCkt().gate(g).idmap())[m] +1;
      }
      ofs << std::endl;
   }
   ofs << "\n\nOriginal --> Optimized"
       << std::endl;
   for( int g=0; g < initialCkt().numNodes(); g++ ) {
      ofs << "O" << g+1 << ":";
      for( unsigned m=0; m < initialCkt().gate(g).idmap().size(); m++) {
         ofs << " " << (initialCkt().gate(g).idmap())[m] +1;
      }
      ofs << std::endl;
   }
   ofs.close();

   useOptimizedNetlist( true );
}


void CircuitOnHost::dumpOneReducedGate(
                       const std::vector<ReducedGate>& nodes,
                       unsigned g )
{
      HLog << g+1
           << " " << Gate::typeToName( nodes[g].type() );
      HLog << " p" << nodes[g].preds().size();
      if( nodes[g].preds().size() ) {
         HLog << "[";
      }
      for( size_t p=0; p < nodes[g].preds().size(); p++ ) {
         HLog << " " << nodes[g].preds()[p] + 1;
      }
      if( nodes[g].preds().size() ) {
         HLog << " ]";
      }
      HLog << " s" << nodes[g].succs().size();
      if( nodes[g].succs().size() ) {
         HLog << "[";
      }
      for( size_t s=0; s < nodes[g].succs().size(); s++ ) {
         HLog << " " << nodes[g].succs()[s] + 1;
      }
      if( nodes[g].succs().size() ) {
         HLog << " ]";
      }
}


void CircuitOnHost::dumpNetlistOfReducedGates(
                       const std::vector<ReducedGate>& nodes )
{
   for( size_t g=0; g < nodes.size(); g++ ) {
      dumpOneReducedGate( nodes, g );
      HLog << std::endl;
   }
}


void CircuitOnHost::dumpNetlistOfReducedGates(
                       const std::vector<ReducedGate>& nodes,
                       const std::vector<unsigned> & flags,
                       const std::vector< std::vector<int>> & id_map )
{
   for( size_t g=0; g < nodes.size(); g++ ) {
      dumpOneReducedGate( nodes, g );
      if( g < flags.size() ) {
         if( flags[g] & GATE_OPTIMIZED_AWAY) {
            HLog << " OPT_AWAY";
            if( flags[g] & GATE_POLARITY ) {
               HLog << " POLAR(1)";
            } else {
               HLog << " POLAR(0)";
            }
         }
         if( flags[g] & GATE_REMODELED ) {
            HLog << " REMODEL";
         }
      }
      if( g < id_map.size() ) {
         const std::vector<int> & ids = id_map[g];
         if( ids.size() ) {
            HLog << " idmap(";
            for( size_t i=0; i < ids.size(); i++) {
               HLog << " " << ids[i]+1;
            }
            HLog << " )";
         }
      }
      HLog << std::endl;
   }
}


void CircuitOnHost::dumpNetlistOfReducedGates(
                       const std::vector<ReducedGate>& nodes,
                       const std::vector<int>& levels )
{
   for( size_t g=0; g < nodes.size(); g++ ) {
      dumpOneReducedGate( nodes, g );
      if( g < levels.size() ) {
         HLog << " level=" << levels[g];
      }
      HLog << std::endl;
   }
}


bool CircuitOnHost::addClocks( const std::vector<int>& clk_ids, int off_state)
{

   if( initialCkt().numClocks() ) {
      HLog << InfoError
           << "multiple clocks are not supported for now"
           << std::endl;
      return false;
   }

   if( false==initialCkt().addClocks( clk_ids, off_state )) {
      return false;
   }
   if( optimizedCkt().numNodes() ) {
      // The assumption is that index of inputs may be different between
      //   initial circuit and optimized circuit.
      std::vector<int> new_clkids;
      for( auto it=clk_ids.begin(); it != clk_ids.end(); ++it) {
         assert( initialCkt().gate(*it).idmap().size()==1 );
         new_clkids.push_back( initialCkt().gate(*it).idmap()[0] );
      }
      if( false==optimizedCkt().addClocks( new_clkids, off_state )) {
         return false;
      }
   }
   return true;
}


bool CircuitOnHost::deleteClocks()
{
   if( !initialCkt().numClocks() ) {
      HLog << InfoNote
           << "no clock is defined."
           << std::endl;
      return true;
   }
   initialCkt().deleteClocks();
   if( optimizedCkt().numNodes() ) {
      optimizedCkt().deleteClocks();
   }
   HLog << InfoNote
        << "all clocks are deleted."
        << std::endl;
   return true;
}
