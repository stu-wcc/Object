/////////////////////////////////////////////////////////////////
//                       IdealCircuit.cpp
//
//        Function definition for MakeIdealCircuit class
//
/////////////////////////////////////////////////////////////////


#include "IdealCircuit.h"
#include <string.h>
#include "app-common/command.h"

// Print individual node in levelized format
void IdealCircuit::printNodeInLevFormat( int id,
                                         int type,
                                         int level,
                                         int numPreds,
                                         std::vector<int>& predC0,
                                         std::vector<int>& predC1,
                                         int numSuccs,
                                         std::vector<int>& succ,
                                         int obs,
                                         int C0,
                                         int C1,
                                         std::ostream &ofs ) const
{
   ofs << id+1 << " " << type << " " << level*5 << " " << numPreds << " ";
   for (int n = 0; n<numPreds; n++ ) {
      ofs << predC0[n]+1 << " ";
   }
   for (int n = 0; n<numPreds; n++ ) {
      ofs << predC1[n]+1 << " ";
   }
   ofs << numSuccs << " ";
   for (int n = 0; n<numSuccs; n++) {
      ofs << succ[n]+1 << " ";
   }
   ofs << obs << " ; " << C0 << " " << C1 << "\n";
}


std::vector<int> IdealCircuit::rectPred(int startId,
                                        int id,
                                        int lev_size)const{
   std::vector<int> predC0;
   predC0.push_back( id - lev_size );
   if ( id == ( startId + lev_size - 1 ) ){
      predC0.push_back( startId - lev_size );
   } else {
      predC0.push_back( id - lev_size + 1 );
   }
   return predC0;
}


std::vector<int> IdealCircuit::rectSucc(int startId,
                                        int id,
                                        int lev_size) const
{
   std::vector<int> succ;
   if ( id == startId ) {
      succ.push_back( id + 2*lev_size - 1 );
   } else {
      succ.push_back( id + lev_size - 1 );
   }
   succ.push_back( id + lev_size );
   return succ;
}


std::vector<int> IdealCircuit::triPred(int startId,
                                       int id,
                                       int preId,
                                       int lev_size,
                                       int preLevSize ) const {
   std::vector<int> predC0;
   predC0.push_back( id - startId + preId );
   predC0.push_back( id - startId + preId + preLevSize - lev_size );
   return predC0;
}


std::vector<int> IdealCircuit::triSucc(int startId,
                                       int id,
                                       int nextId,
                                       int lev_size,
                                       int nextLevSize) const
{
   std::vector<int> succ;
   int distance = id - startId;
   if ( distance < nextLevSize) {
      succ.push_back( id + lev_size );
   }
   if ( distance >= ( lev_size - nextLevSize) ) {
      succ.push_back( id + nextLevSize );
   }
   if ( startId != 0 ) {
      if ( (id >= nextId - lev_size + nextLevSize) && ( id < nextId ) ) {
         if ( ( id - startId + 1) != id) {
           succ.push_back( id - startId ); // map to id of level 0
         }
      }

   }
   return succ;
}


// Calculate the first id and gate size of each level for customize shape
bool IdealCircuit::customizeCircuit( std::ostream & os,
                                     std::vector<int> int_array,
                                     bool seq )const
{
   std::vector <int> firstnodes;
   std::vector <int> lev_sizes;
   std::vector <int> blk_widths;
   int num_levels = 0;
   int num_nodes = 0;
   for (size_t i=1; i<int_array.size(); i=i+2 ) {
      // int_array[i-1] is block height (level size)
      // int_array[i]   is block width (blk_widths)

      // Verify the height of blocks is decreasing; move to code of customize
      if( i != 1 ) {
         if( int_array[i-1] > int_array[i-3] )  {
            HLog << InfoError
                 << "height of current block should be less than that of "
                 << "previous block."
                 << std::endl;
            return false;
         }
      }
      blk_widths.push_back( int_array[i] );
      for (int count=0; count<int_array[i]; count++) {
         lev_sizes.push_back( int_array[i-1] );
         num_nodes = num_nodes + int_array[i-1];
         int curr_lev = num_levels;
         num_levels++;
         if( 0 == curr_lev ) {
            firstnodes.push_back( 0 );
         } else {
            int pre_lev = curr_lev - 1;
            firstnodes.push_back( lev_sizes[pre_lev] + firstnodes[pre_lev] );
         }
      }
   }
   return writeLevCircuit( os, lev_sizes, firstnodes, num_nodes, num_levels,
                    blk_widths, true);
}


// Calculate the first id and the gate size of each level for ordinary shape
bool IdealCircuit::ordinaryCircuit( std::ostream & os,
                                    int num_inputs,
                                    int num_levels,
                                    int decrement,
                                    int keep,
                                    bool seq ) const
{
   int num_nodes = 0;
   std::vector <int> firstnodes;
   std::vector <int> lev_sizes;
   std::vector <int> blk_widths;
   int cnt = 0;
   int height = num_inputs;
   for (int l=0; l<num_levels; l++) {
      lev_sizes.push_back( height );
      firstnodes.push_back( (l==0) ? 0 : ( firstnodes[l-1]+lev_sizes[l-1] ));
      num_nodes += height;
      cnt++;
      // either the block reaches its maximum level count or the level is
      //   the last level.
      if(cnt == keep || (num_levels-1) == l ){
         if (cnt == keep) { // Now it is time to decrease the height
            height -= decrement;
         }
         blk_widths.push_back( cnt );
         cnt = 0;
      }
   }
   return writeLevCircuit( os, lev_sizes, firstnodes, num_nodes, num_levels,
                    blk_widths, true);
}


// Write out levelized circuit
bool IdealCircuit::writeLevCircuit( std::ostream & os,
                                    std::vector<int>& lev_sizes,
                                    std::vector<int>& firstnodes,
                                    int num_nodes,
                                    int num_levels,
                                    std::vector<int>& blk_widths,
                                    bool seq ) const
{
   int id;
   int type;
   int level=0;
   int numPreds;
   int numSuccs;
   int obs=0;
   int C0=0;
   int C1=0;
   // Verify gate size of level 0 is not more than twice of that of level 1
   if ( blk_widths[0]==1 ) {
      if ( lev_sizes[0] > 2*lev_sizes[1] ) {
         HLog << InfoError
              << "Gate count at level 0 should be no more than twice "
              << "of that at level 1. "
              << std::endl;
         return false;
      }
   }

   os << num_nodes+1 << "\n"
      << "10\n";

   // For level 0 with DFFs
   // blk_id init to last block
   int blk_id = blk_widths.size() - 1;
   // predLev init to the last level of the 2nd-to-last block
   int predLev = num_levels - blk_widths[blk_id] - 1;
   for (id=0; id<lev_sizes[0]; id++) {
      type = 5; // type of DFF
      numPreds = 1;
      std::vector<int> predC0,predC1;
      std::vector<int> succ;

      // compute pred[]
      if ( 0 == lev_sizes[0] - lev_sizes[ num_levels-1 ] )  {
         // represent shape of rectangle
         predC0.push_back( firstnodes[num_levels-1] + id );
      } else if( id < lev_sizes[num_levels-1] ) {
         // its predecessor comes from last level
         predC0.push_back( firstnodes[num_levels-1] + id );
      } else if( (1 == blk_widths[0]) && (id >= lev_sizes[1]) ) {
         // Starting block has only 1 level and the gate id is in the range
         //   of [ lev_sizes[1], lev_sizes[0]-1 ]. The gate output of last
         //   level in 2nd block is used as it predecessor.
         // Consistency check is added to make sure that
         //   lev_sizes[0] < 2 * lev_sizes[1]
         predC0.push_back( id +lev_sizes[1]*blk_widths[1] );
      } else { // cases where predecessor comes from middle levels
         if( id < lev_sizes[predLev] ) {
            predC0.push_back( firstnodes[predLev] + id );
         } else {
            // update predLev to the last level of previous block
            predLev = predLev - blk_widths[--blk_id];
            predC0.push_back( firstnodes[predLev] + id );
         }
      }
      predC1.push_back( predC0[0] );
      // compute succ[]
      if ( lev_sizes[0] == lev_sizes[1] ){
         succ = rectSucc( 0, id, lev_sizes[0] );
      } else {
         succ = triSucc( 0, id, firstnodes[1], lev_sizes[0], lev_sizes[1] );
      }
      numSuccs = succ.size();
      printNodeInLevFormat( id, type, level, numPreds, predC0, predC1,
               numSuccs, succ, obs, C0, C1, os);
   }

   // For other levels with combinational logic gates
   for(int id=lev_sizes[0]; id<num_nodes; id++) {
      type = 6; // type of AND
      std::vector<int> predC0,predC1;
      std::vector<int> succ;
      if ( id == firstnodes[level+1] ) {
         level++;
      }
      // compute pred[]
      if ( lev_sizes[level-1] == lev_sizes[level] ) {
         predC0 = predC1 = rectPred( firstnodes[level], id, lev_sizes[level]);
      } else {
         predC0 = predC1 = triPred( firstnodes[level], id, firstnodes[level-1],
                               lev_sizes[level], lev_sizes[level-1] );
      }
      // compute succ[]
      if( level != num_levels-1) {
         if( lev_sizes[level+1] == lev_sizes[level] ) {
            succ=rectSucc( firstnodes[level], id, lev_sizes[level] );
         } else {
            succ=triSucc( firstnodes[level], id, firstnodes[level+1],
                          lev_sizes[level], lev_sizes[level+1] );
         }
      } else { // last level
         succ.push_back( id - firstnodes[level]); // map to id of level 0
      }
      // Handle the succ case when block 0 has only one level and the current
      //   level is the last level of block 1.
      if ( ( level == blk_widths[1] ) && ( lev_sizes[0]-lev_sizes[1]!=0 ) ) {
         int vv = firstnodes[blk_widths[1]+1] - (lev_sizes[0]-lev_sizes[1]);
         if (id >= vv ) {
            succ.push_back( id-lev_sizes[1] * blk_widths[1] );
         }
      }
      numPreds = predC0.size();
      numSuccs = succ.size();
      printNodeInLevFormat( id, type, level, numPreds, predC0, predC1,
               numSuccs, succ, obs, C0, C1, os);
   }
   return true;
}
