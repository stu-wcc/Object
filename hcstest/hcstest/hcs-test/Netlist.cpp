/////////////////////////////////////////////////////////////////
//                        Netlist.cpp
//
//  function definition for Netlist class
//
//////////////////////////////////////////////////////////////////

#include "app-common/utility.h"
#include "HtSetting.h"
#include "Netlist.h"
#include <assert.h>
#include <algorithm>
#include <climits>

Gate::Gate() : _id( -1 ),
               _pinName( "" ),
               _type( JUNK ),
               _level( -1 ),
               _numPred( -1 ),
               _numSucc( -1 ),
               _obs( -1 ),
               _c0( -1 ),
               _c1( -1 ),
               _flag( 0 )
{
   ;
}


Gate::~Gate() {
   ;
}


//////////////////////////////////////////////////////////////////
//      Gate type to string
//////////////////////////////////////////////////////////////////
std::string Gate::typeToName(GType t) {
   switch( t ) {
      //case JUNK: {          // 0
      //   return "JUNK";
      //}
      case T_INPUT: {       // 1
         return "INPUT";
      }
      case T_OUTPUT: {      // 2
         return "OUTPUT";
      }
      case T_XOR: {         // 3
         return "XOR";
      }
      case T_XNOR: {        // 4
         return "XNOR";
      }
      case T_DFF: {         // 5
         return "DFF";
      }
      case T_AND: {         // 6
         return "AND";
      }
      case T_NAND: {        // 7
         return "NAND";
      }
      case T_OR: {          // 8
         return "OR";
      }
      case T_NOR: {         // 9
         return "NOR";
      }
      case T_NOT: {         // 10
         return "NOT";
      }
      case T_BUF: {         // 11
         return "BUF";
      }
      case T_TIE1: {        // 12
         return "TIE1";
      }
      case T_TIE0: {        // 13
         return "TIE0";
      }
      case T_TIEX: {        // 14
         return "TIEX";
      }
      case T_TIEZ: {        // 15
         return "TIEZ";
      }
      case T_MUX_2: {       // 16
         return "MUX";
      }
      case T_BUS:{          // 17
         return "BUS";
      }
      case T_BUS_GOHIGH: {  // 18
         return "BUS_GOHIGH";
      }
      case T_BUS_GOLOW: {   // 19
         return "BUS_GOLOW";
      }
      case T_BUFZ: {         /* 20 */
         return "BUFZ";
      }
      case T_TRISTATE: {     /* 21 */
         return "TRISTATE";
      }
      case T_TRISTATEINV: {  /* 22 */
         return "TRISTATEINV";
      }
      case T_TRISTATE1: {    /* 23 */
         return "TRISTATE1";
      }
      case T_DFF_L: {        /* 24 */
         return "DFF_L";
      }
      case T_DFF_LCP: {      /* 25 */
         return "DFF_LCP";
      }
      case T_DFF_LC: {       /* 26 */
         return "DFF_LC";
      }
      case T_DFF_LP: {       /* 27 */
         return "DFF_LP";
      }
      case T_DFF_CP: {       /* 28 */
         return "DFF_CP";
      }
      case T_DFF_C: {        /* 29 */
         return "DFF_C";
      }
      case T_DFF_P: {        /* 30 */
         return "DFF_P";
      }
      case T_NAND_LATCH: {   /* 31 */
         return "NAND_LATCH";
      }
      case T_NOR_LATCH: {    /* 32 */
         return "NOR_LATCH";
      }
      case T_CMOS: {         /* 33 */
         return "CMOS";
      }
      case T_NMOS: {         /* 34 */
         return "NMOS";
      }
      case T_PMOS: {         /* 35 */
         return "PMOS";
      }
      case T_NOTIF1: {       /* 36 */
         return "NOTIF1";
      }
      case T_NOTIF0: {       /* 37 */
         return "NOTIF0";
      }
      case T_BUFIF1: {       /* 38 */
         return "BUFIF1";
      }
      case T_BUFIF0: {       /* 39 */
         return "BUFIF0";
      }
      case T_D_LATCH: {      /* 40 */
         return "T_D_LATCH";
      }
      case T_BUF_FB: {       /* 41 */
         return "T_BUF_FB";
      }
      default: {
         return "Something Wrong!!!";
      }
   }
}


//////////////////////////////////////////////////////////////////
//      Gate type to Verilog primitive gate (FastScan)
//////////////////////////////////////////////////////////////////
std::string Gate::typeToVlog(GType t) {
   switch( t ) {
      //case JUNK: {          // 0
      //   return "JUNK";
      //}
      /*
      case T_INPUT: {       // 1
         return "INPUT";
      }
      case T_OUTPUT: {      // 2
         return "OUTPUT";
      }
      */
      case T_XOR: {         // 3
         return "xor2";     // A0, A1, Z
      }
      case T_XNOR: {        // 4
         return "xnor2";
      }
      case T_DFF: {         // 5
         return "Dff";      // D, CLK, Q
      }
      case T_AND: {         // 6
         return "and";      // can be 02, 03, 04; A0, A1, ... Y
      }
      case T_NAND: {        // 7
         return "nand";
      }
      case T_OR: {          // 8
         return "or";
      }
      case T_NOR: {         // 9
         return "nor";
      }
      case T_NOT: {         // 10
         return "inv02";    // A, Y
      }
      case T_BUF: {         // 11
         return "buf02";    // A, Y
      }
      /*
      case T_TIE1: {        // 12
         return "TIE1";
      }
      case T_TIE0: {        // 13
         return "TIE0";
      }
      case T_TIEX: {        // 14
         return "TIEX";
      }
      case T_TIEZ: {        // 15
         return "TIEZ";
      }
      case T_MUX_2: {       // 16
         return "MUX";
      }
      case T_BUS:{          // 17
         return "BUS";
      }
      case T_BUS_GOHIGH: {  // 18
         return "BUS_GOHIGH";
      }
      case T_BUS_GOLOW: {   // 19
         return "BUS_GOLOW";
      }
      case T_BUSZ: {         // 20
         return "BUSZ";
      }
      case T_TRISTATE: {     // 21
         return "TRISTATE";
      }
      case T_TRISTATEINV: {  // 22
         return "TRISTATEINV";
      }
      case T_TRISTATE1: {    // 23
         return "TRISTATE1";
      }
      case T_DFF_L: {        // 24
         return "DFF_L";
      }
      case T_DFF_LCP: {      // 25
         return "DFF_LCP";
      }
      case T_DFF_LC: {       // 26
         return "DFF_LC";
      }
      case T_DFF_LP: {       // 27
         return "DFF_LP";
      }
      case T_DFF_CP: {       // 28
         return "DFF_CP";
      }
      case T_DFF_C: {        // 29
         return "DFF_C";
      }
      case T_DFF_P: {        // 30
         return "DFF_P";
      }
      case T_NAND_LATCH: {   // 31
         return "NAND_LATCH";
      }
      case T_NOR_LATCH: {    // 32
         return "NOR_LATCH";
      }
      case T_CMOS: {         // 33
         return "CMOS";
      }
      case T_NMOS: {         // 34
         return "NMOS";
      }
      case T_PMOS: {         // 35
         return "PMOS";
      }
      case T_NOTIF1: {       // 36
         return "NOTIF1";
      }
      case T_NOTIF0: {       // 37
         return "NOTIF0";
      }
      case T_BUFIF1: {       // 38
         return "BUFIF1";
      }
      case T_BUFIF0: {       // 39
         return "BUFIF0";
      }
      case T_D_LATCH: {      // 40
         return "T_D_LATCH";
      }
      case T_BUF_FB: {       // 41
         return "T_BUF_FB";
      }
      */
      default: {
         HLog <<  "unsupported gate type in Vlog!!!" << std::endl;
         assert( 0 );
      }
   }
}


LevelizedNetlist::LevelizedNetlist() : _isHitecLevelConvention( false ),
                                       _remodeledDFlipflopDetected( false ),
                                       _numNodes( 0 ),
                                       _junk( 0 ),
                                       _numPIs( 0 ),
                                       _numTIEs( 0 ),
                                       _numPOs( 0 ),
                                       _numDEs( 0 ),
                                       _numRDFFs( 0 ),
                                       _numFBBUFs( 0 ),
                                       _numLatches( 0 ),
                                       _firstNodePI( -1 ),
                                       _firstNodeTIE( -1 ),
                                       _firstNodeDE( -1 ),
                                       _firstNodeRDFF( -1 ),
                                       _firstNodeFBBUF( -1 ),
                                       _firstNodeLatch( -1 ),
                                       _numLevels( 0 ),
                                       _maxLevelWidth( 0 ),
                                       _numNodesPerType((int)T_LAST_GTYPE, 0)
{
   ;
}


LevelizedNetlist::~LevelizedNetlist() {
   ;
}


void LevelizedNetlist::clear() {
   _isHitecLevelConvention = false;
   _remodeledDFlipflopDetected = false;
   _numNodes = 0;
   _junk = 0;
   _numPIs = 0;
   _numTIEs = 0;
   _numPOs = 0;
   _numDEs = 0;
   _numRDFFs = 0;
   _numFBBUFs = 0;
   _firstNodePI = -1;
   _firstNodeTIE = -1;
   _firstNodeDE = -1;
   _firstNodeRDFF = -1;
   _firstNodeFBBUF = -1;
   _numLevels = 0;
   _maxLevelWidth = 0;
   std::fill(_numNodesPerType.begin(), _numNodesPerType.end(), 0);
   _numNodesInLevel.clear();
   _firstNodeInLevel.clear();
   _maxInputsInLevel.clear();
   _andIsOnInLevel.clear();
   _orIsOnInLevel.clear();
   _xorIsOnInLevel.clear();
   _invIsOnInLevel.clear();
   _clockInputNodes.clear();
   _dataInputNodes.clear();
   _outputNodes.clear();
   _nodes.clear();
   _combinedFlags.clear();
   _types.clear();
   _levels.clear();
   _preds.clear();
   _predFirst.clear();
   _succs.clear();
   _succFirst.clear();
   _clockIdAndOffstates.clear();
}


void LevelizedNetlist::populateOclVectors() {
   for( int g=0; g < numNodes(); g++ ) {
      const Gate & gat = constGate( g );

      _types.push_back( (int)gat.type() );
      // assert( (int)gat.type() < USHRT_MAX );
      // initialize _combinedFlags. Some fields will be adjusted later
      _combinedFlags.push_back( (char)gat.type() );
      _combinedFlags.push_back( 1 ); // default #gates to sim is 1
      _combinedFlags.push_back( 0 ); // default event offset is 0
      _combinedFlags.push_back( 0 ); // unused field for now

      _levels.push_back( gat.level() );
      // assert( gat.level() < USHRT_MAX );
      if( 0==gat.numPred() ) { // special case when numPred is 0
         _predFirst.push_back( -1 );
      }
      for( int p=0; p < gat.numPred(); p++ ) {
         if( 0==p ) { // first predecessor
            _predFirst.push_back( (int)_preds.size() );
         }
         int pred_id = gat.predC0()[p];
         _preds.push_back( pred_id ); // ( gat.predC0()[p] );
      }
      if( 0==gat.numSucc() ) { // special case when numSucc is 0
         _succFirst.push_back( -1 );
      }
      for( int s=0; s < gat.numSucc(); s++ ) {
         if( 0==s ) { // first successor
            _succFirst.push_back( (int)_succs.size() );
         }
         int succ_id = gat.succ()[s];
         _succs.push_back( succ_id ); // ( gat.succ()[s] );
      }
   }

   // Adjust for last node such that length of _predFirst and _succFirst
   // is 'numNodes() + 1' ???? check validity of the adjustment
   _predFirst.push_back( (int)_preds.size() );
   _succFirst.push_back( (int)_succs.size() );
   assert( _predFirst.size()==_succFirst.size() );
   assert( _succs.size()==_preds.size() );
   // Adjust the size of _types and _levels to be the same as _predFirst
   //   and _succFirst (to avoid out of bound access in ocl kernel )
   _types.push_back( 0 );
   _levels.push_back( 0 );
   // scan backward to adjust those predFirst[] and succFirst[] with value -1
   int curr_preds_index = -1;
   int curr_succs_index = -1;
   for( int i = (int)(_predFirst.size()-1); i >= 0 ; i-- ) {
      // Change predFirst[i] of -1 to its vector upperbound
      if( -1==_predFirst[i] ) {
         _predFirst[i] = curr_preds_index; // (int)_preds.size();
      } else {
         curr_preds_index = _predFirst[i];
      }
      if( -1==_succFirst[i] ) {
         _succFirst[i] = curr_succs_index; // (int)_succs.size();
      } else {
         curr_succs_index = _succFirst[i];
      }
   }
   if( clockedDffIsUsed() ) {
      for( int i=0; i<numRDFFs(); i++ ) {
         int gid_inv = firstNodeFF() + 5*i;
         int gid_master_mux = gid_inv + 1;
         int gid_slave_mux  = gid_inv + 2;

         _combinedFlags[(gid_inv        <<2) +GATES_SIM_OFFSET] = 5;
         _combinedFlags[(gid_master_mux <<2) +EVENT_ID_OFFSET] = -1;
         _combinedFlags[(gid_slave_mux  <<2) +EVENT_ID_OFFSET] = -2;
      }
   }

   // Init data structures for event-driven simulation on GPU
   // The following data are used for Ocl events-sched kernel
   //   _qEventsFlags[] id above numNodes()
   //   _qSizes[ numLevels() ]
   //   _qOffsets[ numLevels() ]
   _qEventsFlags.assign( numNodes() * 2, 0);
   _qSizes.assign( numLevels() + 1, 0);
   _qFlags.assign( numNodes(), 0);
   _qOffsets.assign( firstNodeInLevel().begin(), firstNodeInLevel().end() );
   _qOffsets.push_back( numNodes() );

   // shrink_to_fit to save memory usage
   _qEventsFlags.shrink_to_fit();
   _qOffsets.shrink_to_fit();
   _qSizes.shrink_to_fit();
   _qFlags.shrink_to_fit();
   _clockInputsAndOffstates.shrink_to_fit();
   _clockInputNodes.shrink_to_fit();
   _dataInputNodes.shrink_to_fit();
   _outputNodes.shrink_to_fit();
   _combinedFlags.shrink_to_fit();
   _types.shrink_to_fit();
   _levels.shrink_to_fit();
   _preds.shrink_to_fit();
   _predFirst.shrink_to_fit();
   _succs.shrink_to_fit();
   _succFirst.shrink_to_fit();
}


//trace pinname
std::string LevelizedNetlist::tracePinName( int id ) {
   if( "" != gate(id).pinName() ) {
      return gate(id).pinName();
   }
   int cid = id;
   while( "" ==gate(cid).pinName() ) {
      assert( gate(cid).numPred()==1 );
      assert( gate(cid).type()==T_BUF );
      cid = gate(cid).predC0(0);
   }
   return gate(cid).pinName();
}


bool LevelizedNetlist::readLevelizedCircuit( const std::string & filename ) {
   std::ifstream ifs( filename );
   if( !(ifs.is_open()) ) {
      HLog << InfoError
           << "failed to open levelized circuit '" << filename
           << std::endl;
      return false;
   }
   // Read in number of nodes
   int num_nodes=0;
   int max_level=0; // maximum level in circuit
   ifs >> num_nodes;
   num_nodes -= 1;
   // Read in junk
   int jk=0;
   ifs >> jk;

   // Read in information for each gate
   for( int i=0; i< num_nodes ; i++ ) {
      Gate gat;
      // Get gate id
      int gid=0;
      ifs >> gid;
      assert( (i+1)==gid );
      gat.id( gid );

      // Get gate type
      int gtype=0;
      ifs >> gtype;
      if( gtype <= (int)JUNK || gtype >= (int)T_LAST_GTYPE ) {
         HLog << "Undefined gate type " << gtype << " for gate "
              << gid << std::endl;
      }
      gat.type( (GType)gtype );

      // Get level
      int lev=0;
      ifs >> lev;
      lev = lev / 5; // adjust level by divding 5
      if( lev > max_level ) {
         max_level = lev;
      }
      gat.level( lev );

      // Get number of predecessors
      int n_pred=0;
      ifs >> n_pred;
      gat.numPred( n_pred );
      std::vector<int> vec_c0;
      for( int l=0; l<gat.numPred(); l++) {
         int n=0;
         ifs >> n;
         vec_c0.push_back( n-1 ); // offset 'n' by 1
      }
      gat.predC0( vec_c0 );
      std::vector<int> vec_c1;
      for( int l=0; l<gat.numPred(); l++) {
         int n=0;
         ifs >> n;
         vec_c1.push_back( n-1 );
      }
      gat.predC1( vec_c1 );

      // Get number of successors
      int n_succ=0;
      ifs >> n_succ;
      gat.numSucc( n_succ );
      std::vector<int> vec_s;
      for( int l=0; l<gat.numSucc(); l++) {
         int n=0;
         ifs >> n;
         vec_s.push_back( n-1 ); // offset 'n' by 1
      }
      gat.succ( vec_s );

      // Get observability value
      int ob=0;
      ifs >> ob;
      gat.obs( ob );

      // Get output flag
      std::string str("");
      ifs >> str;
      if( "O" == str ) {
         assert( T_OUTPUT==gat.type() );
      } else if (";" == str ) {
         assert( T_OUTPUT!=gat.type() );
      } else {
         HLog << "unknown primary output flag " << str << std::endl;
      }

      // Get C0 and C1
      int c0=0, c1=0;
      ifs >> c0;
      ifs >> c1;
      gat.c0( c0 );
      gat.c1( c1 );

      // Finish one gate
      _nodes.push_back( gat );
   }

   // Close input file stream
   ifs.close();

   // Compute statistics based on _nodes[] information
   computeCircuitStatistics();

   // populate Ocl vectors
   populateOclVectors(); // create vectors for OpenCL kernels
#ifdef HT_DEBUG
   std::ofstream ofs( "netlist_topology" );
   dumpSimplifiedNetlistTopology( ofs );
   ofs.close();
#endif
   return true;
}


void LevelizedNetlist::computeCircuitStatistics() {

   numNodes( (int)_nodes.size() );
   junk( 10 );
   int max_level = 0;
   for( int i=0; i < (int)_nodes.size(); i++) {
      const Gate & gat = _nodes[i];
      if( gat.level() > max_level ) {
         max_level = _nodes[i].level();
      }
      switch( gat.type() ) {
         case T_INPUT: {
            if( 0==_numPIs ) {
               _firstNodePI = i;
            }
            _dataInputNodes.push_back( i );
            _numPIs++;
            break;
         }
         case T_TIE1:
         case T_TIE0:
         case T_TIEX:
         case T_TIEZ: {
            if( 0==_numTIEs ) {
               _firstNodeTIE = i;
            }
            _numTIEs++;
            break;
         }
         case T_OUTPUT: {
            _numPOs++;
            _outputNodes.push_back( i );
            break;
         }
         case T_DFF_L:
         case T_DFF_LCP:
         case T_DFF_LC:
         case T_DFF_LP:
         case T_DFF_CP:
         case T_DFF_C:
         case T_DFF_P:
         case T_DFF: {
            if( 0==_numDEs ) {
               _firstNodeDE = i;
            }
            if( gat.level()==0 ) {
               // In hitec, DFF is at level 0
               _isHitecLevelConvention = true;
            }
            _numDEs++;
            break;
         }
         case T_BUF_FB: {
            if( 0==_numFBBUFs ) {
               _firstNodeFBBUF = i;
            }
            _numFBBUFs++;
            break;
         }
         case T_NAND_LATCH: {
            if( 0==_numLatches) {
               _firstNodeLatch = i;
            }
            _numLatches++;
            break;
         }
         default: {
            break;
         }
      }
   }
   _numLevels = max_level + 1;

   if( _nodes[_firstNodeDE].level() == max_level &&
       _nodes[_firstNodeDE-3].type() == T_NOT  &&
       _nodes[_firstNodeDE-2].type() == T_MUX_2 &&
       _nodes[_firstNodeDE-1].type() == T_MUX_2 &&
       _nodes[_firstNodeDE].type() == T_DFF &&
       _nodes[_firstNodeDE+1].type() == T_DFF ) {
      _remodeledDFlipflopDetected = true;
      assert( 0==(_numDEs%2) );
      _numRDFFs = _numDEs / 2;
      _firstNodeRDFF = _firstNodeDE - 3;
      assert( 0==(((int)_nodes.size()-firstNodeRDFF())%5) );
   }
   if( false==_isHitecLevelConvention && 0==numDEs() ) { // combinational ckt
      int pre_oid = 0; // previous output gate id
      for( size_t oi=0; oi < _outputNodes.size(); oi++ ) {
         const Gate & gat = _nodes[ _outputNodes[oi] ];
         if( gat.level() != max_level ) {
            // For non-hitec, output nodes must be at last level in case
            //   there is no DFF
            _isHitecLevelConvention = true;
            break;
         } else if( oi > 0 ) {
            if( (pre_oid + 1) != _outputNodes[oi] ) {
               // For non-hitec, output nodes must be placed consecutively
               _isHitecLevelConvention = true;
               break;
            }
         }
         pre_oid = _outputNodes[oi];
      }
   }

   std::fill(_numNodesPerType.begin(), _numNodesPerType.end(), 0);
   _numNodesInLevel.resize( numLevels() );
   _firstNodeInLevel.resize( numLevels() );
   _maxInputsInLevel.resize( numLevels() );
   _andIsOnInLevel.resize( numLevels() );
   _orIsOnInLevel.resize( numLevels() );
   _xorIsOnInLevel.resize( numLevels() );
   _invIsOnInLevel.resize( numLevels() );
   for( int l=0; l<numLevels(); l++ ) {
      _numNodesInLevel[ l ] = 0;
      _firstNodeInLevel[ l ] = -1;
      _maxInputsInLevel[ l ] = 0;
      _andIsOnInLevel[ l ] = 0;
      _orIsOnInLevel[ l ] = 0;
      _xorIsOnInLevel[ l ] = 0;
      _invIsOnInLevel[ l ] = 0;
   }
   for( int i=0; i<numNodes(); i++ ) {
      int lev = gate(i).level();
      _numNodesInLevel[ gate(i).level() ]++;
      if( -1 == _firstNodeInLevel[ gate(i).level() ] ) {
         _firstNodeInLevel[ gate(i).level() ] = i;
      }
      if( gate(i).numPred() > _maxInputsInLevel[lev]) {
         _maxInputsInLevel[lev] = gate(i).numPred();
      }
      _numNodesPerType[ (int)gate(i).type() ]++;
      if( gate(i).type()==T_AND || gate(i).type()==T_NAND) {
         _andIsOnInLevel[lev] = 1;
      }
      if( gate(i).type()==T_OR || gate(i).type()==T_NOR) {
         _orIsOnInLevel[lev] = 1;
      }
      if( gate(i).type()==T_XOR || gate(i).type()==T_XNOR) {
         _xorIsOnInLevel[lev] = 1;
      }
      if( gate(i).type()==T_NOT ||
          gate(i).type()==T_NOR ||
          gate(i).type()==T_NAND ||
          gate(i).type()==T_XNOR) {
         _invIsOnInLevel[lev] = 1;
      }
   }
   _maxLevelWidth = *(std::max_element( _numNodesInLevel.begin(),
                                        _numNodesInLevel.end() ) );

}


bool LevelizedNetlist::readPinnameMapping( const std::string & filename ) {
   std::ifstream ifs( filename );
   if( !(ifs.is_open()) ) {
      HLog << InfoError
           << "failed to open pinname mapping file '" << filename
           << std::endl;
      return false;
   }

   // NOTE, there may be inserted buffers which do not have pin name
   for( int i=0; i<numNodes(); i++ ) {
      // Replace the splitter with a general utility function later
      std::string line("");
      std::getline( ifs, line);
      std::stringstream ss( line );
      std::vector< std::string > tokens;
      std::string buf;
      while( ss >> buf ) {
         tokens.push_back( buf );
      }
      int gid=0;
      if( tokens.size()==2 ) {
         gid = atoi( tokens[0].c_str() );
         _nodes[gid-1].pinName( tokens[1] );
      }
   }
   return true;
}


void LevelizedNetlist::dumpSimplifiedNetlistTopology( std::ostream& os) const
{
   for( int g=0; g < numNodes(); g++) {
      const Gate& gat = _nodes[g];
      os << Gate::typeToName( logic(g) ) << "(" << g+1 << ") = ";
      os << "p( ";
      for( int l=0; l<gat.numPred(); l++) {
         os << (gat.predC0())[l] + 1 << " ";
      }
      os << "); ";
      os << "s( ";
      for( int l=0; l<gat.numSucc(); l++) {
         os << (gat.succ())[l] + 1 << " ";
      }
      os << ") level:" << gat.level();
      os << std::endl;
   }
}


void LevelizedNetlist::dumpMultipliedLevelCircuit( std::ostream& os,int mul) {

   assert( mul >= 1 );

   os << (_nodes.size() * mul) + 1 << std::endl;
   os << junk() << std::endl;
   for( int m=0; m < mul; m++) {
      int offset = m * numNodes();
      for( size_t i=0; i < _nodes.size(); i++) {
         const Gate& gat = _nodes[i];

         os << (i+1)+offset << " ";
         os << _types[i] << " ";
         os << (_levels[i] * 5) << " ";

         os << gat.numPred() << " ";
         for( int l=0; l<gat.numPred(); l++) {
            os << (gat.predC0())[l] + 1 + offset << " ";
         }
         for( int l=0; l<gat.numPred(); l++) {
            os << (gat.predC1())[l] + 1 + offset << " ";
         }

         os << gat.numSucc() << " ";
         for( int l=0; l<gat.numSucc(); l++) {
            os << (gat.succ())[l] + 1 + offset << " ";
         }

         os << gat.obs() << " ";
         if( T_OUTPUT == gat.type() ) {
            os << "O ";
         } else {
            os << "; ";
         }
         os << gat.c0() << " " << gat.c1() << std::endl;
      }
   }
}


void LevelizedNetlist::dumpMultipliedBenchCircuit( std::ostream& os,int mul) {

   // Add a header in dump file to indicate the original circuit
   //   and number of multiplication
   std::string pin_name("");
   // Write out inputs
   for( int g=0; g<numPIs(); g++ ) {
      for( int m=0; m<mul; m++ ) {
         pin_name = gate(g).pinName();
         pin_name = pin_name + "_" + std::to_string( m );
         os << "INPUT( " << pin_name << " )" << std::endl;
      }
   }
   // Write out outputs
   for( int i=0; i<numPOs(); i++ ) {
      int gPO = output(i);
      int pre_id = gate(gPO).predC0(0);
      for( int m=0; m<mul; m++ ) {
         pin_name = gate(pre_id).pinName();
         pin_name = pin_name +"_" + std::to_string( m );
         os << "OUTPUT( "<< pin_name << " )" << std::endl;
      }
   }
   // Write out DFFs
   for( int gFF=firstNodeDE(); gFF < firstNodeDE()+numDEs(); gFF++ ) {
      std::string pin_name_pre("");
      std::string gate_name = gate(gFF).typeName();
      pin_name = gate(gFF).pinName();
      int pre_id = gate(gFF).predC0(0);
      pin_name_pre = tracePinName(pre_id);
      for( int m=0; m<mul; m++ ) {
         os << pin_name <<"_"<< std::to_string(m) << " = " << gate_name
            << "( "<< pin_name_pre << "_" << std::to_string(m)<< " )"
            << std::endl;
      }
   }
   // Write out normal gates excluding OUTPUTs and virtual buffers
   for( int gGat=numPIs()+numDEs(); gGat < numNodes(); gGat++) {
      std::string gate_name = gate(gGat).typeName();
      pin_name = gate(gGat).pinName();
      if( (T_OUTPUT!=gate(gGat).type()) && (""!=gate(gGat).pinName())) {
         for( int m=0; m<mul; m++ ) {
            std::string pin_name_pre("");
            for (int i=0; i<gate(gGat).numPred(); i++) {
               int pre_id = gate(gGat).predC0(i);
               if ( "" == pin_name_pre ){
                  pin_name_pre = tracePinName(pre_id) + "_"  +
                                 std::to_string( m );
               } else {
                  pin_name_pre = pin_name_pre +" , "
                                 + tracePinName(pre_id)
                                 + "_" +std::to_string( m );
               }
            }
            os << pin_name << "_" <<std::to_string(m) << " = " <<gate_name
               << "( "<< pin_name_pre << " )"
               << std::endl;
         }
      }
   }
   os << "END" << std::endl;
   HLog << InfoNote
        << "circuit written successfully. Duplication: "<< mul
        << std::endl;
}


void LevelizedNetlist::dumpLevelizedCircuitWithGroupedTypes(
                       std::ostream& os ) const
{
   // Vector to store OLD gate id and NEW gate id; index are
   //   [new gate id] and [old gate id] respectively
   std::vector<int>  new_gate_id( numNodes(), -1 );
   std::vector<int>  old_gate_id( numNodes(), -1 );

   int last_node = (numLevels()==1) ? numNodes() : _firstNodeInLevel[1];
   for( int g = _firstNodeInLevel[0]; g < last_node; g++) {
      new_gate_id[g] = g;
      old_gate_id[g] = g;
      //HLog << "O:N (" << g << ":" << new_gate_id[g] << ")" << std::endl;
   }
   for( int lev=0; lev < numLevels(); lev++ ) {
      std::vector<int> empty_vec(0, 0);
      // allow for maximum 6-input gate
      int max_inputs = 6;
      std::vector< std::vector<int> > cnt_in(max_inputs, empty_vec);
      int n_types = (int)T_LAST_GTYPE;
      // Model as 3-d array [ gate_type ][ num_inputs ][ gate_count ]
      std::vector<std::vector<std::vector<int>>> group_gates(n_types, cnt_in);
      last_node=(numLevels()==(lev+1))? numNodes() : _firstNodeInLevel[lev+1];
      for( int g = _firstNodeInLevel[lev]; g < last_node; g++) {
         GType gtype = _nodes[g].type();
         switch ( gtype ) {
            case T_OUTPUT:
            case T_NOT:
            case T_BUF:
            case T_BUFZ:
            {
               // all the above types have only 1 input
               group_gates[(int)gtype][0].push_back( g );
               break;
            }
            case T_XOR:
            case T_XNOR:
            case T_AND:
            case T_NAND:
            case T_OR:
            case T_NOR:
            case T_BUS:
            case T_TRISTATE1:
            {
               // input count can vary for the above gate types.
               int num_preds = _nodes[g].numPred();
               group_gates[(int)gtype][num_preds-1].push_back( g );
               break;
            }
            case T_INPUT:
            case T_DFF:
            case T_DFF_L:
            case T_TIE1:
            case T_TIE0:
            case T_TIEX:
            case T_TIEZ:
            {
               // ignore the fact some TIEX may have lots of fanins
               // TIE1, TIE0 and TIEZ have no fanin
               group_gates[(int)gtype][0].push_back( g );
               break;
            }
            case T_MUX_2:
            {
               int num_preds = _nodes[g].numPred();
               group_gates[(int)gtype][num_preds-1].push_back( g );
               break;
            }
            default: // such as T_INPUT, T_DFF and other unsupported types
            {
               HLog << InfoError
                    << "unexpected gate type '" << _nodes[g].typeName() << "'"
                    << " on gate " << g
                    << std::endl;
               return;
            }
         }
      }
      // update new_gate_id and old_gate_id for level 'lev'
      std::vector<int> gate_in_level;
      int myint[] = { (int)T_INPUT, // the ordered types in lev 0
                      (int)T_TIE1,
                      (int)T_TIE0,
                      (int)T_TIEZ,
                      (int)T_TIEX,
                      (int)T_DFF,
                      (int)T_DFF_L };
      std::vector<int> pseudoin_types( myint,
                                       myint + sizeof(myint)/sizeof(int));
      if( 0==lev ) { // special treatment at level 0
         for( size_t t=0; t < pseudoin_types.size(); t++) {
            int type = pseudoin_types[t];
            for( size_t i=0; i< group_gates[type][0].size(); i++) {
               gate_in_level.push_back( group_gates[type][0][i] );
            }
         }
      }
      for( int type=0; type < n_types; type++ ) {
         // Take care of 1-input gate first
         if( (int)T_OUTPUT == type || (int)T_NOT == type ||
             (int)T_BUF == type ) {
            for( int ins=0; ins < max_inputs; ins++ ) {
               for(size_t ui=0; ui < group_gates[type][ins].size(); ui++) {
               //HLog << "(" << type << ":" << ins << ":" << ui << ")-"
               //     << group_gates[type][ins][ui] << std::endl;
                  gate_in_level.push_back( group_gates[type][ins][ui]);
               }
            }
         }
      }
      for( int type=0; type < n_types; type++ ) {
         if( std::find( pseudoin_types.begin(), pseudoin_types.end(), type )
             != pseudoin_types.end()) {
            continue; // skip pseudo input types as they are handled already
         }
         // Take care of 2-or-more-input gate
         if( (int)T_OUTPUT != type && (int)T_NOT != type &&
             (int)T_BUF != type ) {
            for( int ins=0; ins < max_inputs; ins++ ) {
               for(size_t ui=0; ui < group_gates[type][ins].size(); ui++) {
               //HLog << "(" << type << ":" << ins << ":" << ui << ")-"
               //     << group_gates[type][ins][ui] << std::endl;
                  gate_in_level.push_back( group_gates[type][ins][ui]);
               }
            }
         }
      }
      assert((int)(gate_in_level.size()) == _numNodesInLevel[lev]);
      for( size_t i=0; i < gate_in_level.size(); i++) {
         int g = gate_in_level[i];
         new_gate_id[g] = _firstNodeInLevel[lev] + i;
         old_gate_id[_firstNodeInLevel[lev] + i] = g;
         //HLog << "O:N (" << g << ":" << new_gate_id[g] << ")" << std::endl;
      }
   }
   // write out new levelized circuit
   os << numNodes()+1 << std::endl;
   os << junk() << std::endl;
   for( int i=0; i<numNodes(); i++) {
      os << (i+1) << " "; // 'i' is the new gate id
      os << _types[ old_gate_id[i] ] << " ";
      os << (_levels[ old_gate_id[i] ] * 5) << " ";

      const Gate & gat = _nodes[old_gate_id[i]];
      os << gat.numPred() << " ";
      for( int l=0; l<gat.numPred(); l++) {
         os << new_gate_id[(gat.predC0())[l]] + 1 << " ";
      }
      for( int l=0; l<gat.numPred(); l++) {
         os << new_gate_id[(gat.predC1())[l]] + 1 << " ";
      }
      os << gat.numSucc() << " ";
      for( int l=0; l<gat.numSucc(); l++) {
         os << new_gate_id[(gat.succ())[l]] + 1 << " ";
      }
      os << gat.obs() << " ";
      if( T_OUTPUT == gat.type() ) {
         os << "O ";
      } else {
         os << "; ";
      }
      os << gat.c0() << " " << gat.c1() << std::endl;
   }
}


// Experiment on the effect of level rebalancing
void LevelizedNetlist::dumpRebalancedLevelNetlist( std::ostream& os, int align)
{

   std::vector< int > new_level( numNodes(), 0);

   int ave_level_size = (numNodes()-numNodesPerLevel()[0]) / (numLevels()-1);
   if( (numNodes()-numNodesPerLevel()[0]) % (numLevels()-1)) {
      ave_level_size += 1;
   }
   if( 0 != (ave_level_size % align) ) {
      ave_level_size += (align - (ave_level_size % align));
   }
   int current_lev = 1;
   int gates_in_lev = 0;
   for( int i=numNodesPerLevel()[0]; i<numNodes(); i++) {
      new_level[i] = current_lev;
      gates_in_lev += 1;
      if( gates_in_lev==ave_level_size ) {
         gates_in_lev = 0;
         current_lev += 1;
      }
   }

   // write out new levelized circuit
   os << numNodes()+1 << std::endl;
   os << junk() << std::endl;
   for( int i=0; i<numNodes(); i++) {
      const Gate & gat = _nodes[i];
      os << (i+1) << " ";        // gate id
      os << _types[i] << " ";    // gate type

      os << (new_level[i] * 5) << " ";
      os << gat.numPred() << " ";
      for( int l=0; l<gat.numPred(); l++) {
         os << (gat.predC0())[l] + 1 << " ";
      }
      for( int l=0; l<gat.numPred(); l++) {
         os << (gat.predC1())[l] + 1 << " ";
      }
      os << gat.numSucc() << " ";
      for( int l=0; l<gat.numSucc(); l++) {
         os << (gat.succ())[l] + 1 << " ";
      }
      os << gat.obs() << " ";
      if( T_OUTPUT == gat.type() ) {
         os << "O ";
      } else {
         os << "; ";
      }
      os << gat.c0() << " " << gat.c1() << std::endl;
   }
}


void LevelizedNetlist::processAlienLevelNetlist( std::ostream& os ) {

   int max_level = 0;
   std::vector< int > new_level( numNodes(), 0);
   std::vector< int > input_nodes;
   std::vector< int > output_nodes;
   std::vector< int > tie_nodes;
   std::vector< int > ff_nodes;
   for( int g=0; g < numNodes(); g++) {
      const Gate& gat = _nodes[g];
      switch( gat.type() ) {
         case T_INPUT: {
            input_nodes.push_back( g );
            new_level[g] = 0;
            break;
         }
         case T_OUTPUT: {
            output_nodes.push_back( g );
            new_level[g] = _nodes[g].level(); // Use the level as is
            if( max_level < new_level[g]) { max_level = new_level[g]; }
            break;
         }
         case T_TIE1:
         case T_TIE0:
         case T_TIEX:
         case T_TIEZ: {
            tie_nodes.push_back( g );
            new_level[g] = 0;
            break;
         }
         case T_DFF:
         case T_DFF_L:        /* 24 */
         case T_DFF_LCP:      /* 25 */
         case T_DFF_LC:       /* 26 */
         case T_DFF_LP:       /* 27 */
         case T_DFF_CP:       /* 28 */
         case T_DFF_C:        /* 29 */
         case T_DFF_P:        /* 30 */
         case T_NAND_LATCH:   /* 31 */
         case T_NOR_LATCH:    /* 32 */ {
            ff_nodes.push_back( g );
            new_level[g] = 0;
            break;
         }
         default: {
            // assert( _nodes[g].level()!=0 );
            // There are gates at level 0 that are not INPUTS, TIE, DFF etc
            //    move those gates to level 1
            new_level[g] = (_nodes[g].level()==0) ? 1 : _nodes[g].level();
            if( max_level < new_level[g]) { max_level = new_level[g]; }
            break;
         }
      }
   }
   std::vector< int > start_points;
   start_points.insert( start_points.end(),
                        input_nodes.begin(), input_nodes.end());
   start_points.insert( start_points.end(),
                        tie_nodes.begin(), tie_nodes.end());
   start_points.insert( start_points.end(),
                        ff_nodes.begin(), ff_nodes.end());

   std::vector<int> empty_vec;
   std::vector< std::vector<int> > nodes_at_level( max_level+1, empty_vec);

   // group nodes at level 0
   for( size_t s=0; s < start_points.size(); s++ ) {
      nodes_at_level[0].push_back( start_points[s] );
   }
   // group nodes at level other than 0
   for( int g=0; g < numNodes(); g++) {
      if( !(_nodes[g].isPseudoInput()) ) {
         nodes_at_level[ new_level[g] ].push_back( g );
      }
   }
   // special treatment for level with 0 node count
   int adj_levels = 0;
   for( int l=0; l < max_level+1; l++ ) {
      if( nodes_at_level[l].size()) { adj_levels++; }
   }
   HLog << InfoNote
        << "org_levels: " << max_level+1 << "  adj_levels: " << adj_levels
        << std::endl;

   // obtain mapping between old and new gate ids
   std::vector< int > old_gids; // Given new gid, obtain the old one
   int adj_l = 0;
   for( int l=0; l < max_level+1; l++ ) {
      for( size_t k=0; k < nodes_at_level[l].size(); k++ ) {
         new_level[ nodes_at_level[l][k] ] = adj_l; // update new_level[]
      }
      old_gids.insert( old_gids.end(),
                       nodes_at_level[l].begin(), nodes_at_level[l].end() );
      if( nodes_at_level[l].size() ) {
         adj_l += 1; // increase adj_l only when nodes_at_level[l] is non-0
      }
   }
   assert( numNodes()==(int)old_gids.size() );
   std::vector< int > new_gids( numNodes(), 0 ); // From old id to new id
   for( int g=0; g < numNodes(); g++ ) {
      new_gids[ old_gids[g] ] = g;
   }
   // write out new levelized circuit
   os << numNodes()+1 << std::endl;
   os << junk() << std::endl;
   for( int i=0; i<numNodes(); i++) {
      const Gate & gat = _nodes[old_gids[i]];
      os << (i+1) << " "; // 'i' is the new gate id
      if( gat.isIndustrialDffOrLatch() ) { // ignore the effect of set,
         os << (int)T_DFF_L << " ";  //   reset and clock
      } else {
         os << _types[ old_gids[i] ] << " ";
      }

      os << (new_level[ old_gids[i] ] * 5) << " ";
      os << gat.numPred() << " ";
      for( int l=0; l<gat.numPred(); l++) {
         os << new_gids[(gat.predC0())[l]] + 1 << " ";
      }
      for( int l=0; l<gat.numPred(); l++) {
         os << new_gids[(gat.predC1())[l]] + 1 << " ";
      }
      os << gat.numSucc() << " ";
      for( int l=0; l<gat.numSucc(); l++) {
         os << new_gids[(gat.succ())[l]] + 1 << " ";
      }
      os << gat.obs() << " ";
      if( T_OUTPUT == gat.type() ) {
         os << "O ";
      } else {
         os << "; ";
      }
      os << gat.c0() << " " << gat.c1() << std::endl;
   }
}


bool LevelizedNetlist::levelizeFromInputsAndFfs( std::ostream& os ) {

   std::vector< int > input_nodes;
   std::vector< int > output_nodes;
   std::vector< int > tie_nodes;
   std::vector< int > ff_nodes;
   for( int g=0; g < numNodes(); g++) {
      const Gate& gat = _nodes[g];
      switch( gat.type() ) {
         case T_INPUT: {
            input_nodes.push_back( g );
            break;
         }
         case T_OUTPUT: {
            output_nodes.push_back( g );
            break;
         }
         case T_TIE1:
         case T_TIE0:
         case T_TIEX:
         case T_TIEZ: {
            tie_nodes.push_back( g );
            break;
         }
         case T_DFF:
         case T_DFF_L:        /* 24 */
         case T_DFF_LCP:      /* 25 */
         case T_DFF_LC:       /* 26 */
         case T_DFF_LP:       /* 27 */
         case T_DFF_CP:       /* 28 */
         case T_DFF_C:        /* 29 */
         case T_DFF_P:        /* 30 */
         case T_NAND_LATCH:   /* 31 */
         case T_NOR_LATCH:    /* 32 */ {
            ff_nodes.push_back( g );
            break;
         }
         default: {
            break;
         }
      }
   }

   std::vector< int > start_points;
   start_points.insert( start_points.end(),
                        input_nodes.begin(), input_nodes.end());
   start_points.insert( start_points.end(),
                        tie_nodes.begin(), tie_nodes.end());
   start_points.insert( start_points.end(),
                        ff_nodes.begin(), ff_nodes.end());

   std::vector<int> visited( numNodes(), 0 );
   std::vector<int> new_level( numNodes(), -1);
   std::vector<int> topo_order; // topologicial order of nodes
   std::vector< std::vector<int>> in_edges; // store incoming edges
   for( int i=0; i < numNodes(); i++) {     //  or predecessors
      in_edges.push_back( _nodes[i].predC0());
   }
   int max_level=0;
   // push into Queue with PIs, TIEs and FFs
   std::queue<int> queue;
   for( size_t s=0; s < start_points.size(); s++ ) {
      new_level[ start_points[s] ] = 0; // assign start points with lev 0
      in_edges[ start_points[s] ].clear(); //   and clear its in-edges
      int start_id = start_points[s];
      queue.push( start_id );
   }
   // Traverse netlist to obtain topological order
   while( !queue.empty() ) {
      int cid = queue.front(); // current gate id
      topo_order.push_back( cid ); // add it to topological order
      queue.pop();
      const Gate& gat = _nodes[cid];
      for( int o=0; o<gat.numSucc(); o++) {
         int succ_id = (gat.succ())[o];
         if( 0==new_level[succ_id] ) {
            continue; // DONOT loop over level 0 node
         }
         if( succ_id >= numNodes() ) {
             HLog << InfoError
                  << "gate id: " << cid+1 << ";  succ_id: " << succ_id+1
                  << std::endl;
         }
         in_edges[succ_id].erase( std::find(
            in_edges[succ_id].begin(), in_edges[succ_id].end(), cid));
         if( in_edges[succ_id].size()==0
             && !(_nodes[succ_id].isDffOrLatch())) {
            queue.push( succ_id );
            // compute new_level as all predecessors are touched
            int new_l = 0;
            for( int p=0; p < _nodes[succ_id].numPred(); p++) {
               int lev = new_level[ (_nodes[succ_id].predC0())[p]];
               if( new_l < lev)  { new_l = lev; }
            }
            new_level[succ_id] = new_l + 1;
            if( max_level < new_level[succ_id]) {
               max_level = new_level[succ_id];
            }
         }
      }
   }
   int num_loop_nodes = 0;
   for( int g=0; g < numNodes(); g++ ) {
      if( -1==new_level[g] ) { // Check if node is in a loop
         new_level[g] = _nodes[g].level();
         num_loop_nodes++;
      }
   }
   if( num_loop_nodes ) {
      HLog << InfoNote << num_loop_nodes << " nodes are in loops. "
           << "Old levels are used." << std::endl;
   }
   // Special consideration for level of FFs; reset it to 0
   for( size_t i=0; i < ff_nodes.size(); i++) {
      new_level[ ff_nodes[i] ] = 0;
   }
   // Generate new levelized order
   std::fill( visited.begin(), visited.end(), 0);
   std::vector<int> empty_vec;
   std::vector< std::vector<int> > nodes_at_level( max_level+1, empty_vec);
   for( size_t s=0; s < start_points.size(); s++ ) {
      int start_id = start_points[s];
      queue.push( start_id );
      visited[ start_id ] = 1;
   }
   while( !queue.empty() ) { // Breadth First Search
      int cid = queue.front(); // current gate id
      nodes_at_level[ new_level[cid] ].push_back( cid );
      queue.pop();
      const Gate& gat = _nodes[cid];
      for( int o=0; o<gat.numSucc(); o++) {
         int succ_id = (gat.succ())[o];
         if( 0==visited[succ_id] ) {
            visited[ succ_id ] = 1;
            queue.push( succ_id );
         }
      }
   }
   std::vector< int > old_gids; // Given new gid, obtain the old one
   for( int l=0; l < max_level+1; l++ ) {
      old_gids.insert( old_gids.end(),
                       nodes_at_level[l].begin(), nodes_at_level[l].end() );
   }
   assert( numNodes()==(int)old_gids.size() );
   std::vector< int > new_gids( numNodes(), 0 ); // From old id to new id
   for( int g=0; g < numNodes(); g++ ) {
      new_gids[ old_gids[g] ] = g;
   }

   // write out new levelized circuit
   os << numNodes()+1 << std::endl;
   os << junk() << std::endl;
   for( int i=0; i<numNodes(); i++) {
      const Gate & gat = _nodes[old_gids[i]];
      os << (i+1) << " "; // 'i' is the new gate id
      if( gat.isIndustrialDffOrLatch() ) { // ignore the effect of set,
         os << (int)T_DFF_L << " ";  //   reset and clock
      } else {
         os << _types[ old_gids[i] ] << " ";
      }

      os << (new_level[ old_gids[i] ] * 5) << " ";
      os << gat.numPred() << " ";
      for( int l=0; l<gat.numPred(); l++) {
         os << new_gids[(gat.predC0())[l]] + 1 << " ";
      }
      for( int l=0; l<gat.numPred(); l++) {
         os << new_gids[(gat.predC1())[l]] + 1 << " ";
      }
      os << gat.numSucc() << " ";
      for( int l=0; l<gat.numSucc(); l++) {
         os << new_gids[(gat.succ())[l]] + 1 << " ";
      }
      os << gat.obs() << " ";
      if( T_OUTPUT == gat.type() ) {
         os << "O ";
      } else {
         os << "; ";
      }
      os << gat.c0() << " " << gat.c1() << std::endl;
   }

   return true;
}


void LevelizedNetlist::dumpLevelizedCircuit( std::string filename,
                                             bool test_ocl ) const
{
   std::ofstream ofs( filename );
   if( !(ofs.is_open()) ) {
      HLog << InfoError
           << "failed to open file '" << filename << " for write"
           << std::endl;
      return;
   }
   ofs << _nodes.size()+1 << std::endl;
   ofs << junk() << std::endl;
   for( size_t i=0; i < _nodes.size(); i++) {
      const Gate& gat = _nodes[i];

      if( test_ocl ) {
         ofs << gat.id() << " ";
         ofs << gat.type() << " ";
         ofs << (gat.level() * 5) << " ";
      } else {
         ofs << (i+1) << " ";
         ofs << _types[i] << " ";
         ofs << (_levels[i] * 5) << " ";
      }

      if( test_ocl ) {
         int num_preds = _predFirst[i+1] - _predFirst[i];
         ofs << num_preds << " ";
         for( int l=_predFirst[i]; l<_predFirst[i+1]; l++ ) {
            ofs << _preds[l] + 1 << " ";
         }
      } else {
         ofs << gat.numPred() << " ";
         for( int l=0; l<gat.numPred(); l++) {
            ofs << (gat.predC0())[l] + 1 << " ";
         }
      }
      for( int l=0; l<gat.numPred(); l++) {
         ofs << (gat.predC1())[l] + 1 << " ";
      }

      if( test_ocl ) {
         int num_succs = _succFirst[i+1] - _succFirst[i];
         ofs << num_succs << " ";
         for( int l=_succFirst[i]; l<_succFirst[i+1]; l++ ) {
            ofs << _succs[l] + 1 << " ";
         }
      } else {
         ofs << gat.numSucc() << " ";
         for( int l=0; l<gat.numSucc(); l++) {
            ofs << (gat.succ())[l] + 1 << " ";
         }
      }
      ofs << gat.obs() << " ";
      if( T_OUTPUT == gat.type() ) {
         ofs << "O ";
      } else {
         ofs << "; ";
      }
      ofs << gat.c0() << " " << gat.c1() << std::endl;
   }
   ofs.close();
}


bool LevelizedNetlist::dumpClockedCircuit( std::string filename,
                                          int clocks_edge ) const
{
   if( clockedDffIsUsed() ) {
      HLog << InfoError
           << "clocked Dff is used"
           << std::endl;
      return false;
   }
   for( int i=0; i<numFFs(); i++) {
      if( _nodes[firstNodeFF()+i].type() != T_DFF ) {
         HLog << InfoError
              << "only delay elements ared supported"
              << std::endl;
         return false;
      }
   }
   std::ofstream ofs( filename );
   if( !(ofs.is_open()) ) {
      HLog << InfoError
           << "failed to open file '" << filename << " for write"
           << std::endl;
      return false;
   }
   ofs << _nodes.size()+5 << std::endl;
   ofs << junk() << std::endl;
   for( size_t i=0; i < _nodes.size(); i++) {
      const Gate& gat = _nodes[i];
      ofs << gat.id() << " ";
      if( gat.type() == (int)T_DFF ) {
         // change dff to D Flip Flop
         ofs << (int)T_DFF_L << " ";
      } else {
         ofs << gat.type() << " ";
      }
      ofs << (gat.level() * 5) << " ";

      if( gat.type() == T_DFF ) {
         int num_preds = 4;
         assert( gat.numPred()==1 );
         ofs << num_preds << " ";
         for (int i=0; i<2; i++ ) {
            ofs << _nodes.size()+1 << " "; // set
            ofs << _nodes.size()+2 << " "; // reset
            ofs << _nodes.size()+4 << " "; // buffer/inverter
            ofs << (gat.predC0())[0]+1 << " ";
         }
      } else {
         ofs << gat.numPred() << " ";
         for( int l=0; l<gat.numPred(); l++) {
            ofs << (gat.predC0())[l] + 1 << " ";
         }
         for( int l=0; l<gat.numPred(); l++) {
            ofs << (gat.predC1())[l] + 1 << " ";
         }
      }
      ofs << gat.numSucc() << " ";
      for( int l=0; l<gat.numSucc(); l++) {
         ofs << (gat.succ())[l] + 1 << " ";
      }
      ofs << gat.obs() << " ";
      if( T_OUTPUT == gat.type() ) {
         ofs << "O ";
      } else {
         ofs << "; ";
      }
      ofs << gat.c0() << " " << gat.c1() << std::endl;
   }
   //new gates
   for( int g=0; g<4; g++ ) {
      int gid = _nodes.size()+g;
      ofs << gid+1 << " ";
      if( g==0 || g==1 ) {
         ofs << (int)T_TIE0 << " "
             << "0 " << "0 ";
      }else if ( g==2 ) {
         ofs << (int)T_INPUT << " "
             << "0 " << "0 "
             << "1 " << gid+2 << " ";
      // type is inverter/buffer
      }else {
         if( clocks_edge == 0 ) {
            // Rising edges
            ofs <<(int)T_BUF << " ";
         } else {
            // Falling edges
            ofs << (int)T_NOT << " ";
         }
         ofs << "5 "
             << "1 "<< gid << " " << gid << " ";
      }
      // if the new gate is not clock pin and its successors are all the dffs
      if( 2 != g ) {
         ofs << numFFs() << " ";
         for( int n = firstNodeFF(); n < firstNodeFF()+numFFs(); n++) {
            ofs << n+1 << " ";
         }
      }
      ofs << "0 " << "; " << "0 0"
          << std::endl;
   }
   return true;
}


void LevelizedNetlist::dumpFscanVerilogNetlist( std::ostream& ofs,
                                                std::string& fname ) const
{
   // print module header
   ofs << "// Verilog file '" << fname << "'\n"
       << "//   dumped at " << currentDateTime( SECOND_PRECISION )
       << std::endl;
   ofs << "// " << numPIs() << " inputs" << std::endl;
   ofs << "// " << numPOs() << " outputs" << std::endl;
   ofs << "// " << numFFs() << " D flip flops" << std::endl;
   ofs << "module top( CK";
   for( int i=0; i < numPIs(); i++ ) {
      ofs << ", G" << i+1;
   }
   for( int i=0; i < numPOs(); ++i ) {
      ofs << ", G" << output(i)+1;
   }
   ofs << ");" << std::endl;

   // declare INPUT
   ofs << "input CK";
   for( int i=0; i < numPIs(); i++ ) {
      ofs << ",G" << i+1;
   }
   ofs << ";" << std::endl;

   // declare OUTPUT
   ofs << "output ";
   for( int i=0; i < numPOs(); ++i ) {
      ofs << ((0==i) ? " G" : ",G")
          << output(i)+1;
   }
   ofs << ";" << std::endl << std::endl;

   // print logic gates
   for( int i=0; i < numNodes(); i++ ) {
      const Gate& gat = _nodes[i];
      switch( logic(i) ) {
         case T_INPUT:
         case T_OUTPUT: {
            break; // skip
         }
         case T_XOR:
         case T_XNOR: {
            ofs << TwoSpace
                << Gate::typeToVlog( logic(i)) << " U" << i+1 << "(";
            for( int p=0; p < gat.numPred(); p++) {
               ofs << ".A" << p << " (G" << gat.predC0(p)+1 << "), ";
            }
            ofs << ".Y (G" << i+1 << "));" << std::endl;
            break;
         }
         case T_DFF: {
            ofs << TwoSpace
                << Gate::typeToVlog( logic(i)) << " U" << i+1 << "(";
            ofs << ".D (G" << gat.predC0(0)+1 << "), "
                << ".CLK (CK), "
                << ".Q (G" << i+1 << "));"
                << std::endl;
            break;
         }
         case T_AND:
         case T_NAND:
         case T_OR:
         case T_NOR: {
            ofs << TwoSpace
                << Gate::typeToVlog( logic(i)) << "0" << gat.numPred()
                << " U" << i+1 << "(";
            for( int p=0; p < gat.numPred(); p++) {
               ofs << ".A" << p << " (G" << gat.predC0(p)+1 << "), ";
            }
            ofs << ".Y (G" << i+1 << "));" << std::endl;
            break;
         }
         case T_NOT:
         case T_BUF: {
            ofs << TwoSpace
                << Gate::typeToVlog( logic(i)) << " U" << i+1 << "(";
            ofs << ".A (G" << gat.predC0(0)+1 << "), "
                << ".Y (G" << i+1 << "));"
                << std::endl;
            break;
         }
         default: {
            HLog << InfoError
                 << "unsupported gate type " << Gate::typeToName( logic(i))
                 << std::endl;
            assert(0);
         }
      }
   }

   // print endmodule
   ofs << std::endl;
   ofs << "endmodule // top" << std::endl;
}


void LevelizedNetlist::dumpPinnameMapping( std::string filename ) const {
   std::ofstream ofs( filename );
   if( !(ofs.is_open()) ) {
      HLog << InfoError
           << "failed to open file '" << filename << " for write"
           << std::endl;
      return;
   }
   for( int i=0; i<numNodes(); i++) {
      const Gate& gat = _nodes[i];
      ofs << gat.id() << " " << gat.pinName() << std::endl;
   }
   ofs.close();
}


void LevelizedNetlist::printCktStatistics( bool verbose ) const {

   HLog << TwoSpace << numPIs() << " inputs\n"
        << TwoSpace << numPOs() << " outputs\n"
        << TwoSpace << numDEs() << " delay elements\n"
        << TwoSpace << numRDFFs() << " D flip flops (remodeled)\n"
        << TwoSpace << numFBBUFs() << " feedback buffers\n"
        << TwoSpace << numNodes() << " simulation gates\n"
        << TwoSpace << numLevels() << " total levels\n"
        << TwoSpace << maxLevelWidth() << " max level width"
        << std::endl;
   HLog << TwoSpace << "Statistics of gate types:\n";
   for( int i=0; i<(int)T_LAST_GTYPE; i++) {
      if( _numNodesPerType[i]  > 0 ) {
         HLog << TwoSpace << TwoSpace
              << Gate::typeToName((GType)i) << ": "
              << _numNodesPerType[i] << std::endl;
      }
   }
   HLog << TwoSpace << "Gates per level:" << std::endl;;
   for( int l=0; l < numLevels(); l++ ) {
      char str[1024];
      sprintf(str, "    %4d : %6d   (1st_node: %10d)",
              l, _numNodesInLevel[l], _firstNodeInLevel[l]);
      HLog << str << std::endl;
   }
   if( verbose ) {
      HLog << "\n" << TwoSpace << "Netlist for Opencl kernel:\n";
      HLog << TwoSpace
           << _types.size() << " : size of types array\n"
           << TwoSpace
           << _levels.size() << " : size of levels array\n"
           << TwoSpace
           << _preds.size() << " : size of predecessors array\n"
           << TwoSpace
           << _predFirst.size() << " : size of predFirst array\n"
           << TwoSpace
           << _succs.size() << " : size of successors array\n"
           << TwoSpace
           << _succFirst.size() << " : size of succFirst array\n"
           << std::endl;
   }
}


void LevelizedNetlist::makeCopyOfNetlistTopology(
                    std::vector<ReducedGate> & copied_nodes )
{
   assert( 0==copied_nodes.size() );
   for( size_t i=0; i < _nodes.size(); i++ ) {
      ReducedGate rg( _nodes[i].type(), _nodes[i].predC0(), _nodes[i].succ());
      copied_nodes.push_back( rg );
   }
}


bool LevelizedNetlist::addClocks(
                          const std::vector<int>& clk_ids, int off_state )
{
   for( size_t i=0; i<clk_ids.size(); i++) {
      if( clk_ids[i] > numNodes() || clk_ids[i] < 1 ) {
         HLog << InfoError
              << "gate id " << clk_ids[i] << " is not valid"
              << " (1:" << numNodes() << ")"
              << std::endl;
         return false;
      }
      if( _nodes[ clk_ids[i]-1].type() != T_INPUT ) {
         HLog << InfoError
              << "gate id " << clk_ids[i] << " is not an INPUT"
              << std::endl;
         return false;
      }
   }
   for( size_t i=0; i<clk_ids.size(); i++) {
      int gid = clk_ids[i] - 1; // adjust as internal gid starts with 0
      if(_clockIdAndOffstates.find( gid ) ==_clockIdAndOffstates.end()) {
         _clockIdAndOffstates[gid] = off_state;
         _clockInputNodes.push_back( gid );
         _clockInputsAndOffstates.push_back( gid );
         _clockInputsAndOffstates.push_back( off_state );

         auto it = std::find( _dataInputNodes.begin(),
                              _dataInputNodes.end(),
                              gid );
         _dataInputNodes.erase( it ); // remove gid from data inputs
      } else {
         HLog << InfoError
              << "gate id " << clk_ids[i] << " had been added a clock."
              << std::endl;
      }
   }
   return true;
}


void LevelizedNetlist::deleteClocks()
{
   _clockInputNodes.clear();
   _dataInputNodes.clear();
   for( int i=0; i < numPIs(); i++ ) { // restore original dataInputNodes[]
      _dataInputNodes.push_back( i );
   }
   _clockIdAndOffstates.clear();
   _clockInputsAndOffstates.clear(); // for OpenCL
}


void LevelizedNetlist::dumpOclSimEvents(int cur_lev) const {
   HLog << "Current level: " << cur_lev << std::endl;
   std::map<unsigned, int> freq;
   for( int l=0; l<numLevels(); l++ ) {
      HLog << "Level" << l << ":";
      for( int e=0; e<_qSizes[l]; e++ ) {
         unsigned gid = (_qEventsFlags[_qOffsets[l] +e] &MASK_UPTO_32[31])+1;
         if( freq.find( gid ) == freq.end()) {
            freq[ gid ] = 1;
         } else {
            freq[ gid ]++;
         }
         HLog << " " << gid;
      }
      // dump qFlags
      HLog << "  Q_flags:";
      int g = _firstNodeInLevel[l];
      for( int i=0; i < _numNodesInLevel[l]; i++, g++) {
         if( 1==_qFlags[g] ) {
            HLog << " " << g+1;
         }
      }
      HLog << std::endl;
   }
   for( auto it=freq.begin(); it != freq.end(); ++it) {
      if( it->second > 1 ) {
         HLog << "????? " << it->first << " is duplicated "
              << it->second << " times" << std::endl;
      }
   }
}


void LevelizedNetlist::dumpSuccsSucc1st() const {
   HLog << "\n#### Succ 1st: " << std::endl;
   for( size_t i=0; i < _succFirst.size(); i++ ) {
      HLog << "(" << i+1 << ":" << _succFirst[i] << ") ";
   }
   HLog << std::endl;

   HLog << "\n#### Succs: " << std::endl;
   for( size_t i=0; i < _succs.size(); i++ ) {
      HLog << "(" << i << ":" << _succs[i] + 1 << ") ";
   }
   HLog << std::endl;
}
