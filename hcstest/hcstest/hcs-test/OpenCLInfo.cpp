/////////////////////////////////////////////////////////////////
//                    OpenCLInfo.cpp
/////////////////////////////////////////////////////////////////
#include "OpenCLInfo.h"
#include "app-common/AppCommonSetting.h"

// Convert ocl function error code into readable string
const char* oclErrorString(cl_int error) {
   switch( error ) {
      // run-time and JIT compiler errors
      case CL_SUCCESS: { // 0:
         return "CL_SUCCESS";
      }
      case CL_DEVICE_NOT_FOUND: { // -1:
         return "CL_DEVICE_NOT_FOUND";
      }
      case CL_DEVICE_NOT_AVAILABLE: { // -2:
         return "CL_DEVICE_NOT_AVAILABLE";
      }
      case CL_COMPILER_NOT_AVAILABLE: { // -3:
         return "CL_COMPILER_NOT_AVAILABLE";
      }
      case CL_MEM_OBJECT_ALLOCATION_FAILURE: { // -4:
         return "CL_MEM_OBJECT_ALLOCATION_FAILURE";
      }
      case CL_OUT_OF_RESOURCES: { // -5:
         return "CL_OUT_OF_RESOURCES";
      }
      case CL_OUT_OF_HOST_MEMORY: { // -6:
         return "CL_OUT_OF_HOST_MEMORY";
      }
      case CL_PROFILING_INFO_NOT_AVAILABLE: { // -7:
         return "CL_PROFILING_INFO_NOT_AVAILABLE";
      }
      case CL_MEM_COPY_OVERLAP: { // -8:
         return "CL_MEM_COPY_OVERLAP";
      }
      case CL_IMAGE_FORMAT_MISMATCH: { // -9:
         return "CL_IMAGE_FORMAT_MISMATCH";
      }
      case CL_IMAGE_FORMAT_NOT_SUPPORTED: { // -10:
         return "CL_IMAGE_FORMAT_NOT_SUPPORTED";
      }
      case CL_BUILD_PROGRAM_FAILURE: { // -11:
         return "CL_BUILD_PROGRAM_FAILURE";
      }
      case CL_MAP_FAILURE: { // -12:
         return "CL_MAP_FAILURE";
      }
      case CL_MISALIGNED_SUB_BUFFER_OFFSET: { // -13:
         return "CL_MISALIGNED_SUB_BUFFER_OFFSET";
      }
      case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST: { // -14:
         return "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST";
      }
      case CL_COMPILE_PROGRAM_FAILURE: { // -15:
         return "CL_COMPILE_PROGRAM_FAILURE";
      }
      case CL_LINKER_NOT_AVAILABLE: { // -16:
         return "CL_LINKER_NOT_AVAILABLE";
      }
      case CL_LINK_PROGRAM_FAILURE: { // -17:
         return "CL_LINK_PROGRAM_FAILURE";
      }
      case CL_DEVICE_PARTITION_FAILED: { // -18:
         return "CL_DEVICE_PARTITION_FAILED";
      }
      case CL_KERNEL_ARG_INFO_NOT_AVAILABLE: { // -19:
         return "CL_KERNEL_ARG_INFO_NOT_AVAILABLE";
      }
      // compile-time errors
      case CL_INVALID_VALUE: { // -30:
         return "CL_INVALID_VALUE";
      }
      case CL_INVALID_DEVICE_TYPE: { // -31:
         return "CL_INVALID_DEVICE_TYPE";
      }
      case CL_INVALID_PLATFORM: { // -32:
         return "CL_INVALID_PLATFORM";
      }
      case CL_INVALID_DEVICE: { // -33:
         return "CL_INVALID_DEVICE";
      }
      case CL_INVALID_CONTEXT: { // -34:
         return "CL_INVALID_CONTEXT";
      }
      case CL_INVALID_QUEUE_PROPERTIES: { // -35:
         return "CL_INVALID_QUEUE_PROPERTIES";
      }
      case CL_INVALID_COMMAND_QUEUE: { // -36:
         return "CL_INVALID_COMMAND_QUEUE";
      }
      case CL_INVALID_HOST_PTR: { // -37:
         return "CL_INVALID_HOST_PTR";
      }
      case CL_INVALID_MEM_OBJECT: { // -38:
         return "CL_INVALID_MEM_OBJECT";
      }
      case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: { // -39:
         return "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR";
      }
      case CL_INVALID_IMAGE_SIZE: { // -40:
         return "CL_INVALID_IMAGE_SIZE";
      }
      case CL_INVALID_SAMPLER: { // -41:
         return "CL_INVALID_SAMPLER";
      }
      case CL_INVALID_BINARY: { // -42:
         return "CL_INVALID_BINARY";
      }
      case CL_INVALID_BUILD_OPTIONS: { // -43:
         return "CL_INVALID_BUILD_OPTIONS";
      }
      case CL_INVALID_PROGRAM: { // -44:
         return "CL_INVALID_PROGRAM";
      }
      case CL_INVALID_PROGRAM_EXECUTABLE: { // -45:
         return "CL_INVALID_PROGRAM_EXECUTABLE";
      }
      case CL_INVALID_KERNEL_NAME: { // -46:
         return "CL_INVALID_KERNEL_NAME";
      }
      case CL_INVALID_KERNEL_DEFINITION: { // -47:
         return "CL_INVALID_KERNEL_DEFINITION";
      }
      case CL_INVALID_KERNEL: { // -48:
         return "CL_INVALID_KERNEL";
      }
      case CL_INVALID_ARG_INDEX: { // -49:
         return "CL_INVALID_ARG_INDEX";
      }
      case CL_INVALID_ARG_VALUE: { // -50:
         return "CL_INVALID_ARG_VALUE";
      }
      case CL_INVALID_ARG_SIZE: { // -51:
         return "CL_INVALID_ARG_SIZE";
      }
      case CL_INVALID_KERNEL_ARGS: { // -52:
         return "CL_INVALID_KERNEL_ARGS";
      }
      case CL_INVALID_WORK_DIMENSION: { // -53:
         return "CL_INVALID_WORK_DIMENSION";
      }
      case CL_INVALID_WORK_GROUP_SIZE: { // -54:
         return "CL_INVALID_WORK_GROUP_SIZE";
      }
      case CL_INVALID_WORK_ITEM_SIZE: { // -55:
         return "CL_INVALID_WORK_ITEM_SIZE";
      }
      case CL_INVALID_GLOBAL_OFFSET: { // -56:
         return "CL_INVALID_GLOBAL_OFFSET";
      }
      case CL_INVALID_EVENT_WAIT_LIST: { // -57:
         return "CL_INVALID_EVENT_WAIT_LIST";
      }
      case CL_INVALID_EVENT: { // -58:
         return "CL_INVALID_EVENT";
      }
      case CL_INVALID_OPERATION: { // -59:
         return "CL_INVALID_OPERATION";
      }
      case CL_INVALID_GL_OBJECT: { // -60:
         return "CL_INVALID_GL_OBJECT";
      }
      case CL_INVALID_BUFFER_SIZE: { // -61:
         return "CL_INVALID_BUFFER_SIZE";
      }
      case CL_INVALID_MIP_LEVEL: { // -62:
         return "CL_INVALID_MIP_LEVEL";
      }
      case CL_INVALID_GLOBAL_WORK_SIZE: { // -63:
         return "CL_INVALID_GLOBAL_WORK_SIZE";
      }
      case CL_INVALID_PROPERTY: { // -64:
         return "CL_INVALID_PROPERTY";
      }
      case CL_INVALID_IMAGE_DESCRIPTOR: { // -65:
         return "CL_INVALID_IMAGE_DESCRIPTOR";
      }
      case CL_INVALID_COMPILER_OPTIONS: { // -66:
         return "CL_INVALID_COMPILER_OPTIONS";
      }
      case CL_INVALID_LINKER_OPTIONS: { // -67:
         return "CL_INVALID_LINKER_OPTIONS";
      }
      case CL_INVALID_DEVICE_PARTITION_COUNT: { // -68:
         return "CL_INVALID_DEVICE_PARTITION_COUNT";
      }
      // extension errors, not defined in nvidia opencl header
      /*
      case CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR: { // -1000:
         return "CL_INVALID_GL_SHAREGROUP_REFERENCE_KHR";
      }
      case CL_PLATFORM_NOT_FOUND_KHR: { // -1001:
         return "CL_PLATFORM_NOT_FOUND_KHR";
      }
      case CL_INVALID_D3D10_DEVICE_KHR: { // -1002:
         return "CL_INVALID_D3D10_DEVICE_KHR";
      }
      case CL_INVALID_D3D10_RESOURCE_KHR: { // -1003:
         return "CL_INVALID_D3D10_RESOURCE_KHR";
      }
      case CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR: { // -1004:
         return "CL_D3D10_RESOURCE_ALREADY_ACQUIRED_KHR";
      }
      case CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR: { // -1005:
         return "CL_D3D10_RESOURCE_NOT_ACQUIRED_KHR";
      }
      */
      default: {
         return "Unknown OpenCL error code";
      }
   }
}


// Check ocl function return code and print corresponding message
bool oclFunctionFailed( cl_int err_code, const std::string & msg ) {
   if( err_code != CL_SUCCESS) {
     HLog << InfoError
          << "fail to " << msg << " "
          << "code(" << err_code << "): " << oclErrorString( err_code )
          << std::endl;
     return true;
   }
   return false;
}


// Defining static member
std::map< cl_platform_info, std::string >  OpenCLPlatform::_infoName = {
   { CL_PLATFORM_PROFILE,    "CL_PLATFORM_PROFILE"},
   { CL_PLATFORM_VERSION,    "CL_PLATFORM_VERSION"},
   { CL_PLATFORM_VENDOR,     "CL_PLATFORM_VENDOR"},
   { CL_PLATFORM_EXTENSIONS, "CL_PLATFORM_EXTENSIONS"}
};


std::map< cl_device_info, std::string >  OpenCLDevice::_infoName = {

   { CL_DEVICE_TYPE,
    "CL_DEVICE_TYPE"},
   { CL_DEVICE_NAME,
    "CL_DEVICE_NAME"},
   { CL_DEVICE_VENDOR,
    "CL_DEVICE_VENDOR"},
   { CL_DRIVER_VERSION,
    "CL_DRIVER_VERSION"},
   { CL_DEVICE_PROFILE,
    "CL_DEVICE_PROFILE"},
   { CL_DEVICE_VERSION,
    "CL_DEVICE_VERSION"},
#ifdef CL_DEVICE_OPENCL_C_VERSION
   { CL_DEVICE_OPENCL_C_VERSION,
    "CL_DEVICE_OPENCL_C_VERSION"},
#endif
   { CL_DEVICE_VENDOR_ID,
    "CL_DEVICE_VENDOR_ID"},
   { CL_DEVICE_MAX_COMPUTE_UNITS,
    "CL_DEVICE_MAX_COMPUTE_UNITS"},
   { CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
    "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS"},
   { CL_DEVICE_MAX_WORK_ITEM_SIZES,
    "CL_DEVICE_MAX_WORK_ITEM_SIZES"},
   { CL_DEVICE_MAX_WORK_GROUP_SIZE,
    "CL_DEVICE_MAX_WORK_GROUP_SIZE"},
   { CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR,
    "CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR"},
   { CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT,
    "CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT"},
   { CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,
    "CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT"},
   { CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,
    "CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG"},
   { CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,
    "CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT"},
   { CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE,
    "CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE"},
#ifdef CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF
   { CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,
    "CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF"},
   { CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,
    "CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR"},
   { CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,
    "CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT"},
   { CL_DEVICE_NATIVE_VECTOR_WIDTH_INT,
    "CL_DEVICE_NATIVE_VECTOR_WIDTH_INT"},
   { CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,
    "CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG"},
   { CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
    "CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT"},
   { CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,
    "CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE"},
   { CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,
    "CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF"},
#endif
   { CL_DEVICE_MAX_CLOCK_FREQUENCY,
    "CL_DEVICE_MAX_CLOCK_FREQUENCY"},
   { CL_DEVICE_ADDRESS_BITS,
    "CL_DEVICE_ADDRESS_BITS"},
   { CL_DEVICE_MAX_MEM_ALLOC_SIZE,
    "CL_DEVICE_MAX_MEM_ALLOC_SIZE"},
   { CL_DEVICE_IMAGE_SUPPORT,
    "CL_DEVICE_IMAGE_SUPPORT"},
   { CL_DEVICE_MAX_READ_IMAGE_ARGS,
    "CL_DEVICE_MAX_READ_IMAGE_ARGS"},
   { CL_DEVICE_MAX_WRITE_IMAGE_ARGS,
    "CL_DEVICE_MAX_WRITE_IMAGE_ARGS"},
   { CL_DEVICE_IMAGE2D_MAX_WIDTH,
    "CL_DEVICE_IMAGE2D_MAX_WIDTH"},
   { CL_DEVICE_IMAGE2D_MAX_WIDTH,
    "CL_DEVICE_IMAGE2D_MAX_WIDTH"},
   { CL_DEVICE_IMAGE2D_MAX_HEIGHT,
    "CL_DEVICE_IMAGE2D_MAX_HEIGHT"},
   { CL_DEVICE_IMAGE3D_MAX_WIDTH,
    "CL_DEVICE_IMAGE3D_MAX_WIDTH"},
   { CL_DEVICE_IMAGE3D_MAX_HEIGHT,
    "CL_DEVICE_IMAGE3D_MAX_HEIGHT"},
   { CL_DEVICE_IMAGE3D_MAX_DEPTH,
    "CL_DEVICE_IMAGE3D_MAX_DEPTH"},
   { CL_DEVICE_IMAGE_MAX_BUFFER_SIZE,
    "CL_DEVICE_IMAGE_MAX_BUFFER_SIZE"},
   { CL_DEVICE_IMAGE_MAX_ARRAY_SIZE,
    "CL_DEVICE_IMAGE_MAX_ARRAY_SIZE"},
   { CL_DEVICE_MAX_SAMPLERS,
    "CL_DEVICE_MAX_SAMPLERS"},
   { CL_DEVICE_MAX_PARAMETER_SIZE,
    "CL_DEVICE_MAX_PARAMETER_SIZE"},
   { CL_DEVICE_MEM_BASE_ADDR_ALIGN,
    "CL_DEVICE_MEM_BASE_ADDR_ALIGN"},
   { CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE,
    "CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE"},
   { CL_DEVICE_SINGLE_FP_CONFIG,
    "CL_DEVICE_SINGLE_FP_CONFIG"},
   { CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
    "CL_DEVICE_GLOBAL_MEM_CACHE_TYPE"},
   { CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,
    "CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE"},
   { CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
    "CL_DEVICE_GLOBAL_MEM_CACHE_SIZE"},
   { CL_DEVICE_GLOBAL_MEM_SIZE,
    "CL_DEVICE_GLOBAL_MEM_SIZE"},
   { CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
    "CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE"},
   { CL_DEVICE_MAX_CONSTANT_ARGS,
    "CL_DEVICE_MAX_CONSTANT_ARGS"},
   { CL_DEVICE_LOCAL_MEM_TYPE,
    "CL_DEVICE_LOCAL_MEM_TYPE"},
   { CL_DEVICE_LOCAL_MEM_SIZE,
    "CL_DEVICE_LOCAL_MEM_SIZE"},
   { CL_DEVICE_ERROR_CORRECTION_SUPPORT,
    "CL_DEVICE_ERROR_CORRECTION_SUPPORT"},
#ifdef CL_DEVICE_HOST_UNIFIED_MEMORY
   { CL_DEVICE_HOST_UNIFIED_MEMORY,
    "CL_DEVICE_HOST_UNIFIED_MEMORY"},
#endif
   { CL_DEVICE_PROFILING_TIMER_RESOLUTION,
    "CL_DEVICE_PROFILING_TIMER_RESOLUTION"},
   { CL_DEVICE_ENDIAN_LITTLE,
    "CL_DEVICE_ENDIAN_LITTLE"},
   { CL_DEVICE_AVAILABLE,
    "CL_DEVICE_AVAILABLE"},
   { CL_DEVICE_COMPILER_AVAILABLE,
    "CL_DEVICE_COMPILER_AVAILABLE"},
   { CL_DEVICE_EXECUTION_CAPABILITIES,
    "CL_DEVICE_EXECUTION_CAPABILITIES"},
   { CL_DEVICE_QUEUE_PROPERTIES,
    "CL_DEVICE_QUEUE_PROPERTIES"},
   { CL_DEVICE_PLATFORM,
    "CL_DEVICE_PLATFORM"},
   { CL_DEVICE_NAME,
    "CL_DEVICE_NAME"},
   { CL_DEVICE_VENDOR,
    "CL_DEVICE_VENDOR"},
   { CL_DRIVER_VERSION,
    "CL_DRIVER_VERSION"},
   { CL_DEVICE_PROFILE,
    "CL_DEVICE_PROFILE"},
   { CL_DEVICE_VERSION,
    "CL_DEVICE_VERSION"},
#ifdef CL_DEVICE_OPENCL_C_VERSION
   { CL_DEVICE_OPENCL_C_VERSION,
    "CL_DEVICE_OPENCL_C_VERSION"},
#endif
   { CL_DEVICE_EXTENSIONS,
    "CL_DEVICE_EXTENSIONS"},
   // case CL_DEVICE_TYPE
   { CL_DEVICE_TYPE_CPU,
    "CL_DEVICE_TYPE_CPU"},
   { CL_DEVICE_TYPE_GPU,
    "CL_DEVICE_TYPE_GPU"},
   { CL_DEVICE_TYPE_ACCELERATOR,
    "CL_DEVICE_TYPE_ACCELERATOR"},
   { CL_DEVICE_TYPE_DEFAULT,
    "CL_DEVICE_TYPE_DEFAULT"}
};

std::map< cl_uint,std::string >OpenCLDevice::_globalMemoryCacheType = {
   { CL_NONE,
    "CL_NONE"},
   { CL_READ_ONLY_CACHE,
    "CL_READ_ONLY_CACHE"},
   { CL_READ_WRITE_CACHE,
    "CL_READ_WRITE_CACHE"}
};


std::map< cl_uint,std::string >OpenCLDevice::_localMemoryType = {
   { CL_LOCAL,
    "CL_LOCAL"},
   { CL_GLOBAL,
    "CL_GLOBAL"}
};


std::map< cl_bitfield, std::string > OpenCLDevice::_fpConfigType = {
   { CL_FP_DENORM,
    "CL_FP_DENORM"},
   { CL_FP_INF_NAN,
    "CL_FP_INF_NAN"},
   { CL_FP_ROUND_TO_NEAREST,
    "CL_FP_ROUND_TO_NEAREST"},
   { CL_FP_ROUND_TO_ZERO,
    "CL_FP_ROUND_TO_ZERO"},
   { CL_FP_ROUND_TO_INF,
    "CL_FP_ROUND_TO_INF"},
   { CL_FP_FMA,
    "CL_FP_FMA"},
   { CL_FP_SOFT_FLOAT,
    "CL_FP_SOFT_FLOAT"}
};


std::map< cl_bitfield, std::string > OpenCLDevice::_queuePropertiesType = {
   { CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE,
    "CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE"},
   { CL_QUEUE_PROFILING_ENABLE,
    "CL_QUEUE_PROFILING_ENABLE"}
};


std::map< cl_bitfield, std::string > OpenCLDevice::_execCapabilitiesType = {
   { CL_EXEC_KERNEL,
    "CL_EXEC_KERNEL"},
   { CL_EXEC_NATIVE_KERNEL,
    "CL_EXEC_NATIVE_KERNEL"}
};


OpenCLInfo::OpenCLInfo() : _isQueried( false ),
                           _numPlatforms ( 0 ) {
   ;
}


OpenCLInfo::~OpenCLInfo() {
   ;
}


// problem with const qualifier; debug it later
bool OpenCLInfo::checkContextAndQueue( cl_uint pfm_id, cl_uint dev_id)
{
   if( !checkPlatformIdRange( pfm_id )) {
      return false;
   }
   if( NULL==_platforms[pfm_id].context() ) {
      HLog << InfoError
           << "please create_ocl_context on platform " << pfm_id << " first"
           << std::endl;
      return false;
   }
   OpenCLDevice& dev = _platforms[pfm_id].device(dev_id);
   if( NULL==dev.queue() ) {
      HLog << InfoError
           << "please create_ocl_queue on device " << dev_id << " of"
           << " platform " << pfm_id << " first"
           << std::endl;
      return false;
   }
   return true;
}


bool OpenCLInfo::createContext( int pfm_id ) {
   if( !checkPlatformIdRange( pfm_id )) {
      return false;
   }
   bool res = platform(pfm_id).createContext();
   return res;
}


bool OpenCLInfo::releaseContext( int pfm_id ) {
   if( !checkPlatformIdRange( pfm_id )) {
      return false;
   }
   bool res = platform(pfm_id).releaseContext();
   return res;
}


bool OpenCLInfo::createCmdQueue( int pfm_id, int dev_id,
                                 cl_command_queue_properties properties )
{
   if( !checkPlatformIdRange( pfm_id )) {
      return false;
   }
   if( !platform(pfm_id).checkDeviceIdRange( dev_id )) {
      return false;
   }
   if( NULL == platform(pfm_id).context() ) {
      HLog << InfoError
           << "please 'create_ocl_context' first for platform " << pfm_id
           << std::endl;
      return false;
   }
   bool res = platform(pfm_id).device(dev_id).createCmdQueue(
      platform(pfm_id).context(), properties );
   return res;
}


bool OpenCLInfo::releaseCmdQueue( int pfm_id, int dev_id ) {
   if( !checkPlatformIdRange( pfm_id )) {
      return false;
   }
   if( !platform(pfm_id).checkDeviceIdRange( dev_id )) {
      return false;
   }
   bool res = platform(pfm_id).device(dev_id).releaseCmdQueue();
   return res;
}


bool OpenCLInfo::checkPlatformIdRange( int pfm_id ) const {
   if( pfm_id<0 || (unsigned)pfm_id>=numPlatforms() ) {
      HLog << InfoError << "platform id must be within range [0:"
           << numPlatforms()-1 << "]"
           << std::endl;
      return false;
   }
   return true;
}


// functions relevant global work size and local work size
void OpenCLInfo::printUserPreferences() {
   for( cl_uint p=0; p < numPlatforms(); p++ ) {
      OpenCLPlatform& platform = this->platform(p);
      HLog << "\nplatform number: " << p << "\n";
      platform.printUserPreferences();
   }
}


bool OpenCLInfo::setUserPreferences( std::vector<int> gw_size,
                                     std::vector<int> lw_size,
                                     CktClMemType type,
                                     bool print_only )
{
   if( print_only ) {
      printUserPreferences();
      return true;
   }
   // check validity of gw_size and lw_size across all platforms and devices
   for( cl_uint p=0; p < numPlatforms(); p++ ) {
      OpenCLPlatform& platform = this->platform(p);
      for( cl_uint d=0; d < platform.numDevices(); d++ ) {
         OpenCLDevice& device = platform.device(d);
         if( !device.validWorkSizeSetting( gw_size, lw_size)) {
            return false;
         }
      }
   }
   // set gw_size and lw_size across
   for( cl_uint p=0; p < numPlatforms(); p++ ) {
      OpenCLPlatform& platform = this->platform(p);
      for( cl_uint d=0; d < platform.numDevices(); d++ ) {
         OpenCLDevice& device = platform.device(d);
         device.globalWorkSizes( gw_size );
         device.localWorkSizes( lw_size );
         device.cktClMemType( type );
      }
   }
   return true;
}


bool OpenCLInfo::setUserPreferences( int pfm_id,
                                     std::vector<int> gw_size,
                                     std::vector<int> lw_size,
                                     CktClMemType type,
                                     bool print_only )
{
   // validity of pfm_id is checked pre-function-call
   OpenCLPlatform& platform = this->platform((cl_uint)pfm_id);
   if( print_only ) {
      platform.printUserPreferences();
      return true;
   }
   // check validity of gw_size and lw_size across all devices
   for( cl_uint i=0; i < platform.numDevices(); i++ ) {
      OpenCLDevice& device = platform.device(i);
      if( !device.validWorkSizeSetting( gw_size, lw_size)) {
         return false;
      }
   }
   // set gw_size and lw_size
   for( cl_uint i=0; i < platform.numDevices(); i++ ) {
      OpenCLDevice& device = platform.device(i);
      device.globalWorkSizes( gw_size );
      device.localWorkSizes( lw_size );
      device.cktClMemType( type );
   }
   return true;
}


bool OpenCLInfo::setUserPreferences( int pfm_id,
                                     int dev_id,
                                     std::vector<int> gw_size,
                                     std::vector<int> lw_size,
                                     CktClMemType type,
                                     bool print_only )
{
   // validity of pfm_id and dev_id is checked pre-function-call
   OpenCLDevice& device = platform((cl_uint)pfm_id).device((cl_uint)dev_id);
   if( print_only ) {
      device.printUserPreferences();
      return true;
   }
   if( device.validWorkSizeSetting( gw_size, lw_size)) {
      device.globalWorkSizes( gw_size );
      device.localWorkSizes( lw_size );
      device.cktClMemType( type );
   } else {
      return false;
   }
   return true;
}


bool OpenCLInfo::query() {
   cl_int errNum;

   _isQueried = true; // Set query status to true

   // First, query the total number of platforms
   errNum = clGetPlatformIDs(0, NULL, &_numPlatforms);
   if (errNum != CL_SUCCESS || _numPlatforms <= 0) {
     HLog << "Failed to find any OpenCL platform." << std::endl;
     return false;
   }

   // Next, allocate memory for the installed plaforms, and qeury
   // to get the list.
   // platformIds =
   //   (cl_platform_id *)alloca(sizeof(cl_platform_id) * numPlatforms);
   _platformIds.resize( _numPlatforms );
   // First, query the total number of platforms
   errNum = clGetPlatformIDs( _numPlatforms, &(_platformIds[0]), NULL);
   if (errNum != CL_SUCCESS) {
      HLog << "Failed to find any OpenCL platforms." << std::endl;
      return false;
   }
   for( cl_uint i=0; i<_numPlatforms; i++ ) {
      OpenCLPlatform platform( _platformIds[i] );
      _platforms.push_back( platform );
   }

   return true;
}


void OpenCLInfo::printProperties( bool summary ) const {
   HLog << "Number of platforms: \t" << numPlatforms() << std::endl;
   for ( cl_uint i=0; i < numPlatforms();i++ ) {
      HLog << "\n"
           << "Platform id: "<< i <<"\n";
      platformConst(i).printProperties( summary );
   }
}


bool OpenCLInfo::display( bool summary ) {

   bool status=true;
   HLog << "Number of platforms: \t" << numPlatforms() << std::endl;
   for( cl_uint i=0; i < numPlatforms(); i++ ) {
      HLog << "\n";
      HLog << "Platform id: " << i << "\n";
      if( false == platform(i).display( summary ) ) {
         status = false;
      }
   }
   return status;
}


bool OpenCLInfo::releaseOclResources( bool for_delete_ckt ) {
   int res = 0;
   for( cl_uint p=0; p < numPlatforms(); p++ ) {
      if( false==platform(p).releaseOclResources( for_delete_ckt )) {
         res += 1;
      }
   }
   return (0==res);
}


OpenCLPlatform::OpenCLPlatform( cl_platform_id id ) :
                   _isQueried( false ),
                   _cktPfmResourcesCreated( false ),
                   _context( NULL ),
                   _program( NULL ),
                   _maxGmvCopiesOnGPU( 16 ),
                   _maxPatternCopies( 16 ),
                   _platformId( id ),
                   _numDevices( 0 ),
                   _platformProfile( "" ),
                   _platformVersion( "" ),
                   _platformVendor( "" ),
                   _platformExtensions( "" )
{
   // Query the properties of platform upon construction
   query();
   // initialize cl_kernel and cl_mem to NULL
   for( int i=0; i<(int)LAST_CL_KERNEL; i++ ) {
      _kernels.push_back( NULL );
   }
   for( int i=0; i<(int)LAST_CL_MEM_CKT; i++ ) {
      _clMemsCkt.push_back( NULL );
   }
   for( int i=0; i<(int)_maxGmvCopiesOnGPU; i++ ) {
      _clMemsGmv0.push_back( NULL );
      _clMemsGmv1.push_back( NULL );
   }
   for( int i=0; i<(int)_maxPatternCopies; i++ ) {
      _clMemsPatInGmv0.push_back( NULL );
      _clMemsPatInGmv1.push_back( NULL );
      _clMemsPatOutGmv0.push_back( NULL );
      _clMemsPatOutGmv1.push_back( NULL );
   }
}


OpenCLPlatform::~OpenCLPlatform() {
   releaseOclResources( false ); // for_delete_ckt = false, release all
}


bool OpenCLPlatform::releaseOclMemForPatternData() {
   cl_int err = 0;
   for( size_t i=0; i < _maxPatternCopies; i++ ) {
      if( NULL != _clMemsPatInGmv0[i] ) {
         err = clReleaseMemObject( _clMemsPatInGmv0[i] );
         HLog << InfoNote
              << "release ocl mem object pattern-in " << i << "....."
              << std::endl;
         if( oclFunctionFailed( err, "release _clMemsPatInGmv0[i]") ) {
            return false;
         }
         _clMemsPatInGmv0[i] = NULL;
      }
      if( NULL != _clMemsPatInGmv1[i] ) {
         err = clReleaseMemObject( _clMemsPatInGmv1[i] );
         HLog << InfoNote
              << "release ocl mem object pattern-in " << i << "....."
              << std::endl;
         if( oclFunctionFailed( err, "release _clMemsPatInGmv1[i]") ) {
            return false;
         }
         _clMemsPatInGmv1[i] = NULL;
      }
      if( NULL != _clMemsPatOutGmv0[i] ) {
         err = clReleaseMemObject( _clMemsPatOutGmv0[i] );
         HLog << InfoNote
              << "release ocl mem object pattern-out " << i << "....."
              << std::endl;
         if( oclFunctionFailed( err, "release _clMemsPatOutGmv0[i]") ) {
            return false;
         }
         _clMemsPatOutGmv0[i] = NULL;
      }
      if( NULL != _clMemsPatOutGmv1[i] ) {
         err = clReleaseMemObject( _clMemsPatOutGmv1[i] );
         HLog << InfoNote
              << "release ocl mem object pattern-out " << i << "....."
              << std::endl;
         if( oclFunctionFailed( err, "release _clMemsPatOutGmv1[i]") ) {
            return false;
         }
         _clMemsPatOutGmv1[i] = NULL;
      }
   }
   return true;
}


bool OpenCLPlatform::releaseOclResources( bool for_delete_ckt ) {

   cl_int err = 0;
   // clear the flag
   cktPfmResourcesCreated( false );
   // release mem objects first
   for( size_t i=0; i<_clMemsCkt.size(); i++ ) {
      if( NULL != _clMemsCkt[i] ) {
         err = clReleaseMemObject( _clMemsCkt[i] );
         HLog << InfoNote
              << "release ocl mem object circuit " << i << " ....."
              << std::endl;
         if( oclFunctionFailed( err, "release _clMemsCkt[i]") ) {
            return false;
         }
         _clMemsCkt[i] = NULL;
      }
   }
   // Assert if _clMemsGmv0 and _clMemsGmv1
   assert( _clMemsGmv0.size() == _clMemsGmv1.size() );
   for( size_t i=0; i<_clMemsGmv0.size(); i++ ) {
      if( NULL != _clMemsGmv0[i] ) {
         err = clReleaseMemObject( _clMemsGmv0[i] );
         HLog << InfoNote
              << "release ocl mem object gmv0 " << i << " ....."
              << std::endl;
         if( oclFunctionFailed( err, "release _clMemsGmv0[i]") ) {
            return false;
         }
         _clMemsGmv0[i] = NULL;
      }
      if( NULL != _clMemsGmv1[i] ) {
         err = clReleaseMemObject( _clMemsGmv1[i] );
         HLog << InfoNote
              << "release ocl mem object gmv1 " << i << " ....."
              << std::endl;
         if( oclFunctionFailed( err, "release _clMemsGmv1[i]") ) {
            return false;
         }
         _clMemsGmv1[i] = NULL;
      }
   }

   if( false==releaseOclMemForPatternData()) { return false; }

   // release ocl resources(queues) on associated devices first
   for( cl_uint d=0; d < numDevices(); d++ ) {
      if( false==device(d).releaseOclResources( for_delete_ckt ) ) {
         return false;
      }
   }

   // release ocl resources on platform
   for( size_t i=0; i<_kernels.size(); i++ ) {
      if( NULL != _kernels[i] ) {
         err = clReleaseKernel( _kernels[i] );
         HLog << InfoNote
              << "release ocl kernel " << i << " ....."
              << std::endl;
         if( oclFunctionFailed( err,"release _kernels[i]")) {
            return false;
         }
         _kernels[i] = NULL;
      }
   }

   if( NULL != _program && !for_delete_ckt ) {
      // DO NOT release program when deleting circuit
      err = clReleaseProgram( _program );
      HLog << InfoNote
           << "release ocl program ....."
           << std::endl;
      if( oclFunctionFailed( err,"release _program")) {
         return false;
      }
      _program = NULL;
   }

   if( NULL != _context && !for_delete_ckt ) {
      // DO NOT release context when deleting circuit
      err = clReleaseContext( _context );
      HLog << InfoNote
           << "release ocl context ....."
           << std::endl;
      if( oclFunctionFailed( err,"release _context")) {
         return false;
      }
      _context = NULL;
   }
   return true;
}


bool OpenCLPlatform::createContext() {
   if( _context ) {
      HLog << InfoError
           << "please release existing context first"
           << std::endl;
      return false;
   }
   cl_int errNum=0;
   cl_context_properties contextProperties[] = {
      CL_CONTEXT_PLATFORM,
      (cl_context_properties)_platformId,
      0
   };
   _context = clCreateContext(
      contextProperties,
      numDevices(),
      &(_deviceIds[0]),
      NULL,
      NULL,
      &errNum
   );
   /* // alternative, create context from certain type
   _context = clCreateContextFromType(
      contextProperties,
      CL_DEVICE_TYPE_GPU, // TODO: adjust for various device type later
      NULL,
      NULL,
      &errNum
   );
   */

   if( errNum != CL_SUCCESS ) {
      HLog << InfoError
           << "could not create GPU context with the specified platform"
           << std::endl;
      return false;
   } else {
      HLog << InfoNote
           << "OpenCL GPU context created with the specified platform"
           << std::endl;
      print( CL_PLATFORM_VENDOR );
      print( CL_PLATFORM_VERSION );
   }
   //createAndBuildProgram("Evaluate.cl");
   return true;
}


bool OpenCLPlatform::releaseContext() {
   if( NULL == _context ) {
      HLog << InfoError
           << "there is no OpenCL context to release"
           << std::endl;
      return false;
   }
   // for_delete_ckt = false, release all
   bool res = releaseOclResources( false );
   if( res ) {
      HLog << InfoNote
           << "OpenCL context successfully released"
           << std::endl;
      return true;
   }
   return res;
}


bool OpenCLPlatform::checkDeviceIdRange( int dev_id ) const {
   if( dev_id<0 || (unsigned)dev_id >=numDevices()) {
      HLog << InfoError << "device id must be within range [0:"
           << numDevices()-1 << "] for the platform"
           << std::endl;
      return false;
   }
   return true;
}


bool OpenCLPlatform::query() {
   // Query the set of devices associated with the platform
   _isQueried = true;

   // Query basic platform properties first
   int re = 0;
   std::string val;
   // Get std::string platformProfile
   re += getPlatformInfo( CL_PLATFORM_PROFILE , val );
   _platformProfile = val;
   // Get std::string platformVersion
   re += getPlatformInfo( CL_PLATFORM_VERSION , val );
   _platformVersion = val;
   // Get std::string platformVendor
   re += getPlatformInfo( CL_PLATFORM_VENDOR , val );
   _platformVendor = val;
   // Get std::string platformExtensions
   re += getPlatformInfo( CL_PLATFORM_EXTENSIONS , val );
   _platformExtensions = val;

   if( re ) { return false; }

   // Query device-related info
   cl_int errNum=0;
   errNum = clGetDeviceIDs(
      _platformId,
      CL_DEVICE_TYPE_ALL,
      0,
      NULL,
      &_numDevices);
   if (errNum != CL_SUCCESS) {
      HLog << "Failed to find OpenCL devices." << std::endl;
      return false;
   }

   _deviceIds.resize( _numDevices );
   errNum = clGetDeviceIDs(
      _platformId,
      CL_DEVICE_TYPE_ALL,
      _numDevices,
      &(_deviceIds[0]),
      NULL);
   if (errNum != CL_SUCCESS) {
      HLog << "Failed to find OpenCL devices." << std::endl;
      return false;
   }

   //HLog << "\tNumber of devices: \t" << _numDevices << std::endl;

   for( cl_uint i=0; i<_numDevices; i++) {
      OpenCLDevice device( this, _deviceIds[i] );
      _devices.push_back( device );
   }
   return true;
}


bool OpenCLPlatform::display( bool summary ) {
   bool status = true;
   int ret = 0;
   ret += print( CL_PLATFORM_PROFILE );
   ret += print( CL_PLATFORM_VERSION );
   ret += print( CL_PLATFORM_VENDOR );
   if( !summary ) { // skip it for summary
      ret += print( CL_PLATFORM_EXTENSIONS );
   }
   if( ret ) { status = false; }

   HLog << "\tNumber of devices: \t" << numDevices() << std::endl;
   for( cl_uint i=0; i<numDevices(); i++ ) {
      HLog << "\n";
      HLog << "\tDevice id: " << i << "\n";
      if( false==device(i).display( summary ) ) {
         status = false;
      }
   }
   return status;
}


int OpenCLPlatform::print( cl_platform_info info ) {
   std::string val;

   if( false==getPlatformInfo( info , val ) ) {
      return CL_PRINT_ERROR;
   }
   HLog << "\t" << infoName(info) << ":\t" << val << std::endl;
   return CL_PRINT_OK;
}


void OpenCLPlatform::printProperties( bool summary ) const {
   HLog << "\t" << infoName(CL_PLATFORM_PROFILE) << ": \t"
        << platformProfile() << std::endl;
   HLog << "\t" << infoName(CL_PLATFORM_VERSION) << ": \t"
        << platformVersion() << std::endl;
   HLog << "\t" << infoName(CL_PLATFORM_VENDOR) << ": \t"
        << platformVendor() << std::endl;
   if ( !summary) {
      HLog << "\t" << infoName(CL_PLATFORM_EXTENSIONS) << ": \t"
           << platformExtensions() << std::endl;
   }
   HLog << "\tNumber of devices: \t" << numDevices() << std::endl;
   for (cl_uint i=0; i<numDevices();i++) {
      HLog << "\n";
      HLog << "\tDevice id:  " << i << "\n";
      deviceConst(i).printProperties( summary );
   }
}


int OpenCLPlatform::getPlatformInfo( cl_platform_info info,
                                      std::string& val )
{
   cl_int err=0;
   std::string err_msg("");
   std::size_t para_size=0;
   err = clGetPlatformInfo(_platformId, info, 0, NULL, &para_size);
   err_msg = "get parameter size of " + infoName(info);
   if( oclFunctionFailed( err, err_msg.c_str() )) {
      return STATUS_ERROR;
   }
   // Workaround for unexpected non-ASCII character ^@ returned by
   //   getPlatformInfo
   std::string tval( para_size, ' '); // Temperary place to hold the string
   err = clGetPlatformInfo(_platformId, info, para_size, &(tval[0]), NULL);
   val = tval.c_str();

   err_msg = "retrieve " + infoName(info);
   if( oclFunctionFailed( err, err_msg.c_str() )) {
      return STATUS_ERROR;
   }
   return STATUS_OK;
}


void OpenCLPlatform::printUserPreferences() {
   for( cl_uint d=0; d<numDevices(); d++) {
      OpenCLDevice& device = this->device(d);
      HLog << "\n" << TwoSpace << "device number: " << d
           << std::endl;
      device.printUserPreferences();
   }
}


OpenCLDevice::OpenCLDevice( OpenCLPlatform* parent, cl_device_id id )
             : _platform( parent ),
               _clDeviceId( id ),
               _clQueue( NULL ),
               _ioQueue( NULL ),
               _cktDevResourcesCreated( false ),
               // User-specified or computed parameters
               _maxGlobalWorkSize( 0 ),
               _cktClMemType( CKT_IMAGE ),
               // selected properties
               _deviceType( CL_DEVICE_TYPE_DEFAULT ),
               _deviceName( "" ),
               _deviceVendor( "" ),
               _driverVersion( "" ),
               _deviceProfile( "" ),
               _deviceVersion( "" ),
               _deviceOpenclCVersion( "" ),
               _deviceVendorId(0),
               _maxComputeUnits( 0 ),
               _maxWorkItemsDimens( 0 ),
               _maxWorkItemSizes( 0 ),
               _maxWorkGroupSize( 0 ),
               _preferredVectorWidthChar( 0 ),
               _preferredVectorWidthShort( 0 ),
               _preferredVectorWidthInt( 0 ),
               _preferredVectorWidthLong( 0 ),
               _preferredVectorWidthFloat( 0 ),
               _preferredVectorWidthDouble( 0 ),
               _preferredVectorWidthHalf( 0 ),
               _nativeVectorWidthChar( 0 ),
               _nativeVectorWidthShort( 0 ),
               _nativeVectorWidthInt( 0 ),
               _nativeVectorWidthLong( 0 ),
               _nativeVectorWidthFloat( 0 ),
               _nativeVectorWidthDouble( 0 ),
               _nativeVectorWidthHalf( 0 ),
               _maxClockFrequency( 0 ),
               _addressBits( 0 ),
               _maxMemAllocSize( 0 ),
               _imageSupport( 0 ),
               _maxReadImageArgs( 0 ),
               _maxWriteImageArgs( 0 ),
               _image2dMaxWidth( 0 ),
               _image2dMaxHeight( 0 ),
               _image3dMaxWidth( 0 ),
               _image3dMaxHeight( 0 ),
               _image3dMaxDepth( 0 ),
               _minDataTypeAlignSize( 0 ),
               _singleFpConfig( 0 ),
               _globalMemCacheType( 0 ),
               _globalMemCachelineSize( 0 ),
               _globalMemCacheSize( 0 ),
               _imageMaxBufferSize( 0 ),
               _imageMaxArraySize( 0 ),
               _maxSamplers( 0 ),
               _maxParameterSize( 0 ),
               _memBaseAddrAlign( 0 ),
               _globalMemSize( 0 ),
               _maxConstantBufferSize( 0 ),
               _maxConstantArgs( 0 ),
               _localMemType( 0 ),
               _localMemSize( 0 ),
               _errorCorrectionSupport( 0 ),
               _hostUnifiedMemory( 0 ),
               _profilingTimerResolution( 0 ),
               _endianLittle( 0 ),
               _compilerAvailable( 0 ),
               _executionCapabilities( 0 )
            //_platform
{
   // Query device properties upon construction
   query();
}


OpenCLDevice::~OpenCLDevice() {
   // for_delete_ckt = false, release all
   releaseOclResources( false );
}


bool OpenCLDevice::releaseOclResources( bool for_delete_ckt ) {
   // clear the flag first
   cktDevResourcesCreated( false );

   cl_int err = 0;
   if( !for_delete_ckt ) {
      // DO NOT release command queue when deleting circuit
      if( NULL != _clQueue ) {
         err = clReleaseCommandQueue( _clQueue );
         HLog << InfoNote
              << "release ocl command queue ....."
              << std::endl;
         if( oclFunctionFailed( err,"fail to release command queue")) {
            return false;
         }
         _clQueue = NULL;
      }
      if( NULL != _ioQueue ) {
         err = clReleaseCommandQueue( _ioQueue );
         if( oclFunctionFailed( err,"fail to release IO queue")) {
            return false;
         }
         _ioQueue = NULL;
      }
   }
   return true;
}


bool OpenCLDevice::createCmdQueue( cl_context context,
                                   cl_command_queue_properties properties ) {
   //assert( platform()->context() );
   //HLog << "context: " << platform()->context()
   //     << ":" << context << std::endl;
   // It shows that context and platform()->context() are different
   //    TODO, debug later
   assert( context );
   if( _clQueue || _ioQueue ) {
      HLog << InfoError
           << "please release existing command queue first or do nothing"
           << std::endl;
      return false;
   }
   cl_int err = 0;
   // Problem, access context through parent pointer platform()->context()
   //  causes a seg fault in clCreateCommandQueue(), not sure why ?????
   _clQueue = clCreateCommandQueue( context, deviceId(), properties, &err);
   if( CL_SUCCESS != err ) {
      HLog << InfoError
           << "failed to create command queue" << std::endl;
      return false;
   }
   _ioQueue = clCreateCommandQueue( context, deviceId(), properties, &err);
   if( CL_SUCCESS != err ) {
      HLog << InfoError
           << "failed to create IO queue" << std::endl;
      return false;
   }
   HLog << InfoNote
        << "command queue successfully created with the specified device"
        << std::endl;
   HLog << "\tCL_DEVICE_NAME: " << deviceName()
        << std::endl;
   return true;
}


bool OpenCLDevice::releaseCmdQueue() {
   if( NULL==_clQueue && NULL==_ioQueue) {
      HLog << InfoError
           << "there is no command queue for the device"
           << std::endl;
      return false;
   }
   // for_delete_ckt = false, release all
   bool res = releaseOclResources( false );
   if( res ) {
      HLog << InfoNote
           << "command queue with the device is successfully released"
           << std::endl;
   }
   return res;
}


const std::string OpenCLDevice::deviceTypeName( cl_device_type type) const
{
   switch( type ) {
      case CL_DEVICE_TYPE_CPU: {
         return "CL_DEVICE_TYPE_CPU";
      }
      case CL_DEVICE_TYPE_GPU: {
         return "CL_DEVICE_TYPE_GPU";
      }
      case CL_DEVICE_TYPE_ACCELERATOR: {
         return "CL_DEVICE_TYPE_ACCELERATOR";
      }
      case CL_DEVICE_TYPE_DEFAULT: {
         return "CL_DEVICE_TYPE_DEFAULT";
      }
      default: {
         return "Unknown CL_DEVICE_TYPE";
      }
   }
}

const std::string OpenCLDevice::printSpacer( int count ) const
{
   if (count > 1) {
      return " | ";
   }
   return "";
}


void OpenCLDevice::printFpConfig ( cl_device_fp_config _singleFPConfig ) const
{
   int count = 0;
   std::string str = "";
   if (_singleFpConfig & CL_FP_DENORM) {
      count ++;
      HLog << printSpacer(count)
           << fpConfigType ( CL_FP_DENORM );
   }
   if (_singleFpConfig & CL_FP_INF_NAN) {
      count ++;
      HLog << printSpacer(count)
           << fpConfigType ( CL_FP_INF_NAN );
   }
   if (_singleFpConfig & CL_FP_ROUND_TO_NEAREST) {
      count ++;
      HLog << printSpacer(count)
           << fpConfigType ( CL_FP_ROUND_TO_NEAREST );
   }
   if (_singleFpConfig & CL_FP_ROUND_TO_ZERO) {
      count ++;
      HLog << printSpacer(count)
           << fpConfigType ( CL_FP_ROUND_TO_ZERO );
   }
   if (_singleFpConfig & CL_FP_ROUND_TO_INF) {
      count ++;
      HLog << printSpacer(count)
           << fpConfigType ( CL_FP_ROUND_TO_INF );
   }
   if (_singleFpConfig & CL_FP_FMA) {
      count ++;
      HLog << printSpacer(count)
           << fpConfigType ( CL_FP_FMA );
   }
   if (_singleFpConfig & CL_FP_SOFT_FLOAT) {
      count ++;
      HLog << printSpacer(count)
           << fpConfigType( CL_FP_SOFT_FLOAT );
   }
   HLog << std::endl;
}


void OpenCLDevice::printGlobalMemCacheType
                  (cl_device_mem_cache_type _globalMemCache) const {
   int count = 0;
   if (_globalMemCacheType & CL_NONE) {
      count ++;
      HLog << printSpacer( count )
           << globalMemoryCacheType (CL_NONE);
   }
   if (_globalMemCacheType & CL_READ_ONLY_CACHE) {
      count ++;
      HLog << printSpacer( count )
           << globalMemoryCacheType (CL_READ_ONLY_CACHE);
   }
   if (_globalMemCacheType & CL_READ_WRITE_CACHE) {
      count ++;
      HLog << printSpacer( count )
           << globalMemoryCacheType ( CL_READ_WRITE_CACHE );
   }
   HLog << std::endl;
}


void OpenCLDevice::printLocalMemType
                  ( cl_device_local_mem_type _localMemType ) const {
   int count = 0;
   if (_localMemType & CL_LOCAL) {
      count ++ ;
      HLog << printSpacer( count )
           << localMemoryType( CL_LOCAL ) ;
   }
   if (_localMemType & CL_GLOBAL) {
      count ++ ;
      HLog << printSpacer( count )
           << localMemoryType( CL_GLOBAL );
   }
   HLog <<std::endl;
}


void OpenCLDevice::printExecCapabilities
                (cl_device_exec_capabilities _executionCapabilities ) const {
   int count = 0;
   if ( _executionCapabilities & CL_EXEC_KERNEL ) {
      count ++;
      HLog << printSpacer( count )
           << execCapabilitiesType( CL_EXEC_KERNEL ) ; }
   if ( _executionCapabilities & CL_EXEC_NATIVE_KERNEL ) {
      count ++;
      HLog << printSpacer( count )
           << execCapabilitiesType( CL_EXEC_NATIVE_KERNEL ) ; }
   HLog << std::endl;
}


void OpenCLDevice::printQueueProperties
                   (cl_command_queue_properties _queueProperties ) const {

   int count = 0;
   if ( _queueProperties & CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE) {
      count ++;
      HLog << printSpacer( count )
           << queuePropertiesType( CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE ); }
   if ( _queueProperties & CL_QUEUE_PROFILING_ENABLE ) {
      count++;
      HLog << printSpacer( count )
           << queuePropertiesType( CL_QUEUE_PROFILING_ENABLE ); }
   HLog << std::endl;
}


void OpenCLDevice::printProperties( bool summary ) const {
   if( summary ) {
      HLog << "\t\t" << infoName( CL_DEVICE_TYPE ) << ": \t"
           << deviceTypeName(deviceType()) << std::endl;
      HLog << "\t\t" << infoName( CL_DEVICE_NAME ) << ": \t"
           <<  deviceName() << std::endl;
      HLog << "\t\t" << infoName( CL_DEVICE_VENDOR ) << ": \t"
           <<  deviceVendor() << std::endl;
      HLog << "\t\t" << infoName( CL_DRIVER_VERSION ) << ": \t"
           <<  driverVersion() << std::endl;
      HLog << "\t\t" << infoName( CL_DEVICE_PROFILE ) << ": \t"
           <<  deviceProfile() << std::endl;
      HLog << "\t\t" << infoName( CL_DEVICE_VERSION ) << ": \t"
           <<  deviceVersion() << std::endl;
      HLog << "\t\t" << infoName( CL_DEVICE_OPENCL_C_VERSION ) << ": \t"
           <<  deviceOpenclCVersion() << std::endl;
      return;
   }
   // Get cl_device_type _deviceType
   HLog << "\t\t" << infoName(CL_DEVICE_TYPE) << ": \t"
        << deviceTypeName(_deviceType) << std::endl;
   HLog << "\t\t" << infoName( CL_DEVICE_VENDOR_ID ) <<": \t"
        <<  deviceVendorId() << std::endl;
   // Get cl_uint _maxComputeUnits
   HLog << "\t\t" << infoName(CL_DEVICE_MAX_COMPUTE_UNITS) << ": \t"
        << maxComputeUnits() << std::endl;
   // Get cl_uint _maxWorkItemsDimens
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS) << ": \t"
        << maxWorkItemsDimens() << std::endl;
   // Get std::vector< size_t > _maxWorkItemSizes
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_WORK_ITEM_SIZES ) << ": \t";
   // _maxWorkItemSizes.size() is much greater than _maxWorkItemsDimens
   for( cl_uint i=0; i<_maxWorkItemsDimens; i++ ) {
      HLog << _maxWorkItemSizes[i] << " ";
   }
   HLog << std::endl;
   // Get cl_uint _maxWorkGroupSize
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_WORK_GROUP_SIZE )
        << ": \t" << maxWorkGroupSize() << std::endl;
   // Get _preferredVectorWidthChar
   HLog << "\t\t" << infoName( CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR )
        << ": \t" << preferredVectorWidthChar() << std::endl;
   // Get _preferredVectorWidthShort
   HLog << "\t\t" << infoName( CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT )
        << ": \t" << preferredVectorWidthShort() << std::endl;
   // Get _preferredVectorWidthInt
   HLog << "\t\t" << infoName( CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT )
        << ": \t" << preferredVectorWidthInt() << std::endl;
   // Get _preferredVectorWidthLong
   HLog << "\t\t" << infoName( CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG )
        << ": \t" << preferredVectorWidthLong() << std::endl;
   // Get _preferredVectorWidthFloat
   HLog << "\t\t" << infoName( CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT )
        << ": \t" << preferredVectorWidthFloat() << std::endl;
   // Get _preferredVectorWidthDouble
   HLog << "\t\t" << infoName( CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE )
        << ": \t" << preferredVectorWidthDouble() << std::endl;
   // Get _preferredVectorWidthHalf
   HLog << "\t\t" << infoName( CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF )
        << ": \t" << preferredVectorWidthHalf() << std::endl;
   // Get _nativeVectorWidthChar
   HLog << "\t\t" << infoName( CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR )
        << ": \t" << nativeVectorWidthChar() << std::endl;
   // Get _nativeVectorWidthShort
   HLog << "\t\t" << infoName( CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT )
        << ": \t" << nativeVectorWidthShort() << std::endl;
   // Get _nativeVectorWidthInt
   HLog << "\t\t" << infoName( CL_DEVICE_NATIVE_VECTOR_WIDTH_INT )
        << ": \t" << nativeVectorWidthInt() << std::endl;
   // Get _nativeVectorWidthLong
   HLog << "\t\t" << infoName( CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG )
        << ": \t" << nativeVectorWidthLong() << std::endl;
   // Get _nativeVectorWidthFloat
   HLog << "\t\t" << infoName( CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT )
        << ": \t" << nativeVectorWidthFloat() << std::endl;
   // Get _nativeVectorWidthDouble
   HLog << "\t\t" << infoName( CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE)
        << ": \t" << nativeVectorWidthDouble() << std::endl;
   // Get _nativeVectorWidthHalf
   HLog << "\t\t" << infoName( CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF )
        << ": \t" << nativeVectorWidthHalf() << std::endl;
   // Get _maxClockFrequency
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_CLOCK_FREQUENCY )
        << ": \t" << maxClockFrequency() <<std::endl;
   // Get cl_uint _addressBits
   HLog << "\t\t" << infoName( CL_DEVICE_ADDRESS_BITS )
        << ": \t" << addressBits() << std::endl;
   // Get cl_ulong _maxMemAllocSize
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_MEM_ALLOC_SIZE )
        << ": \t" << maxMemAllocSize() << std::endl;
   // Get cl_bool _imageSupport
   HLog << "\t\t" << infoName( CL_DEVICE_IMAGE_SUPPORT )
        << ": \t" << imageSupport() << std::endl;
   // Get cl_uint _maxReadImageArgs
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_READ_IMAGE_ARGS )
        << ": \t" << maxReadImageArgs() << std::endl;
   // Get cl_uint _maxWriteImageArgs
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_WRITE_IMAGE_ARGS )
        << ": \t" << maxWriteImageArgs() << std::endl;
   // Get size_t _image2dMaxWidth
   HLog << "\t\t" << infoName( CL_DEVICE_IMAGE2D_MAX_WIDTH )
        << ": \t" << image2dMaxWidth() << std::endl;
   // Get size_t _image2dMaxHeight
   HLog << "\t\t" << infoName( CL_DEVICE_IMAGE2D_MAX_HEIGHT )
        << ": \t" << image2dMaxHeight() << std::endl;
   // Get size_t _image3dMaxWidth
   HLog << "\t\t" << infoName( CL_DEVICE_IMAGE3D_MAX_WIDTH )
        << ": \t" << image3dMaxWidth() << std::endl;
   // Get size_t _image3dMaxHeight
   HLog << "\t\t" << infoName( CL_DEVICE_IMAGE3D_MAX_HEIGHT )
        << ": \t" << image3dMaxHeight() << std::endl;
   // Get size_t _image3dMaxDepth
   HLog << "\t\t" << infoName( CL_DEVICE_IMAGE3D_MAX_DEPTH )
        << ": \t" << image3dMaxDepth() << std::endl;
   // Get size_t _imageMaxBufferSize
   HLog << "\t\t" << infoName( CL_DEVICE_IMAGE_MAX_BUFFER_SIZE )
        << ": \t" << imageMaxBufferSize() << std::endl;
   // Get size_t _imageMaxArraySize
   HLog << "\t\t" << infoName( CL_DEVICE_IMAGE_MAX_ARRAY_SIZE )
        << ": \t"
        << imageMaxArraySize() << std::endl;
   // Get cl_uint _maxSamplers
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_SAMPLERS )
        << ": \t"
        << maxSamplers() << std::endl;
   // Get size_t _maxParameterSize
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_PARAMETER_SIZE )
        << ": \t"
        << maxParameterSize() << std::endl;
   // Get cl_uint _memBaseAddrAlign
   HLog << "\t\t" << infoName( CL_DEVICE_MEM_BASE_ADDR_ALIGN )
        << ": \t"
        << memBaseAddrAlign() << std::endl;
   // Get cl_uint _minDataTypeAlignSize
   HLog << "\t\t" << infoName( CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE ) << ": \t"
        << minDataTypeAlignSize() << std::endl;
   // Get cl_device_fp_config _singleFpConfig
   HLog << "\t\t" << infoName( CL_DEVICE_SINGLE_FP_CONFIG ) << ": \t";
   printFpConfig (_singleFpConfig);
   // Get cl_device_mem_cache_type _globalMemCacheType
   HLog << "\t\t" << infoName( CL_DEVICE_GLOBAL_MEM_CACHE_TYPE ) << ": \t";
   printGlobalMemCacheType(_globalMemCacheType);
   // Get cl_uint _globalMemCachelineSize
   HLog << "\t\t" << infoName( CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE )
        << ": \t"
        << globalMemCachelineSize() << std::endl;
   // Get cl_ulong _globalMemCacheSize
   HLog << "\t\t" << infoName( CL_DEVICE_GLOBAL_MEM_CACHE_SIZE )
        << ": \t"
        << globalMemCacheSize() << std::endl;
   // Get cl_ulong _globalMemSize
   HLog << "\t\t" << infoName( CL_DEVICE_GLOBAL_MEM_SIZE )
        << ": \t"
        << globalMemSize() << std::endl;
   // Get cl_ulong _maxConstantBufferSize
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE )
        << ": \t"
        << maxConstantBufferSize() << std::endl;
   // Get cl_uint _maxConstantArgs
   HLog << "\t\t" << infoName( CL_DEVICE_MAX_CONSTANT_ARGS )
        <<": \t"
        << maxConstantArgs() << std::endl;
   // Get cl_device_local_mem_type _localMemType
   HLog << "\t\t" << infoName( CL_DEVICE_LOCAL_MEM_TYPE ) <<": \t";
   printLocalMemType(_localMemType);
   // Get cl_ulong _localMemSize
   HLog << "\t\t" << infoName( CL_DEVICE_LOCAL_MEM_SIZE )
        << ": \t"
        << localMemSize() << std::endl;
   // Get cl_bool _errorCorrectionSupport
   HLog << "\t\t" << infoName( CL_DEVICE_ERROR_CORRECTION_SUPPORT )
        << ": \t"
        << errorCorrectionSupport() << std::endl;
   // Get cl_bool _hostUnifiedMemory
   HLog << "\t\t" << infoName( CL_DEVICE_HOST_UNIFIED_MEMORY )
        << ": \t"
        << hostUnifiedMemory() << std::endl;
   // Get cl_bool _profilingTimerResolution
   HLog << "\t\t" << infoName( CL_DEVICE_PROFILING_TIMER_RESOLUTION )
        << ": \t"
        << profilingTimerResolution() << std::endl;
   // Get cl_bool _endianLitGtle
   HLog << "\t\t" << infoName( CL_DEVICE_ENDIAN_LITTLE )
        << ": \t" << endianLittle() << std::endl;
   // Get cl_bool _available
   HLog << "\t\t" << infoName( CL_DEVICE_AVAILABLE )
        << ": \t" << available() << std::endl;
   // Get cl_bool _compilerAvailable
   HLog << "\t\t" << infoName( CL_DEVICE_COMPILER_AVAILABLE )
        << ": \t" << compilerAvailable() << std::endl;
   // Get cl_device_exec_capabilities _executionCapabilities
   HLog << "\t\t" << infoName( CL_DEVICE_EXECUTION_CAPABILITIES ) << ": \t";
   printExecCapabilities(_executionCapabilities);
   // Get _queueProperties
   HLog << "\t\t" << infoName( CL_DEVICE_QUEUE_PROPERTIES ) << ": \t";
   printQueueProperties( _queueProperties );
   // Get _platform
   HLog << "\t\t" << infoName( CL_DEVICE_PLATFORM )
        << ": \t" << devicePlatform() << std::endl;
   HLog << "\t\t" << infoName( CL_DEVICE_NAME )
        << ": \t" << deviceName() << std::endl;
   HLog << "\t\t" << infoName( CL_DEVICE_VENDOR )
        << ": \t" << deviceVendor() << std::endl;
   HLog << "\t\t" << infoName( CL_DRIVER_VERSION )
        << ": \t" << driverVersion() << std::endl;
   HLog << "\t\t" << infoName( CL_DEVICE_PROFILE )
        << ": \t" << deviceProfile() << std::endl;
   HLog << "\t\t" << infoName( CL_DEVICE_VERSION )
        << ": \t" << deviceVersion() << std::endl;
   HLog << "\t\t" << infoName( CL_DEVICE_OPENCL_C_VERSION )
        << ": \t" << deviceOpenclCVersion() << std::endl;
   // Get _deviceExtensions
   HLog << "\t\t" << infoName( CL_DEVICE_EXTENSIONS )
        << ": \t" << deviceExtensions() << std::endl;
}


bool OpenCLDevice::query() {
   int re = 0;
   // Get cl_device_type _deviceType
   std::vector< cl_device_type > device_types;
   re += getDeviceInfo<cl_device_type>( CL_DEVICE_TYPE,
                                        device_types );
   _deviceType = device_types[0];
   // Get std::string _deviceName;
   std::vector< char > val_chars;
   re += getDeviceInfo< char >( CL_DEVICE_NAME,
                                val_chars );
   _deviceName = &(val_chars[0]);
   // Get std::string _deviceVendor;
   re += getDeviceInfo< char >( CL_DEVICE_VENDOR,
                                val_chars );
   _deviceVendor = &(val_chars[0]);
   // Get std::string _driverVersion;
   re += getDeviceInfo< char >( CL_DRIVER_VERSION,
                                val_chars );
   _driverVersion = &(val_chars[0]);
   // Get std::string _deviceProfile;
   re += getDeviceInfo< char >( CL_DEVICE_PROFILE,
                                val_chars );
   _deviceProfile = &(val_chars[0]);
   // Get std::string _deviceVersion;
   re += getDeviceInfo< char >( CL_DEVICE_VERSION,
                                val_chars );
   _deviceVersion = &(val_chars[0]);
   // Get std::string _deviceOpenclCVersion;
   re += getDeviceInfo< char >( CL_DEVICE_OPENCL_C_VERSION,
                                val_chars );
   _deviceOpenclCVersion = &(val_chars[0]);
   //Get cl_ulong _deviceVendorId;
   std::vector < cl_ulong > val_ulongs;
   re += getDeviceInfo< cl_ulong >( CL_DEVICE_VENDOR_ID,
                                 val_ulongs);
   _deviceVendorId = val_ulongs[0];
   // Get cl_uint _maxComputeUnits
   std::vector< cl_uint > val_uints;
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_COMPUTE_UNITS,
                                   val_uints);
   _maxComputeUnits = val_uints[0];
   // Get cl_uint _maxWorkItemsDimens
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
                                   val_uints);
   _maxWorkItemsDimens = val_uints[0];
   // Get std::vector< size_t > _maxWorkItemSizes
   std::vector< size_t > val_sizets;
   re += getDeviceInfo< size_t >( CL_DEVICE_MAX_WORK_ITEM_SIZES,
                                  val_sizets);
   _maxWorkItemSizes = val_sizets;
   // init _globalWorkSizes, _localWorkSizes, and _maxGlobalWorkSize
   _globalWorkSizes.resize( _maxWorkItemsDimens );
   _localWorkSizes.resize( _maxWorkItemsDimens );
   _localWorkSizes[0] = 128;
   _localWorkSizes[1] = 1;
   _localWorkSizes[2] = 1;
   // Get _maxGlobalWorkSize by multiplying _maxWorkItemSizes per dimension
   _maxGlobalWorkSize = 1;
   for( cl_uint ui=0; ui < _maxWorkItemsDimens; ui++ ) {
       _globalWorkSizes[ ui ] = _maxWorkItemSizes[ui];
       //_localWorkSizes[ ui ] = 0;
       _maxGlobalWorkSize *= _maxWorkItemSizes[ui];
   }
   // Get cl_uint _maxWorkGroupSize
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_WORK_GROUP_SIZE,
                                   val_uints);
   _maxWorkGroupSize = val_uints[0];

   // Get cl_uint _preferredVectorWidthChar
   std::vector< cl_bool > val_bools;
   re += getDeviceInfo< cl_uint >( CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR,
                                   val_uints);
   _preferredVectorWidthChar = val_uints[0];
   // Get cl_uint _preferredVectorWidth
   re += getDeviceInfo< cl_uint >( CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT,
                                   val_uints);
   _preferredVectorWidthShort = val_uints[0];

   // Get cl_uint _preferredVectorWidthInt
   re += getDeviceInfo< cl_uint >( CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,
                                   val_uints);
   _preferredVectorWidthInt = val_uints[0];

   // Get cl_uint _preferredVectorWidthLong
   re += getDeviceInfo< cl_uint >( CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,
                                   val_uints);
   _preferredVectorWidthLong = val_uints[0];
   // Get cl_uint _preferredVectorWidthFloat
   re += getDeviceInfo< cl_uint >( CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,
                                   val_uints);
   _preferredVectorWidthFloat = val_uints[0];
   // Get cl_uint _preferredVectorWidthDouble
   re += getDeviceInfo< cl_uint >( CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE,
                                   val_uints);
   _preferredVectorWidthDouble = val_uints[0];
   // Get cl_uint _preferredVectorWidthHalf
   re += getDeviceInfo< cl_uint >( CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,
                                   val_uints);
   _preferredVectorWidthHalf = val_uints[0];
   // Get cl_uint _nativeVectorWidthChar
   re += getDeviceInfo< cl_uint >( CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,
                                   val_uints);
   _nativeVectorWidthChar = val_uints[0];

   // Get cl_uint _nativeVectorWidthShort
   re += getDeviceInfo< cl_uint >( CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,
                                   val_uints);
   _nativeVectorWidthShort = val_uints[0];
   // Get cl_uint _nativeVectorWidthInt
   re += getDeviceInfo< cl_uint >( CL_DEVICE_NATIVE_VECTOR_WIDTH_INT,
                                   val_uints);
   _nativeVectorWidthInt = val_uints[0];
   // Get cl_uint _nativeVectorWidthLong
   re += getDeviceInfo< cl_uint >( CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,
                                   val_uints);
   _nativeVectorWidthLong = val_uints[0];
   // Get cl_uint _nativeVectorWidthFloat
   re += getDeviceInfo< cl_uint >( CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
                                   val_uints);
   _nativeVectorWidthFloat = val_uints[0];
   // Get cl_uint _nativeVectorWidthDouble
   re += getDeviceInfo< cl_uint >( CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,
                                   val_uints);
   _nativeVectorWidthDouble = val_uints[0];
   // Get cl_uint _nativeVectorWidthHalf
   re += getDeviceInfo< cl_uint >( CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,
                                   val_uints);
   _nativeVectorWidthHalf = val_uints[0];
   // Get cl_uint _maxClockFrequency
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_CLOCK_FREQUENCY,
                                   val_uints);
   _maxClockFrequency = val_uints[0];

   // Get cl_uint _addressBits
   re += getDeviceInfo< cl_uint >( CL_DEVICE_ADDRESS_BITS,
                                   val_uints);
   _addressBits = val_uints[0];
   // Get cl_ulong _maxMemAllocSize
   //std::vector< cl_ulong > val_ulongs;
   re += getDeviceInfo< cl_ulong >( CL_DEVICE_MAX_MEM_ALLOC_SIZE,
                                    val_ulongs);
   _maxMemAllocSize = val_ulongs[0];
   // Get cl_bool _imageSupport
   //std::vector< cl_bool > val_bools;
   re += getDeviceInfo< cl_bool >( CL_DEVICE_IMAGE_SUPPORT,
                                   val_bools);
   _imageSupport = val_bools[0];
   // Get cl_uint _maxReadImageArgs
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_READ_IMAGE_ARGS,
                                   val_uints);
   _maxReadImageArgs = val_uints[0];
   // Get cl_uint _maxWriteImageArgs
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_WRITE_IMAGE_ARGS,
                                   val_uints);
   _maxWriteImageArgs = val_uints[0];
   // Get size_t _image2dMaxWidth
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE2D_MAX_WIDTH,
                                  val_sizets);
   _image2dMaxWidth = val_sizets[0];
   // Get size_t _image2dMaxHeight
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE2D_MAX_HEIGHT,
                                  val_sizets);
   _image2dMaxHeight = val_sizets[0];
   // Get size_t _image3dMaxWidth
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE3D_MAX_WIDTH,
                                  val_sizets);
   _image3dMaxWidth = val_sizets[0];


   // Get cl_uint _addressBits
   re += getDeviceInfo< cl_uint >( CL_DEVICE_ADDRESS_BITS,
                                   val_uints);
   _addressBits = val_uints[0];
   // Get cl_ulong _maxMemAllocSize
   //std::vector< cl_ulong > val_ulongs;
   re += getDeviceInfo< cl_ulong >( CL_DEVICE_MAX_MEM_ALLOC_SIZE,
                                    val_ulongs);
   _maxMemAllocSize = val_ulongs[0];
   // Get cl_bool _imageSupport
   //std::vector< cl_bool > val_bools;
   re += getDeviceInfo< cl_bool >( CL_DEVICE_IMAGE_SUPPORT,
                                   val_bools);
   _imageSupport = val_bools[0];
   // Get cl_uint _maxReadImageArgs
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_READ_IMAGE_ARGS,
                                   val_uints);
   _maxReadImageArgs = val_uints[0];
   // Get cl_uint _maxWriteImageArgs
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_WRITE_IMAGE_ARGS,
                                   val_uints);
   _maxWriteImageArgs = val_uints[0];
   // Get size_t _image2dMaxWidth
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE2D_MAX_WIDTH,
                                  val_sizets);
   _image2dMaxWidth = val_sizets[0];
   // Get size_t _image2dMaxHeight
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE2D_MAX_HEIGHT,
                                  val_sizets);
   _image2dMaxHeight = val_sizets[0];
   // Get size_t _image3dMaxWidth
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE3D_MAX_WIDTH,
                                  val_sizets);
   _image3dMaxWidth = val_sizets[0];
   // Get size_t _image3dMaxHeight
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE3D_MAX_HEIGHT,
                                  val_sizets);
   _image3dMaxHeight = val_sizets[0];
   // Get size_t _image3dMaxDepth
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE3D_MAX_DEPTH,
                                  val_sizets);
   _image3dMaxDepth = val_sizets[0];


   // Get size_t _imageMaxBufferSize
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE_MAX_BUFFER_SIZE,
                                  val_sizets);
   _imageMaxBufferSize = val_sizets[0];
   // Get size_t _imageMaxArraySize
   re += getDeviceInfo< size_t >( CL_DEVICE_IMAGE_MAX_ARRAY_SIZE,
                                  val_sizets);
   _imageMaxArraySize = val_sizets[0];
   // Get cl_uint _maxSamplers
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_SAMPLERS,
                                   val_uints);
   _maxSamplers = val_uints[0];
   // Get size_t _maxParameterSize
   re += getDeviceInfo< size_t >( CL_DEVICE_MAX_PARAMETER_SIZE,
                                  val_sizets);
   _maxParameterSize = val_sizets[0];
   // Get cl_uint _memBaseAddrAlign
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MEM_BASE_ADDR_ALIGN,
                                   val_uints);
   _memBaseAddrAlign = val_uints[0];


   // Get cl_uint _minDataTypeAlignSize
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE,
                                   val_uints);
   _minDataTypeAlignSize = val_uints[0];
   // Get device_fp_config _singleFpConfig
   std::vector< cl_device_fp_config > fp_config;
   re += getDeviceInfo< cl_device_fp_config >( CL_DEVICE_SINGLE_FP_CONFIG,
                                 fp_config );
   _singleFpConfig = fp_config[0];


   // Get cl_device_mem_cache_type _globalMemCacheType
   std::vector< cl_device_mem_cache_type > cache_type;
   re += getDeviceInfo< cl_device_mem_cache_type >
                            ( CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
                              cache_type);
   _globalMemCacheType =cache_type[0];

   // Get cl_ulong _globalMemCachelineSize
   re += getDeviceInfo< cl_uint >( CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,
                                    val_uints);
   _globalMemCachelineSize = val_uints[0];
   // Get cl_uint _globalMemCacheSize
   re += getDeviceInfo< cl_uint >( CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
                                   val_uints);
   _globalMemCacheSize = val_uints[0];

   // Get cl_ulong _globalMemSize
   re += getDeviceInfo< cl_ulong >( CL_DEVICE_GLOBAL_MEM_SIZE,
                                    val_ulongs);
   _globalMemSize = val_ulongs[0];
   // Get cl_ulong _maxConstantBufferSize
   re += getDeviceInfo< cl_ulong >( CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
                                    val_ulongs);
   _maxConstantBufferSize = val_ulongs[0];
   // Get cl_uint _maxConstantArgs
   re += getDeviceInfo< cl_uint >( CL_DEVICE_MAX_CONSTANT_ARGS,
                                   val_uints);
   _maxConstantArgs = val_uints[0];
   // Get cl_device_local_mem_type _localMemType
   std::vector< cl_device_local_mem_type > local_mem_type;
   re +=  getDeviceInfo< cl_device_local_mem_type>( CL_DEVICE_LOCAL_MEM_TYPE,
                                                    local_mem_type );
   _localMemType = local_mem_type[0];
   // Get cl_ulong _localMemSize
   re += getDeviceInfo< cl_ulong >( CL_DEVICE_LOCAL_MEM_SIZE,
                                   val_ulongs);
   _localMemSize = val_ulongs[0];
   // Get cl_bool _errorCorrectionSupport
   re += getDeviceInfo< cl_bool >( CL_DEVICE_ERROR_CORRECTION_SUPPORT,
                                   val_bools);
   _errorCorrectionSupport = val_bools[0];
   // Get cl_bool _hostUnifiedMemory
   re += getDeviceInfo< cl_bool >( CL_DEVICE_HOST_UNIFIED_MEMORY,
                                   val_bools);
   _hostUnifiedMemory = val_bools[0];

   // Get size_t _profilingTimerResolution
   re += getDeviceInfo< size_t >( CL_DEVICE_PROFILING_TIMER_RESOLUTION,
                                   val_sizets);
   _profilingTimerResolution = val_sizets[0];

   // Get cl_bool _endianLittle
   re += getDeviceInfo< cl_bool >( CL_DEVICE_ENDIAN_LITTLE,
                                   val_bools);
   _endianLittle = val_bools[0];

   // Get cl_bool _available
   re += getDeviceInfo< cl_bool >( CL_DEVICE_AVAILABLE,
                                   val_bools);
   _available = val_bools[0];

   // Get cl_bool _compilerAvailable
   re += getDeviceInfo< cl_bool >( CL_DEVICE_COMPILER_AVAILABLE,
                                   val_bools);
   _compilerAvailable = val_bools[0];

   // Get  _executionCapabilities
   std::vector< cl_device_exec_capabilities > exec_capabilities;
   re += getDeviceInfo< cl_device_exec_capabilities >
                                   ( CL_DEVICE_EXECUTION_CAPABILITIES,
                                     exec_capabilities);
   _executionCapabilities = exec_capabilities[0];

   // Get _platform_id _devicePlatform
   std::vector< cl_platform_id > platform_id;
   re += getDeviceInfo< cl_platform_id >( CL_DEVICE_PLATFORM,
                                          platform_id);
   _devicePlatform = platform_id[0];

   // Get string _deviceExtensions
   re += getDeviceInfo< char >( CL_DEVICE_EXTENSIONS,
                            val_chars);
   _deviceExtensions = &(val_chars[0]);

   // Get  _queueProperties
   std::vector< cl_command_queue_properties > queue_properties;
   re += getDeviceInfo< cl_command_queue_properties >
                    ( CL_DEVICE_QUEUE_PROPERTIES,
                                   queue_properties);
   _queueProperties = queue_properties[0];
   if( re ) { return false; }
   return true;
}


std::string OpenCLDevice::cktClMemTypeName() const {
   switch( _cktClMemType ) {
      case CKT_BUFFER: return "BUFFER";
      case CKT_IMAGE: return "IMAGE";
      default: return "unsupported cktClMemType";
   }
}


void OpenCLDevice::printUserPreferences() const {
   HLog << TwoSpace
        << "device name: " << deviceName() << std::endl;
   HLog << TwoSpace
        << "device type: " << deviceTypeName(deviceType()) << std::endl;
   HLog << TwoSpace
        << "global work size: ("
        << globalWorkSizes(0) << ":"
        << globalWorkSizes(1) << ":"
        << globalWorkSizes(2) << ")" << std::endl;
   HLog << TwoSpace
        << "local work size: ("
        << localWorkSizes(0) << ":"
        << localWorkSizes(1) << ":"
        << localWorkSizes(2) << ")" << std::endl;
   HLog << TwoSpace
        << "circuit cl_mem: " << cktClMemTypeName() << std::endl;
}


bool OpenCLDevice::validWorkSizeSetting(const std::vector<int>& gw_size,
                                        const std::vector<int>& lw_size) const
{
   // Assume 3-dimensional NDR
   for( cl_uint dim=0; dim < maxWorkItemsDimens(); dim++ ) {
      if( -1 != gw_size[dim] &&
         (gw_size[dim] < 1 || gw_size[dim] > (int)maxWorkItemSize(dim))) {
         HLog << InfoWarn
              << "global work size(" << dim
              << ") for device '" << deviceName() << "' is set to"
              << " its maximum value of " << maxWorkItemSize( dim )
              << std::endl;
      }
      if( -1 != lw_size[dim] &&
         ( lw_size[dim] < 1 || lw_size[dim] > (int)maxWorkGroupSize())) {
         HLog << InfoWarn
              << "local work size(" << dim
              << ") for device '" << deviceName() << "' is set to"
              << " its maximum value of " << maxWorkGroupSize()
              << std::endl;
      }
      if( 0 != (gw_size[dim] % lw_size[dim]) ) {
         HLog << InfoError
              << "global work size (" << gw_size[dim] << ")"
              << " must be divisible by local work size ("
              << lw_size[dim] << ")"
              << std::endl;
         return false;
      }
   }
   return true;
}


bool OpenCLDevice::display( bool summary ) {
   bool status=true;
   int ret=0;
   // Print limited info in summary; it is kind of duplicated and
   //    can be refactored later.
   if( summary ) {
      ret += display<cl_device_type>(
         CL_DEVICE_TYPE,
         "CL_DEVICE_TYPE");

      ret += display< char >(
         CL_DEVICE_NAME,
         "CL_DEVICE_NAME",
         /* isArray = */ true );

      ret += display< char >(
         CL_DEVICE_VENDOR,
         "CL_DEVICE_VENDOR",
         /* isArray = */ true );

      ret += display< char >(
         CL_DRIVER_VERSION,
         "CL_DRIVER_VERSION",
         /* isArray = */ true );

      ret += display< char >(
         CL_DEVICE_PROFILE,
         "CL_DEVICE_PROFILE",
         /* isArray = */ true );

      ret += display< char >(
         CL_DEVICE_VERSION,
         "CL_DEVICE_VERSION",
         /* isArray = */ true );

#ifdef CL_DEVICE_OPENCL_C_VERSION
      ret += display< char >(
         CL_DEVICE_OPENCL_C_VERSION,
         "CL_DEVICE_OPENCL_C_VERSION",
         /* isArray = */ true );
#endif
      return (0==ret);
   }

   ret += display<cl_device_type>(
      CL_DEVICE_TYPE,
      "CL_DEVICE_TYPE");

   ret += display<cl_uint>(
      CL_DEVICE_VENDOR_ID,
      "CL_DEVICE_VENDOR_ID");

   ret += display<cl_uint>(
      CL_DEVICE_MAX_COMPUTE_UNITS,
      "CL_DEVICE_MAX_COMPUTE_UNITS");

   ret += display<cl_uint>(
      CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
      "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS");

   ret += display<size_t>(
      CL_DEVICE_MAX_WORK_ITEM_SIZES,
      "CL_DEVICE_MAX_WORK_ITEM_SIZES",
      /* isArray = */ true );

   ret += display<std::size_t>(
      CL_DEVICE_MAX_WORK_GROUP_SIZE,
      "CL_DEVICE_MAX_WORK_GROUP_SIZE");

   ret += display<cl_uint>(
      CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR,
      "CL_DEVICE_PREFERRED_VECTOR_WIDTH_CHAR");

   ret += display<cl_uint>(
      CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT,
      "CL_DEVICE_PREFERRED_VECTOR_WIDTH_SHORT");

   ret += display<cl_uint>(
      CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT,
      "CL_DEVICE_PREFERRED_VECTOR_WIDTH_INT");

   ret += display<cl_uint>(
      CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG,
      "CL_DEVICE_PREFERRED_VECTOR_WIDTH_LONG");

   ret += display<cl_uint>(
      CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT,
      "CL_DEVICE_PREFERRED_VECTOR_WIDTH_FLOAT");

   ret += display<cl_uint>(
      CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE,
      "CL_DEVICE_PREFERRED_VECTOR_WIDTH_DOUBLE");

#ifdef CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF

   ret += display<cl_uint>(
      CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF,
      "CL_DEVICE_PREFERRED_VECTOR_WIDTH_HALF");

   ret += display<cl_uint>(
      CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR,
      "CL_DEVICE_NATIVE_VECTOR_WIDTH_CHAR");

   ret += display<cl_uint>(
      CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT,
      "CL_DEVICE_NATIVE_VECTOR_WIDTH_SHORT");

   ret += display<cl_uint>(
      CL_DEVICE_NATIVE_VECTOR_WIDTH_INT,
      "CL_DEVICE_NATIVE_VECTOR_WIDTH_INT");

   ret += display<cl_uint>(
      CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG,
      "CL_DEVICE_NATIVE_VECTOR_WIDTH_LONG");

   ret += display<cl_uint>(
      CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT,
      "CL_DEVICE_NATIVE_VECTOR_WIDTH_FLOAT");

   ret += display<cl_uint>(
      CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE,
      "CL_DEVICE_NATIVE_VECTOR_WIDTH_DOUBLE");

   ret += display<cl_uint>(
      CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF,
      "CL_DEVICE_NATIVE_VECTOR_WIDTH_HALF");
#endif

   ret += display<cl_uint>(
      CL_DEVICE_MAX_CLOCK_FREQUENCY,
      "CL_DEVICE_MAX_CLOCK_FREQUENCY");

   ret += display<cl_uint>(
      CL_DEVICE_ADDRESS_BITS,
      "CL_DEVICE_ADDRESS_BITS");

   ret += display<cl_ulong>(
      CL_DEVICE_MAX_MEM_ALLOC_SIZE,
      "CL_DEVICE_MAX_MEM_ALLOC_SIZE");

   ret += display<cl_bool>(
      CL_DEVICE_IMAGE_SUPPORT,
      "CL_DEVICE_IMAGE_SUPPORT");

   ret += display<cl_uint>(
      CL_DEVICE_MAX_READ_IMAGE_ARGS,
      "CL_DEVICE_MAX_READ_IMAGE_ARGS");

   ret += display<cl_uint>(
      CL_DEVICE_MAX_WRITE_IMAGE_ARGS,
      "CL_DEVICE_MAX_WRITE_IMAGE_ARGS");

   ret += display<std::size_t>(
      CL_DEVICE_IMAGE2D_MAX_WIDTH,
      "CL_DEVICE_IMAGE2D_MAX_WIDTH");

//   ret += display<std::size_t>(
//      CL_DEVICE_IMAGE2D_MAX_WIDTH,
//      "CL_DEVICE_IMAGE2D_MAX_WIDTH");

   ret += display<std::size_t>(
      CL_DEVICE_IMAGE2D_MAX_HEIGHT,
      "CL_DEVICE_IMAGE2D_MAX_HEIGHT");

   ret += display<std::size_t>(
      CL_DEVICE_IMAGE3D_MAX_WIDTH,
      "CL_DEVICE_IMAGE3D_MAX_WIDTH");

   ret += display<std::size_t>(
      CL_DEVICE_IMAGE3D_MAX_HEIGHT,
      "CL_DEVICE_IMAGE3D_MAX_HEIGHT");

   ret += display<std::size_t>(
      CL_DEVICE_IMAGE3D_MAX_DEPTH,
      "CL_DEVICE_IMAGE3D_MAX_DEPTH");

   ret += display<cl_uint>(
      CL_DEVICE_MAX_SAMPLERS,
      "CL_DEVICE_MAX_SAMPLERS");

   ret += display<std::size_t>(
      CL_DEVICE_MAX_PARAMETER_SIZE,
      "CL_DEVICE_MAX_PARAMETER_SIZE");

   ret += display<cl_uint>(
      CL_DEVICE_MEM_BASE_ADDR_ALIGN,
      "CL_DEVICE_MEM_BASE_ADDR_ALIGN");

   ret += display<cl_uint>(
      CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE,
      "CL_DEVICE_MIN_DATA_TYPE_ALIGN_SIZE");

   ret += display<cl_device_fp_config>(
      CL_DEVICE_SINGLE_FP_CONFIG,
      "CL_DEVICE_SINGLE_FP_CONFIG");

   ret += display<cl_device_mem_cache_type>(
      CL_DEVICE_GLOBAL_MEM_CACHE_TYPE,
      "CL_DEVICE_GLOBAL_MEM_CACHE_TYPE");

   ret += display<cl_uint>(
      CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE,
      "CL_DEVICE_GLOBAL_MEM_CACHELINE_SIZE");

   ret += display<cl_ulong>(
      CL_DEVICE_GLOBAL_MEM_CACHE_SIZE,
      "CL_DEVICE_GLOBAL_MEM_CACHE_SIZE");

   ret += display<cl_ulong>(
      CL_DEVICE_GLOBAL_MEM_SIZE,
      "CL_DEVICE_GLOBAL_MEM_SIZE");

   ret += display<cl_ulong>(
      CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE,
      "CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE");

   ret += display<cl_uint>(
      CL_DEVICE_MAX_CONSTANT_ARGS,
      "CL_DEVICE_MAX_CONSTANT_ARGS");

   ret += display<cl_device_local_mem_type>(
      CL_DEVICE_LOCAL_MEM_TYPE,
      "CL_DEVICE_LOCAL_MEM_TYPE");

   ret += display<cl_ulong>(
      CL_DEVICE_LOCAL_MEM_SIZE,
      "CL_DEVICE_LOCAL_MEM_SIZE");

   ret += display<cl_bool>(
      CL_DEVICE_ERROR_CORRECTION_SUPPORT,
      "CL_DEVICE_ERROR_CORRECTION_SUPPORT");

#ifdef CL_DEVICE_HOST_UNIFIED_MEMORY
   ret += display<cl_bool>(
      CL_DEVICE_HOST_UNIFIED_MEMORY,
      "CL_DEVICE_HOST_UNIFIED_MEMORY");
#endif

   ret += display<std::size_t>(
      CL_DEVICE_PROFILING_TIMER_RESOLUTION,
      "CL_DEVICE_PROFILING_TIMER_RESOLUTION");

   ret += display<cl_bool>(
      CL_DEVICE_ENDIAN_LITTLE,
      "CL_DEVICE_ENDIAN_LITTLE");

   ret += display<cl_bool>(
      CL_DEVICE_AVAILABLE,
      "CL_DEVICE_AVAILABLE");

   ret += display<cl_bool>(
      CL_DEVICE_COMPILER_AVAILABLE,
      "CL_DEVICE_COMPILER_AVAILABLE");

   ret += display<cl_device_exec_capabilities>(
      CL_DEVICE_EXECUTION_CAPABILITIES,
      "CL_DEVICE_EXECUTION_CAPABILITIES");

   ret += display<cl_command_queue_properties>(
      CL_DEVICE_QUEUE_PROPERTIES,
      "CL_DEVICE_QUEUE_PROPERTIES");

   ret += display<cl_platform_id>(
      CL_DEVICE_PLATFORM,
      "CL_DEVICE_PLATFORM");

   ret += display< char >(
      CL_DEVICE_NAME,
      "CL_DEVICE_NAME",
      /* isArray = */ true );

   ret += display< char >(
      CL_DEVICE_VENDOR,
      "CL_DEVICE_VENDOR",
      /* isArray = */ true );

   ret += display< char >(
      CL_DRIVER_VERSION,
      "CL_DRIVER_VERSION",
      /* isArray = */ true );

   ret += display< char >(
      CL_DEVICE_PROFILE,
      "CL_DEVICE_PROFILE",
      /* isArray = */ true );

   ret += display< char >(
      CL_DEVICE_VERSION,
      "CL_DEVICE_VERSION",
      /* isArray = */ true );

#ifdef CL_DEVICE_OPENCL_C_VERSION
   ret += display< char >(
      CL_DEVICE_OPENCL_C_VERSION,
      "CL_DEVICE_OPENCL_C_VERSION",
      /* isArray = */ true );
#endif

   ret += display< char >(
      CL_DEVICE_EXTENSIONS,
      "CL_DEVICE_EXTENSIONS",
      /* isArray = */ true );

   if( ret ) { status=false; }
   HLog << std::endl << std::endl;

   return status;
}


template<typename T>
void OpenCLDevice::appendBitfield(
                      T info,
                      T value,
                      std::string name,
                      std::string & str)
{
   if (info & value) {
      if (str.length() > 0) {
         str.append(" | ");
      }
      str.append(name);
   }
}


template< typename T >
int OpenCLDevice::getDeviceInfo( cl_device_info info,
                                  std::vector< T >& val )
{
   cl_int err=0;
   std::string err_msg("");
   std::size_t para_size=0;
   // get parameter size first
   err = clGetDeviceInfo( _clDeviceId,
                          info,
                          0,
                          NULL,
                          &para_size );
   err_msg = "get parameter size of " + infoName(info);
   if( oclFunctionFailed( err, err_msg.c_str() )) {
      return STATUS_ERROR;
   }
   // get the actual parameters
   val.resize( para_size );
   err = clGetDeviceInfo( _clDeviceId,
                          info,
                          para_size,
                          &(val[0]),
                          NULL );
   err_msg = "retrieve " + infoName(info);
   if( oclFunctionFailed( err, err_msg.c_str() )) {
      return STATUS_ERROR;
   }
   return STATUS_OK;
}


template< typename T >
int  OpenCLDevice::display(
                      cl_device_info name,
                      std::string str,
                      bool isArray)
{
   cl_int errNum;
   std::size_t paramValueSize;

   errNum = clGetDeviceInfo( _clDeviceId,
                             name,
                             0,
                             NULL,
                             &paramValueSize );
   if (errNum != CL_SUCCESS) {
      HLog << "Failed to find OpenCL device info " << str << "." << std::endl;
      return CL_PRINT_ERROR;
   }

   T * info = (T *)alloca(sizeof(T) * paramValueSize);
   errNum = clGetDeviceInfo(
      _clDeviceId,
      name,
      paramValueSize,
      info,
      NULL);
   if (errNum != CL_SUCCESS) {
      HLog << "Failed to find OpenCL device info " << str << "." << std::endl;
      return CL_PRINT_ERROR;
   }

   // Handle a few special cases
   if( isArray ) { // for array-type property
      if ( CL_DEVICE_MAX_WORK_ITEM_SIZES == name ) {
         cl_uint maxWorkItemDimensions;

         errNum = clGetDeviceInfo(
            _clDeviceId,
            CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS,
            sizeof(cl_uint),
            &maxWorkItemDimensions,
            NULL);
         if (errNum != CL_SUCCESS) {
            HLog
               << "Failed to find OpenCL device info "
               << "CL_DEVICE_MAX_WORK_ITEM_DIMENSIONS."
               << std::endl;
            return CL_PRINT_ERROR;
         }

         HLog << "\t\t" << str << ":\t" ;
         for (cl_uint i = 0; i < maxWorkItemDimensions; i++) {
            HLog << info[i] << " ";
         }
         HLog << std::endl;
      } else { // char-type array
         HLog << "\t\t" << str << ":\t" << info << std::endl;
      }
   } else { // for non-array type property
      switch (name) {
         case CL_DEVICE_TYPE: {
            std::string deviceType;

            appendBitfield<cl_device_type>(
               *(reinterpret_cast<cl_device_type*>(info)),
               CL_DEVICE_TYPE_CPU,
               "CL_DEVICE_TYPE_CPU",
               deviceType);

            appendBitfield<cl_device_type>(
               *(reinterpret_cast<cl_device_type*>(info)),
               CL_DEVICE_TYPE_GPU,
               "CL_DEVICE_TYPE_GPU",
               deviceType);

            appendBitfield<cl_device_type>(
               *(reinterpret_cast<cl_device_type*>(info)),
               CL_DEVICE_TYPE_ACCELERATOR,
               "CL_DEVICE_TYPE_ACCELERATOR",
               deviceType);

            appendBitfield<cl_device_type>(
               *(reinterpret_cast<cl_device_type*>(info)),
               CL_DEVICE_TYPE_DEFAULT,
               "CL_DEVICE_TYPE_DEFAULT",
               deviceType);

            HLog << "\t\t" << str << ":\t" << deviceType << std::endl;
            break;
         }
         case CL_DEVICE_SINGLE_FP_CONFIG: {
            std::string fpType;

            appendBitfield<cl_device_fp_config>(
               *(reinterpret_cast<cl_device_fp_config*>(info)),
               CL_FP_DENORM,
               "CL_FP_DENORM",
               fpType);

            appendBitfield<cl_device_fp_config>(
               *(reinterpret_cast<cl_device_fp_config*>(info)),
               CL_FP_INF_NAN,
               "CL_FP_INF_NAN",
               fpType);

            appendBitfield<cl_device_fp_config>(
               *(reinterpret_cast<cl_device_fp_config*>(info)),
               CL_FP_ROUND_TO_NEAREST,
               "CL_FP_ROUND_TO_NEAREST",
               fpType);

            appendBitfield<cl_device_fp_config>(
               *(reinterpret_cast<cl_device_fp_config*>(info)),
               CL_FP_ROUND_TO_ZERO,
               "CL_FP_ROUND_TO_ZERO",
               fpType);

            appendBitfield<cl_device_fp_config>(
               *(reinterpret_cast<cl_device_fp_config*>(info)),
               CL_FP_ROUND_TO_INF,
               "CL_FP_ROUND_TO_INF",
               fpType);

            appendBitfield<cl_device_fp_config>(
               *(reinterpret_cast<cl_device_fp_config*>(info)),
               CL_FP_FMA,
               "CL_FP_FMA",
               fpType);

   #ifdef CL_FP_SOFT_FLOAT
            appendBitfield<cl_device_fp_config>(
               *(reinterpret_cast<cl_device_fp_config*>(info)),
               CL_FP_SOFT_FLOAT,
               "CL_FP_SOFT_FLOAT",
               fpType);
   #endif

            HLog << "\t\t" << str << ":\t" << fpType << std::endl;
            break;
         }
         case CL_DEVICE_GLOBAL_MEM_CACHE_TYPE: {
            std::string memType;

            appendBitfield<cl_device_mem_cache_type>(
               *(reinterpret_cast<cl_device_mem_cache_type*>(info)),
               CL_NONE,
               "CL_NONE",
               memType);
            appendBitfield<cl_device_mem_cache_type>(
               *(reinterpret_cast<cl_device_mem_cache_type*>(info)),
               CL_READ_ONLY_CACHE,
               "CL_READ_ONLY_CACHE",
               memType);

            appendBitfield<cl_device_mem_cache_type>(
               *(reinterpret_cast<cl_device_mem_cache_type*>(info)),
               CL_READ_WRITE_CACHE,
               "CL_READ_WRITE_CACHE",
               memType);

            HLog << "\t\t" << str << ":\t" << memType << std::endl;
            break;
         }
         case CL_DEVICE_LOCAL_MEM_TYPE: {
            std::string memType;

            appendBitfield<cl_device_local_mem_type>(
               *(reinterpret_cast<cl_device_local_mem_type*>(info)),
               CL_LOCAL,
               "CL_LOCAL",
               memType);

            appendBitfield<cl_device_local_mem_type>(
               *(reinterpret_cast<cl_device_local_mem_type*>(info)),
               CL_GLOBAL,
               "CL_GLOBAL",
               memType);

            HLog << "\t\t" << str << ":\t" << memType << std::endl;
            break;
         }
         case CL_DEVICE_EXECUTION_CAPABILITIES: {
            std::string memType;

            appendBitfield<cl_device_exec_capabilities>(
               *(reinterpret_cast<cl_device_exec_capabilities*>(info)),
               CL_EXEC_KERNEL,
               "CL_EXEC_KERNEL",
               memType);

            appendBitfield<cl_device_exec_capabilities>(
               *(reinterpret_cast<cl_device_exec_capabilities*>(info)),
               CL_EXEC_NATIVE_KERNEL,
               "CL_EXEC_NATIVE_KERNEL",
               memType);

            HLog << "\t\t" << str << ":\t" << memType << std::endl;
            break;
         }
         case CL_DEVICE_QUEUE_PROPERTIES: {
            std::string memType;

            appendBitfield<cl_device_exec_capabilities>(
               *(reinterpret_cast<cl_device_exec_capabilities*>(info)),
               CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE,
               "CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE",
               memType);

            appendBitfield<cl_device_exec_capabilities>(
               *(reinterpret_cast<cl_device_exec_capabilities*>(info)),
               CL_QUEUE_PROFILING_ENABLE,
               "CL_QUEUE_PROFILING_ENABLE",
               memType);

            HLog << "\t\t" << str << ":\t" << memType << std::endl;
            break;
         }
         default: {
            HLog << "\t\t" << str << ":\t" << *info << std::endl;
            break;
         }
      }
   }
   return CL_PRINT_OK;
}
