//////////////////////////////////////////////////////////////////
//                        EvaluateCL.h
//
//  various data structure to perform simulation on GPU
//
//////////////////////////////////////////////////////////////////

#ifndef _EVALUATE_CL_H
#define _EVALUATE_CL_H

#include "OpenCLInfo.h"
#include "Netlist.h"

// Flag for printing debug message
#define EVAL_CL_DBG_WORK_SIZE 0x00000001
#define EVAL_CL_DBG_KERNEL    0x00000002
#define EVAL_CL_DBG_PAT_CYCLE 0x00000004
#define EVAL_CL_DBG_GMV_COPY  0x00000008
#define EVAL_CL_DBG_IMG_PARA  0x00000010
#define EVAL_CL_DBG_TEST_IMG  0x00000020

// Print program build log on device
void oclDumpProgramBuildLog( cl_program program, cl_device_id device );

class EvaluateCL {
   public:
      EvaluateCL();
      ~EvaluateCL();

      // Set up simulation environment on GPU.
      //   create memory objects for netlist and good machine value
      bool createAndBuildProgramOnPfm( cl_uint pfm_num,
                                       const std::string& file_name );

      bool setupCktMemObjAndKernelsOnPfm( cl_uint pfm_num,
                                          LevelizedNetlist& ckt );

      bool setupGmvBufAndSimKernelsOnPfm( cl_uint pfm_num,
                                          LevelizedNetlist& ckt );

      bool setupKernelInitTIEGates( cl_uint pfm_num,
                                    LevelizedNetlist& ckt );

      bool setupKernelInitTIEGatesFlag( cl_uint pfm_num,
                                        LevelizedNetlist& ckt );

      bool setupKernelInitXBackground( cl_uint pfm_num,
                                       LevelizedNetlist& ckt );

      bool setupKernelAssignDataPI( cl_uint pfm_num,
                                    LevelizedNetlist& ckt );

      bool setupKernelAssignDataPIFlag( cl_uint pfm_num,
                                        LevelizedNetlist& ckt );

      bool setupKernelAssignClockPI( cl_uint pfm_num,
                                     LevelizedNetlist& ckt );

      bool setupKernelAssignClockPIFlag( cl_uint pfm_num,
                                         LevelizedNetlist& ckt );

      bool setupKernelAssignInitFF( cl_uint pfm_num,
                                    LevelizedNetlist& ckt );

      bool setupKernelAssignInitFFFlag( cl_uint pfm_num,
                                        LevelizedNetlist& ckt );

      bool setupKernelGsimBufEvent( cl_uint pfm_num,
                                    LevelizedNetlist& ckt );

      bool setupKernelGsimBufEventFlag( cl_uint pfm_num,
                                        LevelizedNetlist& ckt );

      bool initXBackgroundAndBuffersOnDev( cl_uint pfm_num,
                                           cl_uint dev_num,
                                           LevelizedNetlist& ckt );

      bool initTieGatesOnDev( cl_uint pfm_num,
                              cl_uint dev_num,
                              LevelizedNetlist& ckt,
                              LogicSimMethod lsim_type );

      bool initCktImageOnDev( cl_uint pfm_num,
                              cl_uint dev_num,
                              LevelizedNetlist& ckt );

      bool setupPatternDataOnHostAndGpu( cl_uint pfm_num );

      void retrievePatternInData( bool external_patterns,
                                  int start,
                                  int parallel,
                                  int grp  // which pattern data to fill in
                                );

      bool setupSimulationOnOclDevice( cl_uint pfm_num,
                                       cl_uint dev_num,
                                       LevelizedNetlist& ckt );


      bool assignInputsPerCycleOnGpu( cl_uint pfm_num,
                                      cl_uint dev_num,
                                      LevelizedNetlist& ckt,
                                      int grp,
                                      int cyc,
                                      int frame,
                                      bool last_cycle,
                                      bool io_per_cycle,
                                      LogicSimMethod lsim_type,
                                      bool verbose = false );

      bool assignDataInputsOnGPU( cl_uint pfm_num,
                                  cl_uint dev_num,
                                  LevelizedNetlist& ckt,
                                  int grp,
                                  int cyc,
                                  int frame,
                                  bool last_cycle,
                                  bool io_per_cycle,
                                  LogicSimMethod lsim_type,
                                  std::vector< cl_event >& events,
                                  bool verbose = false );

      bool assignClockInputsOnGPU( cl_uint pfm_num,
                                   cl_uint dev_num,
                                   LevelizedNetlist& ckt,
                                   int grp,
                                   int cyc,
                                   int frame,
                                   bool last_cycle,
                                   bool io_per_cycle,
                                   LogicSimMethod lsim_type,
                                   std::vector< cl_event >& events,
                                   bool verbose = false );

      bool assignInitFFsOnGPU( cl_uint pfm_num,
                               cl_uint dev_num,
                               LevelizedNetlist& ckt,
                               int grp,
                               int cyc,
                               int frame,
                               bool last_cycle,
                               bool io_per_cycle,
                               LogicSimMethod lsim_type,
                               std::vector< cl_event >& events,
                               bool verbose = false );

      bool saveResponsesPerCycleOnGpu( cl_uint pfm_num,
                                       cl_uint dev_num,
                                       LevelizedNetlist& ckt,
                                       int grp,
                                       int cyc,
                                       int frame,
                                       bool last_cycle,
                                       bool io_per_cycle,
                                       bool verbose = false);

      bool saveMeasurePOsOnGpu( cl_uint pfm_num,
                                cl_uint dev_num,
                                LevelizedNetlist& ckt,
                                int grp,
                                int cyc,
                                int frame,
                                bool last_cycle,
                                bool io_per_cycle,
                                std::vector< cl_event >& events,
                                bool verbose = false );

      bool saveObserveFFsOnGpu( cl_uint pfm_num,
                                cl_uint dev_num,
                                LevelizedNetlist& ckt,
                                int grp,
                                int cyc,
                                int frame,
                                bool last_cycle,
                                bool io_per_cycle,
                                std::vector< cl_event >& events,
                                bool verbose = false );

      bool dumpStatusOfSimEvents( int cur_lev,
                                  cl_uint pfm_num,
                                  cl_uint dev_num,
                                  LevelizedNetlist& ckt );

      bool verifyBackgroundofEventDrivenSimulation( cl_uint pfm_num,
                                                    cl_uint dev_num,
                                                    LevelizedNetlist& ckt );

      void dumpAllNodesOnDev( cl_uint pfm_num,
                              cl_uint dev_num,
                              LevelizedNetlist& ckt,
                              int up_to_lev,
                              unsigned long mask1hot,
                              const std::string& fn );

      void dumpOutputsAndDffs( cl_uint pfm_num,
                               cl_uint dev_num,
                               LevelizedNetlist& ckt,
                               unsigned long mask1hot );

      // Now DFFs are at the last level, netlist is simulated in its
      //   natural level order of 1, 2, 3, ..., numLevels-1,
      bool goodSim1CycleOnGpu( cl_uint pfm_num,
                               cl_uint dev_num,
                               LevelizedNetlist& ckt );

      bool goodSim1CycleEventDrivenOnGpu( cl_uint pfm_num,
                                          cl_uint dev_num,
                                          LevelizedNetlist& ckt,
                                          LogicSimMethod lsim_type,
                                          int cycle,
                                          int frame );

      bool goodSim1PsetOnGpu( cl_uint pfm_num,
                              cl_uint dev_num,
                              LevelizedNetlist& ckt,
                              int grp,
                              int n_cycles,
                              bool io_per_cycle,
                              LogicSimMethod lsim_type );

      bool simulatePatternsWithOverlap( cl_uint pfm_num,
                                        cl_uint dev_num,
                                        int start,
                                        int end,
                                        bool external_pattern,
                                        bool store_patterns,
                                        int n_cycles,
                                        bool io_per_cycle,
                                        LogicSimMethod lsim_type );

      void backupPatternInDataForStorePatterns( cl_uint grp_id );

      // From Host to Device
      bool copyPatternInHToD( cl_uint pfm_num,
                              cl_uint dev_num,
                              cl_uint grp_id,
                              cl_event& event_gmv0,
                              cl_event& event_gmv1 );

      // From Device to Host
      bool copyPatternOutDToH( cl_uint pfm_num,
                               cl_uint dev_num,
                               cl_uint grp_id,
                               cl_event& event_gmv0,
                               cl_event& event_gmv1 );

      void storePatterns( const std::vector<uint64_t>& gmv0_in,
                          const std::vector<uint64_t>& gmv1_in,
                          const std::vector<uint64_t>& gmv0_out,
                          const std::vector<uint64_t>& gmv1_out,
                          int parallel,
                          int n_cycles,
                          bool io_per_cycle );

      // Test gmv copy between host and gpu
      bool testGmvCopy( cl_uint pfm_num, cl_uint dev_num );

      void dumpGmvValueOnGpu( cl_uint pfm_num,
                              cl_uint dev_num,
                              cl_uint grp_id,
                              int start_node,
                              int num_nodes,
                              unsigned long mask1hot );

      bool copyGmvFromHostToGpu( cl_uint pfm_num,
                                 cl_uint dev_num,
                                 cl_uint grp_id, // which group of GMV
                                 int start_node,
                                 int num_nodes);

      bool copyGmvFromGpuToHost( cl_uint pfm_num,
                                 cl_uint dev_num,
                                 cl_uint grp_id, // which group of GMV
                                 int start_node,
                                 int num_nodes);

      // Debug function to verify circuit image objects created on GPU
      bool verifyCircuitImage( cl_uint pfm_num,
                               cl_uint dev_num,
                               LevelizedNetlist& ckt,
                               cl_mem& node_image,
                               cl_mem& conn_image );

      // Make sure that image width is always power of 2 such that (x,y)
      // coordinates can be easily computed with shift and mask in ocl kernel.
      //   mask = image_width - 1
      //   shift = ( number of shifts for 'mask' until it reaches 0.
      void computeCktImageSize( LevelizedNetlist& ckt,
                                cl_int2& node_img_size,
                                cl_int2& conn_img_size,
                                int& shift,
                                unsigned& mask );

      // Allocate global work size and local work size based on various inputs
      void allocateWorkSize( size_t remaining_work_items,
                             const OpenCLDevice & cl_dev,
                             cl_uint & work_dim,
                             std::vector<size_t> & global_work_size,
                             std::vector<size_t> & local_work_size,
                             size_t & effective_size );

      // Check kernel work group size before enqueue
      bool checkKernelWorkGroupSize( cl_kernel kernel,
                                     cl_device_id device,
                                     size_t local_work_size );

      cl_int2 nodeImageSize() const { return _nodeImageSize; }
      cl_int2 connImageSize() const { return _connImageSize; }
      int yShift() const { return _shift; }
      unsigned xMask() const { return _mask; }

      // Debug control
      void printClEnqueueParameters( cl_uint work_dim,
                                     std::vector<size_t>& global_work_offset,
                                     std::vector<size_t>& global_work_size,
                                     std::vector<size_t>& local_work_size,
                                     unsigned base_node_id,
                                     unsigned padding_bound,
                                     size_t effective_size )  const;
      void debugFlag( unsigned v ) { _debugFlag = v; }
      unsigned debugFlag() const { return _debugFlag; }

      // Measurements
      void resetOclMeasurements();
      unsigned numGsimEnqueues() const { return _numGsimEnqueues; }
      unsigned numGmvCopiesHost2Gpu() const {
         return _numGmvCopiesHost2Gpu;
      }
      unsigned numGmvCopiesGpu2Host() const {
         return _numGmvCopiesGpu2Host;
      }
      double elapsedSysTimeGmvHost2Gpu() const {
         return _elapsedSysTimeGmvHost2Gpu;
      }
      double elapsedCpuTimeGmvHost2Gpu() const {
         return _elapsedCpuTimeGmvHost2Gpu;
      }
      double elapsedSysTimeGmvGpu2Host() const {
         return _elapsedSysTimeGmvGpu2Host;
      }
      double elapsedCpuTimeGmvGpu2Host() const {
         return _elapsedCpuTimeGmvGpu2Host;
      }
      int patternInSize() const { return _patInSize; }
      int patternOutSize() const { return _patOutSize; }
      std::vector<uint64_t>& patternInGmv0Backup() {
         return _patInBackupGmv0;
      }
      std::vector<uint64_t>& patternInGmv1Backup() {
         return _patInBackupGmv1;
      }
      std::vector<uint64_t>& patternInGmv0( unsigned g ) {
         return _patternsDataInGmv0[ g ];
      }
      std::vector<uint64_t>& patternInGmv1( unsigned g ) {
         return _patternsDataInGmv1[ g ];
      }
      std::vector<uint64_t>& patternOutGmv0( unsigned g ) {
         return _patternsDataOutGmv0[ g ];
      }
      std::vector<uint64_t>& patternOutGmv1( unsigned g ) {
         return _patternsDataOutGmv1[ g ];
      }

   private:
      // buffer for ckt image of types[], levels[], predFirst[], succFirst[]
      std::vector< cl_int4 > _cktNodeImgIoBuf;
      // buffer for ckt image of preds[], succs[]
      std::vector< cl_ushort4 > _cktConnImgIoBuf;

      unsigned _debugFlag;

      cl_int2 _nodeImageSize; // size of node image
      cl_int2 _connImageSize; // size of connection image
      int _shift;  // number of shifts to compute Y coordinate
      unsigned _mask;  // bit-AND mask to compute X coordinate

      unsigned _numGsimEnqueues;
      unsigned _numGmvCopiesHost2Gpu;
      unsigned _numGmvCopiesGpu2Host;
      double _elapsedSysTimeGmvHost2Gpu;
      double _elapsedCpuTimeGmvHost2Gpu;
      double _elapsedSysTimeGmvGpu2Host;
      double _elapsedCpuTimeGmvGpu2Host;

      // Could use the one exists on Evaluate class instead
      // std::vector< cl_ulong > _gmv0;
      // std::vector< cl_ulong > _gmv1;
      // To hold pattern data for transferring between host and gpu
      // int _numCycles;     // get from Evaluate class
      int _patInSize;   // size of single pattern input set
      int _patOutSize;  // size of single pattern output set

      // vector to hold backup patternIn for store_patterns
      std::vector<uint64_t>  _patInBackupGmv0;
      std::vector<uint64_t>  _patInBackupGmv1;

      // vectors to hold multiple copies of pattern in and out
      std::vector< std::vector<uint64_t>> _patternsDataInGmv0;
      std::vector< std::vector<uint64_t>> _patternsDataInGmv1;
      std::vector< std::vector<uint64_t>> _patternsDataOutGmv0;
      std::vector< std::vector<uint64_t>> _patternsDataOutGmv1;

};

#endif
