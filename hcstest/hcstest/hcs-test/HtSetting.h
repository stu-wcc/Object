/////////////////////////////////////////////////////////////////
//                      HtSetting.h
//  Class holder for Hcs-test related settings and objects
//  It is instantiated as a singleton class.
/////////////////////////////////////////////////////////////////

#ifndef _HT_SETTING
#define _HT_SETTING

#include "app-common/AppCommonSetting.h"
#include "OpenCLInfo.h"
#include "Design.h"
#include "Evaluate.h"
#include "EvaluateCL.h"
//#include "StaticLearning.h"


class RandomPatterns {
   public:
      RandomPatterns() :
         _count( 1024 ),
         _sequentialDepth( 1 ),
         _seed( 1 )
      { ; }

      ~RandomPatterns() {
        ;
      }
      void number( int n ) { _count = n; }
      int number() const { return _count; }

      void sequentialDepth( int n ) { _sequentialDepth = n; }
      int sequentialDepth() const { return _sequentialDepth; }

      void seed( int n ) { _seed = n; }
      int seed() const { return _seed; }

   private:
      int _count;
      int _sequentialDepth;
      int _seed;
};


class SimulationOptions {
   public:
      SimulationOptions() :
         _fourValueLogic( true ),
         _ioOpPerCycle( false ),
         _lsimMethod( IN_ORDER ),
         _simLoc( HOST ),
         _pfmId( 0 ),
         _devId( 0 ),
         _numParallelPsetsOnGpu( 0 ),
         _nonDivergentGsimKernel( false ),
         _vectorModeOn( false )
      { ; }

      ~SimulationOptions() {; }

      void printOptions() const;

      void fourValueLogic( bool v ) { _fourValueLogic = v; }
      bool fourValueLogic() const { return _fourValueLogic; }

      void ioOpPerCycle( bool v ) { _ioOpPerCycle = v; }
      bool ioOpPerCycle() const { return _ioOpPerCycle; }

      void logicSimMethod( LogicSimMethod t ) { _lsimMethod = t; }
      LogicSimMethod logicSimMethod() const { return _lsimMethod; }
      bool isEventDriven() const { return (_lsimMethod != IN_ORDER); }

      void simLocation( SimLocation sim_loc ) { _simLoc = sim_loc; }
      SimLocation simLocation() const { return _simLoc; }
      std::string simLocationStr() const {
         if( simLocation()==OCL_GPU ) { return "OCL_GPU"; }
         else if( simLocation()==OCL_CPU ) { return "OCL_CPU"; }
         else { return "HOST"; }
      }
      unsigned platformNum() const { return _pfmId; }
      unsigned deviceNum() const { return _devId; }
      int numParallelPsetsOnGpu() const { return _numParallelPsetsOnGpu; }
      void numParallelPsetsOnGpu( int n ) { _numParallelPsetsOnGpu = n; }

      bool nonDivergentGsimKernel() const { return _nonDivergentGsimKernel; }
      void nonDivergentGsimKernel(bool v) { _nonDivergentGsimKernel = v; }

      bool vectorModeOn() const { return _vectorModeOn; }
      void vectorModeOn( bool v ) { _vectorModeOn = v; }

      bool verifyAndSetOclPlatformDevice( SimLocation sim_loc,
                                          int pfm_id,
                                          int dev_id );

   private:

      bool _fourValueLogic; // use 4-value (0, 1, X, Z) logic
      bool _ioOpPerCycle;   // Apply force_pi and measure_po
                            //   per pattern cycle
      LogicSimMethod _lsimMethod; // logic simulation method/algorithm
      SimLocation _simLoc;  // location where simulation happens
      unsigned _pfmId;      // platform id of OpenCL
      unsigned _devId;      // device id of OpenCL
      int _numParallelPsetsOnGpu; // number of parallel pattern set on GPU
      bool _nonDivergentGsimKernel;
      bool _vectorModeOn;   // switch to turn on/off vector support of Ocl
};


class HcsTestSetting {
   public:
      HcsTestSetting();
      ~HcsTestSetting();

      OpenCLInfo & openCLInfo() { return _oclInfo; }

      Design & design() { return _design; }

      Evaluate & eval() { return _evaluate; }

      EvaluateCL & evalCl() { return _evaluateCL; }

      Random & randomGen() { return _randomGen; }

      RandomPatterns & randomPatterns() { return _randPatterns; }

      SimulationOptions & simOptions() { return _simOptions; }

      bool storePatterns() const { return _storePatterns; }
      void storePatterns(bool v) { _storePatterns = v; }

      PatternSource patternSource() const { return _patSource; }
      void patternSource( PatternSource src ) { _patSource = src; }

      PatternSet & externalPatternSet() { return _psetExternal; }
      PatternSet & internalPatternSet() { return _psetInternal; }

   protected:

   private:
      OpenCLInfo _oclInfo;          // OpenCL platform/device information
      Design     _design;           // Design under test
      Evaluate   _evaluate;         // Evaluate class
      //StaticLearning _staticlearning;
      EvaluateCL _evaluateCL;       // OpenCL based Evaluate class

      Random     _randomGen;        // Random number generator
      RandomPatterns _randPatterns; // Attributes for random patterns source
      SimulationOptions _simOptions; // Options for simulation

      PatternSource _patSource;

      bool _storePatterns;
      PatternSet _psetExternal; // External pattern set, used in read_patterns
      PatternSet _psetInternal; // Internal pattern set, used in random
                                //   patterns or ATPG

};

typedef Singleton< HcsTestSetting > htDB;  // global declaration of hcs-test DB

#endif
