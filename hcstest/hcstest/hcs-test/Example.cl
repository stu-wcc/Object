//////////////////////////////////////////////////////////////////////////
//  make sure there is no memory out of bound access 
//  otherwise it may cause CL_OUT_OF_RESOURCES in clEnqueue* function
//////////////////////////////////////////////////////////////////////////


__kernel void kernel_buf_trans( __global const int *buf_src,
                               __global int *buf_des,
                                const int buf_size )
{
   int gid = get_global_id(0);
   // int4 intColor = (int4)(0, 0, 0, 0);
   // intColor = (int4)(gid, gid, gid, gid);
   if( gid < buf_size ) {
      buf_des[gid] = buf_src[gid] / 4;
   }
}


__kernel void kernel_init_ckt( __write_only image2d_t desImg,
                               __global const int *buf_src,
                                const int buf_size,
                                const int2 image_size ) // size of image
{
   int gid = get_global_id(0);
   // int4 intColor = (int4)(0, 0, 0, 0);
   // intColor = (int4)(gid, gid, gid, gid);
   if( gid < buf_size ) {
      int4 pixel_color = (int4)(buf_src[gid], buf_src[gid], buf_src[gid], buf_src[gid]-1);
      int x = gid >> 4; // gid / 16;
      int y = gid & 0x0000000f; // gid % 16;
      write_imagei( desImg, (int2)(x, y), pixel_color);
   }
}


__kernel void kernel_read_ckt( __read_only image2d_t srcImg,
                               sampler_t sampler,
                               __global int *buf_des,
                               const int buf_size )
{
   int gid = get_global_id(0);
   if( gid < buf_size ) {
      int x = gid >> 4; // gid / 16;
      int y = gid & 0x0000000f; // gid % 16;
      int4 pixel_color = read_imagei( srcImg, sampler, (int2)(x, y) );
      buf_des[gid] = pixel_color.w; // has .x .y .z and .w
   }
}
     
