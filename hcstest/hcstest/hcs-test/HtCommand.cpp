/////////////////////////////////////////////////////////////////
//                HtCommand.cpp
/////////////////////////////////////////////////////////////////

#include "HtCommand.h"
#include "OpenCLInfo.h"
#include "HtSetting.h"
#include "IdealCircuit.h"
#include <strings.h>

extern int Level_Main(int argc, char** argv, TeeStream *ts_ptr, int detail);

//////////////////////////////////////////////////////////////////////
// Macro extension to define C function/procedure for a tcl command
// 1st argument is the name of C function
// 2nd argument is the corresponding command class
//////////////////////////////////////////////////////////////////////
DEFINE_TCL_CMD_PROC(        ReportOpenclCmdProc,         ReportOpenclCmd );
DEFINE_TCL_CMD_PROC(    CreateOclContextCmdProc,     CreateOclContextCmd );
DEFINE_TCL_CMD_PROC(   ReleaseOclContextCmdProc,    ReleaseOclContextCmd );
DEFINE_TCL_CMD_PROC(      CreateOclQueueCmdProc,       CreateOclQueueCmd );
DEFINE_TCL_CMD_PROC(     ReleaseOclQueueCmdProc,      ReleaseOclQueueCmd );
DEFINE_TCL_CMD_PROC(         ReadCircuitCmdProc,          ReadCircuitCmd );
DEFINE_TCL_CMD_PROC(        WriteCircuitCmdProc,         WriteCircuitCmd );
DEFINE_TCL_CMD_PROC(       DeleteCircuitCmdProc,        DeleteCircuitCmd );
DEFINE_TCL_CMD_PROC(        ReadPatternsCmdProc,         ReadPatternsCmd );
DEFINE_TCL_CMD_PROC(       WritePatternsCmdProc,        WritePatternsCmd );
DEFINE_TCL_CMD_PROC(      DeletePatternsCmdProc,       DeletePatternsCmd );
DEFINE_TCL_CMD_PROC(    SimulatePatternsCmdProc,     SimulatePatternsCmd );
DEFINE_TCL_CMD_PROC(   SetRandomPatternsCmdProc,    SetRandomPatternsCmd );
DEFINE_TCL_CMD_PROC( SetSimulatorOptionsCmdProc,  SetSimulatorOptionsCmd );
DEFINE_TCL_CMD_PROC(       SetOclOptionsCmdProc,        SetOclOptionsCmd );
DEFINE_TCL_CMD_PROC(    MakeIdealCircuitCmdProc,     MakeIdealCircuitCmd );
DEFINE_TCL_CMD_PROC(           AddClocksCmdProc,            AddClocksCmd );
DEFINE_TCL_CMD_PROC(        DeleteClocksCmdProc,         DeleteClocksCmd );
DEFINE_TCL_CMD_PROC(        ReportClocksCmdProc,         ReportClocksCmd );
DEFINE_TCL_CMD_PROC(    DoStaticLearningCmdProc,     DoStaticLearningCmd );


///////////////////////////////////////////////////////////////////////
// Central place to register all user-defined tcl commands
// newTclCommand() is a macro of Tcl_CreateObjCommand and sentences
//   to update command map in appCommonDB.
// For example,
//   Tcl_CreateObjCommand( interp, "help", tclCmdHelp,
//                       (ClientData)NULL, (Tcl_CmdDeleteProc *)NULL);
//   (appCommonDB::instance())->commandMap()["help"] = new HelpCmd;
///////////////////////////////////////////////////////////////////////
void registerHtTclCommands( Tcl_Interp* interp ) {

   newTclCommand( "report_opencl",
                   ReportOpenclCmd,
                   ReportOpenclCmdProc );
   newTclCommand( "create_ocl_context",
                   CreateOclContextCmd,
                   CreateOclContextCmdProc );
   newTclCommand( "release_ocl_context",
                   ReleaseOclContextCmd,
                   ReleaseOclContextCmdProc );
   newTclCommand( "create_ocl_queue",
                   CreateOclQueueCmd,
                   CreateOclQueueCmdProc );
   newTclCommand( "release_ocl_queue",
                   ReleaseOclQueueCmd,
                   ReleaseOclQueueCmdProc );
   newTclCommand( "read_circuit",
                   ReadCircuitCmd,
                   ReadCircuitCmdProc );
   newTclCommand( "write_circuit",
                   WriteCircuitCmd,
                   WriteCircuitCmdProc );
   newTclCommand( "delete_circuit",
                   DeleteCircuitCmd,
                   DeleteCircuitCmdProc );
   newTclCommand( "read_patterns",
                   ReadPatternsCmd,
                   ReadPatternsCmdProc );
   newTclCommand( "write_patterns",
                   WritePatternsCmd,
                   WritePatternsCmdProc );
   newTclCommand( "delete_patterns",
                   DeletePatternsCmd,
                   DeletePatternsCmdProc );
   newTclCommand( "simulate_patterns",
                   SimulatePatternsCmd,
                   SimulatePatternsCmdProc );
   newTclCommand( "set_random_patterns",
                   SetRandomPatternsCmd,
                   SetRandomPatternsCmdProc );
   newTclCommand( "set_simulator_options",
                   SetSimulatorOptionsCmd,
                   SetSimulatorOptionsCmdProc );
   newTclCommand( "set_ocl_options",
                   SetOclOptionsCmd,
                   SetOclOptionsCmdProc );
   newTclCommand( "make_ideal_circuit",
                   MakeIdealCircuitCmd,
                   MakeIdealCircuitCmdProc );
   newTclCommand( "add_clocks",
                   AddClocksCmd,
                   AddClocksCmdProc );
   newTclCommand( "delete_clocks",
                   DeleteClocksCmd,
                   DeleteClocksCmdProc );
   newTclCommand( "report_clocks",
                   ReportClocksCmd,
                   ReportClocksCmdProc );
   newTclCommand( "do_static_learning",
                   DoStaticLearningCmd,
                   DoStaticLearningCmdProc );


}




//Constructor for DoStaticLearningCmd
DoStaticLearningCmd::DoStaticLearningCmd() : Command() {
   _options.add_options()
      ("static_learning", po::bool_switch()->default_value(false),
                            "set the value of node to X")
   ;
}

// Destructor for DoStaticLearningCmd
DoStaticLearningCmd::~DoStaticLearningCmd() {
   ;
}

void DoStaticLearningCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: do static learning [--static|-s]\n";
   HLog.flush();
}

bool DoStaticLearningCmd::action() {
   
    return true;
}






// Constructor for ReadCircuitCmd
ReadCircuitCmd::ReadCircuitCmd() : Command() {
   _options.add_options()
      ("ckt", po::value<std::string>()->required(), "circuit file name")
      ("verbose,v", po::bool_switch()->default_value(false),
                      "print verbose information")
      ("optimize", po::bool_switch()->default_value(false),
                      "turn on netlist optimization")
      ("skip_buf_inv_opt", po::bool_switch()->default_value(false),
                      "skip buffer/inverter optimization")
      ("dump,d", po::bool_switch()->default_value(false),
                      "dump levelized circuit and pinname mapping")
      ("add_clocks", po::bool_switch()->default_value(false),
                      "add clocks for sequential circuit")
      ("clocks_edge,c", po::value<int>(), "clocks edge")
      ("ocl_test,o", po::bool_switch()->default_value(false),
                      "test circuit data for OpenCL kernels")
      // --level and --name are used to reduce loading time for large ckt
      ("level,l", po::value<std::string>(), "levelized circuit file")
      ("name,n", po::value<std::string>(), "pin name mapping file")
   ;
   _posOptions.add("ckt", 1);
}


// Destructor for ReadCircuitCmd
ReadCircuitCmd::~ReadCircuitCmd() {
   ;
}


void ReadCircuitCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: read_circuit <ckt_file> [--verbose|-v]\n"
        << "                               [--optimize]\n"
        << "                               [--skip_buf_inv_opt]\n"
        << "                               [--add_clocks|-a]\n"
        << "                               [--clocks_edge|-c <0(r)|1(f)>]\n"
        << "                               [--level|-l <lev_file>]\n"
        << "                               [--name|-n <name_file>]\n";
   HLog.flush();
}


bool ReadCircuitCmd::action() {
   std::string ckt_filename("");
   // Since 'ckt' option is required, we can check this directly
   ckt_filename = _variablesMap["ckt"].as<std::string>();

   // Check if any circuit is loaded already
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   if( cktOnHost.circuitLoaded() ) {
      HLog << InfoError
           << "circuit '" << cktOnHost.iscasCktFile() << "' is loaded. "
           << "Please run 'delete_circuit' first."
           << std::endl;
      return false;
   }

   // Check if circuit exists
   if( !fileExists( ckt_filename.c_str() ) ) {
      HLog << InfoError
           << "circuit file '" << ckt_filename << "' does not exist"
           << std::endl;
      return false;
   }

   // Check file extension
   std::string extension = getFileExtension( ckt_filename );
   if( "bench" != extension ) {
      HLog << InfoError
           << " only ISCAS bench circuit format is supported"
           << std::endl;
      return false;
   }

   // check if --dump and --ocl_test are specified together
   bool dump = _variablesMap["dump"].as<bool>();
   bool ocl_test = _variablesMap["ocl_test"].as<bool>();
   if( !dump && ocl_test ) {
      HLog << InfoError
           << "option '--dump' has to be specified together with '--ocl_test'"
           << std::endl;
      return false;
   }
   //check if --clock_edges and --add_clock are specified together
   int clocks_edge = 0;
   bool add_clocks = _variablesMap["add_clocks"].as<bool>();
   if( _variablesMap.count("clocks_edge") ){
       clocks_edge = _variablesMap["clocks_edge"].as<int>();
      if( !add_clocks && clocks_edge ) {
         HLog << InfoError
              << "option '--add_clocks' has to be specified with '--clocks_edge'"
              << std::endl;
         return false;
      }
      if( 0!=clocks_edge && 1!=clocks_edge ) {
         HLog << InfoError
              << "only clocks_edge 0 and 1 are supported"
              << std::endl;
         return false;
      }
   }
   // Check if --level and --name are provided
   bool read_lev_and_name_file = false;
   std::string lev_file("");
   std::string pinname_file("");
   int both_val=0;
   if( _variablesMap.count("level")) {
      lev_file = _variablesMap["level"].as<std::string>();
      both_val += 1;
   }
   if( _variablesMap.count("name")) {
      pinname_file = _variablesMap["name"].as<std::string>();
      both_val += 1;
   }
   if( 1==both_val ) {
      HLog << InfoError
           << "option '--level' and '--name' must be used together"
           << std::endl;
      return false;
   } else if( 2==both_val ) {
      read_lev_and_name_file = true;
   }
   // Fake level_argc and level_argv
   int level_argc = 2;
   char* level_argv[2];
   std::string buf1("level");
   std::string buf2( ckt_filename );
   // Replace ".bench" with NULL
   buf2.replace( buf2.find_last_of("."), 6, "");
   level_argv[0] = &(buf1[0]);
   level_argv[1] = &(buf2[0]);
   TeeStream * ts_ptr = &((appCommonDB::instance())->teeStream());
   bool verbose = _variablesMap["verbose"].as<bool>();

   // get start time
   std::chrono::system_clock::time_point
      start_t = std::chrono::system_clock::now();
   std::clock_t start_cpu_t = std::clock();

   if( read_lev_and_name_file ) {
      HLog << InfoNote
           << "read level file '" << lev_file << "'"
           << " and name file '" << pinname_file << "' directly"
           << std::endl;
   } else {
      // skip this when reading from level/name files directly
      if( Level_Main( level_argc, level_argv, ts_ptr, verbose?1:0 )) {
         return false;
      }
   }

   cktOnHost.iscasCktFile( ckt_filename );

   // Read in levelized circuit file <ckt>.lev
   if( !read_lev_and_name_file ) {
      lev_file = buf2 + ".lev";
   }
   if( false==cktOnHost.levelizedCktFile( lev_file )) {
      return false;
   }
   bool status = cktOnHost.initialCkt().readLevelizedCircuit( lev_file );
   cktOnHost.circuitLoaded( status );
   bool optimize = _variablesMap["optimize"].as<bool>();
   bool skip_buf_inv_opt = _variablesMap["skip_buf_inv_opt"].as<bool>();
   if( status) {
      if( optimize ) {
         cktOnHost.optimizeAndRemodelNetlist( skip_buf_inv_opt,
                                              /*print_debug_info=*/ false );
      } else if( cktOnHost.initialCkt().isHitecLevelConvention() ) {
         cktOnHost.modifyHitecLevelNetlist();
      }
   } else {
      cktOnHost.initialCkt().clear();
      return false;
   }

   // Read in pinname mapping file <ckt>.name
   if( !read_lev_and_name_file ) {
      pinname_file = buf2 + ".name";
   }
   cktOnHost.pinnameMappingFile( pinname_file );
   status = cktOnHost.initialCkt().readPinnameMapping( pinname_file );
   if( false==status) {
       cktOnHost.initialCkt().clear();
       return false;
   }

   // get end time
   std::chrono::system_clock::time_point
      end_t = std::chrono::system_clock::now();
   std::clock_t end_cpu_t = std::clock();

   if( dump ) {
      std::string lev_file_dump = lev_file + ".dump";
      std::string pinname_file_dump = pinname_file + ".dump";
      cktOnHost.initialCkt().dumpLevelizedCircuit( lev_file_dump, ocl_test );
      cktOnHost.initialCkt().dumpPinnameMapping( pinname_file_dump );
   }
   if( add_clocks ) {
      if( !cktOnHost.initialCkt().numDEs() ) {
         HLog << InfoError
              << "circuit has no delay elements."
              << std::endl;
         return false;
      }
      std::string addclocks_file_dump = buf2 +".add_clocks.lev";
      status = cktOnHost.initialCkt().dumpClockedCircuit
         ( addclocks_file_dump, clocks_edge );
      if( false == status ){
         return false;
      }
      HLog << InfoNote
           << "file '" << addclocks_file_dump << "' is saved"
           << std::endl;
   }

   HLog << InfoNote
        << "circuit '" << cktOnHost.iscasCktFile()
        << "' successfully loaded"
        << std::endl;
   HLog << InfoCont
        << "elapsed Wall time(seconds): "
        << elapsedSysTimeInSeconds(start_t, end_t)
        << std::endl
        << InfoCont
        << "elapsed CPU time(seconds): "
        << elapsedCpuTimeInSeconds(start_cpu_t, end_cpu_t)
        << std::endl;

   cktOnHost.printCktStatistics( verbose );
   ((htDB::instance())->eval()).bindToCkt( cktOnHost.simNetlist() );
   return true;
}


// Constructor for WriteCircuitCmd
WriteCircuitCmd::WriteCircuitCmd() : Command() {
   _options.add_options()
      ("ckt", po::value<std::string>()->required(), "circuit file name")
      ("format,f", po::value<std::string>(), "circuit format")
      ("process,p", po::value<std::string>(), "process levelized circuit")
      ("level_rebalance,l", po::value<int>(),
                      "rebalance level size w.r.t alignment")
      ("multiply,m", po::value<int>(),
                      "number of circuit duplications")
      ("replace,r", po::bool_switch()->default_value(false),
                      "replace existing file")
   ;
   _posOptions.add("ckt", 1);
}


// Destructor for WriteCircuitCmd
WriteCircuitCmd::~WriteCircuitCmd() {
   ;
}


void WriteCircuitCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: write_circuit <ckt_file> [--format|-f <BENCH|lev|vlog>]\n"
        << "                                [--process|-p <GROUP|industry>]\n"
        << "                                [--level_balance|-l <align>\n"
        << "                                [--multiply|-m <num>]\n"
        << "                                [--replace|-r]\n";
   HLog.flush();
}


bool WriteCircuitCmd::action() {

   typedef enum {
      BENCH,
      LEV,
      VLOG
   } CircuitFormat;
   CircuitFormat format = BENCH;
   if( _variablesMap.count("format")) {
      std::string str_cmp = _variablesMap["format"].as<std::string>();
      if( str_cmp.compare("bench")==0 ) {
         format = BENCH;
      } else if( str_cmp.compare("lev")==0 ) {
         format = LEV;
      } else if( str_cmp.compare("vlog")==0 ) {
         format = VLOG;
      } else {
         HLog << InfoError
              << "unsupported circuit format '" << str_cmp << "'"
              << std::endl;
         return false;
      }
   }

   typedef enum {
      GROUP,
      INDUSTRY
   } ProcessCkt;
   ProcessCkt process = GROUP;
   if( _variablesMap.count("process")) {
      std::string str_cmp = _variablesMap["process"].as<std::string>();
      if( str_cmp.compare("group")==0 ) {
         process = GROUP;
      } else if( str_cmp.compare("industry")==0 ) {
         process = INDUSTRY;
      } else {
         HLog << InfoError
              << "unsupported levelized circuit process option '"
              << str_cmp << "'"
              << std::endl;
         return false;
      }
   }

   int lev_align = -1;
   if( _variablesMap.count("level_rebalance")) {
      lev_align = _variablesMap["level_rebalance"].as<int>();
      if( lev_align <= 0 ) {
         HLog << InfoError
              << "alignment of level rebalance must be greater than 0"
              << std::endl;
         return false;
      }
   }

   bool replace = _variablesMap["replace"].as<bool>();
   std::string wri_filename("");
   // Since 'ckt' option is required,we can check this directly
   wri_filename = _variablesMap["ckt"].as<std::string>();

   // Check if any circuit is loaded already
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   if( !cktOnHost.circuitLoaded()) {
      HLog << InfoError
           << "circuit is not loaded. "
           << "Please run 'read_circuit' first."
           << std::endl;
      return false;
   }

   // Check file extension
   std::string extension = getFileExtension( wri_filename );
   if( (BENCH==format && "bench" != extension) ||
       (  LEV==format &&   "lev" != extension) ||
       ( VLOG==format &&     "v" != extension) ) {
      HLog << InfoError
           << " please use file extension 'bench', 'lev' or 'v' that "
           << "matches its format"
           << std::endl;
      return false;
   }

   // Check if --replace is needed
   if( fileExists( wri_filename.c_str() )&& !replace ) {
      HLog << InfoError
           << "circuit file '" << wri_filename
           << "' exist. Please use '--replace' to overwrite"
           << std::endl;
      return false;
   }

   // Verify the number of --multiple
   int n_mul = 1;
   if( _variablesMap.count("multiply") ) {
      n_mul = _variablesMap["multiply"].as<int>();
      if( n_mul < 1) {
         HLog << InfoError
              << "number of multify must be greater than or equal to 1"
              << std::endl;
         return false;
      }
      if( VLOG == format ) {
         HLog << InfoError
              << "VLOG format is not supported for circuit multiplication"
              << std::endl;
         return false;
      }
      if( _variablesMap.count("process") ||
          _variablesMap.count("level_rebalance")) {
         HLog << InfoError
              << "switch --multiply cannot be used together with --process "
              << "or --level_rebalance"
              << std::endl;
         return false;
      }
   }

   // Write out circuit
   std::ofstream ofs( wri_filename );
   if( !ofs.is_open() ) {
      HLog << InfoError
           << "failed to open file '" << wri_filename << "' to write"
           << std::endl;
      return false;
   }

   if( _variablesMap.count("multiply") ) {
      if( BENCH==format ) {
         cktOnHost.initialCkt().dumpMultipliedBenchCircuit( ofs, n_mul );
      } else if( LEV==format ) {
         cktOnHost.initialCkt().dumpMultipliedLevelCircuit( ofs, n_mul );
      }
   } else if( LEV==format ) {
      if( _variablesMap.count("process")) {
         if( GROUP == process ) {
            cktOnHost.initialCkt().dumpLevelizedCircuitWithGroupedTypes( ofs);
         } else if( INDUSTRY == process ) {
            //cktOnHost.levelizeFromInputsAndFfs( ofs );
            cktOnHost.initialCkt().processAlienLevelNetlist( ofs );
         }
      }
      if( _variablesMap.count("level_rebalance")) {
         cktOnHost.initialCkt().dumpRebalancedLevelNetlist( ofs, lev_align);
      }
   } else if( VLOG==format ) {
      cktOnHost.initialCkt().dumpFscanVerilogNetlist( ofs, wri_filename );
   }
   ofs.close();
   return true;
}


// Constructor for DeleteCircuitCmd
DeleteCircuitCmd::DeleteCircuitCmd() : Command() {
   ;
}


// Destructor for DeleteCircuitCmd
DeleteCircuitCmd::~DeleteCircuitCmd() {
   ;
}


void DeleteCircuitCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: delete_circuit\n";
   HLog.flush();
}


bool DeleteCircuitCmd::action() {
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   cktOnHost.clear();
   if(!((htDB::instance())->externalPatternSet().isEmpty() &&
        (htDB::instance())->internalPatternSet().isEmpty()) ) {
      HLog << InfoWarn
           << "patterns associated with circuit are deleted as well"
           << std::endl;
      (htDB::instance())->externalPatternSet().clearPatterns();
      (htDB::instance())->internalPatternSet().clearPatterns();
   }
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   ocl_info.releaseOclResources(true); // for_delete_ckt: true
   return true;
}


// Constructor for ReportOpenclCmd
ReportOpenclCmd::ReportOpenclCmd() : Command() {
   _options.add_options()
      ("platform,p", po::value<int>(), "specify OpenCL platform id")
      ("device,d", po::value<int>(), "specify OpenCL device id")
      ("verbose,v", po::bool_switch()->default_value(false),
                      "print verbose information");
}


// Destructor for ReportOpenclCmd
ReportOpenclCmd::~ReportOpenclCmd() {
   ;
}


void ReportOpenclCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: report_opencl [--platform|-p  <platform_id> ]\n";
   HLog << "                     [--device|-d    <device_id> ]\n";
   HLog << "                     [--verbose|-v ]\n";

   HLog.flush();
}


bool ReportOpenclCmd::action() {
   bool verbose = _variablesMap["verbose"].as<bool>();
   bool summary = !verbose;
   int pfm_id = -1; // stand for platform id; -1:unspecified
   int dev_id = -1; // stand for device id; -1:unspecified

   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   std::string buf("");

   // Get platform id
   if( _variablesMap.count("platform")) {
      pfm_id = _variablesMap["platform"].as<int>();
      if( !ocl_info.checkPlatformIdRange( pfm_id ) ) {
         return false;
      }
   }
   // Get device id
   if( _variablesMap.count("device")) {
      dev_id = _variablesMap["device"].as<int>();
   }

   if( pfm_id >= 0 ) { // platform id is is valid
      if( dev_id >= 0 ) { // device id is specified
         if( !ocl_info.platform(cl_uint(pfm_id)).checkDeviceIdRange(dev_id)) {
            return false;
         } else {
            //ocl_info.platform( pfm_id ).device( dev_id ).display( summary );
            OpenCLDevice& dev = ocl_info.platform( pfm_id ).device( dev_id );
            dev.printProperties( summary );
            return true;
         }
      } else { // device id not specified; print all device info
         //ocl_info.platform( pfm_id ).display( summary );
         ocl_info.platform( pfm_id ).printProperties( summary );
         return true;
      }
   } else { // platform id not specified
     if( dev_id >= 0 ) { // device id specified
         HLog << InfoError
              << "a platform id must be specified with device id"
              << std::endl;
         return false;
      }
   }

   // print all platforms and all devices
   //ocl_info.display( summary );
   ocl_info.printProperties( summary );
   HLog.flush();
   return true;
}


// Constructor for CreateOclContextCmd
CreateOclContextCmd::CreateOclContextCmd() : Command() {
   _options.add_options()
      ("platform,p", po::value<int>()->required(), "specify OpenCL platform id")
   ;
}


// Destructor for CreateOclContextCmd
CreateOclContextCmd::~CreateOclContextCmd() {
   ;
}


void CreateOclContextCmd::help() const {
   HLog << "Usage: create_ocl_context --platform|-p <platform_id>\n";
   HLog.flush();
}


bool CreateOclContextCmd::action() {
   // Get platform id directly as it is required.
   int pfm_id = _variablesMap["platform"].as<int>();
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   if( ocl_info.createContext( pfm_id ) ) {
      // create and build program upon context creation
      EvaluateCL& eval_cl = (htDB::instance())->evalCl();
      if( !eval_cl.createAndBuildProgramOnPfm( pfm_id, "Evaluate.cl") ) {
         return false;
      }
   } else {
      return false;
   }
   return true;
}


// Constructor for ReleaseOclContextCmd
ReleaseOclContextCmd::ReleaseOclContextCmd() : Command() {
   _options.add_options()
      ("platform,p", po::value<int>()->required(), "specify platform id")
   ;
}


// Destructor for ReleaseOclContextCmd
ReleaseOclContextCmd::~ReleaseOclContextCmd() {
   ;
}


void ReleaseOclContextCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: release_ocl_context --platform|-p <platform_id> \n";
   HLog.flush();
}


bool ReleaseOclContextCmd::action() {
   // Get platform id directly as it is required.
   int pfm_id = _variablesMap["platform"].as<int>();
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   std::string buf("");
   if( !ocl_info.releaseContext( pfm_id ) ) {
      return false;
   }
   return true;
}


// Constructor for CreateOclQueueCmd
CreateOclQueueCmd::CreateOclQueueCmd() : Command() {
   _options.add_options()
      ("platform,p", po::value<int>()->required(), "specify platform id")
      ("device,d", po::value<int>()->required(), "specify device id")
   ;
}


// Destructor for CreateOclQueueCmd
CreateOclQueueCmd::~CreateOclQueueCmd() {
   ;
}


void CreateOclQueueCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: create_ocl_queue --platform|-p <platform_id>\n";
   HLog << "                        --device|-d   <device_id>\n";
   HLog.flush();
}


bool CreateOclQueueCmd::action() {
   // Get pfm_id and dev_id directly as they are required.
   int pfm_id = _variablesMap["platform"].as<int>();
   int dev_id = _variablesMap["device"].as<int>();
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   // use property CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE if needed
   cl_command_queue_properties properties = 0;
   // properties = CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE;
   return ocl_info.createCmdQueue( pfm_id, dev_id, properties );
}


// Constructor for ReleaseOclQueueCmd
ReleaseOclQueueCmd::ReleaseOclQueueCmd() : Command() {
   _options.add_options()
      ("platform,p", po::value<int>()->required(), "specify platform id")
      ("device,d", po::value<int>()->required(), "specify device id")
   ;
}


// Destructor for ReleaseOclQueueCmd
ReleaseOclQueueCmd::~ReleaseOclQueueCmd() {
   ;
}


void ReleaseOclQueueCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: release_ocl_queue  --platform|-p <platform_id> \n";
   HLog << "                          --device|-d   <device_id> \n";
   HLog.flush();
}


bool ReleaseOclQueueCmd::action() {
   int pfm_id=-1, dev_id=-1;
   pfm_id = _variablesMap["platform"].as<int>();
   dev_id = _variablesMap["device"].as<int>();
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   //if( _variablesMap.count("platform")) {
   //   pfm_id = _variablesMap["platform"].as<int>();
   //   if( !ocl_info.checkPlatformIdRange( pfm_id )) {
   //      return false;
   //   }
   //}
   //int dev_id=-1;
   //if( _variablesMap.count("device")) {
   //   dev_id = _variablesMap["device"].as<int>();
   //}
   bool res = ocl_info.releaseCmdQueue( pfm_id, dev_id );
   return res;
}


// Constructor for ReadPatternsCmd
ReadPatternsCmd::ReadPatternsCmd() : Command() {
   _options.add_options()
      ("file", po::value<std::string>()->required(), "pattern file name")
      ("append,a", po::bool_switch()->default_value(false),
                      "append pattern file to existing patterns")
;
   _posOptions.add("file", 1);
}


// Destructor for ReadPatternsCmd
ReadPatternsCmd::~ReadPatternsCmd() {
   ;
}


void ReadPatternsCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: read_patterns <pat_file>\n";
   //HLog << "Usage: read_patterns <pat_file> [--append|-a]\n";
   HLog.flush();
}


bool ReadPatternsCmd::action() {
   // Since 'file' option is required, we can check this directly
   std::string pat_filename = _variablesMap["file"].as<std::string>();

   // Check if circuit is loaded
   if( !((htDB::instance())->design()).cktOnHost().circuitLoaded() ) {
      HLog << InfoError
           << "please 'read_circuit' first"
           << std::endl;
      return false;
   }

   // Check if pattern file exists
   if( !fileExists( pat_filename.c_str() ) ) {
      HLog << InfoError
           << "Pattern file '" << pat_filename << "' does not exist"
           << std::endl;
      return false;
   }
   // check 'append' switch
   PatternSet & pset = (htDB::instance())->externalPatternSet();
   bool append = _variablesMap["append"].as<bool>();
   if( append ) {
      HLog << InfoError
           << "option '--append' is not supported"
           << std::endl;
      return false;
   } else { // Overwrite if not append
      if( !pset.isEmpty() ) {
         HLog << InfoWarn
              << "existing patterns (external) are deleted"
              << std::endl;
         pset.clearPatterns();
      }
   }

   if( !pset.readPatterns( pat_filename ) ) {
      return false;
   }

   return true;
}


// Constructor for WritePatternsCmd
WritePatternsCmd::WritePatternsCmd() : Command() {
   _options.add_options()
      ("file", po::value<std::string>()->required(), "pattern file name")
      ("external,e", po::bool_switch()->default_value(false),
                      "dump out external patterns to file")
      ("replace,r", po::bool_switch()->default_value(false),
                      "replace existing file")
;
   _posOptions.add("file", 1);
}


// Destructor for WritePatternsCmd
WritePatternsCmd::~WritePatternsCmd() {
   ;
}


void WritePatternsCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: write_patterns <pat_file> [--external|-e] [--replace|-r]\n";
   //HLog << "Usage: read_patterns <pat_file> [--append|-a]\n";
   HLog.flush();
}


bool WritePatternsCmd::action() {
   // Since 'file' option is required, we can check this directly
   std::string pat_filename = _variablesMap["file"].as<std::string>();
   bool replace = _variablesMap["replace"].as<bool>();
   bool external = _variablesMap["external"].as<bool>();

   // Check if pattern set is empty
   bool empty_pset = false;
   if( external ) {
      empty_pset = (htDB::instance())->externalPatternSet().isEmpty();
   } else {
      empty_pset = (htDB::instance())->internalPatternSet().isEmpty();
   }
   if( empty_pset ) {
      HLog << InfoError
           << (external ? "external" : "internal") << " pattern set is empty."
           << " No pattern to write."
           << std::endl;
      return false;
   }

   // Check if pattern file exists
   if( fileExists( pat_filename.c_str() ) && !replace ) {
      HLog << InfoError
           << "file '" << pat_filename
           << "' exists. Please use '--replace' to overwrite"
           << std::endl;
      return false;
   }

   // Write patterns to file
   std::ofstream ofs( pat_filename );
   if( !ofs.is_open() ) {
      HLog << InfoError
           << "failed to open file '" << pat_filename << "' to write"
           << std::endl;
      return false;
   }
   if( external ) {
      (htDB::instance())->externalPatternSet().writePatterns( ofs );
   } else {
      (htDB::instance())->internalPatternSet().writePatterns( ofs );
   }
   ofs.close();
   return true;
}


// Constructor for DeletePatternsCmd
DeletePatternsCmd::DeletePatternsCmd() : Command() {
   _options.add_options()
      ("external,e", po::bool_switch()->default_value(false),
                      "delete external patterns")
      ("internal,i", po::bool_switch()->default_value(false),
                      "delete internal patterns")
   ;
}


// Destructor for DeletePatternsCmd
DeletePatternsCmd::~DeletePatternsCmd() {
   ;
}


void DeletePatternsCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: delete_patterns [--external|-e] [--internal|-i]\n";
   HLog.flush();
}


bool DeletePatternsCmd::action() {
   bool internal = _variablesMap["internal"].as<bool>();
   bool external = _variablesMap["external"].as<bool>();

   if( (!internal && !external) || (internal && external)) {
      // None or both switches are specified
      HLog << InfoNote
           << "both internal and external patterns are cleared"
           << std::endl;
      (htDB::instance())->externalPatternSet().clearPatterns();
      (htDB::instance())->internalPatternSet().clearPatterns();
   } else if( internal ) {
      HLog << InfoNote
           << "internal patterns are cleared"
           << std::endl;
      (htDB::instance())->internalPatternSet().clearPatterns();
   } else {
      HLog << InfoNote
           << "external patterns are cleared"
           << std::endl;
      (htDB::instance())->externalPatternSet().clearPatterns();
   }
   return true;
}


// Constructor for SimulatePatternsCmd
SimulatePatternsCmd::SimulatePatternsCmd() : Command() {
   _options.add_options()
      ("from,f", po::value<std::string>(), "pattern source")
      ("store,s", po::bool_switch()->default_value(false),
                      "store patterns in internal or external pattern set")
   ;
}


// Destructor for SimulatePatternsCmd
SimulatePatternsCmd::~SimulatePatternsCmd() {
   ;
}


void SimulatePatternsCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: simulate_patterns [--from|-f <EXTERNAL|random> ]\n"
        << "                         [--store|-s]\n";
   HLog.flush();
}


bool SimulatePatternsCmd::action() {

   // Check if circuit is loaded
   if( !((htDB::instance())->design()).cktOnHost().circuitLoaded() ) {
      HLog << InfoError
           << "please 'read_circuit' first"
           << std::endl;
      return false;
   }
   // Check if clocks are added
   if((htDB::instance())->design().simNetlist().clockedDffIsUsed() &&
      !(htDB::instance())->design().simNetlist().numClockPIs()) {
      HLog << InfoError
           << "clocked D flip-floped is used. Please 'add_clocks' first"
           << std::endl;
      return false;
   }

   // Check validity of pattern source
   PatternSource pat_src = PAT_SRC_EXTERNAL;
   if( _variablesMap.count("from")) {
      std::string src_str = _variablesMap["from"].as<std::string>();
      if( src_str.compare("external")==0 ) {
         pat_src = PAT_SRC_EXTERNAL;
      } else if( src_str.compare("random")==0 ) {
         pat_src = PAT_SRC_RANDOM;
      } else {
         HLog << InfoError
              << "unsupported pattern source '" << src_str << "'"
              << std::endl;
         return false;
      }
   }

   // Action for external patterns
   if( PAT_SRC_EXTERNAL == pat_src ) {
      if( (htDB::instance())->externalPatternSet().isEmpty() ) {
         HLog << InfoError
              << "no external patterns are loaded"
              << std::endl;
         return false;
      }
   }

   // Action for random patterns
   if( PAT_SRC_RANDOM == pat_src ) {
      if( ((htDB::instance())->randomPatterns()).number() == 0 ) {
         HLog << InfoError
              << "please set_random_patterns to greater than 0"
              << std::endl;
         return false;
      }
   }

   (htDB::instance())->patternSource( pat_src );
   // Update parameter for --store per command
   bool store = _variablesMap["store"].as<bool>();
   (htDB::instance())->storePatterns( store );

   bool external = ( (htDB::instance())->patternSource()==PAT_SRC_EXTERNAL);
   bool res = ((htDB::instance())->eval()).simulatePatterns(external,store);

   return res;
}


// Constructor for SetRandomPatternsCmd
SetRandomPatternsCmd::SetRandomPatternsCmd() : Command() {
   _options.add_options()
      ("num", po::value<int>()->required(), "number of random patterns")
      ("cycles,c", po::value<int>(), "number of cycles")
      ("seed,s", po::value<int>(), "seed used for random pattern generation")
   ;
   _posOptions.add("num", 1);
}


// Destructor for SetRandomPatternsCmd
SetRandomPatternsCmd::~SetRandomPatternsCmd() {
   ;
}


void SetRandomPatternsCmd::help() const {
   //HLog << _options << "\n";
   // Seed of 0 means that seed is changed per call.
   HLog << "Usage: set_random_patterns <number> "
        << "[--cycles|-c <num>] [--seed|-s <num>]\n";
   HLog.flush();
}


bool SetRandomPatternsCmd::action() {
   int n_rand = _variablesMap["num"].as<int>();
   int n_cycles = ((htDB::instance())->randomPatterns()).sequentialDepth();
   int seed = ((htDB::instance())->randomPatterns()).seed();

   if( n_rand < 0 ) {
      HLog << InfoError
           << "number of random patterns must be greater than or equal to 0"
           << std::endl;
      return false;
   }

   if( _variablesMap.count("cycles")) {
      n_cycles = _variablesMap["cycles"].as<int>();
      if( n_cycles < 1 ) {
         HLog << InfoError
              << "number of cycles must be greater than or equal to 1"
              << std::endl;
         return false;
      }
   }

   if( _variablesMap.count("seed")) {
      seed = _variablesMap["seed"].as<int>();
      if( seed < 0 ) {
         HLog << InfoError
              << "seed of random pattern must be greater than or equal to 0"
              << std::endl;
         return false;
      }
   }

   ((htDB::instance())->randomPatterns()).number( n_rand );
   ((htDB::instance())->randomPatterns()).sequentialDepth( n_cycles );
   ((htDB::instance())->randomPatterns()).seed( seed );

   return true;
}


// Constructor for AddClocksCmd
AddClocksCmd::AddClocksCmd() : Command() {
   _options.add_options()
      ("off_state", po::value<int>()->required(), "off-state value of clock")
      ("pin,p", po::value<std::vector<std::string> >()->multitoken(),
         "list of pin names for clock")
      ("id,i", po::value<std::vector<int> >()->multitoken(),
         "list of gate ids for clock") // gate id start with 1
   ;
   _posOptions.add("off_state", 1);
}


// Destructor for AddClocksCmd
AddClocksCmd::~AddClocksCmd() {
   ;
}


void AddClocksCmd::help() const {
   //HLog << _options << "\n";
   // Even though command grammar allows multiple pin names or gate ids,
   //   it is not supported for now. That is why it is disguised in
   //   help message.
   HLog << "Usage: add_clocks <off_state> "
        << "[--id|-i <gateid>]\n";
   //     << "[--pin|-p <pinname>] [--id|-i <gateid>]\n";
   HLog.flush();
}


bool AddClocksCmd::action() {
   // Check if any circuit is loaded already
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   if( !cktOnHost.circuitLoaded() ) {
      HLog << InfoError
           << "Please run 'read_circuit' first."
           << std::endl;
      return false;
   }
   if( cktOnHost.initialCkt().isCombinationalCkt() ) {
      HLog << InfoError
           << "Clock cannot be added in combinational circuit."
           << std::endl;
      return false;
   }
   if( _variablesMap.count("pin") && _variablesMap.count("id") ) {
      HLog << InfoError
           << "options '--pin' and '--id' cannot be simultaneously"
           << std::endl;
      return false;
   }
   if( !(_variablesMap.count("pin") || _variablesMap.count("id")) ) {
      HLog << InfoError
           << "either '--pin' or '--id' must be specified"
           << std::endl;
      return false;
   }
   if( _variablesMap.count("pin") ) {
      HLog << InfoError
           << "'--pin' is not supported for now"
           << std::endl;
      return false;
   }
   int off_state = _variablesMap["off_state"].as<int>();
   std::vector<int> clk_ids;
   std::vector<std::string> clk_pins;
   if( 0!=off_state && 1!=off_state ) {
      HLog << InfoError
           << "only off_state 0 and 1 are supported"
           << std::endl;
      return false;
   }

   if( _variablesMap.count("id") ) {
      clk_ids = _variablesMap["id"].as<std::vector<int> >();
      if( clk_ids.size() > 1 ) {
         HLog << InfoError
              << "multiple clocks are not supported for now"
              << std::endl;
         return false;
      }
   }

   bool status = cktOnHost.addClocks( clk_ids, off_state);
   return status;
}


// Constructor for DeleteClocksCmd
DeleteClocksCmd::DeleteClocksCmd() : Command() {
   _options.add_options()
   ;
}


// Destructor for DeleteClocksCmd
DeleteClocksCmd::~DeleteClocksCmd() {
   ;
}


void DeleteClocksCmd::help() const {
   HLog << "Usage: delete_clocks "
        << std::endl;
}


bool DeleteClocksCmd::action() {
   // Check if any circuit is loaded already
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   if( !cktOnHost.circuitLoaded() ) {
      HLog << InfoError
           << "Please run 'read_circuit' first."
           << std::endl;
      return false;
   }
   return cktOnHost.deleteClocks();
}


// Constructor for ReportClocksCmd
ReportClocksCmd::ReportClocksCmd() : Command() {
   _options.add_options()
   ;
}


// Destructor for ReportClocksCmd
ReportClocksCmd::~ReportClocksCmd() {
   ;
}


void ReportClocksCmd::help() const {
   HLog << "Usage: report_clocks "
        << std::endl;
}


bool ReportClocksCmd::action() {
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   if( !cktOnHost.circuitLoaded() ) {
      HLog << InfoError
           << "Please run 'read_circuit' first."
           << std::endl;
      return false;
   }
   int numClocks = cktOnHost.initialCkt().numClocks();
   assert( numClocks>=0 );
   HLog << TwoSpace
        << "Number of Clock:" << numClocks << std::endl;
   std::map<int,int> clockIdAndOffstates =
      cktOnHost.initialCkt().clockIdAndOffstates();
   std::map<int,int>::const_iterator it;
   for( it=clockIdAndOffstates.begin(); it!=clockIdAndOffstates.end(); it++){
      HLog << TwoSpace
           << "Clock ID:"
           << it ->first +1
           << "    "
           << "off_state:"
           << it->second
           << std::endl;
   }
   return true;
}


// Constructor for SetOclOptionsCmd
SetOclOptionsCmd::SetOclOptionsCmd() : Command() {
   _options.add_options()
      ("platform,p", po::value<int>(), "specify platform id")
      ("device,d", po::value<int>(), "specify device id")
      ("global_work_size,g", po::value<std::vector<int> >()->multitoken(),
         "set max global work size")
      ("local_work_size,l", po::value<std::vector<int> >()->multitoken(),
         "set desired local work size or work group size")
      ("ckt_mem,c", po::value<std::string>(),
         "specify cl_mem type for storing circuit on ocl device")
   ;
}


// Destructor for SetOclOptionsCmd
SetOclOptionsCmd::~SetOclOptionsCmd() {
   ;
}


void SetOclOptionsCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: set_ocl_options [--platform|-p  <platform_id> ]\n"
        << "                       [--device|-d    <device_id> ]\n"
        << "                       [--global_work_sizes|-g <n0> <n1> <n2> ]\n"
        << "                       [--local_work_sizes|-l <n0> <n1> <n2> ]\n"
        << "                       [--ckt_mem|-c <IMAGE|buffer> ]\n";
   HLog.flush();
}


bool SetOclOptionsCmd::action() {
   int pfm_id = -1; // stand for platform id; -1:unspecified
   int dev_id = -1; // stand for device id; -1:unspecified
   std::vector<int> gw_size(3, -1); // stand for global_work_size
   std::vector<int> lw_size(3, -1); // stand for local_work_size
   CktClMemType ckt_memtype = CKT_IMAGE;
   bool res = false;

   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   std::string buf("");

   // Get and verify platform id
   if( _variablesMap.count("platform")) {
      pfm_id = _variablesMap["platform"].as<int>();
      if( !ocl_info.checkPlatformIdRange( pfm_id ) ) {
         return false;
      }
   }
   // Get device id
   if( _variablesMap.count("device")) {
      dev_id = _variablesMap["device"].as<int>();
   }
   // Get global work size and local work size
   int count_gw_lw_size_switches = 0;
   if( _variablesMap.count("global_work_size")) {
      count_gw_lw_size_switches += 1;
      gw_size = _variablesMap["global_work_size"].as<std::vector<int> >();
      if( 3 != gw_size.size()) {
         HLog << InfoError
              << " option --global_work_sizes must be followed by 3 integers"
              << std::endl;
         return false;
      }
   }
   if( _variablesMap.count("local_work_size")) {
      count_gw_lw_size_switches += 1;
      lw_size = _variablesMap["local_work_size"].as<std::vector<int> >();
      if( 3 != lw_size.size()) {
         HLog << InfoError
              << " option --local_work_sizes must be followed by 3 integers"
              << std::endl;
         return false;
      }
   }
   if( 1==count_gw_lw_size_switches ) {
      HLog << InfoError
           << "global_work_sizes and local_work_sizes must be specified"
           << " together"
           << std::endl;
      return false;
   }
   if( _variablesMap.count("ckt_mem")) {
      std::string mem_str = _variablesMap["ckt_mem"].as<std::string>();
      if( mem_str.compare("image")==0 ) {
         ckt_memtype = CKT_IMAGE;
      } else if( mem_str.compare("buffer")==0 ) {
         ckt_memtype = CKT_BUFFER;
      } else {
         HLog << InfoError
              << "unsupported circuit cl_mem type '" << mem_str << "'"
              << std::endl;
         return false;
      }
   }
   bool print_only = !(_variablesMap.count("global_work_size") ||
                       _variablesMap.count("local_work_size") ||
                       _variablesMap.count("ckt_mem") );
   if( pfm_id >= 0 ) { // platform id is is valid
      if( dev_id >= 0 ) { // device id is specified
         if( !ocl_info.platform(cl_uint(pfm_id)).checkDeviceIdRange(dev_id)) {
            return false;
         } else {
            res = ocl_info.setUserPreferences(
               pfm_id, dev_id, gw_size, lw_size, ckt_memtype, print_only );
            return res;
         }
      } else { // dev_id not specified; apply to all devices of the platform
         res = ocl_info.setUserPreferences(
            pfm_id, gw_size, lw_size, ckt_memtype, print_only );
         return res;
      }
   } else { // platform id not specified
     if( dev_id >= 0 ) { // device id specified
         HLog << InfoError
              << "a platform id must be specified with device id"
              << std::endl;
         return false;
      }
   }

   // apply to all platforms and all devices
   res = ocl_info.setUserPreferences(
      gw_size, lw_size, ckt_memtype, print_only );
   return res;
}


// Constructor for SetSimulatorOptionsCmd
SetSimulatorOptionsCmd::SetSimulatorOptionsCmd() : Command() {
   _options.add_options()
      ("two_value_logic,t", po::value<std::string>(),
         "enable or disable two value logic")
      ("io_op_per_cycle,i", po::value<std::string>(),
         "enable or disable force_pi and observe_po per pattern cycle")
      ("event_driven,e", po::value<std::string>(),
         "enable or disable event driven simulator")
      ("location,l", po::value<std::string>(),
         "set where main simulation happens")
      ("platform,p", po::value<int>(), "specify platform id")
      ("device,d", po::value<int>(), "specify device id")
      ("gpu_psets,g", po::value<int>(),
         "specify number of parallel pattern sets on gpu")
      ("vector_mode,v", po::value<std::string>(),
         "enable or disable vector mode on OpenCL CPU sim kernel")
      ("no_diverge,n", po::value<std::string>(),
         "enable or disable non-divergent good sim kernel")
   ;
}


// Destructor for SetSimulatorOptionsCmd
SetSimulatorOptionsCmd::~SetSimulatorOptionsCmd() {
   ;
}


void SetSimulatorOptionsCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage: set_simulator_options [--two_value_logic|-t <on|OFF>]\n"
        << "                             [--io_op_per_cycle|-i <on|OFF>]\n"
        << "  "
        << "                           [--event_driven|-e <flag|queue|OFF>]\n"
        << "                             [--location|-l <HOST|gpu|ocl_cpu>]\n"
        << "                             [--platform|-p  <platform_id>]\n"
        << "                             [--device|-d    <device_id>]\n"
        << "                             [--gpu_psets|-g <psets_on_gpu>\n"
        << "                             [--vector_mode|-v <on|OFF>\n"
        << "                             [--no_diverge|-n <on|OFF>\n";
   HLog.flush();
}


bool SetSimulatorOptionsCmd::action() {

   SimulationOptions & sim_opt = (htDB::instance())->simOptions();
   bool four_val_logic = sim_opt.fourValueLogic();
   bool io_per_cycle = sim_opt.ioOpPerCycle();
   LogicSimMethod lsim_type = sim_opt.logicSimMethod();
   SimLocation sim_loc = sim_opt.simLocation();
   int pfm_id = -1; // stand for platform id; -1:unspecified
   int dev_id = -1; // stand for device id; -1:unspecified
   int gpu_psets = -1; // stand for number of parallel pattern sets on gpu
   bool non_divergent_gsim = false; // for non-divergent gpu gsim kernel
   bool vector_mode_on = false;

   if( !_variablesMap.count("two_value_logic") &&
       !_variablesMap.count("io_op_per_cycle") &&
       !_variablesMap.count("event_driven") &&
       !_variablesMap.count("location") )
   {
       sim_opt.printOptions();
   }

   if( _variablesMap.count("two_value_logic")) {
      std::string two_v_str =
         _variablesMap["two_value_logic"].as<std::string>();
      if( two_v_str.compare("on")==0 ) {
         four_val_logic = false;
      } else if( two_v_str.compare("off")==0 ) {
         four_val_logic = true;
      } else {
         HLog << InfoError
              << "invalid option '" << two_v_str << "' for --two_value_logic"
              << std::endl;
         return false;
      }
   }

   if( _variablesMap.count("io_op_per_cycle")) {
      std::string io_str = _variablesMap["io_op_per_cycle"].as<std::string>();
      if( io_str.compare("on")==0 ) {
         io_per_cycle = true;
      } else if( io_str.compare("off")==0 ) {
         io_per_cycle = false;
      } else {
         HLog << InfoError
              << "invalid option '" << io_str << "' for --io_op_per_cycle"
              << std::endl;
         return false;
      }
   }

   if( _variablesMap.count("event_driven")) {
      std::string event_str = _variablesMap["event_driven"].as<std::string>();
      if( event_str.compare("on")==0 ) {
         //lsim_type = EVENT_FLAGS_BASED; // default event manager
         lsim_type = EVENT_QUEUE_BASED;
      } else if( event_str.compare("flag")==0 ) {
         lsim_type = EVENT_FLAGS_BASED;
      } else if( event_str.compare("queue")==0 ) {
         lsim_type = EVENT_QUEUE_BASED;
      } else if( event_str.compare("off")==0 ) {
         lsim_type = IN_ORDER;
      } else {
         HLog << InfoError
              << "invalid option '" << event_str << "' for --event_driven"
              << std::endl;
         return false;
      }
   }

   if( _variablesMap.count("platform")) {
      pfm_id = _variablesMap["platform"].as<int>();
   }
   if( _variablesMap.count("device")) {
      dev_id = _variablesMap["device"].as<int>();
   }
   if( _variablesMap.count("location")) {
      std::string loc_str = _variablesMap["location"].as<std::string>();
      if( loc_str.compare("gpu")==0 ) {
         sim_loc = OCL_GPU;
      } else if( loc_str.compare("host")==0 ) {
         sim_loc = HOST;
      } else if( loc_str.compare("ocl_cpu")==0 ) {
         sim_loc = OCL_CPU;
      } else {
         HLog << InfoError
              << "unsupported simulation node location '" << loc_str << "'"
              << std::endl;
         return false;
      }
   }
   // check combination of sim location with pfm_id/dev_id
   if( HOST==sim_loc && (pfm_id >= 0 || dev_id >= 0 )) {
      HLog << InfoError
           << "options --platform and --device cannot be specified"
           << " when simulation is done on host"
           << std::endl;
      return false;
   }
   if( _variablesMap.count("location") &&
      (OCL_GPU==sim_loc || OCL_CPU==sim_loc) &&
      (-1==pfm_id || -1==dev_id)) {
      HLog << InfoError
           << "both options --platform and --device must be specified"
           << " when simulation is done on GPU"
           << std::endl;
      return false;
   }

   // check validity of pfm_id and dev_id
   if( pfm_id >= 0 && dev_id >= 0 ) {
      if( !sim_opt.verifyAndSetOclPlatformDevice(sim_loc, pfm_id, dev_id)) {
         return false;
      }
   }
   if( _variablesMap.count("gpu_psets")) {
      gpu_psets = _variablesMap["gpu_psets"].as<int>();
      if( 0 != gpu_psets && 1 != gpu_psets ) {
         HLog << InfoError
              << "only 0 or 1 parallel psets on GPU are supported for now"
              << std::endl;
         return false;
      }
      sim_opt.numParallelPsetsOnGpu( gpu_psets );
   }
   if( _variablesMap.count("no_diverge")) {
      std::string str_tmp =
         _variablesMap["no_diverge"].as<std::string>();
      if( str_tmp.compare("on")==0 ) {
         non_divergent_gsim = true;
      } else if( str_tmp.compare("off")==0 ) {
         non_divergent_gsim = false;
      } else {
         HLog << InfoError
              << "invalid option '" << str_tmp << "' for --no_diverge"
              << std::endl;
         return false;
      }
   }
   // vector mode only supported on OCL_CPU for now
   if( _variablesMap.count("vector_mode") && OCL_CPU==sim_loc) {
      std::string str_tmp =
         _variablesMap["vector_mode"].as<std::string>();
      if( str_tmp.compare("on")==0 ) {
         vector_mode_on = true;
      } else if( str_tmp.compare("off")==0 ) {
         vector_mode_on = false;
      } else {
         HLog << InfoError
              << "invalid option '" << str_tmp << "' for --vector_mode"
              << std::endl;
         return false;
      }
   }
   sim_opt.vectorModeOn( vector_mode_on );
   sim_opt.nonDivergentGsimKernel( non_divergent_gsim );
   sim_opt.fourValueLogic( four_val_logic );
   sim_opt.ioOpPerCycle( io_per_cycle );
   sim_opt.logicSimMethod( lsim_type );
   sim_opt.simLocation( sim_loc );

   return true;
}

//    ----------------
//    |  |  |  |  |  |
//    |  |  |  |  |  |  height
//    |  |  |  |---
//    |  |---
//    ---
//     width
//
//    ordinary type :
//       Ideal circuit is composed of rectangles with same width "--keep".
//       Neighboring rectangles are with decreasing height. The height
//       decrement is specified by ""--decre"
//       "--inputs" is the height of the 1st rectangle.
//       "--levels" is the total level count across all rectangles.
//
//    custom type :
//       "--customize" is followed with multiple integer pairs
//       (even number of integers). Each pair represents (height, width)
//       of a rectangle.
//
//    Currently only AND gate type is supported.
//
// Constructor for MakeIdealCircuitCmd
// --output|-o : output circuit
// --format|-f : level | bench | verilog
// --gates |-g : AND | AND_OR | ....
// --inputs|-i : <num>
// --levels|-l : <num>
// --decre |-d : <num>
// --keep  |-k : <num>
// --customize|-c : <num> (gate size of 1st level, keep of this size, ...)
MakeIdealCircuitCmd::MakeIdealCircuitCmd() : Command() {
   _options.add_options()
      ("ckt", po::value<std::string>()->required(),"circuit file name")
      ("format,f", po::value<std::string>(), "format of circuit")
      ("gates,g", po::value<std::string>(), "gate type of circuit")
      ("inputs,i", po::value<int>(), "number of inputs/flip-flops")
      ("levels,l", po::value<int>(), "number of levels")
      ("decre,d", po::value<int>(), "number of decrement")
      ("keep,k", po::value<int>(),
                "number of level to keep the same size util decrement")
      ("customize,c", po::value<std::vector<int> >()->multitoken(),
         "set customized shape with multiple rectangles")
      ("replace,r", po::bool_switch()->default_value(false),
                      "replace existing file")
   ;
   _posOptions.add("ckt",1);
}


// Destructor for MakeIdealCircuitCmd
MakeIdealCircuitCmd::~MakeIdealCircuitCmd() {
   ;
}


void MakeIdealCircuitCmd::help() const {
   //HLog << _options << "\n";
   HLog << "Usage:\n"
        << "make_ideal_circuit <ckt_file>\n"
        << "                   [--format|-f <LEVEL|bench|verilog>]\n"
        << "                   [--gates|-g <AND|and_or|and_xor|and_or_xor_6>]\n"
        << "                   [--inputs|-i <num>]\n"
        << "                   [--levels|-l <num>]\n"
        << "                   [--decre|-d <num>]\n"
        << "                   [--keep|k <num>]\n"
        << "                   [--customzie|c <height1 width1 ...>]\n"
        << "                   [--replace|-r]\n";
   HLog.flush();
}


bool MakeIdealCircuitCmd::action() {

   IdealCircuit idealCkt ;

   // Check --format
   typedef enum {
      LEVEL,
      BENCH,
      VERILOG,
   } OutputFormat;
   OutputFormat format = LEVEL;
   if( _variablesMap.count("format") ) {
      std::string str_cmp = _variablesMap["format"].as<std::string>();
      if( str_cmp.compare("level")==0 ) {
         format = LEVEL;
      } else if ( str_cmp.compare("bench")==0 ) {
         format = BENCH; // TBD later
      } else if ( str_cmp.compare("verilog")==0 ) {
         format = VERILOG; // TBD later
      } else {
         HLog << InfoError
              << "unsupported circuit format '" << str_cmp << "'"
              << std::endl;
         return false;
      }
      if( LEVEL!=format ) {
         HLog << InfoError
              << "only level format is supported for now."
              << std::endl;
         return false;
      }
   }

   // Check --circuit_type
   typedef enum {
      SEQ ,
      COM
   } CircuitType;
   bool seq;
   if( _variablesMap.count("circuit_type") ) {
      std::string str_cmp = _variablesMap["circuit_type"].as<std::string>();
      if ( str_cmp.compare("seq")==0 ){
         seq = true;
      } else if ( str_cmp.compare("com")==0 ) {
         seq = false;
      } else  {
         HLog << InfoError
              << "unsupported circuit type '" << str_cmp <<"'"
              << std::endl;
         return false;
      }
   }

   // Check --gate
   typedef enum {
      AND ,
      AND_OR ,
      AND_XOR ,
      AND_OR_XOR_6
   } Gate;
   Gate gateType = AND;
   if( _variablesMap.count("gates") ) {
      std::string str_cmp = _variablesMap["gates"].as<std::string>();
      if ( str_cmp.compare("and")==0 ){
         gateType = AND;
      /*
      } else if ( str_cmp.compare("and_or")==0 ) {
         gateType = AND_OR; // TBD later
      } else if ( str_cmp.compare("and_xor")==0 ) {
         gateType = AND_XOR; // TBD later
      } else if ( str_cmp.compare("and_or_xor_6")==0 ) {
         gateType = AND_OR_XOR_6; // TBD later
      */
      } else  {
         HLog << InfoError
              << "unsupported circuit type '" << str_cmp <<"'"
              << std::endl;
         return false;
      }
   }

   // Check file extension
   bool replace = _variablesMap["replace"].as<bool>();
   std::string wri_filename("");

   wri_filename = _variablesMap["ckt"].as<std::string>();

   // Check if --replace is needed
   if ( fileExists( wri_filename.c_str() ) && !replace ) {
      HLog << InfoError
           << "circuit file '" << wri_filename
           << "' exist. Please use '--replace' to overwrite"
           << std::endl;
      return false;
   }

   // Write out circuit
   std::ofstream ofs( wri_filename );
   if ( !ofs.is_open() ) {
      HLog << InfoError
           << "failed to open file '"<< wri_filename << "' to write"
           << std::endl;
      return false;
   }

   // Use customized shape
   std::vector< int> int_array;
   if( _variablesMap.count("customize")) {
      int_array = _variablesMap["customize"].as<std::vector<int> >();
      // verify that --shape --inputs --levels --decre --keep are not
      if( _variablesMap.count("inputs") ||
          _variablesMap.count("levels") ||
          _variablesMap.count("decre")  ||
          _variablesMap.count("keep") ) {
         HLog << InfoError
              << "Options --inputs, --levels, --decre, and --keep are "
              << " mutual exclusive with --customize."
              << std::endl;
         return false;
      }
      // verify size of int_array[] is even and each element is >= 1
      if ( 1==(int_array.size() % 2) ) {
         HLog << InfoError
              << "The number of integers followed --customize should be even."
              << std::endl;
         return false;
      }
      for (size_t i=0; i<int_array.size(); i++) {
         if ( int_array[i] < 1 ) {
            HLog << InfoError
                 << "Integers followed --customize should be greater than or "
                 << "equal to 1."
                 << std::endl;
            return false;
         }
      }

      // pass the parameters to circuit generation routine
      idealCkt.customizeCircuit(ofs,int_array,seq);
      ofs.close();
      return true;
   }

   // Use ordinary shape
   // Consistency check of --inputs
   int n_inputs = 1;
   if ( _variablesMap.count("inputs") ) {
      n_inputs = _variablesMap["inputs"].as<int>();
      if( n_inputs < 1) {
         HLog << InfoError
              << "number of inputs/dffs must be greater than or equal to 1"
              << std::endl;
         return false;
      }
   }

   // Consistency check of --levels
   int n_levels = 1;
   if ( _variablesMap.count("levels") ) {
      n_levels = _variablesMap["levels"].as<int>();
      if ( n_levels < 1 ) {
         HLog << InfoError
              << "number of levels must be greater than or equal to 1"
              << std::endl;
         return false;
      }
   }

   // Consistency check of --decre
   int decre = 0;
   if ( _variablesMap.count("decre")) {
      decre = _variablesMap["decre"].as<int>();
      if ( decre < 0 ) {
         HLog << InfoError
              << "number of decre must be greater than or equal to 0"
              << std::endl;
         return false;
      }
   }

   // Consistency check of --keep
   int keep = 1;
   if ( _variablesMap.count("keep")) {
      keep = _variablesMap["keep"].as<int>();
      if ( keep < 1 ) {
         HLog << InfoError
              << "number of keep must be greater than or equal to 1"
              << std::endl;
         return false;
      }
      if ( keep > _variablesMap["levels"].as<int>() ) {
         HLog << InfoError
              << "number of keep must be not greater than levels"
              << std::endl;
      }
   }

   // Verify that the height of last rectangle is positive.
   if( decre!=0 ) {
      int num_blocks = n_levels / keep;
      if ( 0 != (n_levels % keep) ) {
         num_blocks++;
      }
      int min_required_inputs = ( num_blocks-1 ) * decre;
      if ( min_required_inputs > n_inputs ) {
         HLog << InfoError
              << "the setting leads to negative height of the last rectangle."
              << std::endl;
         return false;
      }
   }

   if( gateType==AND ) {
      idealCkt.ordinaryCircuit( ofs, n_inputs, n_levels, decre, keep, seq );
   }

   ofs.close();
   return true;
}
