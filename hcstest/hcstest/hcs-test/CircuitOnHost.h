//////////////////////////////////////////////////////////////////
//                        CircuitOnHost.h
//
//  Class and function definition for circuit class in host cpu
//
//////////////////////////////////////////////////////////////////

#ifndef _CIRCUIT_ON_HOST_H
#define _CIRCUIT_ON_HOST_H

#include "Netlist.h"
#include "app-common/GraphMisc.h"
#include <vector>
#include <string>
#include <ostream>
#include <initializer_list>
#include <chrono>


//////////////////////////////////////////////////////////////////
//      Class definition of circuit on host
//////////////////////////////////////////////////////////////////
class CircuitOnHost {
   friend class Evaluate;

   public:
      CircuitOnHost();
      ~CircuitOnHost();

      void clear();

      const std::string& iscasCktFile() const {
         return _iscasCktFile;
      }

      void iscasCktFile( const std::string& file ) {
         _iscasCktFile = file;
      }

      const std::string& levelizedCktFile() const {
         return _levelizedCktFile;
      }

      // If lev file extension is correct, it saves the file name.
      bool levelizedCktFile( const std::string& file );

      const std::string& pinnameMappingFile() const {
         return _pinnameMappingFile;
      }

      void pinnameMappingFile( const std::string& file ) {
         _pinnameMappingFile = file;
      }

      bool circuitLoaded() const { return _cktLoaded; }
      void circuitLoaded( bool v ) { _cktLoaded = v; }

      bool addClocks( const std::vector<int>& clk_ids, int off_state );
      bool deleteClocks();

      bool useOptimizedNetlist() const { return _useOptimizedNetlist; }
      void useOptimizedNetlist( bool v ) { _useOptimizedNetlist = v; }
      LevelizedNetlist & simNetlist() {
         return useOptimizedNetlist() ? _optimizedCkt : _initialCkt;
      }
      LevelizedNetlist & initialCkt() { return _initialCkt; }
      LevelizedNetlist & optimizedCkt() { return _optimizedCkt; }

      bool isSequentialCkt() const {
         return _initialCkt.isSequentialCkt();
      }

      bool isCombinationalCkt() const {
         return _initialCkt.isCombinationalCkt();
      }

      void printCktStatistics( bool verbose ) const;

      // Move DE nodes to the last level and OUTPUTS to 2nd-to-last
      //   Store the updated netlist in _optimizedCkt
      void modifyHitecLevelNetlist();

      // Generate optimized and remodeled netlist
      void optimizeAndRemodelNetlist( bool skip_buf_inv_optimization,
                                      bool print_debug_info );

      void identifyFeedbackRegions(
              std::vector<ReducedGate> & copied_nodes,
              std::vector< std::vector<int> > & loops,
              std::vector< std::vector<int> > & fbbufs_per_scc,
              std::vector< std::vector<int> > & latches_per_scc );

      void optimizeAwayBuffersAndInverters(
              std::vector<ReducedGate> & copied_nodes,
              std::vector< unsigned > & flags,
              std::vector< std::vector<int> > & id_map,
              bool skip_buf_inv_optimization );

      std::vector<int> unionOfTwoIntSets( const std::vector<int> & set1,
                                          const std::vector<int> & set2,
                                          int id_erased_in_set2 );

      void remodelDLatches(
              size_t original_netlist_size,
              std::vector<ReducedGate> & copied_nodes,
              std::vector< unsigned > & flags,
              std::vector< std::vector<int> > & id_map );

      void remodelDFlipFlops(
              size_t original_netlist_size,
              std::vector<ReducedGate> & copied_nodes,
              std::vector< unsigned > & flags,
              std::vector< std::vector<int> > & id_map );

      void appendGateToNetlist(
              std::vector<ReducedGate> & copied_nodes,
              GType gtype,
              int * pred_lst,
              int * succ_lst );

      void initOptimizedNodes(
              size_t original_netlist_size,
              const std::vector<ReducedGate> & temp_nodes,
              std::vector<ReducedGate> & optimized_nodes,
              const std::vector< unsigned > & flags,
              std::vector< std::vector<int> > & id_map );

      void levelizeOptimizedNodes(
              size_t original_netlist_size,
              const std::vector<ReducedGate> & pre_level_optnodes,
              std::vector<ReducedGate> & post_level_optnodes,
              std::vector< std::vector<int> > & id_map,
              std::vector< int > & levels1,
              std::vector< int > & levels2,
              std::vector< int > & levels_post_opt );

      int obtainOffsetOfRemodelGatesOfDFlipflop(
              const std::vector<ReducedGate> & pre_level_optnodes,
              const std::vector< std::vector<int>> & id_map );

      void computeLevelOfOptimizedNodes(
              const std::vector<ReducedGate> & pre_level_optnodes,
              int offset_per_dff_remodel,
              std::vector< int > & level,
              std::vector< int > & levelized_order,
              std::vector< int > & output_nodes,
              std::vector< int > & de_nodes );

      void adjustLevelsWithDEsAndOutputs(
              const std::vector<ReducedGate> & pre_level_optnodes,
              const std::vector< int > & level1,
              std::vector< int > & level2,
              const std::vector< int > & output_nodes,
              const std::vector< int > & de_nodes );

      void generatePostLevelOptNodesWithTypeGrouping(
              size_t original_netlist_size,
              const std::vector<ReducedGate> & pre_level_optnodes,
              const std::vector< int > & level2,
              std::vector<ReducedGate> & post_level_optnodes,
              std::vector< int >       & levels_post_opt,
              std::vector< std::vector<int> > & id_map );

      void populateOptNodesForSimulation(
              const std::vector<ReducedGate> & post_level_optnodes,
              const std::vector< int >       & levels_post_opt,
              const std::vector< unsigned>   & flags,
              const std::vector< std::vector<int>> & id_map );

      void dumpNetlistOfReducedGates(
              const std::vector<ReducedGate>& nodes );

      void dumpNetlistOfReducedGates(
              const std::vector<ReducedGate>& nodes,
              const std::vector<unsigned>& flags,
              const std::vector< std::vector<int> >& id_map );

      void dumpNetlistOfReducedGates(
              const std::vector<ReducedGate>& nodes,
              const std::vector<int>& levels );

      void dumpOneReducedGate(
              const std::vector<ReducedGate>& nodes,
              unsigned g );

      void appendGateToNetlist(
              std::vector<ReducedGate> & copied_nodes,
              GType gtype,
              const std::vector<int>& preds_in,
              const std::vector<int>& succs_in );

      void printSysTimeInSeconds( std::chrono::system_clock::time_point& start,
                                  std::chrono::system_clock::time_point& end,
                                  const std::string& msg );

   private:
      std::string _iscasCktFile;
      std::string _levelizedCktFile;
      std::string _pinnameMappingFile;
      bool _cktLoaded;
      bool _useOptimizedNetlist;

      LevelizedNetlist _initialCkt;        // original netlist
      LevelizedNetlist _optimizedCkt;      // optimized netlist
};

#endif
