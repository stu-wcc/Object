//////////////////////////////////////////////////////////////////
//                        HtCommand.h
//
//  Class and function definition for Hcs-test specific
//  tcl commands
//////////////////////////////////////////////////////////////////

#ifndef _HT_COMMAND
#define _HT_COMMAND

#include "app-common/command.h"

// Register Hcs-test specific tcl commands
void registerHtTclCommands( Tcl_Interp* interp );

/////////////////////////////////////////////////////////////
//    Class definition for 'read_circuit' command
/////////////////////////////////////////////////////////////
class ReadCircuitCmd : public Command {
   public:
      ReadCircuitCmd();
      ~ReadCircuitCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'write_circuit' command
/////////////////////////////////////////////////////////////
class WriteCircuitCmd : public Command {
   public:
	  WriteCircuitCmd();
	  ~WriteCircuitCmd();
	  bool action();
	  void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'delete_circuit' command
/////////////////////////////////////////////////////////////
class DeleteCircuitCmd : public Command {
   public:
      DeleteCircuitCmd();
      ~DeleteCircuitCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'read_patterns' command
/////////////////////////////////////////////////////////////
class ReadPatternsCmd : public Command {
   public:
      ReadPatternsCmd();
      ~ReadPatternsCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'write_patterns' command
/////////////////////////////////////////////////////////////
class WritePatternsCmd : public Command {
   public:
      WritePatternsCmd();
      ~WritePatternsCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'delete_patterns' command
/////////////////////////////////////////////////////////////
class DeletePatternsCmd : public Command {
   public:
      DeletePatternsCmd();
      ~DeletePatternsCmd();
      bool action();
      void help() const;
};

/////////////////////////////////////////////////////////////
//    Class definition for 'do_static learning' command
/////////////////////////////////////////////////////////////
class DoStaticLearningCmd : public Command {
   public:
      DoStaticLearningCmd();
      ~DoStaticLearningCmd();
      bool action();
      void help() const;
};
/////////////////////////////////////////////////////////////
//    Class definition for 'simulate_patterns' command
/////////////////////////////////////////////////////////////
class SimulatePatternsCmd : public Command {
   public:
      SimulatePatternsCmd();
      ~SimulatePatternsCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'set_random_patterns' command
/////////////////////////////////////////////////////////////
class SetRandomPatternsCmd : public Command {
   public:
      SetRandomPatternsCmd();
      ~SetRandomPatternsCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'set_simulator_options' command
/////////////////////////////////////////////////////////////
class SetSimulatorOptionsCmd : public Command {
   public:
      SetSimulatorOptionsCmd();
      ~SetSimulatorOptionsCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'report_opencl' command
/////////////////////////////////////////////////////////////
class ReportOpenclCmd : public Command {
   public:
      ReportOpenclCmd();
      ~ReportOpenclCmd();
      bool action();
      void help() const;

   protected:
   private:
};


/////////////////////////////////////////////////////////////
//    Class definition for 'create_ocl_context' command
/////////////////////////////////////////////////////////////
class CreateOclContextCmd : public Command {
   public:
      CreateOclContextCmd();
      ~CreateOclContextCmd();
      bool action();
      void help() const;

   protected:
   private:
};


/////////////////////////////////////////////////////////////
//    Class definition for 'release_ocl_context' command
/////////////////////////////////////////////////////////////
class ReleaseOclContextCmd : public Command {
   public:
      ReleaseOclContextCmd();
      ~ReleaseOclContextCmd();
      bool action();
      void help() const;

   protected:
   private:
};


/////////////////////////////////////////////////////////////
//    Class definition for 'create_ocl_queue' command
/////////////////////////////////////////////////////////////
class CreateOclQueueCmd : public Command {
   public:
      CreateOclQueueCmd();
      ~CreateOclQueueCmd();
      bool action();
      void help() const;

   protected:
   private:
};


/////////////////////////////////////////////////////////////
//    Class definition for 'release_ocl_queue' command
/////////////////////////////////////////////////////////////
class ReleaseOclQueueCmd : public Command {
   public:
      ReleaseOclQueueCmd();
      ~ReleaseOclQueueCmd();
      bool action();
      void help() const;

   protected:
   private:
};


/////////////////////////////////////////////////////////////
//    Class definition for 'set_ocl_options' command
/////////////////////////////////////////////////////////////
class SetOclOptionsCmd : public Command {
   public:
      SetOclOptionsCmd();
      ~SetOclOptionsCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'make_ideal_circuit' command
////////////////////////////////////////////////////////////
class MakeIdealCircuitCmd : public Command {
   public:
      MakeIdealCircuitCmd();
      ~MakeIdealCircuitCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'add_clocks' command
////////////////////////////////////////////////////////////
class AddClocksCmd : public Command {
   public:
      AddClocksCmd();
      ~AddClocksCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'delete_clocks' command
////////////////////////////////////////////////////////////
class DeleteClocksCmd : public Command {
   public:
      DeleteClocksCmd();
      ~DeleteClocksCmd();
      bool action();
      void help() const;
};


/////////////////////////////////////////////////////////////
//    Class definition for 'report_clocks' command
////////////////////////////////////////////////////////////
class ReportClocksCmd : public Command {
   public:
      ReportClocksCmd();
      ~ReportClocksCmd();
      bool action();
      void help() const;
};
#endif
