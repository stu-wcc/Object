//////////////////////////////////////////////////////////////////
//                        Evaluate.h
//
//  Class and function definition for Evaluate class
//
//////////////////////////////////////////////////////////////////
#ifndef _EVALUATE_H
#define _EVALUATE_H

#include "Event.h"
#include "CircuitOnHost.h"
//#include "StaticLearning.h"

class Evaluate {
   public:
      Evaluate();
      Evaluate( LevelizedNetlist & ckt );
      ~Evaluate();

      void bindToCkt( LevelizedNetlist& ckt );
      LevelizedNetlist & ckt() const { return *(_cktPtr); }

      void gmv0Dff(int gid, uint64_t v); // init gmv of DFF, can be
      void gmv1Dff(int gid, uint64_t v); //   multiple gates.
      void gmv0(int gid, uint64_t v ) { _gmv0[gid] = v; }
      void gmv1(int gid, uint64_t v ) { _gmv1[gid] = v; }
      uint64_t gmv0(int gid) const { return _gmv0[gid]; }
      uint64_t gmv1(int gid) const { return _gmv1[gid]; }

      void gmv0x4(int gid, int i, uint64_t v );
      void gmv1x4(int gid, int i, uint64_t v );
      void gmv0x4Dff(int gid, int i, uint64_t v );
      void gmv1x4Dff(int gid, int i, uint64_t v );
      uint64_t gmv0x4(int gid, int i) const;
      uint64_t gmv1x4(int gid, int i) const;
      cl_ulong4 gmv0x4(int gid) const { return _gmv0x4[gid]; }
      cl_ulong4 gmv1x4(int gid) const { return _gmv1x4[gid]; }

      // set gmv with 2-value logic, 0:'0', 1:'1'
      void setGmv2val( int gid, int v, uint64_t mask );
      // set gmv with 4-value logic, 0:'0', 1:'1', 2:'X', 3:'Z'
      void setGmv4val( int gid, int v, uint64_t mask );

      // For debug, dump node value
      std::string nodeValue( int g, uint64_t mask) const;
      std::string nodeValues( int g ) const;

      void printNode( int g, uint64_t mask ) const;
      void printNode( int g, uint64_t mask, std::ofstream& ofs ) const;
      void printAllNodeValues( uint64_t mask ) const;
      void printAllNodeValues( uint64_t mask,
                               int up_to_lev,
                               const std::string& fn ) const;
      void printAllNodeValues() const;
      void printInputs(uint64_t mask) const;
      void printDffs(uint64_t mask) const;

      // Initialize node values for either event-driven or non-event-driven
      //   simulation. This is one time job per design.
      void initializeNodeValues( bool is_4val_logic,
                                 bool vector_mode_on,
                                 bool is_event_driven,
                                 EventManager* event_mgr );

      // 'event': maintain the list of gates to be evaluated
      void addSuccessorEvents( int g, EventManager* event_mgr );

      // gate evaluation with non-divergent code path
      void gateEval4valNonDivergent(int id, uint64_t& v0, uint64_t& v1
#ifdef HT_PROFILE
                                   ,int frame_id
#endif
                                   );

      // gate evaluation with 4-value logic
      void gateEval4val(int id, uint64_t& v0, uint64_t& v1
#ifdef HT_PROFILE
                       ,int frame_id
#endif
                       );

      // Evaluate and update gmv of gate (including delay element).
      bool evalAndUpdateGate4val( int g
#ifdef HT_PROFILE
                                 ,int frame_id
#endif
                                );
      bool evalAndUpdateGate2val( int g
#ifdef HT_PROFILE
                                 ,int frame_id
#endif
                                );

      // Evaluate and update gmv of remodeled Dff. TRUE: gmv changed
      bool evalAndUpdateRDFF4val( int g
#ifdef HT_PROFILE
                                 ,int frame_id
#endif
                                );
      bool evalAndUpdateRDFF2val( int g
#ifdef HT_PROFILE
                                 ,int frame_id
#endif
                                );

      // Combined version of evaluate and update all gates in event Queue
      //   return the gid where gmv changed; -1 means gmv does not change
      int  evalAndUpdateQGate4val( int g
#ifdef HT_PROFILE
                                  ,int frame_id
#endif
                                 );
      int  evalAndUpdateQGate2val( int g
#ifdef HT_PROFILE
                                  ,int frame_id
#endif
                                 );

      // gate evaluation with 2-value logic
      void gateEval2val(int id, uint64_t& v
#ifdef HT_PROFILE
                       ,int frame_id
#endif
                       );

      void simImpactOfPinConstraints(); // for breaking loop in levelization

      void goodSim( int parallel,           // patterns simulated in parallel
                    int cycle,              // cycle number
                    int frame,              // frame number 0, 1 or 2
                    bool vector_mode_on,    // ocl_cpu vector mode
                    bool is_4val_logic,     // true: 4val, false: 2val
                    bool store_patterns,    // flag to store pattern bits
                    bool io_per_cycle,      // fpi and mpo per cycle
                    bool last_cycle,        // last cycle within a pattern
                    std::vector<uint64_t>& bits_v0, // pattern bit stream
                    std::vector<uint64_t>& bits_v1,
                    std::vector< cl_ulong4 >& bits_v0_256,
                    std::vector< cl_ulong4 >& bits_v1_256 );

      // check if the gate is the starting INV of remodeled Dff
      bool isInverterOfRemodeledDff( int g ) const;

      void goodSimWithEvents( int parallel, // patterns simulated in parallel
                    int cycle,              // cycle number
                    int frame,              // frame number 0, 1 or 2
                    bool is_3frame_sim,     // indicate 3 frame simulation
                    bool is_4val_logic,     // true: 4val, false: 2val
                    bool store_patterns,    // flag to store pattern bits
                    bool io_per_cycle,      // fpi and mpo per cycle
                    bool last_cycle,        // last cycle within a pattern
                    std::vector<uint64_t>& bits_v0, // pattern bit stream
                    std::vector<uint64_t>& bits_v1,
                    EventManager* event_mgr );        // event container

      void storeForcePIAndInitFF(
                    int cycle,              // cycle number
                    int frame,              // frame number
                    bool vector_mode_on,    // ocl_cpu vector mode
                    bool is_4val_logic,     // true: 4val, false: 2val
                    bool io_per_cycle,      // fpi and mpo per cycle
                    bool last_cycle,        // last cycle within a pattern
                    std::vector<uint64_t>& bits_v0, // pattern bit stream
                    std::vector<uint64_t>& bits_v1,
                    std::vector< cl_ulong4 >& bits_v0_256,
                    std::vector< cl_ulong4 >& bits_v1_256 );

      void storeMeasurePOAndObserveFF(
                    int cycle,              // cycle number
                    int frame,              // frame number
                    bool vector_mode_on,    // ocl_cpu vector mode
                    bool is_4val_logic,     // true: 4val, false: 2val
                    bool io_per_cycle,      // fpi and mpo per cycle
                    bool last_cycle,        // last cycle within a pattern
                    std::vector<uint64_t>& bits_v0, // pattern bit stream
                    std::vector<uint64_t>& bits_v1,
                    std::vector< cl_ulong4 >& bits_v0_256,
                    std::vector< cl_ulong4 >& bits_v1_256 );

      bool oldAndNewValuesDiffer( int gid,
                                  uint64_t val0,
                                  uint64_t val1,
                                  bool is_4val_logic );

      // print message for debugging assign and store patterns
      void debugAssignStorePattern( const std::string msg,
                                    int gid,
                                    int cycle,
                                    int frame,
                                    uint64_t val0,
                                    uint64_t val1 );

      void assignBitParallelStimuli( bool external_patterns,
                                     int start,    // starting pattern number
                                     int parallel, // number of patterns
                                     int cycle,
                                     int frame,
                                     bool io_op_per_cycle,
                                     bool is_4val_logic,
                                     bool is_event_driven,
                                     EventManager* event_mgr );

      void assignBitParallelStimuliInVectorMode( bool external_patterns,
                                     int start,    // starting pattern number
                                     int parallel, // number of patterns
                                     int cycle,
                                     int frame,
                                     bool io_op_per_cycle );

      bool simulatePatterns( bool external_patterns,
                             bool store_patterns );

      // To be accessed by EvaluateCL class for cl_mem buffer
      std::vector< uint64_t >& gmv0() { return _gmv0; }
      std::vector< uint64_t >& gmv1() { return _gmv1; }
      std::vector< cl_ulong4 >& gmv0x4() { return _gmv0x4; }
      std::vector< cl_ulong4 >& gmv1x4() { return _gmv1x4; }

      int numCycles() const { return _numCycles; }
      GType type( int id ) const { return ckt().gate(id).type(); }

   protected:

      Evaluate( const Evaluate& eval_obj );

      void getSimCyclesAndReseedRandomGen( bool external_pattern );

      bool clockedDffIsUsed() const { return ckt().clockedDffIsUsed(); }
      const std::vector<int> & constDataInputNodes() const {
         return ckt().constDataInputNodes();
      }
      const std::vector<int> & constClockInputNodes() const {
         return ckt().constClockInputNodes();
      }
      int clockOffState( int gid ) const {
         return ckt().clockOffState( gid );
      }
      int numFramesPerCycle() const { return ckt().numFramesPerCycle(); }
      int numClockPIs() const { return ckt().numClockPIs(); }
      int numDataPIs() const { return ckt().numDataPIs(); }
      int numNodes() const { return ckt().numNodes(); }
      int numPIs() const { return ckt().numPIs(); }
      int numTIEs() const { return ckt().numTIEs(); }
      int numPOs() const { return ckt().numPOs(); }
      int numFFs() const { return ckt().numFFs(); }
      int firstNodePI() const { return ckt().firstNodePI(); }
      int firstNodeTIE() const { return ckt().firstNodeTIE(); }
      int firstNodeFF() const { return ckt().firstNodeFF(); }
      int output( int g ) const { return ckt().output(g); }

      int maxLevel() const { return ckt().maxLevel(); }
      int numLevels() const { return ckt().numLevels(); }
      int maxLevelWidth() const { return ckt().maxLevelWidth(); }

      Gate & gate( int id ) { return ckt().gate(id); }
      GType logic( int id ) const { return ckt().gate(id).type(); }
      int level( int id ) const { return ckt().gate(id).level(); }
      int numSucc( int id ) const { return ckt().gate(id).numSucc(); }
      int numPred( int id ) const { return ckt().gate(id).numPred(); }

      const std::vector< int > & predC0( int id ) const {
         return ckt().gate(id).predC0();
      }
      const std::vector< int > & predC1( int id ) const {
         return ckt().gate(id).predC1();
      }
      void printSimulationStatistics() const;
      void resetMeasurements();
#ifdef HT_PROFILE
      unsigned long numGateEvals() const { return _numGateEvals; }
      void numGateEvals( unsigned long n ) { _numGateEvals = n; }
#endif
      double simSysTime() const { return _simSysTime; }
      double simCpuTime() const { return _simCpuTime; }


   private:
      LevelizedNetlist* _cktPtr;
      // Value encoding: 0-(0, 0), 1-(1, 1), X-(1, 0), Z-(0, 1)
      std::vector< uint64_t > _gmv0; // to encode 64-wide values of 0, 1, X, Z
      std::vector< uint64_t > _gmv1; //   of each node in circuit
      std::vector< cl_ulong4 > _gmv0x4; // to support vector of OpenCL CPU
      std::vector< cl_ulong4 > _gmv1x4; //

      int _numCycles;          // number of cycles per pattern

#ifdef HT_PROFILE
      unsigned long  _numGateEvals; // number of gate evaluation
      // Count number of gate evals per frame
      std::vector< unsigned long >  _numGateEvalsPerFrame;
#endif
      double _simSysTime;
      double _simCpuTime;
};

class Learning {
   public:
      void simLearning();
      void initializeNodeValues( bool is_4val_logic,
                                 bool vector_mode_on,
                                 bool is_static_learning );

            
      // gate evaluation with 4-value logic
      void gateEval4val(int id, uint64_t& v0, uint64_t& v1);
};


#endif
