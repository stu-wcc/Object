/////////////////////////////////////////////////////////////////
//                PatternOnHost.cpp
/////////////////////////////////////////////////////////////////
#include "PatternOnHost.h"
#include "app-common/utility.h"
#include "HtSetting.h"
#include <assert.h>
#include <algorithm>

PatternCycle::PatternCycle(
   unsigned len_force_pi,
   unsigned len_init_ff,
   unsigned len_measure_po,
   unsigned len_observe_ff,
   const std::vector<std::string>& pulse_clk_pins )
   :  _v0( len_force_pi + len_init_ff + len_measure_po + len_observe_ff ),
      _v1( len_force_pi + len_init_ff + len_measure_po + len_observe_ff ),
      _lenForcePI( len_force_pi ),
      _lenInitFF( len_init_ff ),
      _lenMeasurePO( len_measure_po ),
      _lenObserveFF( len_observe_ff ),
      _offsetFPI( 0 ),
      _offsetIFF( len_force_pi ),
      _offsetMPO( len_force_pi + len_init_ff ),
      _offsetOFF( len_force_pi + len_init_ff + len_measure_po ),
      _clkPinsToPulse( pulse_clk_pins )
{
   ;
}


PatternCycle::~PatternCycle() {
   ;
}


char PatternCycle::getBitValue( unsigned n,
                                boost::dynamic_bitset<> v0,
                                boost::dynamic_bitset<> v1 ) const
{
   assert( n < v0.size() );
   assert( n < v1.size() );
   if( v0[n] ) {
      if( v1[n] ) {
         return '1'; // (1, 1)
      } else {
         return 'X'; // (1, 0)
      }
   } else {
      if( v1[n] ) {
         return 'Z'; // (0, 1)
      } else {
         return '0'; // (0, 0)
      }
   }
}


void PatternCycle::setBitValue( unsigned n,
                                boost::dynamic_bitset<> v0,
                                boost::dynamic_bitset<> v1,
                                char v )
{
   assert( n < v0.size() );
   assert( n < v1.size() );
   switch( v ) {
      case 'X': { // (1,0), disallow lowercase 'x'
         v0.set( n );
         v1.reset( n );
         break;
      }
      case 'Z': { // (0,1), disallow lowercase 'z'
         v0.reset( n );
         v1.set( n );
         break;
      }
      case '0': { // (0,0)
         v0.reset( n );
         v1.reset( n );
         break;
      }
      case '1': { // (1,1)
         v0.set( n );
         v1.set( n );
         break;
      }
      default: {
         HLog << InfoError << "unsupported bit value '" << v << "'"
              << std::endl;
         assert(0);
         break;
      }
   }
}


void PatternCycle::vector2bitset( int pos,
                                  uint64_t mask,
                                  const std::vector< cl_ulong4 >& s0,
                                  const std::vector< cl_ulong4 >& s1,
                                  boost::dynamic_bitset<> & v0,
                                  boost::dynamic_bitset<> & v1 )
{
   assert( v0.size() == v1.size() );
   assert( s0.size() == s1.size() );
   assert( s0.size() == v0.size() );
   for( unsigned b=0; b < s0.size(); b++ ) {
      if( s0[b].s[pos] & mask ) {
         if( s1[b].s[pos] & mask ) { // (1,1): '1'
            v0.set( b );
            v1.set( b );
         } else {             // (1,0): 'X'
            v0.set( b );
            v1.reset( b );
         }
      } else {
         if( s1[b].s[pos] & mask ) { // (0,1): 'Z'
            v0.reset( b );
            v1.set( b );
         } else {             // (0,0): '0'
            v0.reset( b );
            v1.reset( b );
         }
      }
   }
}


void PatternCycle::vector2bitset( uint64_t mask,
                                  const std::vector< uint64_t >& s0,
                                  const std::vector< uint64_t >& s1,
                                  boost::dynamic_bitset<> & v0,
                                  boost::dynamic_bitset<> & v1 )
{
   assert( v0.size() == v1.size() );
   assert( s0.size() == s1.size() );
   assert( s0.size() == v0.size() );
   for( unsigned b=0; b < s0.size(); b++ ) {
      if( s0[b] & mask ) {
         if( s1[b] & mask ) { // (1,1): '1'
            v0.set( b );
            v1.set( b );
         } else {             // (1,0): 'X'
            v0.set( b );
            v1.reset( b );
         }
      } else {
         if( s1[b] & mask ) { // (0,1): 'Z'
            v0.reset( b );
            v1.set( b );
         } else {             // (0,0): '0'
            v0.reset( b );
            v1.reset( b );
         }
      }
   }
}


void PatternCycle::bitset2vector( uint64_t mask,
                                  std::vector<uint64_t> & vec0,
                                  std::vector<uint64_t> & vec1 ) const
{
   assert( vec0.size() == _lenForcePI + _lenInitFF );
   assert( vec1.size() == _lenForcePI + _lenInitFF );
   for( unsigned b = _offsetFPI; b < (_offsetFPI + _lenForcePI); b++ ) {
      if( _v0[b] ) {
         vec0[b] |= mask;  // set 1 on bitmask
      } else {
         vec0[b] &= ~mask; // set 0 on bitmask
      }
      if( _v1[b] ) {
         vec1[b] |= mask;
      } else {
         vec1[b] &= ~mask;
      }
   }
   for( unsigned b = _offsetIFF; b < (_offsetIFF + _lenInitFF); b++ ) {
      if( _v0[b] ) {
         vec0[b] |= mask;  // set 1 on bitmask
      } else {
         vec0[b] &= ~mask; // set 0 on bitmask
      }
      if( _v1[b] ) {
         vec1[b] |= mask;
      } else {
         vec1[b] &= ~mask;
      }

   }
}



void PatternCycle::string2bitset( const std::string& str,
                                  unsigned offset,
                                  boost::dynamic_bitset<> & v0,
                                  boost::dynamic_bitset<> & v1 )
{
   assert( offset + str.length() <= v0.size() );
   assert( offset + str.length() <= v1.size() );
   for( unsigned ui=0; ui<str.length(); ui++ ) {
      switch( str[ui] ) {
         case 'X': { // (1, 0), disallow lowercase 'x'
            v0.set( offset + ui );
            v1.reset( offset + ui );
            break;
         }

         case 'Z': { // (0, 1), disallow lowercase 'z'
            v0.reset( offset + ui );
            v1.set( offset + ui );
            break;
         }

         case '0': { // (0, 0)
            v0.reset( offset + ui );
            v1.reset( offset + ui );
            break;
         }

         case '1': { // (1, 1)
            v0.set( offset + ui );
            v1.set( offset + ui );
            break;
         }

         default: {
            HLog << InfoError << "unsupported bit value '" << str[ui]
                 << "' in " << str
                 << std::endl;
            assert(0);
            break;
         }
      }
   }
}


void PatternCycle::bitset2string( const boost::dynamic_bitset<> & v0,
                                  const boost::dynamic_bitset<> & v1,
                                  unsigned offset,
                                  unsigned len,
                                  std::string& str ) const
{
   assert( v0.size()==v1.size() );
   assert( offset + len <= v0.size() );
   str.clear();
   for( unsigned n=offset; n<offset+len; n++ ) {
      if( v0[n] ) {
         if( v1[n] ) {
            str += "1"; // (1, 1)
         } else {
            str += "X"; // (1, 0)
         }
      } else {
         if( v1[n] ) {
            str += "Z"; // (0, 1)
         } else {
            str += "0"; // (0, 0)
         }
      }
   }
}


Pattern::Pattern() {
   ;
}


Pattern::~Pattern() {
   ;
}


PatternSet::PatternSet() : _version( "0.1" ) {
   ;
}


PatternSet::~PatternSet() {
   ;
}


///////////////////////////////////////////////////////////////////////////
// The overhead of readPatterns is that pattern schema is loaded everytime
//   a pattern file is read. But it should not be big as pattern json
//   schema is a small file.
///////////////////////////////////////////////////////////////////////////
bool PatternSet::readPatterns( const std::string& fname ) {

   //////////////////////////////////////////////////////////////////
   // 1. Load pattern JSON schema first
   std::string schema_file = (appCommonDB::instance())->execPath()
                           + "scripts/ascii_pattern.schema.json";
   FILE *fps = fopen( schema_file.c_str(), "r");
   if( !fps ) {
      HLog << InfoError
           << "failed to open pattern schema file '" << schema_file << "'"
           << std::endl;
      return false;
   }
   rapidjson::Document doc;
   char buffer[4096];
   rapidjson::FileReadStream fs(fps, buffer, sizeof(buffer));
   doc.ParseStream( fs );
   if( doc.HasParseError() ) {
      HLog << InfoError
           << "Schema file '" << schema_file << "' is not a valid JSON"
           << std::endl;
      HLog << InfoCont << "Error(offset "
           << static_cast<unsigned>(doc.GetErrorOffset())
           << "): " << GetParseError_En(doc.GetParseError())
           << std::endl;
      fclose(fps);
      return false;
   }
   fclose(fps);

   // Then convert the Document into SchemaDocument
   rapidjson::SchemaDocument sd(doc);

   // Create validator out of 'sd'
   rapidjson::SchemaValidator validator(sd);

   //////////////////////////////////////////////////////////////////
   // 2. Read in the actual pattern file and verify against schema
   FILE *fp = fopen( fname.c_str(), "r");
   if( !fp ) {
      HLog << InfoError
           << "failed to open pattern file '" << fname << "'"
           << std::endl;
      return false;
   }
   rapidjson::FileReadStream ins(fp, buffer, sizeof(buffer));
   rapidjson::Reader reader;

   if( !reader.Parse(ins, validator) &&
        reader.GetParseErrorCode() != rapidjson::kParseErrorTermination) {
      // Schema validator error would cause kParseErrorTermination,
      // which we will handle it in next step.
      HLog << InfoError << "pattern file '" << fname << "' is not a valid JSON"
           << std::endl;
      HLog << InfoCont << "Error(offset "
           << static_cast<unsigned>(reader.GetErrorOffset())
           << ": " << GetParseError_En(reader.GetParseErrorCode())
           << std::endl;
      fclose( fp );
      return false;
   }
   fclose( fp );

   // Check the validation result
   if( !validator.IsValid() ) {
      HLog << InfoError
           << "Input pattern file is invalid."
           << std::endl;
      rapidjson::StringBuffer sb;
      validator.GetInvalidSchemaPointer().StringifyUriFragment(sb);
      HLog << InfoCont
           << "Invalid schema: " << sb.GetString()
           << std::endl;
      HLog << InfoCont
           << "Invalid keyword: " << validator.GetInvalidSchemaKeyword()
           << std::endl;
      sb.Clear();
      validator.GetInvalidDocumentPointer().StringifyUriFragment(sb);
      HLog << InfoCont
           << "Invalid document: " << sb.GetString()
           << std::endl;
      return false;
   }

   //////////////////////////////////////////////////////////////////
   // 3. Additional consistency check
   if( !verifyAndLoadPatternFile( fname ) ) {
      return false;
   }

   return true;
}


bool PatternSet::verifyPropertyInt( int ref_int,
                                    int pat_int,
                                    const std::string& property_name,
                                    bool against_design )
{
   if( ref_int != pat_int ) {
      if( against_design ) {
         HLog << InfoError
              << property_name << "(" << pat_int << ") "
              << "in pattern file does not match that"
              << "(" << ref_int << ") in the design"
              << std::endl;
      } else {
         HLog << InfoError
              << "unsupported " << property_name
              << " with value " << pat_int
              << std::endl;
      }
      return false;
   }
   return true;
}


bool PatternSet::verifyPropertyString( const std::vector<std::string>& ref_vec,
                                       const std::string& pat_str,
                                       const std::string& property_name)
{
   if( std::find( ref_vec.begin(), ref_vec.end(), pat_str )==ref_vec.end()) {
      HLog << InfoError
           << "unsupported " << property_name
           << " '" << pat_str << "'"
           << std::endl;
      return false;
   }
   return true;
}


bool PatternSet::verifyIndividualPattern( unsigned n, rapidjson::Document& d ) {
   unsigned pat_id = d["scan_test"][n]["pattern"].GetUint();
   if( pat_id != n ) {
      HLog << InfoError
           << "in /scan_test/[" << n << "] "
           << "pattern id (" << pat_id << ")"
           << " does not match expected value (" << n << ")"
           << std::endl;
      return false;
   }

   unsigned num_cycles = d["scan_test"][n]["cycles"].Capacity();
   if( 0==num_cycles ) {
      HLog << InfoError
           << "at least one cycle is required for pattern /scan_test/["
           << n << "]"
           << std::endl;
      return false;
   }
   if( isCombinationalLogic() && num_cycles>1 ) {
      HLog << InfoError
           << "for combinational circuit, pattern /scan_test/[" << n << "]"
           << " cannot have more than 1 cycles"
           << std::endl;
      return false;
   }

   // Check individual cycle content
   for( unsigned c=0; c<num_cycles; c++ ) {
      // Check cycle number
      unsigned cycle_id = d["scan_test"][n]["cycles"][c]["cycle"].GetUint();
      if( cycle_id != c ) {
         HLog << InfoError
              << "in /scan_test/[" << n << "]/cycles[" << c << "] "
              << "cycle id (" << cycle_id << ")"
              << " does not match expected value (" << c << ")"
              << std::endl;
         return false;
      }
      ////////////////////////////////////////////
      //  Check force_pi
      rapidjson::Value::ConstMemberIterator
         it_fpi = d["scan_test"][n]["cycles"][c].FindMember("force_pi");
      if( it_fpi !=  d["scan_test"][n]["cycles"][c].MemberEnd() ) {
         std::string val = it_fpi->value.GetString();
         if( numDataPIs() != (int)val.size()) {
            HLog << InfoError
                 << "in /scan_test/[" << n << "]/cycles/[" << c << "], "
                 << "length of 'force_pi' (" << val.size() << ") "
                 << "does not match the number of inputs "
                 << "(" << numDataPIs() << ")"
                 << std::endl;
            return false;
         }
         if( !isTristateString( val )) {
            HLog << InfoError
                 << "invalid value in /scan_test/["
                 << n << "]/cycles/[" << c << "]/force_pi"
                 << std::endl;
            return false;
         }
      } else if( numDataPIs()>0 && 0==c ) {
         // force_pi always needed at cycle 0
         HLog << InfoError
              << "'force_pi' is required in "
              << "/scan_test/[" << n << "]/cycles/[" << c << "] "
              << std::endl;
         return false;
      }
      ////////////////////////////////////////////
      //  Check init_ff
      rapidjson::Value::ConstMemberIterator
         it_iff = d["scan_test"][n]["cycles"][c].FindMember("init_ff");
      if( it_iff !=  d["scan_test"][n]["cycles"][c].MemberEnd() ) {
         std::string val = it_iff->value.GetString();
         if( isCombinationalLogic() ) {
            HLog << InfoError
                 << "in /scan_test/[" << n << "]/cycles/[" << c << "] "
                 << "'init_ff' is not allowed for combinational circuit"
                 << std::endl;
            return false;
         }
         if( c>0 ) {
            HLog << InfoError // init_ff not allowed at cycle > 0
                 << "'init_ff' is not allowed "
                 << "in /scan_test/[" << n << "]/cycles/[" << c << "] "
                 << std::endl;
            return false;
         }
         if( numFFs() != (int)val.size()) {
            HLog << InfoError
                 << "in /scan_test/[" << n << "]/cycles/[" << c << "], "
                 << "length of 'init_ff' (" << val.size() << ") "
                 << "does not match the number of flip-flops "
                 << "(" << numFFs() << ")"
                 << std::endl;
            return false;
         }
         if( !isTristateString( val )) {
            HLog << InfoError
                 << "invalid value in /scan_test/["
                 << n << "]/cycles/[" << c << "]/init_ff"
                 << std::endl;
            return false;
         }
      } else if( isSequentialLogic() && 0==c ) {
         HLog << InfoError // init_ff required at cycle 0
              << "'init_ff' is required "
              << "in /scan_test/[" << n << "]/cycles/[" << c << "] "
              << std::endl;
         return false;
      }
      ////////////////////////////////////////////
      //  Check measure_po
      rapidjson::Value::ConstMemberIterator
         it_mpo = d["scan_test"][n]["cycles"][c].FindMember("measure_po");
      if( it_mpo !=  d["scan_test"][n]["cycles"][c].MemberEnd() ) {
         std::string val = it_mpo->value.GetString();
         if( numPOs() != (int)val.size()) {
            HLog << InfoError
                 << "in /scan_test/[" << n << "]/cycles/[" << c << "], "
                 << "length of 'measure_po' (" << val.size() << ") "
                 << "does not match the number of outputs "
                 << "(" << numPOs() << ")"
                 << std::endl;
            return false;
         }
         if( !isTristateString( val )) {
            HLog << InfoError
                 << "invalid value in /scan_test/["
                 << n << "]/cycles/[" << c << "]/measure_po"
                 << std::endl;
            return false;
         }
      } else if( numPOs()>0 && isCombinationalLogic()) {
         // measure_po always needed in combinational logic
         HLog << InfoError
              << "'measure_po' is required in "
              << "/scan_test/[" << n << "]/cycles/[" << c << "] "
              << std::endl;
         return false;
      }

      ////////////////////////////////////////////
      //  Check pulse_clk
      rapidjson::Value::ConstMemberIterator
         it_clk = d["scan_test"][n]["cycles"][c].FindMember("pulse_clk");
      if( it_clk !=  d["scan_test"][n]["cycles"][c].MemberEnd() ) {
         //std::string val = it_clk->value.GetString();
         if( isCombinationalLogic() ) {
            HLog << InfoError
                 << "in /scan_test/[" << n << "]/cycles/[" << c << "] "
                 << "'pulse_clk' is not allowed for combinational circuit"
                 << std::endl;
            return false;
         }
         std::string first_clk = ((it_clk->value)[0]).GetString();
         if( it_clk->value.Capacity()!=1 ) { // For now, multiple clocks not allowed
            HLog << InfoError
                 << "multiple clock pins are not supported in /scan_test/["
                 << n << "]/cycles/[" << c << "]/pulse_clk"
                 << std::endl;
            return false;
         } else if( "all"!=first_clk) {
            HLog << InfoError
                 << "invalid value '" << first_clk  << "' in /scan_test/["
                 << n << "]/cycles/[" << c << "]/pulse_clk"
                 << std::endl;
            return false;
         }
      } else if( isSequentialLogic() ) {
         HLog << InfoError
              << "'pulse_clk' is required in /scan_test/["
              << n << "]/cycles/[" << c << "]"
              << std::endl;
         return false;
      }
      ////////////////////////////////////////////
      //  Check observe_ff
      rapidjson::Value::ConstMemberIterator
         it_off = d["scan_test"][n]["cycles"][c].FindMember("observe_ff");
      if( it_off !=  d["scan_test"][n]["cycles"][c].MemberEnd() ) {
         std::string val = it_off->value.GetString();
         // Only allowed in last cycle for sequential circuit
         if( isCombinationalLogic() || (num_cycles-1)!=c ) {
            HLog << InfoError
                 << "'observe_ff' is not allowed in "
                 << "/scan_test/[" << n << "]/cycles/[" << c << "] "
                 << std::endl;
            return false;
         }
         if( numFFs() != (int)val.size()) {
            HLog << InfoError
                 << "in /scan_test/[" << n << "]/cycles/[" << c << "], "
                 << "length of 'observe_ff' (" << val.size() << ") "
                 << "does not match the number of flip-flops "
                 << "(" << numFFs() << ")"
                 << std::endl;
            return false;
         }
         if( !isTristateString( val )) {
            HLog << InfoError
                 << "invalid value in /scan_test/["
                 << n << "]/cycles/[" << c << "]/observe_ff"
                 << std::endl;
            return false;
         }
      } else if( isSequentialLogic() && (num_cycles-1)==c ) {
         HLog << InfoError
              << "'observe_ff' is required in "
              << "/scan_test/[" << n << "]/cycles/[" << c << "] "
              << std::endl;
         return false;
      }
   }
   return true;
}


void PatternSet::loadVerifiedPatterns( rapidjson::Document & d ) {

   _version = d["version"].GetString();
   unsigned num_pat = d["setup"]["number_of_patterns"].GetUint();

   for( unsigned p=0; p<num_pat; p++ ) {
      Pattern pat;
      unsigned num_cycles = d["scan_test"][p]["cycles"].Capacity();
      for( unsigned c=0; c<num_cycles; c++ ) {
         unsigned len_fpi=0, len_iff=0, len_mpo=0, len_off=0;
         std::string str_fpi(""), str_iff(""), str_mpo(""), str_off("");
         std::vector< std::string > vec_clks;
         ////////////////////////////////////////////
         //  Check force_pi
         rapidjson::Value::ConstMemberIterator
            it_fpi = d["scan_test"][p]["cycles"][c].FindMember("force_pi");
         if( it_fpi !=  d["scan_test"][p]["cycles"][c].MemberEnd() ) {
            str_fpi = it_fpi->value.GetString();
            len_fpi = str_fpi.size();
         }
         ////////////////////////////////////////////
         //  Check init_ff
         rapidjson::Value::ConstMemberIterator
            it_iff = d["scan_test"][p]["cycles"][c].FindMember("init_ff");
         if( it_iff !=  d["scan_test"][p]["cycles"][c].MemberEnd() ) {
            str_iff = it_iff->value.GetString();
            len_iff = str_iff.size();
         }
         ////////////////////////////////////////////
         //  Check measure_po
         rapidjson::Value::ConstMemberIterator
            it_mpo = d["scan_test"][p]["cycles"][c].FindMember("measure_po");
         if( it_mpo !=  d["scan_test"][p]["cycles"][c].MemberEnd() ) {
            str_mpo = it_mpo->value.GetString();
            len_mpo = str_mpo.size();
         }
         ////////////////////////////////////////////
         //  Check pulse_clk
         rapidjson::Value::ConstMemberIterator
            it_clk = d["scan_test"][p]["cycles"][c].FindMember("pulse_clk");
         if( it_clk !=  d["scan_test"][p]["cycles"][c].MemberEnd() ) {
            for( unsigned ui=0; ui<it_clk->value.Capacity(); ui++) {
               std::string clk = ((it_clk->value)[ui]).GetString();
               vec_clks.push_back( clk );
            }
         }
         ////////////////////////////////////////////
         //  Check observe_ff
         rapidjson::Value::ConstMemberIterator
            it_off = d["scan_test"][p]["cycles"][c].FindMember("observe_ff");
         if( it_off !=  d["scan_test"][p]["cycles"][c].MemberEnd() ) {
            str_off = it_off->value.GetString();
            len_off = str_off.size();
         }

         PatternCycle cyc( len_fpi, len_iff, len_mpo, len_off, vec_clks );
         cyc.setForcePI( str_fpi );
         cyc.setInitFF( str_iff );
         cyc.setMeasurePO( str_mpo );
         cyc.setObserveFF( str_off );

         pat.addCycle( cyc ); // Add one cycle into current pattern
      }
      addPattern( pat ); // Add one pattern into pattern set
   }
}


int PatternSet::numDataPIs() const {
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   return cktOnHost.initialCkt().numDataPIs();
}


int PatternSet::numPOs() const {
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   return cktOnHost.initialCkt().numPOs();
}


int PatternSet::numFFs() const {
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   //return cktOnHost.initialCkt().numFFs();
   return cktOnHost.simNetlist().numFFs();
}


bool PatternSet::isSequentialLogic() const {
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   return cktOnHost.isSequentialCkt();
}


bool PatternSet::isCombinationalLogic() const {
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   return cktOnHost.isCombinationalCkt();
}


bool PatternSet::verifyAndLoadPatternFile( const std::string& fname ) {
   FILE *fp = fopen( fname.c_str(), "r");
   if( !fp ) {
      HLog << InfoError
           << "failed to open pattern file '" << fname << "'"
           << std::endl;
      return false;
   }
   rapidjson::Document doc;
   char buffer[4096];
   rapidjson::FileReadStream fs(fp, buffer, sizeof(buffer));
   doc.ParseStream( fs );
   if( doc.HasParseError() ) {
      HLog << InfoError
           << "pattern file '" << fname << "' is not a valid JSON"
           << std::endl;
      HLog << InfoCont << "Error(offset "
           << static_cast<unsigned>(doc.GetErrorOffset())
           << "): " << GetParseError_En(doc.GetParseError())
           << std::endl;
      fclose(fp);
      return false;
   }
   fclose(fp);

   // Check if design/circuit is loaded beforehand.
   CircuitOnHost & cktOnHost = ((htDB::instance())->design()).cktOnHost();
   if( !cktOnHost.circuitLoaded() ) {
      HLog << InfoError
           << "please run 'read_circuit' first before reading patterns"
           << std::endl;
      return false;
   }

   // Various property checks
   // Version
   std::string version = doc["version"].GetString();
   std::vector< std::string > supported_versions =
      {"0.1"};
   if( !verifyPropertyString( supported_versions, version, "pattern version")) {
      return false;
   }

   // Circuit properties
   int num_pis = doc["setup"]["number_of_PIs"].GetInt();
   if( !verifyPropertyInt( numDataPIs(), num_pis, "num_of_PIs", true)) {
      return false;
   }
   int num_pos = doc["setup"]["number_of_POs"].GetInt();
   if( !verifyPropertyInt( numPOs(), num_pos, "num_of_POs", true)) {
      return false;
   }
   int num_ffs = doc["setup"]["number_of_FFs"].GetInt();
   if( !verifyPropertyInt( numFFs(), num_ffs, "num_of_FFs", true)) {
      return false;
   }
   std::string property_name("");
   unsigned t_fpi = doc["setup"]["cycle_timing_template"]["force_pi"].GetUint();
   property_name = "/setup/cycle_timing_template/force_pi";
   if( !verifyPropertyInt(cycleTiming().timeForcePI(), t_fpi, property_name,
                          /*against_design=*/false)) {
      return false;
   }
   unsigned t_iff = doc["setup"]["cycle_timing_template"]["init_ff"].GetInt();
   property_name = "/setup/cycle_timing_template/init_ff";
   if( !verifyPropertyInt(cycleTiming().timeInitFF(), t_iff, property_name,
                          /*against_design=*/false)) {
      return false;
   }
   unsigned t_mpo = doc["setup"]["cycle_timing_template"]["measure_po"].GetInt();
   property_name = "/setup/cycle_timing_template/measure_po";
   if( !verifyPropertyInt(cycleTiming().timeMeasurePO(), t_mpo, property_name,
                          /*against_design=*/false)) {
      return false;
   }
   unsigned t_clk = doc["setup"]["cycle_timing_template"]["pulse_clk"].GetInt();
   property_name = "/setup/cycle_timing_template/pulse_clk";
   if( !verifyPropertyInt(cycleTiming().timePulseClk(), t_clk, property_name,
                          /*against_design=*/false)) {
      return false;
   }
   unsigned t_off = doc["setup"]["cycle_timing_template"]["observe_ff"].GetInt();
   property_name = "/setup/cycle_timing_template/observe_ff";
   if( !verifyPropertyInt(cycleTiming().timeObserveFF(), t_off, property_name,
                          /*against_design=*/false)) {
      return false;
   }

   // Check number of patterns
   unsigned num_pat_ref = doc["setup"]["number_of_patterns"].GetUint();
   unsigned num_pat_st = doc["scan_test"].Capacity();
   if( num_pat_st != num_pat_ref ) {
      HLog << InfoError
           << "The number of patterns in /scan_test(" << num_pat_st << ") "
           << "does not match /setup/number_of_patterns(" << num_pat_ref << ")"
           <<  std::endl;
      return false;
   }

   for( unsigned ui=0; ui<num_pat_st; ui++ ) {
      if( !verifyIndividualPattern( ui, doc )) {
         return false;
      }
   }

   // Load patterns into memory once all verifications are done
   loadVerifiedPatterns( doc );

   HLog << InfoNote << "pattern file '" << fname << "' successfully loaded"
        << std::endl;

   return true;
}


void PatternSet::writePatterns( std::ostream& os ) {
    // Fill out a writer
    rapidjson::StringBuffer sb;
    rapidjson::PrettyWriter< rapidjson::StringBuffer > pwriter(sb);
    // The following is compact writer
    // rapidjson::Writer<StringBuffer> pwriter(sb);
    pwriter.SetIndent(' ', 2); // Set indentation

    pwriter.StartObject();
    pwriter.Key("version");
    pwriter.String( version().c_str() );
    pwriter.Key("setup");
    {
       pwriter.StartObject();
       pwriter.Key("number_of_PIs");
       pwriter.Int( numDataPIs() );
       pwriter.Key("number_of_POs");
       pwriter.Int( numPOs() );
       pwriter.Key("number_of_FFs");
       pwriter.Int( numFFs() );
       pwriter.Key("number_of_patterns");
       pwriter.Uint( numPatterns() );
       pwriter.Key("cycle_timing_template");
       {
          pwriter.StartObject();
          pwriter.Key("force_pi");
          pwriter.Uint( cycleTiming().timeForcePI() );
          pwriter.Key("init_ff");
          pwriter.Uint( cycleTiming().timeInitFF() );
          pwriter.Key("measure_po");
          pwriter.Uint( cycleTiming().timeMeasurePO() );
          pwriter.Key("pulse_clk");
          pwriter.Uint( cycleTiming().timePulseClk() );
          pwriter.Key("observe_ff");
          pwriter.Uint( cycleTiming().timeObserveFF() );
          pwriter.EndObject();
       }
       pwriter.EndObject();
    }
    pwriter.Key("scan_test");
    pwriter.StartArray();
    for( unsigned p=0; p<numPatterns(); p++ ) {
       Pattern & pat = _patterns[p];
       pwriter.StartObject();
       pwriter.Key("pattern");
       pwriter.Uint( p );
       pwriter.Key("cycles");
       pwriter.StartArray();
       for( unsigned c=0; c<pat.numCycles(); c++ ) {
          const PatternCycle & cyc = pat.cycle( c );
          std::string val("");
          pwriter.StartObject();
          pwriter.Key("cycle");
          pwriter.Uint( c );
          if( cyc.hasForcePI() ) {
             pwriter.Key("force_pi");
             cyc.getForcePI( val );
             pwriter.String( val.c_str() );
          }
          if( cyc.hasInitFF() ) {
             pwriter.Key("init_ff");
             cyc.getInitFF( val );
             pwriter.String( val.c_str() );
          }
          if( cyc.hasMeasurePO() ) {
             pwriter.Key("measure_po");
             cyc.getMeasurePO( val );
             pwriter.String( val.c_str() );
          }
          if( cyc.hasPulseClk() ) {
             pwriter.Key("pulse_clk");
             pwriter.StartArray();
             const std::vector< std::string > & clks = cyc.clkPins();
             typedef std::vector< std::string >::const_iterator VEC_STR_CONST_ITER;
             for( VEC_STR_CONST_ITER it=clks.begin(); it!=clks.end(); ++it ) {
                pwriter.String( it->c_str() );
             }
             pwriter.EndArray();
          }
          if( cyc.hasObserveFF() ) {
             pwriter.Key("observe_ff");
             cyc.getObserveFF( val );
             pwriter.String( val.c_str() );
          }

          pwriter.EndObject();
       }
       pwriter.EndArray();
       pwriter.EndObject();
    }
    pwriter.EndArray();

    pwriter.EndObject();

    os << sb.GetString() << std::endl;
}


void PatternSet::addPattern( const std::vector<uint64_t>& gmv0_in,
                             const std::vector<uint64_t>& gmv1_in,
                             const std::vector<uint64_t>& gmv0_out,
                             const std::vector<uint64_t>& gmv1_out,
                             uint64_t mask,
                             int n_cycles,
                             bool io_per_cycle )
{
   assert( gmv0_in.size() == gmv1_in.size() );
   assert( gmv0_out.size() == gmv1_out.size() );
   Pattern pat;
   unsigned pos_in = 0;  // index in gmv0/1_in
   unsigned pos_out = 0; // index in gmv0/1_out
   std::vector<uint64_t> bits_gmv0;  // temp vector for setting PatternCycle
   std::vector<uint64_t> bits_gmv1;
   for( int cycle=0; cycle < n_cycles; cycle++ ) {
      unsigned len_fpi=0, len_iff=0, len_mpo=0, len_off=0;
      std::vector< std::string > clk_pins;
      if( 0==cycle || io_per_cycle ) {
         len_fpi = numDataPIs();
      }
      if( 0==cycle) {
         len_iff = numFFs();
      }
      if( n_cycles-1 == cycle || io_per_cycle ) {
         len_mpo = numPOs();
      }
      if( n_cycles-1 == cycle ) { // last_cycle
         len_off = numFFs();
      }
      if( numFFs() ) {
         clk_pins.push_back("all");
      }
      PatternCycle pat_cycle( len_fpi, len_iff, len_mpo, len_off, clk_pins );
      unsigned len = len_fpi + len_iff + len_mpo + len_off;

      bits_gmv0.resize( len );
      bits_gmv1.resize( len );
      unsigned start_i = pos_in;
      unsigned   end_i = pos_in + len_fpi + len_iff;
      unsigned start_o = pos_out;
      unsigned   end_o = pos_out + len_mpo + len_off;

      // Copy corresponding values into bits_gmv0/1 for one pattern cycle
      // The order is force_pi, init_ff, measure_po, and observe_ff
      std::copy( &(gmv0_in[start_i]), &(gmv0_in[end_i]), bits_gmv0.begin());
      std::copy( &(gmv1_in[start_i]), &(gmv1_in[end_i]), bits_gmv1.begin());
      std::copy( &(gmv0_out[start_o]), &(gmv0_out[end_o]),
                 bits_gmv0.begin() + len_fpi + len_iff );
      std::copy( &(gmv1_out[start_o]), &(gmv1_out[end_o]),
                 bits_gmv1.begin() + len_fpi + len_iff );

      // set bitset of PatternCycle
      pat_cycle.setBitset( mask, bits_gmv0, bits_gmv1 );
      pat.addCycle( pat_cycle ); // Add one cycle into current pattern

      pos_in  = end_i; // update pos_in  for next cycle
      pos_out = end_o; // update pos_out for next cycle
   }
   addPattern( pat );
}


void PatternSet::addPattern(
                   const std::vector< std::vector<cl_ulong4> > & bits_v0_256,
                   const std::vector< std::vector<cl_ulong4> > & bits_v1_256,
                   int pos,
                   uint64_t mask,
                   bool io_per_cycle )
{
   assert( bits_v0_256.size() == bits_v1_256.size() );

   Pattern pat;
   for( unsigned cycle=0; cycle < bits_v0_256.size(); cycle++ ) {
      assert( bits_v0_256[cycle].size() == bits_v1_256[cycle].size() );
      unsigned len_fpi=0, len_iff=0, len_mpo=0, len_off=0;
      std::vector< std::string > clk_pins;
      if( 0==cycle || io_per_cycle ) {
         len_fpi = numDataPIs();
      }
      if( 0==cycle) {
         len_iff = numFFs();
      }
      if( bits_v0_256.size()-1 == cycle || io_per_cycle ) {
         len_mpo = numPOs();
      }
      if( bits_v0_256.size()-1 == cycle ) { // last_cycle
         len_off = numFFs();
      }
      assert( bits_v0_256[cycle].size() == len_fpi+len_iff+len_mpo+len_off );
      if( numFFs() ) {
         clk_pins.push_back("all");
      }
      PatternCycle pat_cycle( len_fpi, len_iff, len_mpo, len_off, clk_pins );
      pat_cycle.setBitset( pos, mask, bits_v0_256[cycle], bits_v1_256[cycle]);
      pat.addCycle( pat_cycle ); // Add one cycle into current pattern
   }
   addPattern( pat );
}


void PatternSet::addPattern(
                   const std::vector< std::vector<uint64_t> > & bits_v0,
                   const std::vector< std::vector<uint64_t> > & bits_v1,
                   uint64_t mask,
                   bool io_per_cycle )
{
   assert( bits_v0.size() == bits_v1.size() );

   Pattern pat;
   for( unsigned cycle=0; cycle < bits_v0.size(); cycle++ ) {
      assert( bits_v0[cycle].size() == bits_v1[cycle].size() );
      unsigned len_fpi=0, len_iff=0, len_mpo=0, len_off=0;
      std::vector< std::string > clk_pins;
      if( 0==cycle || io_per_cycle ) {
         len_fpi = numDataPIs();
      }
      if( 0==cycle) {
         len_iff = numFFs();
      }
      if( bits_v0.size()-1 == cycle || io_per_cycle ) {
         len_mpo = numPOs();
      }
      if( bits_v0.size()-1 == cycle ) { // last_cycle
         len_off = numFFs();
      }
      assert( bits_v0[cycle].size() == len_fpi+len_iff+len_mpo+len_off );
      if( numFFs() ) {
         clk_pins.push_back("all");
      }
      PatternCycle pat_cycle( len_fpi, len_iff, len_mpo, len_off, clk_pins );
      pat_cycle.setBitset( mask, bits_v0[cycle], bits_v1[cycle] );
      pat.addCycle( pat_cycle ); // Add one cycle into current pattern
   }
   addPattern( pat );
}


void PatternSet::getPatternStimuli( int start,
                                    int parallel,
                                    int cyc,
                                    std::vector<uint64_t>& vec0,
                                    std::vector<uint64_t>& vec1,
                                    bool & force_pi_is_on,
                                    std::vector<std::string>& clk_pins )
{
   vec0.clear();
   vec1.clear();
   clk_pins.clear();
   force_pi_is_on = false;

   // Determine the length of bit stream
   int len = 0;
   if( _patterns[start].cycle(cyc).hasForcePI() ) {
      len += numDataPIs();
      force_pi_is_on = true;
   }
   if( _patterns[start].cycle(cyc).hasInitFF() ) {
      len += numFFs();
   }
   vec0.resize( len );
   vec1.resize( len );

   // Get clk_pins
   clk_pins = _patterns[start].cycle(cyc).clkPins();

   uint64_t mask = 1;
   for( int p = start; p < start + parallel; p++ ) {
      Pattern& pat = _patterns[p];
      const PatternCycle& p_cycle = pat.cycle( cyc );
      p_cycle.bitset2vector( mask, vec0, vec1 );
      mask <<= 1; // left shift mask by 1
   }

}
