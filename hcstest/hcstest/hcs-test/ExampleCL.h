//////////////////////////////////////////////////////////////////
//                        ExampleCL.h
//
//  header file to show the example to run opencl program
//
//////////////////////////////////////////////////////////////////

#ifndef _EXAMPLE_CL_H
#define _EXAMPLE_CL_H

#include "OpenCLInfo.h"
#include "CircuitOnHost.h"

bool test_ocl_kernel_execution();

#endif
