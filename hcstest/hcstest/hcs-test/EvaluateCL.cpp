/////////////////////////////////////////////////////////////////
//                    EvaluateCL.cpp
/////////////////////////////////////////////////////////////////
#include "EvaluateCL.h"
#include "ExampleCL.h"
#include "HtSetting.h"
#include <functional>


// Print program build log on device
void oclDumpProgramBuildLog( cl_program program, cl_device_id device ) {
   size_t len=0;
   cl_int res=0;
   res = clGetProgramBuildInfo(program, device,
      CL_PROGRAM_BUILD_LOG, 0, NULL, &len);
   HLog << "  1st pass in getting CL program build info .....\n"
        << "    Return: " << oclErrorString(res) << ", length:" << len
        << std::endl;
   std::string buildLog( len+1, ' ' );
   res = clGetProgramBuildInfo(program, device,
      CL_PROGRAM_BUILD_LOG, len, &(buildLog[0]), NULL);
   HLog << "  2nd pass in getting CL program build info .....\n"
        << "    Return: " << oclErrorString(res)
        << std::endl;
   HLog << "  CL program build log: \n"
        << &(buildLog[0])  // use char* to fix unexpected non-ASCII ^@
        << std::endl;
}


#define DEBUG_MASK ( EVAL_CL_DBG_PAT_CYCLE | \
                     EVAL_CL_DBG_WORK_SIZE | \
                     EVAL_CL_DBG_KERNEL | \
                     EVAL_CL_DBG_GMV_COPY | \
                     EVAL_CL_DBG_IMG_PARA | \
                     EVAL_CL_DBG_TEST_IMG )

EvaluateCL::EvaluateCL() :  //_debugFlag( DEBUG_MASK ),
                            _debugFlag( 0 ), // use DEBUG_MASK for debugging
                            _shift( 0 ),
                            _mask( 0 ),
                            _numGsimEnqueues( 0 ),
                            _numGmvCopiesHost2Gpu( 0 ),
                            _numGmvCopiesGpu2Host( 0 ),
                            _elapsedSysTimeGmvHost2Gpu( 0 ),
                            _elapsedCpuTimeGmvHost2Gpu( 0 ),
                            _elapsedSysTimeGmvGpu2Host( 0 ),
                            _elapsedCpuTimeGmvGpu2Host( 0 ),
                            _patInSize( 0 ),
                            _patOutSize( 0 )
{
   ;
}


EvaluateCL::~EvaluateCL() {
   ;
}


bool EvaluateCL::setupPatternDataOnHostAndGpu( cl_uint pfm_num ) {

   Evaluate & eval = (htDB::instance())->eval();
   SimulationOptions & sim_opt = (htDB::instance())->simOptions();
   LevelizedNetlist & ckt = ((htDB::instance())->design()).simNetlist();

   bool io_per_cycle = sim_opt.ioOpPerCycle();
   int num_parallel_psets = sim_opt.numParallelPsetsOnGpu();
   const unsigned num_ffs = ckt.numFFs();
   const unsigned num_pis = ckt.numDataPIs();
   const unsigned num_pos = ckt.numPOs();
   const int n_cycles = eval.numCycles();


   // compute _patInSize and _patOutSize
   _patInSize  = num_ffs + num_pis;  // corresponds to init_ff + force_pi
   _patOutSize = num_ffs + num_pos;  // corresponds to obs_ff + measure_po
   if( io_per_cycle ) {
     _patInSize  += num_pis * (n_cycles-1); // additional force_pi
     _patOutSize += num_pos * (n_cycles-1); // additional measure_po
   }

   // Allocate memory on host to store pattern in and out
   _patInBackupGmv0.resize( _patInSize );
   _patInBackupGmv1.resize( _patInSize );
   const int pattern_copies = num_parallel_psets * 2;
   _patternsDataInGmv0.resize( pattern_copies );
   _patternsDataInGmv1.resize( pattern_copies );
   _patternsDataOutGmv0.resize( pattern_copies );
   _patternsDataOutGmv1.resize( pattern_copies );
   for( int i=0; i < pattern_copies; i++ ) {
      (_patternsDataInGmv0[i]).resize( _patInSize );
      (_patternsDataInGmv1[i]).resize( _patInSize );
      (_patternsDataOutGmv0[i]).resize( _patOutSize );
      (_patternsDataOutGmv1[i]).resize( _patOutSize );
   }

   // release old clBuffer for pattern data
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   cl_context context = ocl_info.platform( pfm_num ).context();
   OpenCLPlatform& pfm = ocl_info.platform( pfm_num );
   pfm.releaseOclMemForPatternData();

   // create new clBuffer for pattern data
   cl_int err = 0;
   for( int i=0; i < pattern_copies; i++ ) {
      pfm.clMemPatInGmv0(i) = clCreateBuffer(context,
                             CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                             _patInSize * sizeof(uint64_t),
                             &(_patternsDataInGmv0[i][0]),
                             &err );
      if( oclFunctionFailed( err, "create clMemPatInGmv0(i).")) {
         return false;
      }
      pfm.clMemPatInGmv1(i) = clCreateBuffer(context,
                             CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                             _patInSize * sizeof(uint64_t),
                             &(_patternsDataInGmv1[i][0]),
                             &err );
      if( oclFunctionFailed( err, "create clMemPatInGmv1(i).")) {
         return false;
      }
      pfm.clMemPatOutGmv0(i) = clCreateBuffer(context,
                             CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                             _patOutSize * sizeof(uint64_t),
                             &(_patternsDataOutGmv0[i][0]),
                             &err );
      if( oclFunctionFailed( err, "create clMemPatOutGmv0(i).")) {
         return false;
      }
      pfm.clMemPatOutGmv1(i) = clCreateBuffer(context,
                             CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                             _patOutSize * sizeof(uint64_t),
                             &(_patternsDataOutGmv1[i][0]),
                             &err );
      if( oclFunctionFailed( err, "create clMemPatOutGmv1(i).")) {
         return false;
      }
   }
   return true;
}


void EvaluateCL::resetOclMeasurements() {
   _numGsimEnqueues = 0;
   _numGmvCopiesHost2Gpu = 0;
   _numGmvCopiesGpu2Host = 0;
   _elapsedSysTimeGmvHost2Gpu = 0;
   _elapsedCpuTimeGmvHost2Gpu = 0;
   _elapsedSysTimeGmvGpu2Host = 0;
   _elapsedCpuTimeGmvGpu2Host = 0;
}


void EvaluateCL::computeCktImageSize( LevelizedNetlist& ckt,
                                      cl_int2& node_img_size,
                                      cl_int2& conn_img_size,
                                      int& shift,
                                      unsigned& mask )

{
   int num_nodes = ckt.numNodes();
   int image_width = 1;
   int one = 1;
   if( num_nodes <= (one << 6) ) {  // 2^6, 64
      image_width = 8;
   } else if( num_nodes <= (one << 8)) { // 2^8, 256
      image_width = 16;
   } else if( num_nodes <= (one << 10)) { // 2^10, 1024
      image_width = 32;
   } else if( num_nodes <= (one << 12)) { // 2^12, 4096
      image_width = 64;
   } else if( num_nodes <= (one << 14)) { // 2^14, 16384
      image_width = 128;
   } else if( num_nodes <= (one << 16)) { // 2^16, 65536
      image_width = 256;
   } else if( num_nodes <= (one << 18)) { // 2^18, 262144
      image_width = 512;
   } else if( num_nodes <= (one << 20)) { // 2^20
      image_width = 1024;
   } else if( num_nodes <= (one << 22)) { // 2^22
      image_width = 2048;
   } else if( num_nodes <= (one << 24)) { // 2^24
      image_width = 4096;
   } else if( num_nodes <= (one << 26)) { // 2^26
      image_width = 8192;
   } else if( num_nodes <= (one << 28)) { // 2^28
      image_width = 16384;
   } else {
      HLog << InfoError
           << "unsupported netlist size: " << num_nodes
           << std::endl;
   }

   int succs_size = (int)(ckt.succs().size());
   // image height is the ceiling of division
   int image_height1 = (num_nodes % image_width) ?
      (num_nodes/image_width + 1) : (num_nodes/image_width);
   int image_height2 = (succs_size % image_width) ?
      (succs_size/image_width + 1) : (succs_size/image_width);

   node_img_size.s[0] = image_width;
   node_img_size.s[1] = image_height1;
   conn_img_size.s[0] = image_width;
   conn_img_size.s[1] = image_height2;

   // Assume image_width is always power of 2
   mask = (unsigned)(node_img_size.s[0] - 1);
   shift = 0;
   for( unsigned m=mask; m>0; m >>= 1 ) {
      shift += 1;
   }
}


void EvaluateCL::printClEnqueueParameters(
                    cl_uint work_dim,
                    std::vector<size_t>& global_work_offset,
                    std::vector<size_t>& global_work_size,
                    std::vector<size_t>& local_work_size,
                    unsigned base_node_id,
                    unsigned padding_bound,
                    size_t effective_size )  const
{
   HLog << "\n"
        << "    work dimension: " << work_dim << "\n"
        << "global work offset: ("
        << global_work_offset[0] << ":"
        << global_work_offset[1] << ":"
        << global_work_offset[2] << ")" << "\n"
        << "  global work size: ("
        << global_work_size[0] << ":"
        << global_work_size[1] << ":"
        << global_work_size[2] << ")" << "\n"
        << "   local work size: ("
        << local_work_size[0] << ":"
        << local_work_size[1] << ":"
        << local_work_size[2] << ")" << "\n"
        << "      base node id: " << base_node_id << "\n"
        << "     padding bound: " << padding_bound << "\n"
        << "    effective size: " << effective_size
        << std::endl;
}


void EvaluateCL::allocateWorkSize( size_t remaining_work_items,
                                   const OpenCLDevice & cl_dev,
                                   cl_uint & work_dim,
                                   std::vector<size_t> & global_work_size,
                                   std::vector<size_t> & local_work_size,
                                   size_t & effective_size )
{
   assert( global_work_size.size() == cl_dev.maxWorkItemsDimens() );
   size_t max_global_work_size = cl_dev.maxGlobalWorkSize();
   // Allocate local work size
   for( unsigned dimen=0; dimen < cl_dev.maxWorkItemsDimens(); dimen++) {
      local_work_size[dimen] = cl_dev.localWorkSizes( dimen );
   }
   // Allocate global work size
   if( remaining_work_items >= max_global_work_size ) {
      effective_size = max_global_work_size;
      for( unsigned dimen=0; dimen < cl_dev.maxWorkItemsDimens(); dimen++) {
         global_work_size[dimen] = cl_dev.globalWorkSizes( dimen );
      }
   } else {
      effective_size = remaining_work_items;
      // compute global work size per dimension
      global_work_size[0] = cl_dev.globalWorkSizes( 0 );
      global_work_size[1] = effective_size / global_work_size[0];
      if( (effective_size % global_work_size[0]) > 0) {
         global_work_size[1] += 1;
      }
      global_work_size[2] = 1;
      if( global_work_size[1] > cl_dev.globalWorkSizes(1) ) {
         global_work_size[2] = global_work_size[1] / cl_dev.globalWorkSizes(1);
         if( (global_work_size[1] % cl_dev.globalWorkSizes(1)) > 0) {
            global_work_size[2] += 1;
         }
         // update global_work_size[1]
         global_work_size[1] = cl_dev.globalWorkSizes(1);
      }
   }
   // Compute work_dim
   work_dim = 3;
   if( 1==global_work_size[2]  ) { work_dim -= 1; }
   if( 1==global_work_size[1]  ) { work_dim -= 1; }
}


bool EvaluateCL::checkKernelWorkGroupSize( cl_kernel kernel,
                                           cl_device_id device,
                                           size_t local_work_size )
{
   // get kernel work group size
   size_t kernel_work_group_size = 0;
   cl_int err = 0;

   err = clGetKernelWorkGroupInfo( kernel, device,
      CL_KERNEL_WORK_GROUP_SIZE, sizeof(size_t),
      &kernel_work_group_size, NULL);
   if( debugFlag() & EVAL_CL_DBG_WORK_SIZE ) {
      HLog << "  kernel wkgp size: " << kernel_work_group_size << std::endl;
   }
   if( oclFunctionFailed( err,"get kernel_work_group_size.")) {
      return false;
   }
   if( kernel_work_group_size < local_work_size ) {
      HLog << InfoError
           << "local work size (" << local_work_size << ") "
           << "exceeds kernel work group size (" << kernel_work_group_size
           << ")" << std::endl;
      return false;
   }
   return true;
}


bool EvaluateCL::createAndBuildProgramOnPfm( cl_uint pfm_num,
                                             const std::string& file_name )
{
   // retrieve ocl platform and context
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform& pfm = ocl_info.platform( pfm_num );
   cl_context  context = ocl_info.platform( pfm_num ).context();
   cl_program& program = ocl_info.platform( pfm_num ).program();
   // context must be created but program is not
   assert( context );
   assert( program==NULL );
   // Create OpenCL program from Evaluate.cl kernel source
   std::string exec_path = (appCommonDB::instance())->execPath();
   std::string file_path = exec_path + file_name; // "Evaluate.cl";
   // HLog << "CL file: " << file_path << std::endl;
   std::ifstream cl_file( file_path, std::ios::in);
   if( !cl_file.is_open() ) {
      HLog << "failed to open " << file_path << std::endl;
      return false;
   }
   std::ostringstream oss;
   oss << cl_file.rdbuf();
   std::string srcStdStr = oss.str();
   const char* srcStr = srcStdStr.c_str();
   cl_int err=0;
   program = clCreateProgramWithSource(context,
                                        1,
                                        (const char**)&srcStr,
                                        NULL,
                                        &err );
   if( oclFunctionFailed( err, "fail to create program.")) return false;

   // Build program on specified device
   //   err = clBuildProgram(program, 1, &device, NULL, NULL, NULL);
   // Build the program for all devices on the context
   HLog << InfoNote
        << "build OpenCL program '" << file_name << "'"
        << std::endl;
   std::string include_dirs = "-I" + exec_path;
   include_dirs = include_dirs + " -I" + exec_path + "../app-common/";
   //std::string options_str = include_dirs + " -g"; // add debug options
   //std::string options_str = include_dirs + " -cl-opt-disable";
   std::string options_str = include_dirs;
   //err = clBuildProgram( program, 0, NULL, options_str.c_str(), NULL, NULL);
   // CL_INVALID_BINARY error on building program on NVS-315 with opencl 1.1
   for( cl_uint d=0; d < pfm.numDevices(); d++ ) {
      OpenCLDevice & device = pfm.device(d); // pick up the device
      cl_device_id dev_id = pfm.device(d).deviceId(); // pick up the device
      const std::string opencl_c_version = device.deviceOpenclCVersion();
      // only build program on devices with opencl c 1.2 support or higher
      if(!(opencl_c_version.find("OpenCL C 1.1") != std::string::npos) ||
         (opencl_c_version.find("OpenCL C 1.0") != std::string::npos) ) {
         HLog << InfoNote
              << "retrieve building log on " << device.deviceName()
              << std::endl;
         err = clBuildProgram( program, 1, pfm.ptrToDeviceId(d),
                               options_str.c_str(), NULL, NULL);
         // Print build log even when it succeeds as warning message
         //   in the build log can cause problem and need to be fixed
         oclDumpProgramBuildLog( program, dev_id );
      } else {
        HLog << InfoWarn
             << "device " << device.deviceName() << " of '"
             << opencl_c_version << "' is not supported (<1.2)"
             << std::endl;
      }
   }
   // Check program build failure
   if( oclFunctionFailed( err, "fail to build program.")) {
      cl_int err2 = clReleaseProgram(program);
      oclFunctionFailed( err2, "fail to release program after build failure");
      program = NULL;
      return false;
   }
   return true;
}


bool EvaluateCL::setupSimulationOnOclDevice( cl_uint pfm_num,
                                             cl_uint dev_num,
                                             LevelizedNetlist& ckt )
{
   HLog << InfoNote
        << "entry point for simulating patterns on GPU"
        << std::endl;
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform& pfm = ocl_info.platform( pfm_num );
   OpenCLDevice& dev = pfm.device( dev_num );
   SimulationOptions & sim_opt = (htDB::instance())->simOptions();
   LogicSimMethod lsim_type = sim_opt.logicSimMethod();
   //test_ocl_kernel_execution();
   if( false==ocl_info.checkContextAndQueue( pfm_num, dev_num )) {
      return false;
   }

   // Compute image size first
   computeCktImageSize( ckt, _nodeImageSize, _connImageSize, _shift, _mask );

   if( false==pfm.cktPfmResourcesCreated() ) {
      if( setupCktMemObjAndKernelsOnPfm( pfm_num, ckt )) {
         ; // do sth to release unused resources
      }
      if( setupGmvBufAndSimKernelsOnPfm( pfm_num, ckt )) {
         ; // do sth to release unused resources
      }
      pfm.cktPfmResourcesCreated( true ); // set the flag
      if( sim_opt.isEventDriven() ) {
         initXBackgroundAndBuffersOnDev( pfm_num, dev_num, ckt);
      }
      initTieGatesOnDev( pfm_num, dev_num, ckt, lsim_type );
   }

   // Only create circuit image when cktClMemType is IMAGE
   if( false==dev.cktDevResourcesCreated() && CKT_IMAGE==dev.cktClMemType()) {
      if( initCktImageOnDev( pfm_num, dev_num, ckt )) {
         ; // do sth to release unused resources
      }
      dev.cktDevResourcesCreated( true ); // set the flag
   }
   if( debugFlag() & EVAL_CL_DBG_GMV_COPY) {
      testGmvCopy( pfm_num, dev_num );
   }
   return true;
}


bool EvaluateCL::setupCktMemObjAndKernelsOnPfm( cl_uint pfm_num,
                                                LevelizedNetlist& ckt )
{
   // retrieve ocl platform and context
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform& pfm = ocl_info.platform( pfm_num );
   cl_context context = ocl_info.platform( pfm_num ).context();
   assert( context );

   cl_mem& buf_q_e_flags = pfm.clMemCkt( CL_BUF_Q_EVENTS_FLAGS );
   cl_mem& buf_q_offsets = pfm.clMemCkt( CL_BUF_Q_OFFSETS );
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   cl_mem& buf_q_flags   = pfm.clMemCkt( CL_BUF_Q_FLAGS );
   cl_mem& buf_dinputs   = pfm.clMemCkt( CL_BUF_DATA_INPUTS );
   cl_mem& buf_clkinfo   = pfm.clMemCkt( CL_BUF_CLOCKS_AND_OFFSTATES );
   cl_mem& buf_outputs   = pfm.clMemCkt( CL_BUF_OUTPUTS );
   cl_mem& buf_types     = pfm.clMemCkt( CL_BUF_TYPES );
   cl_mem& buf_levels    = pfm.clMemCkt( CL_BUF_LEVELS );
   cl_mem& buf_preds     = pfm.clMemCkt( CL_BUF_PREDS );
   cl_mem& buf_pred_1st  = pfm.clMemCkt( CL_BUF_PRED1ST );
   cl_mem& buf_succs     = pfm.clMemCkt( CL_BUF_SUCCS );
   cl_mem& buf_succ_1st  = pfm.clMemCkt( CL_BUF_SUCC1ST );
   cl_int err = 0;

   // Adjust netlist_buffer_flags accordingly for CPU/iGPU and dGPU
   cl_mem_flags netlist_buffer_flags = 0;
   if( pfm.platformVendor().find("Intel") != std::string::npos ) {
      netlist_buffer_flags = CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR;
   } else {
      netlist_buffer_flags = CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR;
   }
   // create cl buffer for qEVentsFlags[]
   buf_q_e_flags = clCreateBuffer(context,
                              netlist_buffer_flags,
                              ckt.qEventsFlags().size() * sizeof(unsigned),
                              &(ckt.qEventsFlags()[0]),
                              &err );
   if( oclFunctionFailed( err, "create buf_q_e_flags.")) { return false; }
   // create cl buffer for qOffsets[]
   buf_q_offsets = clCreateBuffer(context,
                              netlist_buffer_flags,
                              ckt.qOffsets().size() * sizeof(int),
                              &(ckt.qOffsets()[0]),
                              &err );
   if( oclFunctionFailed( err, "create buf_q_offsets.")) { return false; }
   // create cl buffer for qSizes[]
   buf_q_sizes = clCreateBuffer(context,
                       netlist_buffer_flags,
                       ckt.qSizes().size() * sizeof(int),
                       &(ckt.qSizes()[0]),
                       &err );
   if( oclFunctionFailed( err, "create buf_q_sizes.")) { return false; }
   // create cl buffer for qFlags[]
   buf_q_flags = clCreateBuffer(context,
                       netlist_buffer_flags,
                       ckt.qFlags().size() * sizeof(char),
                       &(ckt.qFlags()[0]),
                       &err );
   if( oclFunctionFailed( err, "create buf_q_flags.")) { return false; }
   // create cl buffer for dataInputNodes[]
   if( ckt.dataInputNodes().size() ) { // only do it if there are >=1 outputs
      buf_dinputs = clCreateBuffer(context,
                       netlist_buffer_flags,
                       ckt.dataInputNodes().size() * sizeof(int),
                       &(ckt.dataInputNodes()[0]),
                       &err );
   }
   if( oclFunctionFailed( err, "create buf_dinputs.")) { return false; }
   // create cl buffer for clockInputsAndOffstates[]
   if( ckt.clockInputsAndOffstates().size() ) { // only do it if >0 clocks
      buf_clkinfo = clCreateBuffer(context,
                       netlist_buffer_flags,
                       ckt.clockInputsAndOffstates().size() * sizeof(int),
                       &(ckt.clockInputsAndOffstates()[0]),
                       &err );
   }
   if( oclFunctionFailed( err, "create buf_clkinfo.")) { return false; }
   // create cl buffer for outputNodes[]
   if( ckt.outputNodes().size() ) { // only do it if there are >=1 outputs
      buf_outputs = clCreateBuffer(context,
                              netlist_buffer_flags,
                              ckt.outputNodes().size() * sizeof(int),
                              &(ckt.outputNodes()[0]),
                              &err );
   }
   if( oclFunctionFailed( err, "create buf_outputs.")) { return false; }
   // create cl buffer for types[]
   buf_types = clCreateBuffer(context,
                              netlist_buffer_flags,
                              ckt.types().size() * sizeof(int),
                              &(ckt.types()[0]),
                              &err );
   if( oclFunctionFailed( err, "create buf_types.")) { return false; }
   // create cl buffer for levels[]
   buf_levels = clCreateBuffer(context,
                              netlist_buffer_flags,
                              ckt.levels().size() * sizeof(int),
                              &(ckt.levels()[0]),
                              &err );
   if( oclFunctionFailed( err, "create buf_levels.")) { return false; }
   // create cl buffer for preds[]
   buf_preds = clCreateBuffer(context,
                              netlist_buffer_flags,
                              ckt.preds().size() * sizeof(int),
                              &(ckt.preds()[0]),
                              &err );
   if( oclFunctionFailed( err, "create buf_preds.")) { return false; }
   // create cl buffer for predFirst[]
   buf_pred_1st = clCreateBuffer(context,
                              netlist_buffer_flags,
                              ckt.predFirst().size() * sizeof(int),
                              &(ckt.predFirst()[0]),
                              &err );
   if( oclFunctionFailed( err, "create buf_pred_1st.")) { return false; }
   // create cl buffer for succs[]
   buf_succs = clCreateBuffer(context,
                              netlist_buffer_flags,
                              ckt.succs().size() * sizeof(int),
                              &(ckt.succs()[0]),
                              &err );
   if( oclFunctionFailed( err, "create buf_succs.")) { return false; }
   // create cl buffer for succFirst[]
   buf_succ_1st = clCreateBuffer(context,
                              netlist_buffer_flags,
                              ckt.succFirst().size() * sizeof(int),
                              &(ckt.succFirst()[0]),
                              &err );
   if( oclFunctionFailed( err, "create buf_succ_1st.")) { return false; }

   // Get node_img_size and conn_img_size
   cl_int2 node_img_size = nodeImageSize();
   cl_int2 conn_img_size = connImageSize();

   cl_uint pitch_alignment = 128; // 512;
   int intermediate_image_pitch =
      pitch_alignment * (1 + ((node_img_size.s[0] - 1) / pitch_alignment));

   if( debugFlag() & EVAL_CL_DBG_IMG_PARA ) { // print dbg info if needed
      HLog << "intermediate_image_pitch: "
           << intermediate_image_pitch << std::endl;
   }

   // Declare node image format
   cl_image_format nodeImgFormat;
   nodeImgFormat.image_channel_order = CL_RGBA;
   // other channel data type: CL_SIGNED/UNSIGNED_INT32/16/8, CL_UNORM_INT8;
   nodeImgFormat.image_channel_data_type = CL_SIGNED_INT32;
   // Declare node image description
   cl_image_desc nodeImgDesc;
   nodeImgDesc.image_type = CL_MEM_OBJECT_IMAGE2D;
   nodeImgDesc.image_width = node_img_size.s[0];
   nodeImgDesc.image_height = node_img_size.s[1];
   nodeImgDesc.image_row_pitch = intermediate_image_pitch * sizeof(cl_int4);
   nodeImgDesc.image_slice_pitch = 0;
   nodeImgDesc.num_mip_levels = 0;
   nodeImgDesc.num_samples = 0;
   nodeImgDesc.buffer = NULL;

   cl_mem& node_image = pfm.clMemCkt( CL_IMG_NODE );
   // allocate space for node_image_io_buffer
   _cktNodeImgIoBuf.resize( node_img_size.s[0] * node_img_size.s[1] );
   node_image = clCreateImage(context,
                            CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                            &nodeImgFormat,
                            &nodeImgDesc,
                            &(_cktNodeImgIoBuf[0]),
                            &err );
   if( oclFunctionFailed( err, "create node_image.")) { return false; }
   assert( conn_img_size.s[0] > 0 && conn_img_size.s[1] > 0 );
   assert( conn_img_size.s[0]==node_img_size.s[0] );
   // Declare conn image format
   cl_image_format connImgFormat;
   connImgFormat.image_channel_order = CL_RGBA;
   // other channel data type: CL_SIGNED/UNSIGNED_INT32/16/8, CL_UNORM_INT8;
   connImgFormat.image_channel_data_type = CL_UNSIGNED_INT16;
   // Declare conn image description
   cl_image_desc connImgDesc;
   connImgDesc.image_type = CL_MEM_OBJECT_IMAGE2D;
   connImgDesc.image_width = conn_img_size.s[0];
   connImgDesc.image_height = conn_img_size.s[1];
   connImgDesc.image_row_pitch =
      intermediate_image_pitch * sizeof(cl_ushort4);
   connImgDesc.image_slice_pitch = 0;
   connImgDesc.num_mip_levels = 0;
   connImgDesc.num_samples = 0;
   connImgDesc.buffer = NULL;
   cl_mem& conn_image = pfm.clMemCkt( CL_IMG_CONN );
   // allocate space for conn_image_io_buffer
   _cktConnImgIoBuf.resize( conn_img_size.s[0] * conn_img_size.s[1] );
   conn_image = clCreateImage(context,
                            CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                            &connImgFormat,
                            &connImgDesc,
                            &(_cktConnImgIoBuf[0]),
                            &err );
   if( oclFunctionFailed( err, "create conn_image.")) { return false; }

   // retrieve ocl program
   cl_program program = ocl_info.platform( pfm_num ).program();
   assert( program );

   // create ocl kernel
   cl_kernel& kn_node_img = pfm.kernel( KERNEL_INIT_NODE_IMG );
   cl_kernel& kn_conn_img = pfm.kernel( KERNEL_INIT_CONN_IMG );
   kn_node_img = clCreateKernel( program, "kernel_init_node_img", &err );
   if( oclFunctionFailed( err,"create kernel_init_node_img.")) { return false;}
   kn_conn_img = clCreateKernel( program, "kernel_init_conn_img", &err );
   if( oclFunctionFailed( err,"create kernel_init_conn_img.")) { return false;}
   // Assume image_width is always power of 2
   unsigned mask = xMask();
   int shift = yShift();
   if( debugFlag() & EVAL_CL_DBG_IMG_PARA ) { // print dbg info if needed
      HLog << "shift: " << shift << "   mask: " << mask << std::endl; // Dbg
   }
   // set kernel arguments for kn_node_img
   err  = clSetKernelArg(kn_node_img, 0, sizeof(cl_mem), &node_image);
   err |= clSetKernelArg(kn_node_img, 1, sizeof(cl_mem), &buf_types);
   err |= clSetKernelArg(kn_node_img, 2, sizeof(cl_mem), &buf_levels);
   err |= clSetKernelArg(kn_node_img, 3, sizeof(cl_mem), &buf_pred_1st);
   err |= clSetKernelArg(kn_node_img, 4, sizeof(cl_mem), &buf_succ_1st);
   err |= clSetKernelArg(kn_node_img, 5, sizeof(int), &shift);
   err |= clSetKernelArg(kn_node_img, 6, sizeof(unsigned), &mask);
   if( oclFunctionFailed(err,"set args for kernel_init_node_img")) {
      return false;
   }
   // set kernel arguments for kn_conn_img
   err  = clSetKernelArg(kn_conn_img, 0, sizeof(cl_mem), &conn_image);
   err |= clSetKernelArg(kn_conn_img, 1, sizeof(cl_mem), &buf_preds);
   err |= clSetKernelArg(kn_conn_img, 2, sizeof(cl_mem), &buf_succs);
   err |= clSetKernelArg(kn_conn_img, 3, sizeof(int), &shift);
   err |= clSetKernelArg(kn_conn_img, 4, sizeof(unsigned), &mask);
   if( oclFunctionFailed(err,"set args for kernel_init_conn_img")) {
      return false;
   }
   return true;
}


bool EvaluateCL::initXBackgroundAndBuffersOnDev( cl_uint pfm_num,
                                                 cl_uint dev_num,
                                                 LevelizedNetlist& ckt )
{
   // retrieve ocl platform and context
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform& pfm = ocl_info.platform( pfm_num );
   cl_context context = ocl_info.platform( pfm_num ).context();
   assert( context );
   // retrieve ocl command queue
   cl_command_queue queue = ocl_info.platform(pfm_num).device(dev_num).queue();
   //OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device = ocl_info.platform(pfm_num).device(dev_num).deviceId();

   assert( queue );
   assert( device );

   cl_int err = 0;

   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   uint64_t all1s64 = ALL_ONES_64;
   uint64_t all0s64 = 0;
   char all0s8 = 0;
   err |= clEnqueueFillBuffer( queue, buf_gmv0, &all1s64, sizeof(uint64_t),
             0, sizeof(uint64_t)*ckt.numNodes(), 0, NULL, NULL );
   err |= clEnqueueFillBuffer( queue, buf_gmv1, &all0s64,sizeof(uint64_t),
             0, sizeof(uint64_t)*ckt.numNodes(), 0, NULL, NULL );

   cl_mem& buf_q_e_flags = pfm.clMemCkt( CL_BUF_Q_EVENTS_FLAGS );
   // cl_mem& buf_q_offsets = pfm.clMemCkt( CL_BUF_Q_OFFSETS );
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   cl_mem& buf_q_flags   = pfm.clMemCkt( CL_BUF_Q_FLAGS );
   unsigned all0s32 = 0;
   err |= clEnqueueFillBuffer( queue, buf_q_e_flags,&all0s32,sizeof(unsigned),
             0, sizeof(unsigned)*ckt.qEventsFlags().size(), 0, NULL, NULL );
   err |= clEnqueueFillBuffer( queue, buf_q_sizes,&all0s32,sizeof(unsigned),
             0, sizeof(unsigned)*ckt.qSizes().size(), 0, NULL, NULL );
   err |= clEnqueueFillBuffer( queue, buf_q_flags,&all0s8,sizeof(char),
             0, sizeof(char)*ckt.qFlags().size(), 0, NULL, NULL );
   if( oclFunctionFailed(err,"fill buffer in init_x_background and queues")) {
      return false;
   }

   HLog << InfoNote << "x-background and buffers are initialized successfully"
        << std::endl;
#ifdef HT_DEBUG
   //HLog << "### post init x background " << std::endl; // LL debug
   //dumpStatusOfSimEvents( 0, pfm_num, dev_num, ckt ); // LL debug
#endif
   return true;
}


bool EvaluateCL::initTieGatesOnDev( cl_uint pfm_num,
                                    cl_uint dev_num,
                                    LevelizedNetlist& ckt,
                                    LogicSimMethod lsim_type )
{
   if( ckt.numTIEs()==0) {
      return true; // return immediately if there is no tie gate
   }
   // retrieve ocl platform and context
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform& pfm = ocl_info.platform( pfm_num );
   cl_context context = ocl_info.platform( pfm_num ).context();
   assert( context );
   // retrieve ocl command queue
   cl_command_queue queue = ocl_info.platform(pfm_num).device(dev_num).queue();
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device = ocl_info.platform(pfm_num).device(dev_num).deviceId();

   assert( queue );
   assert( device );

   cl_kernel& kn_init_ties = (EVENT_FLAGS_BASED == lsim_type) ?
                             pfm.kernel( KERNEL_INIT_TIE_GATES_FLAG ) :
                             pfm.kernel( KERNEL_INIT_TIE_GATES );

   cl_int err = 0;

   int event_driven = (IN_ORDER != lsim_type) ? 1 : 0;
   // starting kernel parameter index
   const int spi = (EVENT_FLAGS_BASED == lsim_type) ? 12 : 14;
   err |= clSetKernelArg(kn_init_ties,spi, sizeof(int),&event_driven);

   std::vector< cl_event > events_tie;
   HLog << InfoNote
        << "enqueue kernel to initialize tie gates"
        << std::endl;
   size_t remainingWorks = (size_t)(ckt.numTIEs());
   unsigned base_node_id = (size_t)(ckt.firstNodeTIE());

   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   while( remainingWorks > 0 ) {
      size_t effective_size = 0;
      cl_uint work_dim = 0;
      allocateWorkSize( remainingWorks, cl_dev, work_dim,
                        globalWorkSize, localWorkSize, effective_size );
      unsigned padding_bound = (unsigned)(base_node_id + effective_size - 1);
      err = clSetKernelArg(kn_init_ties,spi+1,sizeof(unsigned),&base_node_id);
      err|= clSetKernelArg(kn_init_ties,spi+2,sizeof(unsigned),&padding_bound);
      if( oclFunctionFailed( err, "set args kn_init_tie_gates(15,16)")) {
         return false;
      }
      if( debugFlag() & EVAL_CL_DBG_WORK_SIZE ) {
         printClEnqueueParameters( work_dim, globalWorkOffset, globalWorkSize,
            localWorkSize, base_node_id, padding_bound, effective_size );
      }
      if( !checkKernelWorkGroupSize( kn_init_ties,device,localWorkSize[0])) {
         return false;
      }
      cl_event cur_event;
      err = clEnqueueNDRangeKernel(
                      queue, kn_init_ties, work_dim,
                      &(globalWorkOffset[0]),
                      &(globalWorkSize[0]),
                      &(localWorkSize[0]),
                      0, // num_events_in_wait_list
                      NULL, // event_wait_list
                      &cur_event ); // event
      if( oclFunctionFailed(err,"enqueue kernel_init_tie_gates")) {
         return false;
      }
      events_tie.push_back( cur_event );
      // update base_node_id and remainingWorks
      base_node_id += (unsigned)effective_size;
      remainingWorks -= effective_size;
   }
   err = clWaitForEvents( events_tie.size(), &(events_tie[0]) );
   if( oclFunctionFailed(err,"wait for events in initializing tie gates")) {
      return false;
   }
   HLog << InfoNote
        << ckt.numTIEs() << " TIE gates are initialized successfully"
        << std::endl;
#ifdef HT_DEBUG
   //HLog << "### post init TIE gates " << std::endl; // LL debug
   //dumpStatusOfSimEvents( 0, pfm_num, dev_num, ckt ); // LL debug
#endif
   return true;
}


bool EvaluateCL::initCktImageOnDev( cl_uint pfm_num,
                                    cl_uint dev_num,
                                    LevelizedNetlist& ckt )
{
   // retrieve ocl platform and context
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform& pfm = ocl_info.platform( pfm_num );
   cl_context context = ocl_info.platform( pfm_num ).context();
   assert( context );
   // retrieve ocl command queue
   cl_command_queue queue = ocl_info.platform(pfm_num).device(dev_num).queue();
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device = ocl_info.platform(pfm_num).device(dev_num).deviceId();

   assert( queue );
   assert( device );

   cl_mem& node_image = pfm.clMemCkt( CL_IMG_NODE );
   cl_mem& conn_image = pfm.clMemCkt( CL_IMG_CONN );
   cl_kernel& kn_node_img = pfm.kernel( KERNEL_INIT_NODE_IMG );
   cl_kernel& kn_conn_img = pfm.kernel( KERNEL_INIT_CONN_IMG );

   cl_int err = 0;
   std::vector< cl_event > events_ickt;
   HLog << InfoNote
        << "enqueue kernel to create circuit node image"
        << std::endl;
   size_t remainingWorks = (size_t)(ckt.numNodes() + 1);
   unsigned base_node_id = 0;
   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   while( remainingWorks > 0 ) {
      size_t effective_size = 0;
      cl_uint work_dim = 0;
      allocateWorkSize( remainingWorks, cl_dev, work_dim,
                        globalWorkSize, localWorkSize, effective_size );
      unsigned padding_bound = (unsigned)(base_node_id + effective_size - 1);
      err = clSetKernelArg(kn_node_img, 7, sizeof(unsigned), &base_node_id);
      err|= clSetKernelArg(kn_node_img, 8, sizeof(unsigned), &padding_bound);
      if( oclFunctionFailed( err,
             "set base_node_id and padding_bound for kn_node_img")) {
         return false;
      }
      if( debugFlag() & EVAL_CL_DBG_WORK_SIZE ) {
         printClEnqueueParameters( work_dim, globalWorkOffset, globalWorkSize,
            localWorkSize, base_node_id, padding_bound, effective_size );
      }
      if( !checkKernelWorkGroupSize( kn_node_img, device, localWorkSize[0])) {
         return false;
      }
      cl_event cur_event;
      err = clEnqueueNDRangeKernel(
                      queue, kn_node_img, work_dim,
                      &(globalWorkOffset[0]),
                      &(globalWorkSize[0]),
                      &(localWorkSize[0]),
                      0, // num_events_in_wait_list
                      NULL, // event_wait_list
                      &cur_event ); // event
      if( oclFunctionFailed(err,"enqueue kernel_init_node_img")) {
         return false;
      }
      events_ickt.push_back( cur_event );
      // update base_node_id and remainingWorks
      base_node_id += (unsigned)effective_size;
      remainingWorks -= effective_size;
   }

   // Initialize connection image
   base_node_id = 0;
   remainingWorks = (size_t)(ckt.succs().size() + 1);
   std::fill( globalWorkOffset.begin(), globalWorkOffset.end(), 0);
   std::fill( globalWorkSize.begin(), globalWorkSize.end(), 0);
   std::fill( localWorkSize.begin(), localWorkSize.end(), 0);
   HLog << InfoNote
        << "enqueue kernel to create circuit connection image"
        << std::endl;
   while( remainingWorks > 0 ) {
      size_t effective_size = 0;
      cl_uint work_dim = 0;
      allocateWorkSize( remainingWorks, cl_dev, work_dim,
                        globalWorkSize, localWorkSize, effective_size );
      unsigned padding_bound = (unsigned)(base_node_id + effective_size - 1);
      err = clSetKernelArg(kn_conn_img, 5, sizeof(unsigned), &base_node_id);
      err|= clSetKernelArg(kn_conn_img, 6, sizeof(unsigned), &padding_bound);
      if( oclFunctionFailed( err,
             "set base_node_id and padding_bound for kn_conn_img")) {
         return false;
      }
      if( debugFlag() & EVAL_CL_DBG_WORK_SIZE ) {
         printClEnqueueParameters( work_dim, globalWorkOffset, globalWorkSize,
            localWorkSize, base_node_id, padding_bound, effective_size );
      }

      if( !checkKernelWorkGroupSize( kn_conn_img, device, localWorkSize[0])) {
         return false;
      }
      cl_event cur_event;
      err = clEnqueueNDRangeKernel(
                      queue, kn_conn_img, work_dim,
                      &(globalWorkOffset[0]),
                      &(globalWorkSize[0]),
                      &(localWorkSize[0]),
                      0, // num_events_in_wait_list
                      NULL, // event_wait_list
                      &cur_event ); // event
      if( oclFunctionFailed(err,"enqueue kernel_init_conn_img")) {
         return false;
      }
      events_ickt.push_back( cur_event );
      // update base_node_id and remainingWorks
      base_node_id += (unsigned)effective_size;
      remainingWorks -= effective_size;
   }

   // Wait for completion of circuit image creation
   err = clWaitForEvents( events_ickt.size(), &(events_ickt[0]) );
   if( oclFunctionFailed(err,"wait for events in circuit img creation")) {
      return false;
   } else {
      HLog << InfoNote
           << "image objects for circuit are created successfully"
           << std::endl;
   }
   //
   // test ckt image
   if( debugFlag() & EVAL_CL_DBG_TEST_IMG ) {
      verifyCircuitImage( pfm_num, dev_num, ckt, node_image, conn_image );
   }
   return true;
}


bool EvaluateCL::verifyCircuitImage( cl_uint pfm_num,
                                     cl_uint dev_num,
                                     LevelizedNetlist& ckt,
                                     cl_mem& node_image,
                                     cl_mem& conn_image )
{
   // retrieve ocl context, queue, device, and program
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_context context = ocl_info.platform( pfm_num ).context();
   assert( context );
   cl_command_queue queue = ocl_info.platform(pfm_num).device(dev_num).queue();
   assert( queue );
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device = ocl_info.platform(pfm_num).device(dev_num).deviceId();
   assert( device );
   cl_program program = ocl_info.platform( pfm_num ).program();
   assert( program );

   cl_mem& buf_rdbk_types = pfm.clMemCkt( CL_BUF_TYPES_RDBK );
   cl_mem& buf_rdbk_levels = pfm.clMemCkt( CL_BUF_LEVELS_RDBK );
   cl_mem& buf_rdbk_preds = pfm.clMemCkt( CL_BUF_PREDS_RDBK );
   cl_mem& buf_rdbk_pred1st = pfm.clMemCkt( CL_BUF_PRED1ST_RDBK );
   cl_mem& buf_rdbk_succs = pfm.clMemCkt( CL_BUF_SUCCS_RDBK );
   cl_mem& buf_rdbk_succ1st = pfm.clMemCkt( CL_BUF_SUCC1ST_RDBK );
   cl_int err = 0;
   // create cl buffer for readback_types[]
   std::vector<int> rdbk_types( ckt.types().size(), 0 );
   buf_rdbk_types = clCreateBuffer(context,
                              CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                              ckt.types().size() * sizeof(int),
                              &(rdbk_types[0]),
                              &err );
   if( oclFunctionFailed( err, "create readback_types.")) {
      return false;
   }
   // create cl buffer for readback_levels[]
   std::vector<int> rdbk_levels( ckt.levels().size(), 0 );
   buf_rdbk_levels = clCreateBuffer(context,
                              CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                              ckt.levels().size() * sizeof(int),
                              &(rdbk_levels[0]),
                              &err );
   if( oclFunctionFailed( err, "create readback_levels.")) {
      return false;
   }
   // create cl buffer for readback_preds[]
   std::vector<int> rdbk_preds( ckt.preds().size(), 0 );
   buf_rdbk_preds = clCreateBuffer(context,
                              CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                              ckt.preds().size() * sizeof(int),
                              &(rdbk_preds[0]),
                              &err );
   if( oclFunctionFailed( err, "create readback_preds.")) {
      return false;
   }
   // create cl buffer for readback_predFirst[]
   std::vector<int> rdbk_pred1st( ckt.predFirst().size(), 0 );
   buf_rdbk_pred1st = clCreateBuffer(context,
                              CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                              ckt.predFirst().size() * sizeof(int),
                              &(rdbk_pred1st[0]),
                              &err );
   if( oclFunctionFailed( err, "create readback_pred_1st.")) {
      return false;
   }
   // create cl buffer for readback_succs[]
   std::vector<int> rdbk_succs( ckt.succs().size(), 0 );
   buf_rdbk_succs = clCreateBuffer(context,
                              CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                              ckt.succs().size() * sizeof(int),
                              &(rdbk_succs[0]),
                              &err );
   if( oclFunctionFailed( err, "create readback_succs.")) {
      return false;
   }
   // create cl buffer for readback_succFirst[]
   std::vector<int> rdbk_succ1st( ckt.succFirst().size(), 0 );
   buf_rdbk_succ1st = clCreateBuffer(context,
                              CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
                              ckt.succFirst().size() * sizeof(int),
                              &(rdbk_succ1st[0]),
                              &err );
   if( oclFunctionFailed( err, "create readback_succ_1st.")) {
      return false;
   }

   cl_kernel& kn_test_ckt = pfm.kernel( KERNEL_TEST_CKT_IMAGE );
   kn_test_ckt = clCreateKernel( program, "kernel_read_ckt_image", &err );
   if( oclFunctionFailed( err,"create kernel_read_ckt_image.")) {
      return false;
   }
   //
   unsigned mask = xMask();
   int shift = yShift();
   // set kernel arguments for kn_test_ckt
   err  = clSetKernelArg(kn_test_ckt, 0, sizeof(cl_mem), &node_image);
   err |= clSetKernelArg(kn_test_ckt, 1, sizeof(cl_mem), &conn_image);
   err |= clSetKernelArg(kn_test_ckt, 2, sizeof(cl_mem), &buf_rdbk_types);
   err |= clSetKernelArg(kn_test_ckt, 3, sizeof(cl_mem), &buf_rdbk_levels);
   err |= clSetKernelArg(kn_test_ckt, 4, sizeof(cl_mem), &buf_rdbk_pred1st);
   err |= clSetKernelArg(kn_test_ckt, 5, sizeof(cl_mem), &buf_rdbk_succ1st);
   err |= clSetKernelArg(kn_test_ckt, 6, sizeof(cl_mem), &buf_rdbk_preds);
   err |= clSetKernelArg(kn_test_ckt, 7, sizeof(cl_mem), &buf_rdbk_succs);
   err |= clSetKernelArg(kn_test_ckt, 8, sizeof(int), &shift);
   err |= clSetKernelArg(kn_test_ckt, 9, sizeof(unsigned), &mask);
   if( oclFunctionFailed( err,"set args for kernel_read_ckt.")) {
      return false;
   }
   size_t remainingWorks = (size_t)ckt.numNodes();
   unsigned base_node_id = 0;
   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   HLog << InfoNote
        << "enqueue kernel to read circuit images"
        << std::endl;
   std::vector< cl_event > events_tckt; // events for test ckt image
   while( remainingWorks > 0 ) {
      size_t effective_size = 0;
      cl_uint work_dim = 0;
      allocateWorkSize( remainingWorks, cl_dev, work_dim,
                        globalWorkSize, localWorkSize, effective_size );
      unsigned padding_bound = (unsigned)(base_node_id + effective_size - 1);
      err = clSetKernelArg(kn_test_ckt, 10, sizeof(unsigned), &base_node_id);
      err|= clSetKernelArg(kn_test_ckt, 11, sizeof(unsigned), &padding_bound);
      if( oclFunctionFailed( err,
             "set base_node_id and padding_bound for kn_test_ckt")) {
         return false;
      }
      if( debugFlag() & EVAL_CL_DBG_WORK_SIZE) {
         printClEnqueueParameters( work_dim, globalWorkOffset, globalWorkSize,
            localWorkSize, base_node_id, padding_bound, effective_size );
      }
      if( !checkKernelWorkGroupSize( kn_test_ckt, device, localWorkSize[0])) {
         return false;
      }
      cl_event cur_event;
      err = clEnqueueNDRangeKernel(
                      queue, kn_test_ckt, work_dim,
                      &(globalWorkOffset[0]),
                      &(globalWorkSize[0]),
                      &(localWorkSize[0]),
                      0,
                      NULL,
                      &cur_event ); // event
      if( oclFunctionFailed(err,"enqueue kernel_read_ckt.")) {
         return false;
      }
      events_tckt.push_back( cur_event );
      // update base_node_id and remainingWorks
      base_node_id += (unsigned)effective_size;
      remainingWorks -= effective_size;
   }
   // wait for test ckt image events to finish
   err = clWaitForEvents( events_tckt.size(), &(events_tckt[0]) );
   if( oclFunctionFailed(err,"wait for events in testing circuit image")) {
      return false;
   }
   std::vector< cl_event > events_rdbk;
   cl_event cur_event = NULL;
   // Read the types buffer back to the Host
   err = clEnqueueReadBuffer( queue, buf_rdbk_types, CL_FALSE,
                                 0, rdbk_types.size() * sizeof(int),
                                 &(rdbk_types[0]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue read-buffer types.")) {
      return false;
   }
   events_rdbk.push_back( cur_event );
   // Read the levels buffer back to the Host
   err = clEnqueueReadBuffer( queue, buf_rdbk_levels, CL_FALSE,
                                 0, rdbk_levels.size() * sizeof(int),
                                 &(rdbk_levels[0]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue read-buffer levels.")) {
      return false;
   }
   events_rdbk.push_back( cur_event );
   // Read the pred_1st buffer back to the Host
   err = clEnqueueReadBuffer( queue, buf_rdbk_pred1st, CL_FALSE,
                                 0, rdbk_pred1st.size() * sizeof(int),
                                 &(rdbk_pred1st[0]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue read-buffer pred_1st.")) {
      return false;
   }
   events_rdbk.push_back( cur_event );
   // Read the succ_1st buffer back to the Host
   err = clEnqueueReadBuffer( queue, buf_rdbk_succ1st, CL_FALSE,
                                 0, rdbk_succ1st.size() * sizeof(int),
                                 &(rdbk_succ1st[0]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue read-buffer succ_1st.")) {
      return false;
   }
   events_rdbk.push_back( cur_event );
   // Read the succs buffer back to the Host
   err = clEnqueueReadBuffer( queue, buf_rdbk_succs, CL_FALSE,
                                 0, rdbk_succs.size() * sizeof(int),
                                 &(rdbk_succs[0]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue read-buffer succs.")) {
      return false;
   }
   events_rdbk.push_back( cur_event );
   // Read the preds buffer back to the Host
   err = clEnqueueReadBuffer( queue, buf_rdbk_preds, CL_FALSE,
                                 0, rdbk_preds.size() * sizeof(int),
                                 &(rdbk_preds[0]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue read-buffer preds.")) {
      return false;
   }
   events_rdbk.push_back( cur_event );

   // wait for buffer readback events in testing circuit image to finish
   err = clWaitForEvents( events_rdbk.size(), &(events_rdbk[0]) );
   if( oclFunctionFailed(err,"wait for events in buffer readback.")) {
      return false;
   }

   HLog << "\nlogic type:\n";
   for( unsigned i=0; i<rdbk_types.size(); i++ ) {
      HLog << i << ":\t" << ckt.types()[i] << "\t" << rdbk_types[i]
           << std::endl;
   }
   HLog << "\nLevel:\n";
   for( unsigned i=0; i<rdbk_levels.size(); i++ ) {
      HLog << i << ":\t" << ckt.levels()[i] << "\t" << rdbk_levels[i]
           << std::endl;
   }
   HLog << "\nPred 1st:\n";
   for( unsigned i=0; i<rdbk_pred1st.size(); i++ ) {
      HLog << i << ":\t" << ckt.predFirst()[i]
           << "\t" << rdbk_pred1st[i] << std::endl;
   }
   HLog << "\nPreds:\n";
   for( unsigned i=0; i<rdbk_preds.size(); i++ ) {
      HLog << i << ":\t" << ckt.preds()[i]
           << "\t" << rdbk_preds[i] << std::endl;
   }
   HLog << "\nSucc 1st:\n";
   for( unsigned i=0; i<rdbk_succ1st.size(); i++ ) {
      HLog << i << ":\t" << ckt.succFirst()[i]
           << "\t" << rdbk_succ1st[i] << std::endl;
   }
   HLog << "\nSuccs:\n";
   for( unsigned i=0; i<rdbk_succs.size(); i++ ) {
      HLog << i << ":\t" << ckt.succs()[i]
           << "\t" << rdbk_succs[i] << std::endl;
   }
   return true;
}


bool EvaluateCL::setupGmvBufAndSimKernelsOnPfm( cl_uint pfm_num,
                                                LevelizedNetlist& ckt )
{
   // retrieve ocl platform and context
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   Evaluate & eval = (htDB::instance())->eval();
   cl_context context = ocl_info.platform( pfm_num ).context();
   assert( context );
   cl_program program = ocl_info.platform( pfm_num ).program();
   assert( program );

   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   cl_int err = 0;

   // Adjust gmv_buffer_flags accordingly for CPU/iGPU and dGPU
   cl_mem_flags gmv_buffer_flags = 0;
   const bool vector_mode_on =htDB::instance()->simOptions().vectorModeOn();
   if( pfm.platformVendor().find("Intel") != std::string::npos ) {
      gmv_buffer_flags = CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR;
   } else {
      gmv_buffer_flags = CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR;
   }
   // create cl buffer for gmv0[]
   if( vector_mode_on ) {
      buf_gmv0 = clCreateBuffer(context,
                                gmv_buffer_flags,
                                ckt.numNodes() * sizeof(cl_ulong4),
                                &(eval.gmv0x4()[0]),
                                &err );
   } else {
      buf_gmv0 = clCreateBuffer(context,
                                gmv_buffer_flags,
                                ckt.numNodes() * sizeof(uint64_t),
                                &(eval.gmv0()[0]),
                                &err );
   }
   if( oclFunctionFailed( err, "create buf_gmv0.")) {
      return false;
   }
   // create cl buffer for gmv1[]
   if( vector_mode_on ) {
      buf_gmv1 = clCreateBuffer(context,
                                gmv_buffer_flags,
                                ckt.numNodes() * sizeof(cl_ulong4),
                                &(eval.gmv1x4()[0]),
                                &err );
   } else {
      buf_gmv1 = clCreateBuffer(context,
                                gmv_buffer_flags,
                                ckt.numNodes() * sizeof(uint64_t),
                                &(eval.gmv1()[0]),
                                &err );
   }
   if( oclFunctionFailed( err, "create buf_gmv1.")) {
      return false;
   }
   // create good sim kernel
   cl_kernel& kn_gsim_img = pfm.kernel( KERNEL_GSIM_IMG );
   kn_gsim_img = clCreateKernel( program, "kernel_gsim_img", &err );
   if( oclFunctionFailed( err,"create kernel_gsim_img.")) {
      return false;
   }
   // set kernel arguments
   cl_mem& node_image = pfm.clMemCkt( CL_IMG_NODE );
   cl_mem& conn_image = pfm.clMemCkt( CL_IMG_CONN );

   assert( buf_gmv0 && buf_gmv1 && node_image && conn_image && kn_gsim_img );
   unsigned mask = xMask();
   int shift = yShift();

   // set kernel arguments
   err  = clSetKernelArg(kn_gsim_img, 0, sizeof(cl_mem), &node_image);
   err |= clSetKernelArg(kn_gsim_img, 1, sizeof(cl_mem), &conn_image);
   err |= clSetKernelArg(kn_gsim_img, 2, sizeof(cl_mem), &buf_gmv0);
   err |= clSetKernelArg(kn_gsim_img, 3, sizeof(cl_mem), &buf_gmv1);
   err |= clSetKernelArg(kn_gsim_img, 4, sizeof(int), &shift);
   err |= clSetKernelArg(kn_gsim_img, 5, sizeof(unsigned), &mask);
   if( oclFunctionFailed( err,"set args for kernel_gsim_img.")) {
      return false;
   }

   // create good sim kernel with netlist in buffer
   cl_kernel& kn_gsim_buf = pfm.kernel( KERNEL_GSIM_BUF );
   std::string kn_name("");
   if( (htDB::instance())->simOptions().nonDivergentGsimKernel() ) {
      kn_name = "kernel_gsim_buf_non_divergent";
   } else if( vector_mode_on ) {
      kn_name = "kernel_gsim_buf_ulong4";
   } else {
      kn_name = "kernel_gsim_buf";
   }
   kn_gsim_buf = clCreateKernel( program, kn_name.c_str(), &err );
   // "create kn_name."
   std::string err_msg = "create " + kn_name + ".";
   if( oclFunctionFailed( err, err_msg.c_str())) {
      return false;
   }
   // set kernel arguments
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();

   // set kernel arguments
   err  = clSetKernelArg(kn_gsim_buf, 0, sizeof(cl_mem), &buf_types);
   err |= clSetKernelArg(kn_gsim_buf, 1, sizeof(cl_mem), &buf_levels);
   err |= clSetKernelArg(kn_gsim_buf, 2, sizeof(cl_mem), &buf_pred_1st);
   err |= clSetKernelArg(kn_gsim_buf, 3, sizeof(cl_mem), &buf_succ_1st);
   err |= clSetKernelArg(kn_gsim_buf, 4, sizeof(cl_mem), &buf_preds);
   err |= clSetKernelArg(kn_gsim_buf, 5, sizeof(cl_mem), &buf_succs);
   err |= clSetKernelArg(kn_gsim_buf, 6, sizeof(unsigned),&first_ff_node);
   err |= clSetKernelArg(kn_gsim_buf, 7, sizeof(int),&clocked_dff_is_used);
   err |= clSetKernelArg(kn_gsim_buf, 8, sizeof(cl_mem), &buf_gmv0);
   err |= clSetKernelArg(kn_gsim_buf, 9, sizeof(cl_mem), &buf_gmv1);
   // error message : "set args for kernel_gsim_buf."
   err_msg = "set args for " + kn_name + ".";
   if( oclFunctionFailed( err, err_msg.c_str())) {
      return false;
   }

   // create kernel of init x background
   if( !setupKernelInitXBackground( pfm_num, ckt ) ) {
      return false;
   }
   // create kernel of init tie gates
   if( !setupKernelInitTIEGates( pfm_num, ckt ) ) {
      return false;
   }
   if( !setupKernelInitTIEGatesFlag( pfm_num, ckt ) ) {
      return false;
   }
   // create kernel of assign stimulus
   cl_kernel& kn_assign_sti = pfm.kernel( KERNEL_ASSIGN_STIMULUS );
   kn_assign_sti = clCreateKernel( program, "kernel_assign_stimulus", &err );
   if( oclFunctionFailed( err,"create kernel_assign_stimulus.")) {
      return false;
   }
   err  = clSetKernelArg(kn_assign_sti, 0, sizeof(cl_mem), &buf_gmv0);
   err |= clSetKernelArg(kn_assign_sti, 1, sizeof(cl_mem), &buf_gmv1);
   if( oclFunctionFailed( err,"set args for kernel_assign_stimulus.")) {
      return false;
   }

   // create kernel of assign data pi
   if( !setupKernelAssignDataPI( pfm_num, ckt ) ) {
      return false;
   }
   if( !setupKernelAssignDataPIFlag( pfm_num, ckt ) ) {
      return false;
   }
   // create kernel of assign clock pi
   if( !setupKernelAssignClockPI( pfm_num, ckt ) ) {
      return false;
   }
   if( !setupKernelAssignClockPIFlag( pfm_num, ckt ) ) {
      return false;
   }
   // create kernel of assign init ff
   if( !setupKernelAssignInitFF( pfm_num, ckt ) ) {
      return false;
   }
   if( !setupKernelAssignInitFFFlag( pfm_num, ckt ) ) {
      return false;
   }
   // create kernel of save_outputs
   cl_kernel& kn_save_outputs = pfm.kernel( KERNEL_SAVE_OUTPUTS );
   kn_save_outputs = clCreateKernel( program, "kernel_save_outputs", &err );
   if( oclFunctionFailed( err,"create kernel_save_outputs")) {
      return false;
   }
   cl_mem& buf_outputs = pfm.clMemCkt( CL_BUF_OUTPUTS );
   err  = clSetKernelArg(kn_save_outputs, 0, sizeof(cl_mem), &buf_outputs);
   err |= clSetKernelArg(kn_save_outputs, 1, sizeof(cl_mem), &buf_gmv0);
   err |= clSetKernelArg(kn_save_outputs, 2, sizeof(cl_mem), &buf_gmv1);
   if( oclFunctionFailed( err,"set args for kernel_save_outputs.")) {
      return false;
   }

   // create kernel of save_obs_ffs
   cl_kernel& kn_save_obs_ffs = pfm.kernel( KERNEL_SAVE_OBS_FFS );
   kn_save_obs_ffs = clCreateKernel( program, "kernel_save_obs_ffs", &err );
   if( oclFunctionFailed( err,"create kernel_save_obs_ffs.")) {
      return false;
   }
   // set some kernel arguments; remaining ones are set later
   err  = clSetKernelArg(kn_save_obs_ffs, 0,sizeof(unsigned),&first_ff_node);
   err |= clSetKernelArg(kn_save_obs_ffs, 1,sizeof(int),&clocked_dff_is_used);
   err |= clSetKernelArg(kn_save_obs_ffs, 2,sizeof(cl_mem), &buf_gmv0);
   err |= clSetKernelArg(kn_save_obs_ffs, 3,sizeof(cl_mem), &buf_gmv1);
   if( oclFunctionFailed( err,"set args for kernel_save_obs_ffs.")) {
      return false;
   }

   if( !setupKernelGsimBufEvent( pfm_num, ckt )) {
      return false;
   }
   if( !setupKernelGsimBufEventFlag( pfm_num, ckt )) {
      return false;
   }
   HLog << InfoNote
        << "creating GMV buffers and good sim kernels on gpu ....."
        << std::endl;
   return true;
}


bool EvaluateCL::setupKernelInitXBackground( cl_uint pfm_num,
                                             LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_init_x_bg = pfm.kernel( KERNEL_INIT_X_BACKGROUND );
   kn_init_x_bg = clCreateKernel( program, "kernel_init_x_background", &err );
   if( oclFunctionFailed( err,"create kernel_init_x_background.")) {
      return false;
   }
   //  0     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_init_x_bg, 0, sizeof(cl_mem), &buf_gmv0);
   //  1     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_init_x_bg, 1, sizeof(cl_mem), &buf_gmv1);
   //  2     const unsigned base_node_id, //--
   //  3     const unsigned padding_bound )
   if( oclFunctionFailed( err,"set args for kernel_init_x_background.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelInitTIEGates( cl_uint pfm_num,
                                          LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_init_ties = pfm.kernel( KERNEL_INIT_TIE_GATES );
   kn_init_ties = clCreateKernel( program, "kernel_init_tie_gates", &err );
   if( oclFunctionFailed( err,"create kernel_init_tie_gates.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err  = clSetKernelArg(kn_init_ties, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_init_ties, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_init_ties, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_init_ties, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_init_ties, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_init_ties, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global const int *buf_q_offsets,
   cl_mem& buf_q_offsets = pfm.clMemCkt( CL_BUF_Q_OFFSETS );
   err |= clSetKernelArg(kn_init_ties, 6, sizeof(cl_mem), &buf_q_offsets);
   //  7     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_init_ties, 7, sizeof(cl_mem), &buf_q_sizes);
   //  8     __global  unsigned *buf_q_e_flags,
   cl_mem& buf_q_e_flags = pfm.clMemCkt( CL_BUF_Q_EVENTS_FLAGS );
   err |= clSetKernelArg(kn_init_ties, 8, sizeof(cl_mem), &buf_q_e_flags);
   //  9     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_init_ties, 9, sizeof(cl_mem), &buf_gmv0);
   // 10     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_init_ties,10, sizeof(cl_mem), &buf_gmv1);
   // 11     const int num_levels,
   int num_levels = ckt.numLevels();
   err |= clSetKernelArg(kn_init_ties,11, sizeof(int), &num_levels);
   // 12     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_init_ties,12, sizeof(unsigned), &first_ff_node);
   // 13     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_init_ties,13, sizeof(int),&clocked_dff_is_used);
   // 14     const int is_event_driven,
   // 15     const unsigned base_node_id, //--
   // 16     const unsigned padding_bound
   if( oclFunctionFailed( err,"set args for kernel_init_tie_gates.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelInitTIEGatesFlag( cl_uint pfm_num,
                                              LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_ties_flag = pfm.kernel( KERNEL_INIT_TIE_GATES_FLAG );
   kn_ties_flag = clCreateKernel( program,"kernel_init_tie_gates_flag", &err);
   if( oclFunctionFailed( err,"create kernel_init_tie_gates_flag.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err  = clSetKernelArg(kn_ties_flag, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_ties_flag, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_ties_flag, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_ties_flag, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_ties_flag, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_ties_flag, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_ties_flag, 6, sizeof(cl_mem), &buf_q_sizes);
   //  7     __global      char *buf_flags,
   cl_mem& buf_q_flags   = pfm.clMemCkt( CL_BUF_Q_FLAGS );
   err |= clSetKernelArg(kn_ties_flag, 7, sizeof(cl_mem), &buf_q_flags);
   //  8     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_ties_flag, 8, sizeof(cl_mem), &buf_gmv0);
   //  9     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_ties_flag, 9, sizeof(cl_mem), &buf_gmv1);
   // 10     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_ties_flag,10, sizeof(unsigned), &first_ff_node);
   // 11     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_ties_flag,11, sizeof(int),&clocked_dff_is_used);
   // 12     const int is_event_driven,
   // 13     const unsigned base_node_id, //--
   // 14     const unsigned padding_bound
   if( oclFunctionFailed( err,"set args for kernel_init_tie_gates_flag.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelAssignDataPI( cl_uint pfm_num,
                                          LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_assign_dpi = pfm.kernel( KERNEL_ASSIGN_DATA_PI );
   kn_assign_dpi = clCreateKernel( program, "kernel_assign_data_pi", &err );
   if( oclFunctionFailed( err,"create kernel_assign_data_pi.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err  = clSetKernelArg(kn_assign_dpi, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_assign_dpi, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_assign_dpi, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_assign_dpi, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_assign_dpi, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_assign_dpi, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global const int *buf_q_offsets,
   cl_mem& buf_q_offsets = pfm.clMemCkt( CL_BUF_Q_OFFSETS );
   err |= clSetKernelArg(kn_assign_dpi, 6, sizeof(cl_mem), &buf_q_offsets);
   //  7     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_assign_dpi, 7, sizeof(cl_mem), &buf_q_sizes);
   //  8     __global  unsigned *buf_q_e_flags,
   cl_mem& buf_q_e_flags = pfm.clMemCkt( CL_BUF_Q_EVENTS_FLAGS );
   err |= clSetKernelArg(kn_assign_dpi, 8, sizeof(cl_mem), &buf_q_e_flags);
   //  9     __global const int *data_input_nodes,
   cl_mem& buf_dinputs   = pfm.clMemCkt( CL_BUF_DATA_INPUTS );
   err  = clSetKernelArg(kn_assign_dpi, 9, sizeof(cl_mem), &buf_dinputs);
   // 10     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_assign_dpi,10, sizeof(cl_mem), &buf_gmv0);
   // 11     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_assign_dpi,11, sizeof(cl_mem), &buf_gmv1);
   // 12     const int num_levels,
   int num_levels = ckt.numLevels();
   err |= clSetKernelArg(kn_assign_dpi,12, sizeof(int), &num_levels);
   // 13     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_assign_dpi,13, sizeof(unsigned), &first_ff_node);
   // 14     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_assign_dpi,14, sizeof(int),&clocked_dff_is_used);
   // 15     const int is_event_driven,
   // 16     __global const ulong *pat_in_gmv0, //--
   // 17     __global const ulong *pat_in_gmv1,
   // 18     const unsigned pat_in_offset,
   // 19     const unsigned base_node_id,
   // 20     const unsigned padding_bound )
   if( oclFunctionFailed( err,"set args for kernel_assign_data_pi.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelAssignDataPIFlag( cl_uint pfm_num,
                                              LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_dpi_flag = pfm.kernel( KERNEL_ASSIGN_DATA_PI_FLAG );
   kn_dpi_flag = clCreateKernel( program, "kernel_assign_data_pi_flag", &err);
   if( oclFunctionFailed( err,"create kernel_assign_data_pi_flag.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err  = clSetKernelArg(kn_dpi_flag, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_dpi_flag, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_dpi_flag, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_dpi_flag, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_dpi_flag, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_dpi_flag, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes  = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_dpi_flag, 6, sizeof(cl_mem), &buf_q_sizes);
   //  7     __global  unsigned *buf_flags,
   cl_mem& buf_q_flags  = pfm.clMemCkt( CL_BUF_Q_FLAGS );
   err |= clSetKernelArg(kn_dpi_flag, 7, sizeof(cl_mem), &buf_q_flags);
   //  8     __global const int *data_input_nodes,
   cl_mem& buf_dinputs   = pfm.clMemCkt( CL_BUF_DATA_INPUTS );
   err  = clSetKernelArg(kn_dpi_flag, 8, sizeof(cl_mem), &buf_dinputs);
   //  9     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_dpi_flag,9, sizeof(cl_mem), &buf_gmv0);
   // 10     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_dpi_flag,10, sizeof(cl_mem), &buf_gmv1);
   // 11     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_dpi_flag,11, sizeof(unsigned), &first_ff_node);
   // 12     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_dpi_flag,12, sizeof(int),&clocked_dff_is_used);

   // 13     const int is_event_driven, //--
   // 14     __global const ulong *pat_in_gmv0,
   // 15     __global const ulong *pat_in_gmv1,
   // 16     const unsigned pat_in_offset,
   // 17     const unsigned base_node_id,
   // 18     const unsigned padding_bound
   if( oclFunctionFailed( err,"set args for kernel_assign_data_pi_flag.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelAssignClockPI( cl_uint pfm_num,
                                           LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_assign_cpi = pfm.kernel( KERNEL_ASSIGN_CLOCK_PI );
   kn_assign_cpi = clCreateKernel( program, "kernel_assign_clock_pi", &err );
   if( oclFunctionFailed( err,"create kernel_assign_clock_pi.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err  = clSetKernelArg(kn_assign_cpi, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_assign_cpi, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_assign_cpi, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_assign_cpi, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_assign_cpi, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_assign_cpi, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global const int *buf_q_offsets,
   cl_mem& buf_q_offsets = pfm.clMemCkt( CL_BUF_Q_OFFSETS );
   err |= clSetKernelArg(kn_assign_cpi, 6, sizeof(cl_mem), &buf_q_offsets);
   //  7     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_assign_cpi, 7, sizeof(cl_mem), &buf_q_sizes);
   //  8     __global  unsigned *buf_q_e_flags,
   cl_mem& buf_q_e_flags = pfm.clMemCkt( CL_BUF_Q_EVENTS_FLAGS );
   err |= clSetKernelArg(kn_assign_cpi, 8, sizeof(cl_mem), &buf_q_e_flags);
   //  9     __global const int *clk_inputs_offstates,
   cl_mem& buf_clkinfo   = pfm.clMemCkt( CL_BUF_CLOCKS_AND_OFFSTATES );
   err  = clSetKernelArg(kn_assign_cpi, 9, sizeof(cl_mem), &buf_clkinfo);
   // 10     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_assign_cpi,10, sizeof(cl_mem), &buf_gmv0);
   // 11     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_assign_cpi,11, sizeof(cl_mem), &buf_gmv1);
   // 12     const int num_levels,
   int num_levels = ckt.numLevels();
   err |= clSetKernelArg(kn_assign_cpi,12, sizeof(int), &num_levels);
   // 13     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_assign_cpi,13, sizeof(unsigned), &first_ff_node);
   // 14     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_assign_cpi,14, sizeof(int),&clocked_dff_is_used);
   // 15     const int is_event_driven,
   // 16     const int clock_is_on,
   // 17     const unsigned base_node_id,
   // 18     const unsigned padding_bound )
   if( oclFunctionFailed( err,"set args for kernel_assign_clock_pi.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelAssignClockPIFlag( cl_uint pfm_num,
                                               LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_cpi_flag = pfm.kernel( KERNEL_ASSIGN_CLOCK_PI_FLAG );
   kn_cpi_flag = clCreateKernel( program, "kernel_assign_cpi_flag", &err );
   if( oclFunctionFailed( err,"create kernel_assign_cpi_flag.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err  = clSetKernelArg(kn_cpi_flag, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_cpi_flag, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_cpi_flag, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_cpi_flag, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_cpi_flag, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_cpi_flag, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_cpi_flag, 6, sizeof(cl_mem), &buf_q_sizes);
   //  7     __global      char *buf_flags,
   cl_mem& buf_q_flags   = pfm.clMemCkt( CL_BUF_Q_FLAGS );
   err |= clSetKernelArg(kn_cpi_flag, 7, sizeof(cl_mem), &buf_q_flags);
   //  8     __global const int *clk_inputs_offstates,
   cl_mem& buf_clkinfo   = pfm.clMemCkt( CL_BUF_CLOCKS_AND_OFFSTATES );
   err  = clSetKernelArg(kn_cpi_flag, 8, sizeof(cl_mem), &buf_clkinfo);
   //  9     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_cpi_flag, 9, sizeof(cl_mem), &buf_gmv0);
   // 10     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_cpi_flag,10, sizeof(cl_mem), &buf_gmv1);
   // 11     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_cpi_flag,11, sizeof(unsigned), &first_ff_node);
   // 12     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_cpi_flag,12, sizeof(int),&clocked_dff_is_used);
   // 13     const int is_event_driven, //--
   // 14     const int clock_is_on,
   // 15     const unsigned base_node_id,
   // 16     const unsigned padding_bound

   if( oclFunctionFailed( err,"set args for kernel_assign_cpi_flag.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelAssignInitFF( cl_uint pfm_num,
                                          LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_assign_iff = pfm.kernel( KERNEL_ASSIGN_INIT_FF );
   kn_assign_iff = clCreateKernel( program, "kernel_assign_init_ff", &err );
   if( oclFunctionFailed( err,"create kernel_assign_init_ff.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err  = clSetKernelArg(kn_assign_iff, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_assign_iff, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_assign_iff, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_assign_iff, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_assign_iff, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_assign_iff, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global const int *buf_q_offsets,
   cl_mem& buf_q_offsets = pfm.clMemCkt( CL_BUF_Q_OFFSETS );
   err |= clSetKernelArg(kn_assign_iff, 6, sizeof(cl_mem), &buf_q_offsets);
   //  7     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_assign_iff, 7, sizeof(cl_mem), &buf_q_sizes);
   //  8     __global  unsigned *buf_q_e_flags,
   cl_mem& buf_q_e_flags = pfm.clMemCkt( CL_BUF_Q_EVENTS_FLAGS );
   err |= clSetKernelArg(kn_assign_iff, 8, sizeof(cl_mem), &buf_q_e_flags);
   //  9     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_assign_iff, 9, sizeof(cl_mem), &buf_gmv0);
   // 10     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_assign_iff,10, sizeof(cl_mem), &buf_gmv1);
   // 11     const int num_levels,
   int num_levels = ckt.numLevels();
   err |= clSetKernelArg(kn_assign_iff,11, sizeof(int), &num_levels);
   // 12     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_assign_iff,12, sizeof(unsigned), &first_ff_node);
   // 13     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_assign_iff,13, sizeof(int),&clocked_dff_is_used);
   // 14     const int is_event_driven,
   // 15     __global const ulong *pat_in_gmv0, //--
   // 16     __global const ulong *pat_in_gmv1,
   // 17     const unsigned pat_in_offset,
   // 18     const unsigned base_node_id,
   // 19     const unsigned padding_bound )
   if( oclFunctionFailed( err,"set args for kernel_assign_init_ff.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelAssignInitFFFlag( cl_uint pfm_num,
                                              LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_iff_flag = pfm.kernel( KERNEL_ASSIGN_INIT_FF_FLAG );
   kn_iff_flag = clCreateKernel( program, "kernel_assign_init_ff_flag", &err);
   if( oclFunctionFailed( err,"create kernel_assign_init_ff_flag.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err  = clSetKernelArg(kn_iff_flag, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_iff_flag, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_iff_flag, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_iff_flag, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_iff_flag, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_iff_flag, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_iff_flag, 6, sizeof(cl_mem), &buf_q_sizes);
   //  7     __global      char *buf_flags,
   cl_mem& buf_q_flags   = pfm.clMemCkt( CL_BUF_Q_FLAGS );
   err |= clSetKernelArg(kn_iff_flag, 7, sizeof(cl_mem), &buf_q_flags);
   //  8     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_iff_flag, 8, sizeof(cl_mem), &buf_gmv0);
   //  9     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_iff_flag, 9, sizeof(cl_mem), &buf_gmv1);
   // 10     const int num_levels,
   int num_levels = ckt.numLevels();
   err |= clSetKernelArg(kn_iff_flag,10, sizeof(int), &num_levels);
   // 11     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_iff_flag,11, sizeof(unsigned), &first_ff_node);
   // 12     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_iff_flag,12, sizeof(int),&clocked_dff_is_used);
   // 13     const int is_event_driven, //--
   // 14     __global const ulong *pat_in_gmv0,
   // 15     __global const ulong *pat_in_gmv1,
   // 16     const unsigned pat_in_offset,
   // 17     const unsigned base_node_id,
   // 18     const unsigned padding_bound
   if( oclFunctionFailed( err,"set args for kernel_assign_init_ff_flag.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelGsimBufEvent( cl_uint pfm_num,
                                          LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_gsim_buf_e = pfm.kernel( KERNEL_GSIM_BUF_EVENT );
   kn_gsim_buf_e = clCreateKernel( program, "kernel_gsim_buf_event", &err );
   if( oclFunctionFailed( err,"create kernel_gsim_buf_event.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err |= clSetKernelArg(kn_gsim_buf_e, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_gsim_buf_e, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_gsim_buf_e, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_gsim_buf_e, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_gsim_buf_e, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_gsim_buf_e, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global const int *buf_q_offsets,
   cl_mem& buf_q_offsets = pfm.clMemCkt( CL_BUF_Q_OFFSETS );
   err |= clSetKernelArg(kn_gsim_buf_e, 6, sizeof(cl_mem), &buf_q_offsets);
   //  7     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_gsim_buf_e, 7, sizeof(cl_mem), &buf_q_sizes);
   //  8     __global  unsigned *buf_q_e_flags,
   cl_mem& buf_q_e_flags = pfm.clMemCkt( CL_BUF_Q_EVENTS_FLAGS );
   err |= clSetKernelArg(kn_gsim_buf_e, 8, sizeof(cl_mem), &buf_q_e_flags);
   //  9     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_gsim_buf_e, 9, sizeof(cl_mem), &buf_gmv0);
   // 10     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_gsim_buf_e,10, sizeof(cl_mem), &buf_gmv1);
   // 11     const int num_levels,
   int num_levels = ckt.numLevels();
   err |= clSetKernelArg(kn_gsim_buf_e,11, sizeof(int), &num_levels);
   // 12     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_gsim_buf_e,12, sizeof(unsigned), &first_ff_node);
   // 13     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_gsim_buf_e,13, sizeof(int),&clocked_dff_is_used);
   // 14     const int curr_lev,
   // 15     const unsigned base_node_id,
   // 16     const unsigned padding_bound
   if( oclFunctionFailed( err,"set args for kernel_gsim_buf_event.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::setupKernelGsimBufEventFlag( cl_uint pfm_num,
                                              LevelizedNetlist& ckt )
{
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_program program = ocl_info.platform( pfm_num ).program();

   cl_int err = 0;
   cl_kernel& kn_gsim_eflag = pfm.kernel( KERNEL_GSIM_BUF_EVENT_FLAG );
   kn_gsim_eflag = clCreateKernel(program,"kernel_gsim_buf_event_flag",&err);
   if( oclFunctionFailed( err,"create kernel_gsim_buf_event_flag.")) {
      return false;
   }
   //  0     __global const int *buf_types,
   cl_mem& buf_types    = pfm.clMemCkt( CL_BUF_TYPES );
   err |= clSetKernelArg(kn_gsim_eflag, 0, sizeof(cl_mem), &buf_types);
   //  1     __global const int *buf_levels,
   cl_mem& buf_levels   = pfm.clMemCkt( CL_BUF_LEVELS );
   err |= clSetKernelArg(kn_gsim_eflag, 1, sizeof(cl_mem), &buf_levels);
   //  2     __global const int *buf_pred_1st,
   cl_mem& buf_pred_1st = pfm.clMemCkt( CL_BUF_PRED1ST );
   err |= clSetKernelArg(kn_gsim_eflag, 2, sizeof(cl_mem), &buf_pred_1st);
   //  3     __global const int *buf_succ_1st,
   cl_mem& buf_succ_1st = pfm.clMemCkt( CL_BUF_SUCC1ST );
   err |= clSetKernelArg(kn_gsim_eflag, 3, sizeof(cl_mem), &buf_succ_1st);
   //  4     __global const int *buf_preds,
   cl_mem& buf_preds    = pfm.clMemCkt( CL_BUF_PREDS );
   err |= clSetKernelArg(kn_gsim_eflag, 4, sizeof(cl_mem), &buf_preds);
   //  5     __global const int *buf_succs,
   cl_mem& buf_succs    = pfm.clMemCkt( CL_BUF_SUCCS );
   err |= clSetKernelArg(kn_gsim_eflag, 5, sizeof(cl_mem), &buf_succs);
   //  6     __global       int *buf_q_sizes,
   cl_mem& buf_q_sizes   = pfm.clMemCkt( CL_BUF_Q_SIZES );
   err |= clSetKernelArg(kn_gsim_eflag, 6, sizeof(cl_mem), &buf_q_sizes);
   //  7     __global      char *buf_flags,
   cl_mem& buf_q_flags   = pfm.clMemCkt( CL_BUF_Q_FLAGS );
   err |= clSetKernelArg(kn_gsim_eflag, 7, sizeof(cl_mem), &buf_q_flags);
   //  8     __global ulong *gmv0,
   cl_mem& buf_gmv0   = pfm.clMemGmv0( 0 ); // use GMV of group 0
   err |= clSetKernelArg(kn_gsim_eflag, 8, sizeof(cl_mem), &buf_gmv0);
   //  9     __global ulong *gmv1,
   cl_mem& buf_gmv1   = pfm.clMemGmv1( 0 );
   err |= clSetKernelArg(kn_gsim_eflag, 9, sizeof(cl_mem), &buf_gmv1);
   // 10     const int num_levels,
   int num_levels = ckt.numLevels();
   err |= clSetKernelArg(kn_gsim_eflag,10, sizeof(int), &num_levels);
   // 11     const unsigned first_ff_node,
   unsigned first_ff_node = (unsigned)ckt.firstNodeFF();
   err |= clSetKernelArg(kn_gsim_eflag,11, sizeof(unsigned), &first_ff_node);
   // 12     const int clocked_dff_is_used,
   int clocked_dff_is_used = (int)ckt.clockedDffIsUsed();
   err |= clSetKernelArg(kn_gsim_eflag,12, sizeof(int),&clocked_dff_is_used);
   // 13     const int curr_lev,
   // 14     const unsigned base_node_id,
   // 15     const unsigned padding_bound
   if( oclFunctionFailed( err,"set args for kernel_gsim_buf_event_flag.")) {
      return false;
   }
   return true;
}


void EvaluateCL::backupPatternInDataForStorePatterns( cl_uint grp_id )
{
   assert( patternInGmv0(grp_id).size() == patternInGmv0Backup().size() );
   assert( patternInGmv1(grp_id).size() == patternInGmv1Backup().size() );
   assert( grp_id < _patternsDataInGmv0.size() );
   // Alternatively, we could use
   // _patInBackupGmv0.assign( patternInGmv0(grp_id).begin(),
   //                          patternInGmv0(grp_id).end() );
   // _patInBackupGmv1.assign( patternInGmv1(grp_id).begin(),
   //                          patternInGmv1(grp_id).end() );
   _patInBackupGmv0 = patternInGmv0( grp_id );
   _patInBackupGmv1 = patternInGmv1( grp_id );
}


bool EvaluateCL::copyPatternInHToD( cl_uint pfm_num,
                                    cl_uint dev_num,
                                    cl_uint grp_id,
                                    cl_event& event_gmv0,
                                    cl_event& event_gmv1 )
{
   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue =
      ocl_info.platform(pfm_num).device(dev_num).ioQueue();

   cl_mem& pat_in_gmv0   = pfm.clMemPatInGmv0( grp_id );
   cl_mem& pat_in_gmv1   = pfm.clMemPatInGmv1( grp_id );
   cl_int err = 0;

   size_t offset = 0; // (size_t)0 * sizeof(uint64_t);
   size_t size = (size_t)patternInSize() * sizeof(uint64_t);

   // Non-blocking Write the pattern-in gmv0 buffer on gpu
   err = clEnqueueWriteBuffer( queue, pat_in_gmv0, CL_FALSE,
                                 offset, size,
                                 &(patternInGmv0(grp_id)[0]),
                                 0, NULL, &event_gmv0 );
   if( oclFunctionFailed( err, "enqueue write pat_in_gmv0 buffer on gpu.")) {
      return false;
   }
   // Non-blocking Write the pattern-in gmv1 buffer on gpu
   err = clEnqueueWriteBuffer( queue, pat_in_gmv1, CL_FALSE,
                                 offset, size,
                                 &(patternInGmv1(grp_id)[0]),
                                 0, NULL, &event_gmv1 );
   if( oclFunctionFailed( err, "enqueue write pat_in_gmv1 buffer on gpu.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::copyPatternOutDToH( cl_uint pfm_num,
                                     cl_uint dev_num,
                                     cl_uint grp_id,
                                     cl_event& event_gmv0,
                                     cl_event& event_gmv1 )
{
   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue =
      ocl_info.platform(pfm_num).device(dev_num).ioQueue();

   cl_mem& pat_out_gmv0   = pfm.clMemPatOutGmv0( grp_id );
   cl_mem& pat_out_gmv1   = pfm.clMemPatOutGmv1( grp_id );
   cl_int err = 0;

   size_t offset = 0; // (size_t)0 * sizeof(uint64_t);
   size_t size = (size_t)patternOutSize() * sizeof(uint64_t);

   // Non-blocking read the pattern-out gmv0 buffer on gpu
   err = clEnqueueReadBuffer( queue, pat_out_gmv0, CL_FALSE,
                                 offset, size,
                                 &(patternOutGmv0(grp_id)[0]),
                                 0, NULL, &event_gmv0 );
   if( oclFunctionFailed( err, "enqueue read pat_out_gmv0 buffer on gpu.")) {
      return false;
   }
   // Non-blocking Write the pattern-in gmv1 buffer on gpu
   err = clEnqueueReadBuffer( queue, pat_out_gmv1, CL_FALSE,
                                 offset, size,
                                 &(patternOutGmv1(grp_id)[0]),
                                 0, NULL, &event_gmv1 );
   if( oclFunctionFailed( err, "enqueue read pat_out_gmv1 buffer on gpu.")) {
      return false;
   }
   return true;
}


bool EvaluateCL::testGmvCopy( cl_uint pfm_num, cl_uint dev_num )
{
   // target s27.bench for visual inspection
   Evaluate & eval = (htDB::instance())->eval();

   // initialize gmv0 gmv1 on host
   HLog << "before copying ....\n";
   HLog << "\tgmv0\tgmv1\n";
   for(int i=0; i<eval.ckt().numNodes(); i++ ) {
      eval.gmv0()[i] = (uint64_t)i;
      eval.gmv1()[i] = (uint64_t)i * 2;
      HLog << "\t" << eval.gmv0(i) << "\t" << eval.gmv1(i) << "\n";
   }
   HLog << std::endl;

   cl_uint grp_id = 0;
   copyGmvFromHostToGpu( pfm_num, dev_num, grp_id, 0, eval.ckt().numNodes() );
   HLog << "Cleaning gmv0/gmv1 ....\n";
   HLog << "\tgmv0\tgmv1\n";
   for(int i=0; i<eval.ckt().numNodes(); i++ ) {
      eval.gmv0()[i] = 0;
      eval.gmv1()[i] = 0;
      HLog << "\t" << eval.gmv0(i) << "\t" << eval.gmv1(i) << "\n";
   }
   HLog << std::endl;

   // test copy subrange of node values
   HLog << "Copy first 5 nodes of 0's to gpu ....\n\n";
   copyGmvFromHostToGpu( pfm_num, dev_num, grp_id, 0, 5 );

   HLog << "Copy last 5 nodes of 0's to gpu ....\n\n";
   copyGmvFromHostToGpu( pfm_num, dev_num, grp_id, 13, 5 );

   HLog << "Copy middle 4 nodes of 0's to gpu ....\n\n";
   copyGmvFromHostToGpu( pfm_num, dev_num, grp_id, 7, 4 );


   copyGmvFromGpuToHost( pfm_num, dev_num, grp_id, 0, eval.ckt().numNodes());
   //copyGmvFromGpuToHost( pfm_num, dev_num, 10, 8 );
   HLog << "Copy back gmv0/gmv1 from gpu ....\n";
   HLog << "\tgmv0\tgmv1\n";
   for(int i=0; i<eval.ckt().numNodes(); i++ ) {
      HLog << "\t" << eval.gmv0(i) << "\t" << eval.gmv1(i) << "\n";
   }
   HLog << std::endl;

   return true;
}


bool EvaluateCL::copyGmvFromHostToGpu( cl_uint pfm_num,
                                       cl_uint dev_num,
                                       cl_uint grp_id,
                                       int start_node,
                                       int num_nodes )
{
   _numGmvCopiesHost2Gpu++;
   std::chrono::system_clock::time_point
      start_t = std::chrono::system_clock::now();
   std::clock_t start_cpu_t = std::clock();

   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue = ocl_info.platform(pfm_num).device(dev_num).queue();
   Evaluate & eval = (htDB::instance())->eval();

   cl_mem& buf_gmv0   = pfm.clMemGmv0( grp_id );
   cl_mem& buf_gmv1   = pfm.clMemGmv1( grp_id );
   cl_int err = 0;

   size_t offset = (size_t)start_node * sizeof(uint64_t);
   size_t size = (size_t)num_nodes * sizeof(uint64_t);

   std::vector< cl_event > events_wrbf; // events of write buffer
   cl_event cur_event = NULL;
   // Write the gmv0 buffer on gpu
   err = clEnqueueWriteBuffer( queue, buf_gmv0, CL_FALSE,
                                 offset, size,
                                 &(eval.gmv0()[start_node]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue write gmv0 buffer on gpu.")) {
      return false;
   }
   events_wrbf.push_back( cur_event );
   // Write the gmv1 buffer on gpu
   err = clEnqueueWriteBuffer( queue, buf_gmv1, CL_FALSE,
                                 offset, size,
                                 &(eval.gmv1()[start_node]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue write gmv1 buffer on gpu.")) {
      return false;
   }
   events_wrbf.push_back( cur_event );
   // wait for write buffer events in copying gmv from host to gpu
   err = clWaitForEvents( events_wrbf.size(), &(events_wrbf[0]) );
   if( oclFunctionFailed(err,"wait for events in buffer write.")) {
      return false;
   }
   // get end time
   std::chrono::system_clock::time_point
      end_t = std::chrono::system_clock::now();
   std::clock_t end_cpu_t = std::clock();

   _elapsedSysTimeGmvHost2Gpu +=
      elapsedSysTimeInSeconds(start_t, end_t);
   _elapsedCpuTimeGmvHost2Gpu +=
      elapsedCpuTimeInSeconds(start_cpu_t, end_cpu_t);

   return true;
}


bool EvaluateCL::copyGmvFromGpuToHost( cl_uint pfm_num,
                                       cl_uint dev_num,
                                       cl_uint grp_id,
                                       int start_node,
                                       int num_nodes )
{
   _numGmvCopiesGpu2Host++;
   std::chrono::system_clock::time_point
      start_t = std::chrono::system_clock::now();
   std::clock_t start_cpu_t = std::clock();

   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   Evaluate & eval = (htDB::instance())->eval();

   cl_mem& buf_gmv0 = pfm.clMemGmv0( grp_id );
   cl_mem& buf_gmv1 = pfm.clMemGmv1( grp_id );
   cl_int err = 0;

   size_t offset = (size_t)start_node * sizeof(uint64_t);
   size_t size = (size_t)num_nodes * sizeof(uint64_t);

   std::vector< cl_event > events_rdbk;
   cl_event cur_event = NULL;
   // Read the gmv0 buffer back to the Host
   err = clEnqueueReadBuffer( queue, buf_gmv0, CL_FALSE,
                                 offset, size,
                                 &(eval.gmv0()[start_node]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue read gmv0 from gpu.")) {
      return false;
   }
   events_rdbk.push_back( cur_event );
   // Read the gmv1 buffer back to the Host
   err = clEnqueueReadBuffer( queue, buf_gmv1, CL_FALSE,
                                 offset, size,
                                 &(eval.gmv1()[start_node]),
                                 0, NULL, &cur_event);
   if( oclFunctionFailed( err, "enqueue read gmv1 from gpu.")) {
      return false;
   }
   events_rdbk.push_back( cur_event );
   // wait for buffer readback events in copying gmv from gpu to host
   err = clWaitForEvents( events_rdbk.size(), &(events_rdbk[0]) );
   if( oclFunctionFailed(err,"wait for events in buffer readback.")) {
      return false;
   }
   // get end time
   std::chrono::system_clock::time_point
      end_t = std::chrono::system_clock::now();
   std::clock_t end_cpu_t = std::clock();

   _elapsedSysTimeGmvGpu2Host +=
      elapsedSysTimeInSeconds(start_t, end_t);
   _elapsedCpuTimeGmvGpu2Host +=
      elapsedCpuTimeInSeconds(start_cpu_t, end_cpu_t);

   return true;
}


void EvaluateCL::dumpGmvValueOnGpu( cl_uint pfm_num,
                                    cl_uint dev_num,
                                    cl_uint grp_id,
                                    int start_node,
                                    int num_nodes,
                                    unsigned long mask1hot )
{
   if( !copyGmvFromGpuToHost( pfm_num,dev_num,grp_id,start_node,num_nodes)) {
      return;
   }
   Evaluate & eval = (htDB::instance())->eval();
   eval.printAllNodeValues( mask1hot ); // print value on 1-hot mask bit
}


bool EvaluateCL::dumpStatusOfSimEvents( int cur_lev,
                                        cl_uint pfm_num,
                                        cl_uint dev_num,
                                        LevelizedNetlist& ckt )
{
   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();

   cl_int err = 0;

   err |= clEnqueueReadBuffer( queue, pfm.clMemCkt(CL_BUF_Q_SIZES),
             CL_TRUE, 0, ckt.qSizes().size()*sizeof(int),
             &(ckt.qSizes()[0]), 0, NULL, NULL);

   err |= clEnqueueReadBuffer( queue, pfm.clMemCkt(CL_BUF_Q_FLAGS),
             CL_TRUE, 0, ckt.qFlags().size()*sizeof(char),
             &(ckt.qFlags()[0]), 0, NULL, NULL);

   err |= clEnqueueReadBuffer( queue, pfm.clMemCkt(CL_BUF_Q_EVENTS_FLAGS),
             CL_TRUE, 0, ckt.qEventsFlags().size()*sizeof(unsigned),
             &(ckt.qEventsFlags()[0]), 0, NULL, NULL);
   if( oclFunctionFailed( err, "enqueue read qSizes & qEventsFlags.")) {
      return false;
   }
   ckt.dumpOclSimEvents( cur_lev );
   return true;
}


void EvaluateCL::dumpAllNodesOnDev( cl_uint pfm_num,
                                    cl_uint dev_num,
                                    LevelizedNetlist& ckt,
                                    int up_to_lev,
                                    unsigned long mask1hot,
                                    const std::string& fn )
{
   HLog << "\n###### print all nodes on GPU for mask..\n  "
        << mask64ToBinary( mask1hot ) << "\n"
        << " up to level " << up_to_lev
        << " in file " << fn
        << std::endl;
   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   Evaluate & eval = (htDB::instance())->eval();
   cl_int err = 0;
   err |= clEnqueueReadBuffer( queue, pfm.clMemGmv0( 0 ), CL_TRUE, 0,
             ckt.numNodes()*sizeof(uint64_t), &(eval.gmv0()[0]),
             0, NULL, NULL);  // use GMV of group 0
   err |= clEnqueueReadBuffer( queue, pfm.clMemGmv1( 0 ), CL_TRUE, 0,
             ckt.numNodes()*sizeof(uint64_t), &(eval.gmv1()[0]),
             0, NULL, NULL);  // use GMV of group 0
   if( oclFunctionFailed( err, "enqueue read gmv0 & gmv1.")) {
      return;
   }

   eval.printAllNodeValues( mask1hot, up_to_lev, fn );
}


void EvaluateCL::dumpOutputsAndDffs( cl_uint pfm_num,
                                     cl_uint dev_num,
                                     LevelizedNetlist& ckt,
                                     unsigned long mask1hot )
{
   HLog << "###### print outputs and dffs for mask..\n  "
        << mask64ToBinary( mask1hot )
        << std::endl;
   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   Evaluate & eval = (htDB::instance())->eval();
   cl_int err = 0;
   err |= clEnqueueReadBuffer( queue, pfm.clMemGmv0( 0 ), CL_TRUE, 0,
             ckt.numNodes()*sizeof(uint64_t), &(eval.gmv0()[0]),
             0, NULL, NULL);  // use GMV of group 0
   err |= clEnqueueReadBuffer( queue, pfm.clMemGmv1( 0 ), CL_TRUE, 0,
             ckt.numNodes()*sizeof(uint64_t), &(eval.gmv1()[0]),
             0, NULL, NULL);  // use GMV of group 0
   if( oclFunctionFailed( err, "enqueue read gmv0 & gmv1.")) {
      return;
   }

   HLog << "### All node values\n";
   eval.printAllNodeValues( mask1hot );
   HLog << std::endl;

   std::string vals_po("");
   for( int i=0; i < ckt.numPOs(); i++ ) {
      int gid_po = ckt.output( i );
      vals_po += eval.nodeValue( gid_po, mask1hot);
   }
   // This is based on the old assumption of DFF modeled as a single DE
   // need to be modified. --- TBD
   std::string vals_ff("");
   int gid_ff = ckt.firstNodeFF();
   for( int i=0; i < ckt.numFFs(); i++, gid_ff++ ) {
      vals_ff += eval.nodeValue( gid_ff, mask1hot);
   }
   HLog << "### Measure po \n"
        << vals_po << std::endl;
   HLog << "### observe ff\n"
        << vals_ff << std::endl;
}


bool EvaluateCL::verifyBackgroundofEventDrivenSimulation(
                                                cl_uint pfm_num,
                                                cl_uint dev_num,
                                                LevelizedNetlist& ckt )
{

   HLog << "###### Verify background and others of event-driven simulation.."
        << std::endl;

   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   //OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   //cl_device_id device=ocl_info.platform(pfm_num).device(dev_num).deviceId();
   Evaluate & eval = (htDB::instance())->eval();

   cl_int err = 0;
   err |= clEnqueueReadBuffer( queue, pfm.clMemCkt(CL_BUF_Q_SIZES),CL_TRUE,
             0, ckt.qSizes().size()*sizeof(int), &(ckt.qSizes()[0]), 0,
             NULL, NULL);
   err |= clEnqueueReadBuffer( queue, pfm.clMemCkt(CL_BUF_Q_EVENTS_FLAGS),
             CL_TRUE, 0, ckt.qEventsFlags().size()*sizeof(unsigned),
             &(ckt.qEventsFlags()[0]), 0, NULL, NULL);
   err |= clEnqueueReadBuffer( queue, pfm.clMemGmv0( 0 ), CL_TRUE, 0,
             ckt.numNodes()*sizeof(uint64_t), &(eval.gmv0()[0]),
             0, NULL, NULL);  // use GMV of group 0
   err |= clEnqueueReadBuffer( queue, pfm.clMemGmv1( 0 ), CL_TRUE, 0,
             ckt.numNodes()*sizeof(uint64_t), &(eval.gmv1()[0]),
             0, NULL, NULL);  // use GMV of group 0
   if( oclFunctionFailed( err, "enqueue read-buffer qSizes[].")) {
      return false;
   }
   for( size_t i=0; i < ckt.qSizes().size(); i++) {
      if( ckt.qSizes()[i] != 0 ) {
         HLog << InfoError
              << "error detected in qSizes[" << i << "]:" << ckt.qSizes()[i]
              << std::endl;
         return false;
      }
   }
   for( size_t i=0; i< ckt.qEventsFlags().size(); i++) {
      if( ckt.qEventsFlags()[i] != 0) {
         HLog << InfoError
              << "error detected in qEventsFlags[" << i << "]:"
              << ckt.qEventsFlags()[i]
              << std::endl;
         return false;
      }
   }
   for( int i=0; i < ckt.numNodes(); i++ ) {
      if( eval.gmv0()[i] != ALL_ONES_64 || eval.gmv1()[i] != 0) {
         HLog << InfoError
              << "error detected in GMV of node " << i << "\n"
              << mask64ToBinary( eval.gmv0()[i] ) << "\n"
              << mask64ToBinary( eval.gmv1()[i] ) << std::endl;
         return false;
      }
   }
   return true;
}


bool EvaluateCL::goodSim1CycleEventDrivenOnGpu( cl_uint pfm_num,
                                                cl_uint dev_num,
                                                LevelizedNetlist& ckt,
                                                LogicSimMethod lsim_type,
                                                int cycle,
                                                int frame )
{
   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device=ocl_info.platform(pfm_num).device(dev_num).deviceId();

   //ckt.dumpSuccsSucc1st(); // LL debug
   cl_kernel& kn_gsim = (EVENT_FLAGS_BASED == lsim_type) ?
                        pfm.kernel( KERNEL_GSIM_BUF_EVENT_FLAG ) :
                        pfm.kernel( KERNEL_GSIM_BUF_EVENT );
   //HLog << "# Ocl e-gsim on "<<cl_dev.deviceName()<< std::endl; // LL debug
   cl_int err = 0;
   size_t remainingWorks = 0;
   unsigned base_node_id = 0;
   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   // starting kernel parameter index
   const int spi = (EVENT_FLAGS_BASED == lsim_type) ? 13 : 14;
   // 14     const int curr_lev,
   // 15     const unsigned base_node_id,
   // 16     const unsigned padding_bound
   // copy buf_q_sizes[lev] from GPU to HOST
   err = clEnqueueReadBuffer( queue, pfm.clMemCkt(CL_BUF_Q_SIZES),
            CL_TRUE, 0, ckt.qSizes().size()*sizeof(int),
            &(ckt.qSizes()[0]), 0, NULL, NULL);
   //         lev, sizeof(int), &(ckt.qSizes()[0]), 0, NULL, NULL);
   if( oclFunctionFailed( err, "enqueue read-buffer qSizes[].")) {
      return false;
   }
   bool there_are_events = true;
#ifdef HT_DEBUG
   int pass_num = 0;
   //bool node_value_not_dumped = true; // keep for later debug
#endif
   while( there_are_events ) {
      for( int lev=0; lev < ckt.numLevels(); lev++ ) {
         if( 0==lev ) {
            int ecnt=std::accumulate(
               //ckt.qSizes().begin()+1,ckt.qSizes().end(),0);
               ckt.qSizes().begin(),ckt.qSizes().end(),0);
            if( 0==ecnt ) { // event count is 0 across levels
               there_are_events = false;
               break; // break out of for loop, and then the outer while loop
            }
#ifdef HT_DEBUG
            HLog << "  " << pass_num << " pass of logic sim" << std::endl;
            pass_num++;
#endif
         }
         base_node_id   = (EVENT_FLAGS_BASED == lsim_type) ?
                          (unsigned)ckt.firstNodeInLevel()[ lev ] : 0;
         remainingWorks = (EVENT_FLAGS_BASED == lsim_type) ?
            (ckt.qSizes()[lev] ? (size_t)ckt.numNodesPerLevel()[ lev ] : 0) :
            (size_t)ckt.qSizes()[ lev ];
#ifdef HT_DEBUG
         // dumping GPU sim values up to certain level
         /*
         if( 16== lev && 0==cycle && 0==frame ) {
            if( node_value_not_dumped ) {
               node_value_not_dumped = false;
               dumpAllNodesOnDev(pfm_num,dev_num,ckt,
                  lev-1, MASK_1HOT_64[0], "m21.nodes.gpu");
            }
         }
         */
         //HLog << "Pre simulate ";
         //HLog << "level: "  << lev
         //     << " qSize: " << ckt.qSizes()[lev] << std::endl;
         //HLog << "#### Post cycle:"<< cyc <<" frame:"<<frame<< std::endl;
         //if( lev==3 ) {
         //   dumpAllNodesOnDev(pfm_num,dev_num,ckt,MASK_1HOT_64[0]);// LL debug
         //}
         //dumpStatusOfSimEvents( lev, pfm_num, dev_num, ckt ); //LL debug
#endif
         if( 0==remainingWorks ) {
            continue; // skip current iteration if queue is empty
         }
         err = clSetKernelArg( kn_gsim,spi, sizeof(int),&lev);// for curr_lev
         if( debugFlag() & EVAL_CL_DBG_KERNEL ) {
            HLog << InfoNote
                 << "enqueue kernel of good sim on level " << lev
                 << std::endl;
         }
         std::vector< cl_event > events_lev;
         while( remainingWorks > 0 ) {
            size_t effective_size = 0;
            cl_uint work_dim = 0;
            allocateWorkSize( remainingWorks, cl_dev, work_dim,
                              globalWorkSize, localWorkSize, effective_size );
            unsigned padding_bound=(unsigned)(base_node_id+ effective_size- 1);
            // change padding_bound per iteration
            err|=clSetKernelArg(kn_gsim,spi+1,sizeof(unsigned),&base_node_id);
            err|=clSetKernelArg(kn_gsim,spi+2,sizeof(unsigned),&padding_bound);

            if( oclFunctionFailed( err,"set args for kn_gsim ...")) {
               return false;
            }
            if( debugFlag() & EVAL_CL_DBG_WORK_SIZE) {
               printClEnqueueParameters( work_dim,
                  globalWorkOffset, globalWorkSize, localWorkSize,
                  base_node_id, padding_bound, effective_size );
            }
            if( !checkKernelWorkGroupSize( kn_gsim,device,localWorkSize[0])){
               return false;
            }
            _numGsimEnqueues++; // track number of good sim enqueues
            cl_event cur_event;
            err = clEnqueueNDRangeKernel(
                            queue, kn_gsim, work_dim,
                            &(globalWorkOffset[0]),
                            &(globalWorkSize[0]),
                            &(localWorkSize[0]), // NULL does not show benefit
                            0,
                            NULL,
                            &cur_event ); // event
            if( oclFunctionFailed(err,"enqueue kernel_good_sim_e.")) {
               return false;
            }
            events_lev.push_back( cur_event );
            // update base_node_id and remainingWorks
            base_node_id += (unsigned)effective_size;
            remainingWorks -= effective_size;
         }
         // wait for events in current level before going to next level
         err = clWaitForEvents( events_lev.size(), &(events_lev[0]));
         if( oclFunctionFailed(err,"wait for events in kn_gsim.")) {
            return false;
         }
#ifdef HT_DEBUG // flush out printf in ocl statements
         err = clFinish( queue );
         if( oclFunctionFailed(err,"launch clFinish")) {
            return false;
         }
#endif
         if( EVENT_FLAGS_BASED == lsim_type ) {
            // reset qSizes[lev] as all events in the level are simulated
            int zero = 0;
            err = clEnqueueFillBuffer( queue, pfm.clMemCkt(CL_BUF_Q_SIZES),
                  &zero,sizeof(int),lev*sizeof(int),sizeof(int),0,NULL,NULL );
            if( oclFunctionFailed( err,"enqueue fill-buffer qSizes[lev].")) {
               return false;
            }
         }
         // update ckt.qSizes() when sim of current level is done
         err = clEnqueueReadBuffer( queue, pfm.clMemCkt(CL_BUF_Q_SIZES),
                  CL_TRUE, 0, ckt.qSizes().size()*sizeof(int),
                  &(ckt.qSizes()[0]), 0, NULL, NULL);
         //         lev, sizeof(int), &(ckt.qSizes()[0]), 0, NULL, NULL);
         if( oclFunctionFailed( err, "enqueue read-buffer qSizes[] 2nd.")) {
            return false;
         }
      }
#ifdef HT_DEBUG
      // there_are_events = false; // force exit for debugging
#endif
      // only simulate one-pass for DE-based 1-frame/cycle simulation
      if( ckt.numFramesPerCycle()==1 ) {
         there_are_events = false;
      }
   }
   return true;
}


//////////////////////////////////////////////////////////////////////////
//  Now that DFFs are at last level, levels are simulated in its natural
//    order of 1, 2, 3, ..., numLevels-1
//////////////////////////////////////////////////////////////////////////
bool EvaluateCL::goodSim1CycleOnGpu( cl_uint pfm_num,
                                     cl_uint dev_num,
                                     LevelizedNetlist& ckt )
{
   // retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device=ocl_info.platform(pfm_num).device(dev_num).deviceId();

   bool use_buf_kernel = (cl_dev.cktClMemType() == CKT_BUFFER);
   cl_kernel& kn_good_sim = use_buf_kernel ?
      pfm.kernel( KERNEL_GSIM_BUF ) :
      pfm.kernel( KERNEL_GSIM_IMG );
   // cl_kernel& kn_gsim_buf = pfm.kernel( KERNEL_GSIM_BUF );
   //HLog << "# OcL gsim on " << cl_dev.deviceName() << std::endl; // LL debug
   cl_int err = 0;
   size_t remainingWorks = (size_t)ckt.numNodes();
   unsigned base_node_id = 0;
   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   for( int ix=1; ix < ckt.numLevels(); ix++ ) {
      const int l = ix; // use its natural order
      base_node_id = (unsigned)ckt.firstNodeInLevel()[ l ];
      remainingWorks = (size_t)ckt.numNodesPerLevel()[ l ];
      if( ckt.clockedDffIsUsed() && (l+1)==ckt.numLevels() ) {
         // adjust for remodeled DFF at last level
         remainingWorks = (size_t)(ckt.numFBBUFs() + ckt.numFFs());
      }
      if( debugFlag() & EVAL_CL_DBG_KERNEL ) {
         HLog << InfoNote
              << "enqueue kernel of good sim on level " << l
              << std::endl;
      }
      std::vector< cl_event > events_per_level;
      while( remainingWorks > 0 ) {
         size_t effective_size = 0;
         cl_uint work_dim = 0;
         allocateWorkSize( remainingWorks, cl_dev, work_dim,
                           globalWorkSize, localWorkSize, effective_size );
         unsigned padding_bound = (unsigned)(base_node_id+ effective_size- 1);
         // change padding_bound per iteration
         if( use_buf_kernel ) {
            err = clSetKernelArg( kn_good_sim, 10, sizeof(unsigned),
                                  &base_node_id );
            err|= clSetKernelArg( kn_good_sim, 11, sizeof(unsigned),
                                  &padding_bound );
            if( (htDB::instance())->simOptions().nonDivergentGsimKernel()) {
               int max_ins = ckt.maxInputsInLevel(l);
               int and_on = ckt.andIsOnInLevel(l);
               int or_on = ckt.orIsOnInLevel(l);
               int xor_on = ckt.xorIsOnInLevel(l);
               int inv_on = ckt.invIsOnInLevel(l);
               if(0) {
                  HLog << InfoNote
                       << "level:" << l
                       << " max_ins:" << max_ins
                       << " AND:" << and_on
                       << "  OR:" << or_on
                       << " XOR:" << xor_on
                       << " INV:" << inv_on
                       << std::endl;
               }
               err|= clSetKernelArg( kn_good_sim, 12, sizeof(int), &max_ins);
               err|= clSetKernelArg( kn_good_sim, 13, sizeof(int), &and_on);
               err|= clSetKernelArg( kn_good_sim, 14, sizeof(int), &or_on);
               err|= clSetKernelArg( kn_good_sim, 15, sizeof(int), &xor_on);
               err|= clSetKernelArg( kn_good_sim, 16, sizeof(int), &inv_on);
            }
         } else {
            err = clSetKernelArg( kn_good_sim, 6, sizeof(unsigned),
                                  &base_node_id );
            err|= clSetKernelArg( kn_good_sim, 7, sizeof(unsigned),
                                  &padding_bound );
         }

         if( oclFunctionFailed( err,"set args for kn_good_sim ...")) {
            return false;
         }
         if( debugFlag() & EVAL_CL_DBG_WORK_SIZE) {
            printClEnqueueParameters( work_dim,
               globalWorkOffset, globalWorkSize, localWorkSize,
               base_node_id, padding_bound, effective_size );
         }
         if( !checkKernelWorkGroupSize(kn_good_sim, device,localWorkSize[0])){
            return false;
         }
         _numGsimEnqueues++; // track number of good sim enqueues
         cl_event cur_event;
         err = clEnqueueNDRangeKernel(
                         queue, kn_good_sim, work_dim,
                         &(globalWorkOffset[0]),
                         &(globalWorkSize[0]),
                         &(localWorkSize[0]), // NULL does not show benefit
                         0,
                         NULL,
                         &cur_event ); // event
         if( oclFunctionFailed(err,"enqueue kernel_good_sim.")) {
            return false;
         }
         events_per_level.push_back( cur_event );
         // update base_node_id and remainingWorks
         base_node_id += (unsigned)effective_size;
         remainingWorks -= effective_size;
      }
      // wait for events in current level to finish before going to next level
      err = clWaitForEvents( events_per_level.size(), &(events_per_level[0]));
      if( oclFunctionFailed(err,"wait for events in kn_good_sim.")) {
         return false;
      }
   }
   return true;
}


bool EvaluateCL::assignDataInputsOnGPU( cl_uint pfm_num,
                                        cl_uint dev_num,
                                        LevelizedNetlist& ckt,
                                        int grp,
                                        int cyc,
                                        int frame,
                                        bool last_cycle,
                                        bool io_per_cycle,
                                        LogicSimMethod lsim_type,
                                        std::vector< cl_event >& events,
                                        bool verbose )
{
   if( frame > 0 || (cyc>0 && !io_per_cycle) || ckt.numDataPIs()==0) {
      // no need to assign data inputs for non-0 frames
      //   or cycle > 0 and io_per_cycle is false
      return true;
   }
   // Retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device=ocl_info.platform(pfm_num).device(dev_num).deviceId();

   cl_kernel& kn_assign_dpi = (EVENT_FLAGS_BASED == lsim_type ) ?
                              pfm.kernel( KERNEL_ASSIGN_DATA_PI_FLAG ) :
                              pfm.kernel( KERNEL_ASSIGN_DATA_PI );
   cl_mem& pat_in_gmv0   = pfm.clMemPatInGmv0( grp );
   cl_mem& pat_in_gmv1   = pfm.clMemPatInGmv1( grp );
   // pat_in_offset is non-zero when cyc is not 0 and io_per_cycle is true
   unsigned pat_in_offset = (0==cyc) ? 0 :
                            (unsigned)(ckt.numDataPIs() * cyc + ckt.numFFs());
   cl_int err = 0;

   int event_driven = (IN_ORDER != lsim_type) ? 1 : 0;
   // start kernel parameter index
   const int spi = (EVENT_FLAGS_BASED == lsim_type) ? 13 : 15;
   err|= clSetKernelArg(kn_assign_dpi,spi, sizeof(int),&event_driven);
   err|= clSetKernelArg(kn_assign_dpi,spi+1, sizeof(cl_mem), &pat_in_gmv0);
   err|= clSetKernelArg(kn_assign_dpi,spi+2, sizeof(cl_mem), &pat_in_gmv1);
   err|= clSetKernelArg(kn_assign_dpi,spi+3, sizeof(unsigned), &pat_in_offset);

   if( oclFunctionFailed(err, "set parameters for kn_assign_dpi(15-18)")) {
      return false;
   }
   if( verbose ) {
      HLog << InfoNote
           << "enqueue kernel to assign dpi at cycle " << cyc
           << " frame " << frame << std::endl;
   }

   size_t remainingWorks = (size_t)ckt.numDataPIs();
   unsigned base_node_id = 0;
   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   while( remainingWorks > 0 ) {
      size_t effective_size = 0;
      cl_uint work_dim = 0;
      allocateWorkSize( remainingWorks, cl_dev, work_dim,
                        globalWorkSize, localWorkSize, effective_size );
      unsigned padding_bound = (unsigned)(base_node_id + effective_size - 1);
      err|=clSetKernelArg(kn_assign_dpi,spi+4,sizeof(unsigned),&base_node_id);
      err|=clSetKernelArg(kn_assign_dpi,spi+5,sizeof(unsigned),&padding_bound);
      if( oclFunctionFailed( err,"set parameters for kn_assign_dpi(19,20)")) {
         return false;
      }
      if( debugFlag() & EVAL_CL_DBG_WORK_SIZE ) {
         printClEnqueueParameters( work_dim, globalWorkOffset, globalWorkSize,
            localWorkSize, base_node_id, padding_bound, effective_size );
      }
      if( !checkKernelWorkGroupSize(kn_assign_dpi,device,localWorkSize[0])) {
         return false;
      }
      cl_event cur_event;
      err = clEnqueueNDRangeKernel(
                      queue, kn_assign_dpi, work_dim,
                      &(globalWorkOffset[0]),
                      &(globalWorkSize[0]),
                      &(localWorkSize[0]),
                      0, // num_events_in_wait_list
                      NULL, // event_wait_list
                      &cur_event ); // event
      if( oclFunctionFailed(err,"enqueue kernel_assign_dpi for PIs")) {
         return false;
      }
      events.push_back( cur_event );
      // update base_node_id and remainingWorks
      base_node_id += (unsigned)effective_size;
      remainingWorks -= effective_size;
#ifdef HT_DEBUG
      err = clFinish( queue );
      if( oclFunctionFailed(err,"launch clFinish")) {
         return false;
      }
#endif
   }
#ifdef HT_DEBUG
   //HLog << "post assign data input" << std::endl;
   //dumpStatusOfSimEvents( 0, pfm_num, dev_num, ckt ); // LL debug
#endif
   return true;
}


bool EvaluateCL::assignClockInputsOnGPU( cl_uint pfm_num,
                                         cl_uint dev_num,
                                         LevelizedNetlist& ckt,
                                         int grp,
                                         int cyc,
                                         int frame,
                                         bool last_cycle,
                                         bool io_per_cycle,
                                         LogicSimMethod lsim_type,
                                         std::vector< cl_event >& events,
                                         bool verbose )
{
   if( ckt.numClocks()==0 ) {
      // no need to assign clock inputs if there is no clock
      return true;
   }
   // Retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device=ocl_info.platform(pfm_num).device(dev_num).deviceId();
   cl_kernel& kn_assign_cpi = (EVENT_FLAGS_BASED == lsim_type) ?
                              pfm.kernel( KERNEL_ASSIGN_CLOCK_PI_FLAG ) :
                              pfm.kernel( KERNEL_ASSIGN_CLOCK_PI );
   cl_int err = 0;
   int event_driven = (IN_ORDER != lsim_type) ? 1 : 0;
   int clock_is_on = (1==frame) ? 1 : 0;
   // starting kernel parameter index
   const int spi = (EVENT_FLAGS_BASED == lsim_type) ? 13 : 15;
   err |= clSetKernelArg(kn_assign_cpi,spi, sizeof(int),&event_driven);
   err |= clSetKernelArg(kn_assign_cpi,spi+1,sizeof(int),&clock_is_on);
   if( oclFunctionFailed(err, "set parameters for kn_assign_cpi(15,16)")) {
      return false;
   }
   if( verbose ) {
      HLog << InfoNote
           << "enqueue kernel to assign cpi at cycle " << cyc
           << " frame " << frame << std::endl;
   }
   size_t remainingWorks = (size_t)ckt.numClocks();
   unsigned base_node_id = 0;
   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   while( remainingWorks > 0 ) {
      size_t effective_size = 0;
      cl_uint work_dim = 0;
      allocateWorkSize( remainingWorks, cl_dev, work_dim,
                        globalWorkSize, localWorkSize, effective_size );
      unsigned padding_bound = (unsigned)(base_node_id + effective_size - 1);
      err|=clSetKernelArg(kn_assign_cpi,spi+2,sizeof(unsigned),&base_node_id);
      err|=clSetKernelArg(kn_assign_cpi,spi+3,sizeof(unsigned),&padding_bound);
      if( oclFunctionFailed( err,"set parameters for kn_assign_cpi(17,18)")) {
         return false;
      }
      if( debugFlag() & EVAL_CL_DBG_WORK_SIZE ) {
         printClEnqueueParameters( work_dim, globalWorkOffset, globalWorkSize,
            localWorkSize, base_node_id, padding_bound, effective_size );
      }
      if( !checkKernelWorkGroupSize(kn_assign_cpi,device,localWorkSize[0])) {
         return false;
      }
      cl_event cur_event;
      err = clEnqueueNDRangeKernel(
                      queue, kn_assign_cpi, work_dim,
                      &(globalWorkOffset[0]),
                      &(globalWorkSize[0]),
                      &(localWorkSize[0]),
                      0, // num_events_in_wait_list
                      NULL, // event_wait_list
                      &cur_event ); // event
      if( oclFunctionFailed(err,"enqueue kernel_assign_cpi for Clocks")) {
         return false;
      }
      events.push_back( cur_event );
      // update base_node_id and remainingWorks
      base_node_id += (unsigned)effective_size;
      remainingWorks -= effective_size;
   }
#ifdef HT_DEBUG
   //HLog << "post assign clock input" << std::endl;
   //dumpStatusOfSimEvents( 0, pfm_num, dev_num, ckt ); // LL debug
#endif
   return true;
}


bool EvaluateCL::assignInitFFsOnGPU( cl_uint pfm_num,
                                     cl_uint dev_num,
                                     LevelizedNetlist& ckt,
                                     int grp,
                                     int cyc,
                                     int frame,
                                     bool last_cycle,
                                     bool io_per_cycle,
                                     LogicSimMethod lsim_type,
                                     std::vector< cl_event >& events,
                                     bool verbose )
{
   // Only need to assign init_ff when cycle==0 and frame==0
   if( frame > 0 || cyc > 0 || ckt.numFFs()==0) {
      return true;
   }
   // Retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device=ocl_info.platform(pfm_num).device(dev_num).deviceId();

   cl_kernel& kn_assign_iff = (EVENT_FLAGS_BASED == lsim_type) ?
                              pfm.kernel( KERNEL_ASSIGN_INIT_FF_FLAG ) :
                              pfm.kernel( KERNEL_ASSIGN_INIT_FF );
   cl_mem& pat_in_gmv0   = pfm.clMemPatInGmv0( grp );
   cl_mem& pat_in_gmv1   = pfm.clMemPatInGmv1( grp );
   // pat_in_offset is non-zero when cyc is not 0 and io_per_cycle is true
   unsigned pat_in_offset = (unsigned)ckt.numDataPIs();
   cl_int err = 0;

   int event_driven = (IN_ORDER != lsim_type) ? 1 : 0;
   // starting kernel parameter index
   const int spi = (EVENT_FLAGS_BASED == lsim_type) ? 13 : 14;
   err|= clSetKernelArg(kn_assign_iff,spi,sizeof(int), &event_driven);
   err|= clSetKernelArg(kn_assign_iff,spi+1,sizeof(cl_mem), &pat_in_gmv0);
   err|= clSetKernelArg(kn_assign_iff,spi+2,sizeof(cl_mem), &pat_in_gmv1);
   err|= clSetKernelArg(kn_assign_iff,spi+3,sizeof(unsigned), &pat_in_offset);

   if( oclFunctionFailed(err, "set parameters for kn_assign_iff(14-17)")) {
      return false;
   }
   if( verbose ) {
      HLog << InfoNote
           << "enqueue kernel to assign iff at cycle " << cyc
           << " frame " << frame << std::endl;
   }
   size_t remainingWorks = (size_t)ckt.numFFs();
   unsigned base_node_id = 0;
   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   while( remainingWorks > 0 ) {
      size_t effective_size = 0;
      cl_uint work_dim = 0;
      allocateWorkSize( remainingWorks, cl_dev, work_dim,
                        globalWorkSize, localWorkSize, effective_size );
      unsigned padding_bound = (unsigned)(base_node_id + effective_size - 1);
      err =clSetKernelArg(kn_assign_iff,spi+4,sizeof(unsigned),&base_node_id);
      err|=clSetKernelArg(kn_assign_iff,spi+5,sizeof(unsigned),&padding_bound);
      if( oclFunctionFailed( err,"set parameters for kn_assign_iff(18,19)")) {
         return false;
      }
      if( debugFlag() & EVAL_CL_DBG_WORK_SIZE ) {
         printClEnqueueParameters( work_dim, globalWorkOffset, globalWorkSize,
            localWorkSize, base_node_id, padding_bound, effective_size );
      }
      if( !checkKernelWorkGroupSize(kn_assign_iff,device,localWorkSize[0])) {
         return false;
      }
      cl_event cur_event;
      err = clEnqueueNDRangeKernel(
                      queue, kn_assign_iff, work_dim,
                      &(globalWorkOffset[0]),
                      &(globalWorkSize[0]),
                      &(localWorkSize[0]),
                      0, // num_events_in_wait_list
                      NULL, // event_wait_list
                      &cur_event ); // event
      if( oclFunctionFailed(err,"enqueue kernel_assign_iff for FFs")) {
         return false;
      }
      events.push_back( cur_event );
      // update base_node_id and remainingWorks
      base_node_id += (unsigned)effective_size;
      remainingWorks -= effective_size;
   }
#ifdef HT_DEBUG
   //HLog << "post assign init ff" << std::endl;
   //dumpStatusOfSimEvents( 0, pfm_num, dev_num, ckt ); // LL debug
#endif
   return true;
}


bool EvaluateCL::assignInputsPerCycleOnGpu( cl_uint pfm_num,
                                            cl_uint dev_num,
                                            LevelizedNetlist& ckt,
                                            int grp,
                                            int cyc,
                                            int frame,
                                            bool last_cycle,
                                            bool io_per_cycle,
                                            LogicSimMethod lsim_type,
                                            bool verbose )
{
   std::vector< cl_event > events;
   if( !assignDataInputsOnGPU( pfm_num, dev_num, ckt, grp, cyc, frame,
      last_cycle, io_per_cycle, lsim_type , events, verbose )) {
      return false;
   }
   //unsigned long m1 = 0x1; // 1-hot mask
   //dumpGmvValueOnGpu( pfm_num,dev_num,0,0,ckt.numNodes(),m1); // LL debug
   if( !assignClockInputsOnGPU( pfm_num, dev_num, ckt, grp, cyc, frame,
      last_cycle, io_per_cycle, lsim_type, events, verbose )) {
      return false;
   }
   //dumpGmvValueOnGpu( pfm_num,dev_num,0,0,ckt.numNodes(),m1); // LL debug
   if( !assignInitFFsOnGPU( pfm_num, dev_num, ckt, grp, cyc, frame,
      last_cycle, io_per_cycle, lsim_type, events, verbose )) {
      return false;
   }
   if( events.size()) {
      cl_int err = clWaitForEvents( events.size(), &(events[0]) );
      if( oclFunctionFailed(err,"wait for events in assigning PIs and DFFs")){
         HLog << "events size " << events.size() << std::endl;
         return false;
      }
   }
   //dumpGmvValueOnGpu( pfm_num,dev_num,0,0,ckt.numNodes(),m1); // LL debug
   return true;
}


bool EvaluateCL::saveMeasurePOsOnGpu( cl_uint pfm_num,
                                      cl_uint dev_num,
                                      LevelizedNetlist& ckt,
                                      int grp,
                                      int cyc,
                                      int frame,
                                      bool last_cycle,
                                      bool io_per_cycle,
                                      std::vector< cl_event >& events,
                                      bool verbose )
{
   // POs are measured if it is the last cycle or 'io_per_cycle' is true
   //   and frame==0.
   if(!((last_cycle || io_per_cycle) && 0==frame) || ckt.numPOs()==0) {
      return true;
   }
   // Retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device=ocl_info.platform(pfm_num).device(dev_num).deviceId();

   cl_kernel& kn_save_outputs = pfm.kernel( KERNEL_SAVE_OUTPUTS );
   cl_mem& pat_out_gmv0   = pfm.clMemPatOutGmv0( grp );
   cl_mem& pat_out_gmv1   = pfm.clMemPatOutGmv1( grp );

   // pout_offset is non-zero when cyc is not 0 and io_per_cycle is true
   unsigned pout_offset = (io_per_cycle) ? (unsigned)(ckt.numPOs() * cyc) : 0;
   cl_int err = 0;

   err = clSetKernelArg(kn_save_outputs,3,sizeof(cl_mem), &pat_out_gmv0);
   err|= clSetKernelArg(kn_save_outputs,4,sizeof(cl_mem), &pat_out_gmv1);
   err|= clSetKernelArg(kn_save_outputs,5,sizeof(unsigned), &pout_offset);
   if( oclFunctionFailed( err,
       "set base_node_id and padding_bound for kn_save_outputs(3,4,5)")) {
      return false;
   }
   if( verbose ) {
      HLog << InfoNote
           << "enqueue kernel to save mpo at cycle " << cyc
           << " frame " << frame << std::endl;
   }
   // for kernel_save_outputs
   size_t remainingWorks = (size_t)ckt.numPOs();
   unsigned base_node_id = 0;
   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   while( remainingWorks > 0 ) {
      size_t effective_size = 0;
      cl_uint work_dim = 0;
      allocateWorkSize( remainingWorks, cl_dev, work_dim,
                        globalWorkSize, localWorkSize, effective_size );
      unsigned padding_bound = (unsigned)(base_node_id + effective_size - 1);
      err =clSetKernelArg(kn_save_outputs,6,sizeof(unsigned),&base_node_id);
      err|=clSetKernelArg(kn_save_outputs,7,sizeof(unsigned),&padding_bound);
      if( oclFunctionFailed( err,
          "set base_node_id and padding_bound for kn_save_outputs(6,7)")) {
         return false;
      }
      if( debugFlag() & EVAL_CL_DBG_WORK_SIZE ) {
         printClEnqueueParameters( work_dim, globalWorkOffset, globalWorkSize,
            localWorkSize, base_node_id, padding_bound, effective_size );
      }
      if( !checkKernelWorkGroupSize(kn_save_outputs,device,localWorkSize[0])){
         return false;
      }
      cl_event cur_event;
      err = clEnqueueNDRangeKernel(
                      queue, kn_save_outputs, work_dim,
                      &(globalWorkOffset[0]),
                      &(globalWorkSize[0]),
                      &(localWorkSize[0]),
                      0, // num_events_in_wait_list
                      NULL, // event_wait_list
                      &cur_event ); // event
      if( oclFunctionFailed(err,"enqueue kernel_save_outputs")) {
         return false;
      }
      events.push_back( cur_event );
      // update base_node_id and remainingWorks
      base_node_id += (unsigned)effective_size;
      remainingWorks -= effective_size;
   }
   return true;
}


bool EvaluateCL::saveObserveFFsOnGpu( cl_uint pfm_num,
                                      cl_uint dev_num,
                                      LevelizedNetlist& ckt,
                                      int grp,
                                      int cyc,
                                      int frame,
                                      bool last_cycle,
                                      bool io_per_cycle,
                                      std::vector< cl_event >& events,
                                      bool verbose )
{
   // FFs are observed at last cycle and last frame
   if((ckt.clockedDffIsUsed() &&2!=frame) || !last_cycle || ckt.numFFs()==0) {
      return true;
   }
   // Retrieve ocl pfm, context, queue, and device,
   OpenCLInfo & ocl_info = (htDB::instance())->openCLInfo();
   OpenCLPlatform & pfm = ocl_info.platform( pfm_num );
   cl_command_queue queue=ocl_info.platform(pfm_num).device(dev_num).queue();
   OpenCLDevice& cl_dev = ocl_info.platform(pfm_num).device(dev_num);
   cl_device_id device=ocl_info.platform(pfm_num).device(dev_num).deviceId();

   cl_kernel& kn_save_obs_ffs = pfm.kernel( KERNEL_SAVE_OBS_FFS );
   cl_mem& pat_out_gmv0   = pfm.clMemPatOutGmv0( grp );
   cl_mem& pat_out_gmv1   = pfm.clMemPatOutGmv1( grp );

   // for kernel_save_obs_ffs
   unsigned pout_offset = (io_per_cycle) ? (unsigned)(ckt.numPOs() * cyc) : 0;
   unsigned offs_offset = pout_offset + (unsigned)ckt.numPOs();
   cl_int err = 0;
   err = clSetKernelArg(kn_save_obs_ffs,4,sizeof(cl_mem), &pat_out_gmv0);
   err|= clSetKernelArg(kn_save_obs_ffs,5,sizeof(cl_mem), &pat_out_gmv1);
   err|= clSetKernelArg(kn_save_obs_ffs,6,sizeof(unsigned), &offs_offset);
   if( oclFunctionFailed( err,
       "set base_node_id and padding_bound for kn_save_obs_ffs(4,5,6)")) {
      return false;
   }
   if( verbose ) {
      HLog << InfoNote
           << "enqueue kernel to save off at cycle " << cyc
           << " frame " << frame << std::endl;
   }
   size_t remainingWorks = (size_t)ckt.numFFs();
   std::vector<size_t> globalWorkOffset(3, 0);
   std::vector<size_t> globalWorkSize(3, 0);
   std::vector<size_t> localWorkSize(3, 0);
   unsigned base_node_id = 0;
   while( remainingWorks > 0 ) {
      size_t effective_size = 0;
      cl_uint work_dim = 0;
      allocateWorkSize( remainingWorks, cl_dev, work_dim,
                        globalWorkSize, localWorkSize, effective_size );
      unsigned padding_bound = (unsigned)(base_node_id + effective_size - 1);
      err =clSetKernelArg(kn_save_obs_ffs,7,sizeof(unsigned),&base_node_id);
      err|=clSetKernelArg(kn_save_obs_ffs,8,sizeof(unsigned),&padding_bound);
      if( oclFunctionFailed( err,
          "set base_node_id and padding_bound for kn_save_obs_ffs(7,8)")) {
         return false;
      }
      if( debugFlag() & EVAL_CL_DBG_WORK_SIZE ) {
         printClEnqueueParameters( work_dim, globalWorkOffset, globalWorkSize,
            localWorkSize, base_node_id, padding_bound, effective_size );
      }
      if( !checkKernelWorkGroupSize(kn_save_obs_ffs,device,localWorkSize[0])){
         return false;
      }
      cl_event cur_event;
      err = clEnqueueNDRangeKernel(
                      queue, kn_save_obs_ffs, work_dim,
                      &(globalWorkOffset[0]),
                      &(globalWorkSize[0]),
                      &(localWorkSize[0]),
                      0, // num_events_in_wait_list
                      NULL, // event_wait_list
                      &cur_event ); // event
      if( oclFunctionFailed(err,"enqueue kernel_save_obs_ffs")) {
         return false;
      }
      events.push_back( cur_event );
      // update base_node_id and remainingWorks
      base_node_id += (unsigned)effective_size;
      remainingWorks -= effective_size;
   }
   return true;
}


bool EvaluateCL::saveResponsesPerCycleOnGpu( cl_uint pfm_num,
                                             cl_uint dev_num,
                                             LevelizedNetlist& ckt,
                                             int grp,
                                             int cyc,
                                             int frame,
                                             bool last_cycle,
                                             bool io_per_cycle,
                                             bool verbose )
{
   std::vector< cl_event > events;
   if( !saveMeasurePOsOnGpu( pfm_num, dev_num, ckt, grp, cyc, frame,
                             last_cycle, io_per_cycle, events, verbose )) {
      return false;
   }
   //unsigned long m1 = 0x1; // LL debug, 1-hot mask
   //dumpGmvValueOnGpu( pfm_num,dev_num,0,0,ckt.numNodes(),m1); // LL debug
   if( !saveObserveFFsOnGpu( pfm_num, dev_num, ckt, grp, cyc, frame,
                             last_cycle, io_per_cycle, events, verbose )) {
      return false;
   }
   //dumpGmvValueOnGpu( pfm_num,dev_num,0,0,ckt.numNodes(),m1); // LL debug
   // Wait for completion of saving outputs and observe_ff
   if( events.size() ) {
      cl_int err = clWaitForEvents( events.size(), &(events[0]) );
      if( oclFunctionFailed(err,"wait for events in saving m-po and o-ff")) {
         return false;
      }
   }
   return true;
}


bool EvaluateCL::goodSim1PsetOnGpu( cl_uint pfm_num,
                                    cl_uint dev_num,
                                    LevelizedNetlist& ckt,
                                    int grp,
                                    int n_cycles,
                                    bool io_per_cycle,
                                    LogicSimMethod lsim_type )
{

   bool re = false;
   bool verbose = false; // can be moved to upper level
#ifdef HT_DEBUG
   //verbose = true;
#endif
   for( int cyc=0; cyc < n_cycles; cyc++ ) {
      bool last_cycle = ((n_cycles-1)==cyc) ? true : false;
      for( int frame=0; frame < ckt.numFramesPerCycle(); frame++ ) {
         // Assign input stimulus
         re = assignInputsPerCycleOnGpu( pfm_num, dev_num, ckt, grp, cyc,
                 frame, last_cycle, io_per_cycle, lsim_type, verbose);
#ifdef HT_DEBUG
         HLog <<"in cycle "<< cyc <<"  frame "<< frame << std::endl;
         //dumpStatusOfSimEvents( 0, pfm_num, dev_num, ckt );
#endif
         if( !re ) { return false; }
         // Simulate netlist in its levelized order 1, 2, ..., n-1
         re = (IN_ORDER != lsim_type) ?
              goodSim1CycleEventDrivenOnGpu(
                 pfm_num,dev_num,ckt,lsim_type, cyc, frame ):
              goodSim1CycleOnGpu( pfm_num, dev_num, ckt);
         if( !re ) { return false; }
         // Save output responses
         re = saveResponsesPerCycleOnGpu( pfm_num, dev_num, ckt, grp, cyc,
                 frame, last_cycle, io_per_cycle, verbose);
#ifdef HT_DEBUG
         //HLog << "#### Post cycle:"<< cyc <<" frame:"<<frame<< std::endl;
         //dumpAllNodesOnDev(pfm_num,dev_num,ckt,MASK_1HOT_64[0]);// LL debug
         //dumpOutputsAndDffs(pfm_num,dev_num,ckt,MASK_1HOT_64[2]);// LL debug
         //dumpOutputsAndDffs(pfm_num,dev_num,ckt,MASK_1HOT_64[0]);// LL debug
#endif
         if( !re ) { return false; }
      }
   }
   return true;
}


void EvaluateCL::retrievePatternInData( bool external_patterns,
                                        int start,
                                        int parallel,
                                        int grp )
{
   // For external pattern
   if( external_patterns ) {
      HLog << InfoWarn << "external pattern on GPU is not supported yet."
           << std::endl;
      return;
   }
   //    'parallel' should <= 64
   int pos = 0;
   for(int b=0; b < patternInSize(); b++ ) {
      uint64_t val = ((htDB::instance())->randomGen()).mt64Rand();
      val &= MASK_UPTO_64[parallel]; // use mask when saving pattern
      _patternsDataInGmv0[grp][ pos ] = val;
      _patternsDataInGmv1[grp][ pos ] = val;
      //HLog << mask64ToBinary(val) << ":" << b << std::endl;
      pos++;
   }
}


void EvaluateCL::storePatterns( const std::vector<uint64_t>& gmv0_in,
                                const std::vector<uint64_t>& gmv1_in,
                                const std::vector<uint64_t>& gmv0_out,
                                const std::vector<uint64_t>& gmv1_out,
                                int parallel,
                                int n_cycles,
                                bool io_per_cycle )
{
   //for( unsigned pos=0; pos<gmv0_in.size(); pos++ ) {
   //   HLog << mask64ToBinary( gmv0_in.at(pos) ) << ":" << pos << std::endl;
   //}
   uint64_t mask = 1;
   for( int b=0; b<parallel; b++ ) {
      PatternSet& pset = (htDB::instance())->internalPatternSet();
      pset.addPattern( gmv0_in, gmv1_in, gmv0_out, gmv1_out,
                       mask, n_cycles, io_per_cycle );
      mask <<= 1;
   }
}


// In order to overlap memory copy with kernel execution for pattern
// simulation on GPU, perform the following pseudo algorithm
//
// Compute or retrieve 1st two initial pattern stimulus
// Copy HOST to GPU 1st pattern stimulus
// on each iteration
//   Enqueue Cp GPU_HOST previous pattern out
//   Enqueue Cp HOST_CPU next pattern in (overwrite previous pattern in)
//   Simulate current pattern in
//     1. copy pattern in to gmv0/1
//     2. simulate pattern
//     3. write pattern out from gmv0/1
//   Goto next iteration once all 3 enqueues to finish
//
bool EvaluateCL::simulatePatternsWithOverlap( cl_uint pfm_num,
                                              cl_uint dev_num,
                                              int start,
                                              int end,
                                              bool ext_patterns,
                                              bool store_patterns,
                                              int n_cycles,
                                              bool io_per_cycle,
                                              LogicSimMethod lsim_type )
{
   if( ext_patterns ) {
      HLog << InfoWarn << "external pattern on GPU is not supported yet."
           << std::endl;
      return true;
   }


   SimulationOptions & sim_opt = (htDB::instance())->simOptions();
   LevelizedNetlist & ckt = ((htDB::instance())->design()).simNetlist();
   int num_parallel_psets = sim_opt.numParallelPsetsOnGpu();
   const int pcnt_1pass = num_parallel_psets * 64;
   int parallel = (end-start)< (pcnt_1pass-1) ? (end-start) : pcnt_1pass;
   bool re = false;
   cl_int err = 0;

   //if( is_event_driven ) { // LL debug
   //   verifyBackgroundofEventDrivenSimulation( pfm_num, dev_num, ckt );
   //}

   // *) Populate patIn[0] and patIn[1], and backup patIn[0] in patInStore
   retrievePatternInData( ext_patterns, start, parallel, 0 );
   //    What if parallel < pcnt_1pass ???????
   retrievePatternInData( ext_patterns, start+parallel, parallel, 1);
   if( store_patterns) { backupPatternInDataForStorePatterns( 0 ); }

   // *) Copy patIn[0] from host to gpu
   std::vector< cl_event> events_i(4, NULL);
   re = copyPatternInHToD( pfm_num, dev_num, 0, events_i[0], events_i[1]);
   if( false==re ) { return false; }

   // *) Simulate patIn[0] on gpu and copy patIn[1] to gpu simulataneously
   err = clWaitForEvents( 2, &(events_i[0]) );
   if( oclFunctionFailed(err,"wait for events in events_i[0].")) {
      return false;
   }
   re = copyPatternInHToD( pfm_num, dev_num, 1, events_i[2], events_i[3]);
   if( false==re ) { return false; }
   // simulate patIn[0]
   re = goodSim1PsetOnGpu( pfm_num, dev_num, ckt, 0, n_cycles, io_per_cycle,
                           lsim_type );
   if( false==re ) { return false; }
   // *) Populate 3rd pattern-in in patIn[0]
   retrievePatternInData( ext_patterns, start+parallel*2, parallel, 0 );
   // *) Wait for copying patIn[1] to gpu to finish
   err = clWaitForEvents( 2, &(events_i[2]) );
   if( oclFunctionFailed(err,"wait for events in events_i[2].")) {
      return false;
   }
   start += parallel; // update start pattern number
   int grp = 1;
   while( start < end ) {
      std::vector< cl_event> events(4, NULL);
      // 1) <++ Copy patOut[1-grp] of previous simed pset from D to H
      re = copyPatternOutDToH( pfm_num,dev_num,1-grp,events[0],events[1]);
      if( false==re ) { return false; }
      // 2)     Copy patIn[1-grp] of next pset from H to D
      re = copyPatternInHToD( pfm_num, dev_num,1-grp,events[2],events[3]);
      if( false==re ) { return false; }
      // 3) <++ Simulate patIn[grp] of current pset on gpu
      re = goodSim1PsetOnGpu( pfm_num,dev_num,ckt,grp,n_cycles,io_per_cycle,
                              lsim_type );
      if( false==re ) { return false; }
      // 4) Store pattern of previous pset with patInStore and patOut[1-grp]
      //    and backup patIn[grp] of current pset
      if( store_patterns ) {
         storePatterns( patternInGmv0Backup(), patternInGmv1Backup(),
                        patternOutGmv0( 1-grp ), patternOutGmv1( 1-grp ),
                        parallel, n_cycles, io_per_cycle );
         backupPatternInDataForStorePatterns( grp );
      }
      // *)  populate the one after next pset into patIn[grp]
      retrievePatternInData( ext_patterns, start+parallel*2, parallel, grp);

      parallel = (end-start)< (pcnt_1pass-1) ? (end-start) : pcnt_1pass;
      // *)  Update 'start' and 'grp'
      start += parallel;
      grp = 1 - grp; // alternate pattern group
      // *) Wait for H2D/D2H memcpy to finish before go to next iteration
      err = clWaitForEvents( events.size(), &(events[0]) );
      if( oclFunctionFailed(err,"wait for events in D2H/H2D memcpy.")) {
         return false;
      }
   }
   // store last pattern set
   // a) Copy patOut[grp] from gpu to host
   std::vector< cl_event> events_l(2, NULL);
   re = copyPatternOutDToH( pfm_num,dev_num,1-grp,events_l[0],events_l[1]);
   if( false==re ) { return false; }
   err = clWaitForEvents( events_l.size(), &(events_l[0]) );
   if( oclFunctionFailed(err,"wait for events in memcpy last patternOut.")) {
      return false;
   }
   // b) Store pattern with patInStore and patOut[grp]
   if( store_patterns ) {
      storePatterns( patternInGmv0Backup(), patternInGmv1Backup(),
                     patternOutGmv0( 1-grp ), patternOutGmv1( 1-grp ),
                     parallel, n_cycles, io_per_cycle );
   }
   return true;
}
