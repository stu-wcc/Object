#define GATHER_SUCCESSOR_GATES( GateId, \
                                Levels, \
                                Succ1st, \
                                Succs, \
                                QEFlags, \
                                QOffsets, \
                                QSizes, \
                                NumLevels, \
                                FirstNodeFF, \
                                ClockedDffIsUsed, \
                                CurrentLev ) \
{ \
   int succ_1st = Succ1st[ GateId ]; \
   int succ_1st_n = Succ1st[ GateId + 1 ]; \
   for( int i = succ_1st; i < succ_1st_n; i++) { \
      int sid = Succs[ i ]; \
      int succ_lev = Levels[ sid ]; \
      if( succ_lev != CurrentLev ) { \
         int adj_id = sid; \
         if( ClockedDffIsUsed && sid >= FirstNodeFF ) { \
            adj_id -= (sid - FirstNodeFF) % 5; \
         } \
         int old_size = atomic_inc( QSizes + NumLevels ); \
         QEFlags[ QOffsets[NumLevels] + old_size ] = adj_id; \
      } \
   } \
} \


#define ADD_SUCCESSOR_EVENTS( GateId, \
                              Levels, \
                              Succ1st, \
                              Succs, \
                              QEFlags, \
                              QOffsets, \
                              QSizes, \
                              NumLevels, \
                              FirstNodeFF, \
                              ClockedDffIsUsed, \
                              CurrentLev ) \
{ \
   int succ_1st = Succ1st[ GateId ]; \
   int succ_1st_n = Succ1st[ GateId + 1 ]; \
   for( int i = succ_1st; i < succ_1st_n; i++) { \
      int sid = Succs[ i ]; \
      int succ_lev = Levels[ sid ]; \
      if( succ_lev != CurrentLev ) { \
         int adj_id = sid; \
         if( ClockedDffIsUsed && sid >= FirstNodeFF ) { \
            adj_id -= (sid - FirstNodeFF) % 5; \
         } \
         /* update in-queue flag */ \
         unsigned old_flag = atomic_or( &(QEFlags[adj_id]), 0x80000000); \
         if( 0==(old_flag & 0x80000000)) { \
            int old_size = atomic_inc( &(QSizes[succ_lev]) ); \
            int offset = QOffsets[ succ_lev ]; \
            /* unsigned flag = QEFlags[offset + old_size] & 0x80000000;*/ \
            atomic_and( &(QEFlags[offset + old_size]), 0x80000000); \
            /* QEFlags[offset + old_size] = flag + (unsigned)adj_id;*/ \
            atomic_add( &(QEFlags[offset + old_size]), (unsigned)adj_id); \
            /* It can happen that offset + old_size == adj_id of another thread */ \
            /* causing race between mem-write and mem-read at the same address */ \
         } \
      } \
   } \
} \


#define GSIM_BUF_TEMPLATE( GmvType, \
                         Types,  \
                         Levels,  \
                         Pred1st,  \
                         Succ1st,  \
                         Preds,  \
                         Succs,  \
                         FirstFFNode,  \
                         ClockedDffIsUsed,  \
                         Gmv0,  \
                         Gmv1,  \
                         BaseNodeId,  \
                         PaddingBound )  \
{  \
   unsigned gid = compute_adjusted_flatten_global_id( BaseNodeId );  \
   if( gid > PaddingBound) return;  \
  \
   GmvType val0=0, val1=0;  \
   if( ClockedDffIsUsed && gid >= FirstFFNode ) {  \
      unsigned ui = gid - FirstFFNode;  \
      unsigned gid_ff = FirstFFNode + (ui << 2) + ui;  \
      for( int i=0; i < 5; i++, gid_ff++ ) {  \
         OCL_GATE_EVAL( gid_ff, GmvType, Pred1st, Preds, Types,  \
                        Gmv0, Gmv1, val0, val1 );  \
         Gmv0[gid_ff] = val0;  \
         Gmv1[gid_ff] = val1;  \
      }  \
   } else {  \
      OCL_GATE_EVAL( gid, GmvType, Pred1st, Preds, Types,  \
                     Gmv0, Gmv1, val0, val1 );  \
      Gmv0[gid] = val0;  \
      Gmv1[gid] = val1;  \
   }  \
}  \



#define OCL_GATE_EVAL( Gate_Id,  \
                       Gmv_Type, \
                       Pred1st, \
                       Preds, \
                       Types, \
                       Gmv0, \
                       Gmv1, \
                       Val0, \
                       Val1 ) \
{ \
   int pred_1st = Pred1st[Gate_Id];  \
   int pred_1st_n = Pred1st[Gate_Id+1];  \
   int type = Types[Gate_Id];  \
  \
   switch( type ) {  \
      case T_INPUT:  \
      {  \
         Val0 = Gmv0[Gate_Id];  \
         Val1 = Gmv1[Gate_Id];  \
         break;  \
      }  \
      case T_TIE1:  \
      {  \
         Val0 = ALL_ONES_64;  \
         Val1 = ALL_ONES_64;  \
         break;  \
      }  \
      case T_TIE0:  \
      {  \
         Val0 = 0;  \
         Val1 = 0;  \
         break;  \
      }  \
      case T_TIEX:  \
      {  \
         Val0 = ALL_ONES_64;  \
         Val1 = 0;  \
         break;  \
      }  \
      case T_TIEZ:  \
      {  \
         Val0 = 0;  \
         Val1 = ALL_ONES_64;  \
         break;  \
      }  \
      case T_OUTPUT:  \
      case T_DFF:  \
      case T_BUF:  \
      case T_BUF_FB: \
      {  \
         for( int i=pred_1st; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Val0 = Gmv0[pred_id];  \
            Val1 = Gmv1[pred_id];  \
         }  \
         break;  \
      }  \
      case T_AND:  \
      {  \
         Val1 = Val0 = ALL_ONES_64;  \
         for( int i=pred_1st; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Val0 &= Gmv0[pred_id];  \
            Val1 &= Gmv1[pred_id];  \
         }  \
         break;  \
      }  \
      case T_NAND:  \
      {  \
         Val1 = Val0 = ALL_ONES_64;  \
         for( int i=pred_1st; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Val0 &= Gmv0[pred_id];  \
            Val1 &= Gmv1[pred_id];  \
         }  \
         Gmv_Type v1 = Val0;  \
         Val0 = ALL_ONES_64 ^ Val1;  \
         Val1 = ALL_ONES_64 ^ v1;  \
         break;  \
      }  \
      case T_OR:  \
      {  \
         Val1 = Val0 = 0;  \
         for( int i=pred_1st; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Val0 |= Gmv0[pred_id];  \
            Val1 |= Gmv1[pred_id];  \
         }  \
         break;  \
      }  \
      case T_NOR:  \
      {  \
         Val1 = Val0 = 0;  \
         for( int i=pred_1st; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Val0 |= Gmv0[pred_id];  \
            Val1 |= Gmv1[pred_id];  \
         }  \
         Gmv_Type v1 = Val0;  \
         Val0 = ALL_ONES_64 ^ Val1;  \
         Val1 = ALL_ONES_64 ^ v1;  \
         break;  \
      }  \
      case T_NOT:  \
      {  \
         for( int i=pred_1st; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Val0 = Gmv1[pred_id] ^ ALL_ONES_64;  \
            Val1 = Gmv0[pred_id] ^ ALL_ONES_64;  \
         }  \
         break;  \
      }  \
      case T_XOR:  \
      {  \
         for( int i=pred_1st; i < pred_1st + 1; i++ ) {  \
            int pred_id = Preds[i];  \
            Val0 = Gmv1[pred_id];  \
            Val1 = Gmv0[pred_id];  \
         }  \
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Gmv_Type v1 = ALL_ONES_64 ^  \
               (((ALL_ONES_64^Gmv0[pred_id]) & (ALL_ONES_64 ^ Val0)) |  \
                (Gmv1[pred_id] & Val1));  \
            Val1 = ((ALL_ONES_64 ^ Gmv0[pred_id]) & Val1) |  \
               (Gmv1[pred_id] & (ALL_ONES_64 ^ Val0));  \
            Val0 = v1;  \
         }  \
         break;  \
      }  \
      case T_XNOR:  \
      {  \
         for( int i=pred_1st; i < pred_1st + 1; i++ ) {  \
            int pred_id = Preds[i];  \
            Val0 = Gmv1[pred_id];  \
            Val1 = Gmv0[pred_id];  \
         }  \
         for( int i=pred_1st + 1; i < pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Gmv_Type v1 = ALL_ONES_64 ^  \
               (((ALL_ONES_64^Gmv0[pred_id]) & (ALL_ONES_64 ^ Val0)) |  \
                (Gmv1[pred_id] & Val1));  \
            Val1 = ((ALL_ONES_64 ^ Gmv0[pred_id]) & Val1) |  \
               (Gmv1[pred_id] & (ALL_ONES_64 ^ Val0));  \
            Val0 = v1;  \
         }  \
         Gmv_Type v1 = Val0;  \
         Val0 = ALL_ONES_64 ^ Val1;  \
         Val1 = ALL_ONES_64 ^ v1;  \
         break;  \
      }  \
      case T_BUS:  \
      case T_BUSZ: {  \
         Val0 = 0;  \
         Val1 = ALL_ONES_64;  \
         for( int i=pred_1st; i<pred_1st_n; i++ ) {  \
            int pred_id = Preds[i];  \
            Val0 |= Gmv0[pred_id];  \
            Val1 &= Gmv1[pred_id];  \
         }  \
         Gmv_Type z_mask = (~Val0) & Val1;  \
         Val0 |= z_mask;  \
         Val1 &= (~z_mask);  \
         break;  \
      }  \
      case T_TRISTATE1: {  \
         Gmv_Type e1_mask = Gmv0[ Preds[ pred_1st+1]] &  \
                         Gmv1[ Preds[ pred_1st+1]];  \
         Val0 = Gmv0[ Preds[pred_1st] ] & e1_mask;  \
         Val1 = Gmv1[ Preds[pred_1st] ] & e1_mask;  \
  \
         Gmv_Type e0_mask = ~(Gmv0[ Preds[ pred_1st+1]] |  \
                           Gmv1[ Preds[ pred_1st+1]] );  \
         Val0 &= ~e0_mask;  \
         Val1 |= e0_mask;  \
  \
         Gmv_Type ex_mask = ~(e1_mask | e0_mask);  \
         Val0 |= ex_mask;  \
         Val1 &= ~ex_mask;  \
  \
         break;  \
      }  \
      case T_MUX_2:  \
      {  \
         Val0 = ALL_ONES_64;  \
         Val1 = 0;  \
  \
         Gmv_Type s0_mask = ~(Gmv0[ Preds[ pred_1st]] |  \
                           Gmv1[ Preds[ pred_1st]] );  \
         Gmv_Type dq_mask = ~((Gmv0[ Preds[ pred_1st+1]] ^  \
                            Gmv0[ Preds[ pred_1st+2]] ) |  \
                           (Gmv1[ Preds[ pred_1st+1]] ^  \
                            Gmv1[ Preds[ pred_1st+2]] ));  \
         Gmv_Type s0_dq_mask = s0_mask | dq_mask;  \
         Val0 &= ((Gmv0[ Preds[ pred_1st+1]] & s0_dq_mask) | ~s0_dq_mask);  \
         Val1 |= (Gmv1[ Preds[ pred_1st+1]] & s0_dq_mask);  \
  \
         Gmv_Type s1_mask = Gmv0[ Preds[pred_1st]] &  \
                         Gmv1[ Preds[pred_1st]] ;  \
         Val0 &= ((Gmv0[ Preds[ pred_1st+2]] & s1_mask) | ~s1_mask);  \
         Val1 |= (Gmv1[ Preds[ pred_1st+2]] & s1_mask);  \
  \
         break;  \
      }  \
      case T_DFF_L:  \
      {  \
         Val0 = Gmv0[ Preds[pred_1st+3] ];  \
         Val1 = Gmv1[ Preds[pred_1st+3] ];  \
         break;  \
      }  \
      default:  \
      {  \
         break;  \
      }  \
   }  \
} \
