/*------------------------------------------------
   tokens.h
-------------------------------------------------*/

/************************************************************************
* Copyright (c) 1990 by the University of Illinois at Urbana-Champaign. *
* University of Illinois is the sole owner of this software and has     *
* granted an exclusive license to Sunrise Test Systems, Sunnyvale, CA.  *
* for commercial distribution.                                          *
* Copying or distribution of this software without prior written consent*
* from the University of Illinois will be considered an infringement of *
* the ownership rights.  University is legally obliged to protect this  *
* software due to its exclusive licensing agreement with Sunrise Test.  *
* Any publications resulting from the use of this software must acknowl-*
* -edge the source, and a copy of the publication must be made available*
* to Prof. J.H. Patel at the following address:                         *
*                                                                       *
*                Center for Reliable and High Performance Computing     *
*                University of Illinois at Urbana-Champaign             *
*                1101 W Springfield Ave.                                *
*                Urbana, IL 61801. USA.                                 *
*                (217) 333-6201                                         *
*                                                                       *
* No warranty is expressed or implied about the accuracy or fitness of  *
* this program.                                                         *
************************************************************************/

#define MAXCON 0xfffffff

enum
{
   JUNK,           /* 0 */
   T_INPUT,        /* 1 */
   T_OUTPUT,       /* 2 */
   T_XOR,          /* 3 */
   T_XNOR,         /* 4 */
   T_DFF,          /* 5 */
   T_AND,          /* 6 */
   T_NAND,         /* 7 */
   T_OR,           /* 8 */
   T_NOR,          /* 9 */
   T_NOT,          /* 10 */
   T_BUF,          /* 11 */
   T_TIE1,         /* 12 */
   T_TIE0,         /* 13 */
   T_TIEX,         /* 14 */
   T_TIEZ,         /* 15 */
   T_MUX_2,        /* 16 */
   T_BUS,          /* 17 */
   T_BUS_GOHIGH,   /* 18 */
   T_BUS_GOLOW,    /* 19 */
   T_BUSZ,         /* 20 */
   T_TRISTATE,     /* 21 */
   T_TRISTATEINV,  /* 22 */
   T_TRISTATE1,    /* 23 */
   T_DFF_L,        /* 24 */
   T_DFF_LCP,      /* 25 */
   T_DFF_LC,       /* 26 */
   T_DFF_LP,       /* 27 */
   T_DFF_CP,       /* 28 */
   T_DFF_C,        /* 29 */
   T_DFF_P,        /* 30 */
   T_NAND_LATCH,   /* 31 */
   T_NOR_LATCH,    /* 32 */
   T_CMOS,         /* 33 */
   T_NMOS,         /* 34 */
   T_PMOS,         /* 35 */
   T_NOTIF1,       /* 36 */
   T_NOTIF0,       /* 37 */
   T_BUFIF1,       /* 38 */
   T_BUFIF0        /* 39 */
};

