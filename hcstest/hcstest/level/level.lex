%{
/************************************************************************
 *                           level.lex
 *
 *  ISCAS bench parser to convert .bench to .lev with .name (mapping)
 *  Adapted from code in crhc.uiuc.edu
 *  should be refactored later to better integrate with C++
 *
************************************************************************/

#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>
#include <iostream>
#include <fstream>

#include "tokens.h"
#define MAX_NETS 5000000
#define MAX_PI 22500       /* Size imposed by socket is 64K inputs  */
#define MAX_PO 22500       /* Size imposed by socket is 64K outputs */
#define MAX_FANIN 35
#define Q_SIZE 5000000  // x100 for million gate design
#define HASH_SIZE 25700 // x100 for million gate design
#define MAX_STRING_SIZE 64

#define T_NET      100
#define T_END      101
#define T_END_NETS 102
#define T_EQUAL    103

#define TLog (*parserTeeStreamPtr)

typedef boost::iostreams::tee_device< std::ostream, std::ofstream > TeeDevice;
typedef boost::iostreams::stream< TeeDevice > TeeStream;

int nextname = 1;
char name[MAX_NETS][MAX_STRING_SIZE];

int hash_first[HASH_SIZE];
int hash_next[MAX_NETS];
int net_to_num[MAX_NETS];

FILE * name_file;
FILE * lev_file;
TeeStream * parserTeeStreamPtr = NULL;
int verbose=0;

struct node_struct {
  int net_number;
  int final_node_number;
  int level;
  int pred[MAX_FANIN];
  int pred_net_num[MAX_FANIN];
  int * succ;
  int C0;
  int C1;
  int ob;
  int inq;
  int logic;
  int num_pred;
  int num_succ;
};

int prim_in[MAX_PI], prim_out[MAX_PO];
struct node_struct node[MAX_NETS];
int curr_pi = 0,curr_po = 0,curr_node = 0;
int max_lev;
int lineno = 1;
int extra_buf_count = 0;
/*-------------------- LL: for queue operation, moved up -----------------*/
int queue[Q_SIZE];
int f_q= 0,b_q= 0;
int q_c=0;

/* Function forward declaration */
int net_num();
int parse_tokens();
int finish_structure();
void levelize();
int asynch_detect();
void calc_control();
void calc_observe();
void print_output();



%}

letter   [A-Za-z]
digit    [0-9]
num      {digit}+
us       "_"
dot      "."
slash    "/"
dollar   "$"
net      ({letter}|{digit}|{us}|{dot}|{slash}|{dollar})*
ws       (" "|"\t")
nl       "\n"
%%
","             {return (T_NET);}
"INPUT"         {return (T_INPUT);}
"OUTPUT"        {return (T_OUTPUT);}
"DFF"           {return (T_DFF);}
"DFF_LCP"       {return (T_DFF_LCP);}
"DFF_LC"        {return (T_DFF_LC);}
"DFF_LP"        {return (T_DFF_LP);}
"DFF_L"         {return (T_DFF_L);}
"DFF_CP"        {return (T_DFF_CP);}
"DFF_P"         {return (T_DFF_P);}
"NAND_LATCH"    {return (T_NAND_LATCH);}
"NOR_LATCH"     {return (T_NOR_LATCH);}
"AND"           {return (T_AND);}
"NAND"          {return (T_NAND);}
"OR"            {return (T_OR);}
"NOR"           {return (T_NOR);}
"NOT"           {return (T_NOT);}
"XOR"           {return (T_XOR);}
"XNOR"          {return (T_XNOR);}
"BUF"           {return (T_BUF);}
"BUS"           {return (T_BUS);}
"BUS_GOHIGH"    {return (T_BUS_GOHIGH);}
"BUS_GOLOW"     {return (T_BUS_GOLOW);}
"TRISTATE"      {return (T_TRISTATE);}
"TRISTATEINV"   {return (T_TRISTATEINV);}
"TRISTATE1"     {return (T_TRISTATE1);}
"TIE1"          {return (T_TIE1);}
"TIE0"          {return (T_TIE0);}
"TIEX"          {return (T_TIEX);}
"TIEZ"          {return (T_TIEZ);}
"MUX2"          {return (T_MUX_2);}
"CMOS"          {return (T_CMOS);}
"NMOS"          {return (T_NMOS);}
"PMOS"          {return (T_PMOS);}
"("             {return (T_NET);}
"END"           {return (T_END);}
"input"         {return (T_INPUT);}
"output"        {return (T_OUTPUT);}
"dff"           {return (T_DFF);}
"and"           {return (T_AND);}
"nand"          {return (T_NAND);}
"or"            {return (T_OR);}
"nor"           {return (T_NOR);}
"not"           {return (T_NOT);}
"xor"           {return (T_XOR);}
"xnor"          {return (T_XNOR);}
"buf"           {return (T_BUF);}
"bus"           {return (T_BUS);}
"bus_gohigh"    {return (T_BUS_GOHIGH);}
"bus_golow"     {return (T_BUS_GOLOW);}
"tristate"      {return (T_TRISTATE);}
"tristateinv"   {return (T_TRISTATEINV);}
"tristate1"     {return (T_TRISTATE1);}
"tie1"          {return (T_TIE1);}
"tie0"          {return (T_TIE0);}
"tieX"          {return (T_TIEX);}
"tieZ"          {return (T_TIEZ);}
"mux2"          {return (T_MUX_2);}
"end"           {return (T_END);}
"#"[^\n]*
")"             {return (T_END_NETS);}
"="             {return (T_EQUAL);}
{net}           {return (-1 * net_num());}
{nl}            {lineno++;
                 if (100 * (lineno/100) == lineno) {
                    if( verbose ) {
                       // printf ("ln %d\n", lineno);
                       TLog << "ln " << lineno << std::endl;
                    }
                 }
                }
{ws}            {}
([^{net}]|[^{nl}]|[^{ws}]) {
                 //fprintf(stderr, "Unknown character(s) %s at line %d \n",
                 //         yytext, lineno);
                 TLog << "Error: Unknown character(s) " << yytext
                      << " at line " << lineno << std::endl;
                 return -1;
                }
%%

/*---------------------------------------------------------------------------*/
int yywrap() {
   if( verbose ) {
      //printf ("\nEnd of circuit bench file\n");
      TLog << "\nEnd of circuit bench file" << std::endl;
   }
   return (1);
}

/*-----------------------------added by LL ----------------------------------*/
// Make it work by multiple calls of Level_Main();
void reset_global_variables() {

   nextname = 1;
   name_file = NULL;
   lev_file = NULL;
   parserTeeStreamPtr = NULL;
   verbose=0;
   curr_pi = 0;
   curr_po = 0;
   curr_node = 0;
   max_lev = 0;
   lineno = 1;
   extra_buf_count = 0;
   f_q = 0;
   b_q = 0;
   q_c = 0;

}


/*---------------------------------------------------------------------------*/
int Level_Main(int argc, char** argv, TeeStream * ts_ptr, int detail)
{
   char b_file[50];                 /* bench file */
   char n_file[50];                 /* name file */
   char l_file[50];                 /* level file */
   int i, retval;

   reset_global_variables(); /* LL: reset is added to allow multiple calls */

   parserTeeStreamPtr = ts_ptr;
   assert( parserTeeStreamPtr );
   verbose = detail;

   if (argc != 2) {
      //fprintf(stderr, "Usage: %s <circuit_name>\n", argv[0]);
      TLog << "Usage: " << argv[0] << " <circuit_name>" << std::endl;
      return -1;
   }


                                    /* Initialize hash files */
   for (i = 0; i < HASH_SIZE; i++)
      hash_first[i] = -1;
   for (i = 0; i < MAX_NETS; i++)
      hash_next[i] = -1;
   strcpy (b_file, argv[1]);
   strcpy (n_file, argv[1]);
   strcpy (l_file, argv[1]);
   strcat (b_file, ".bench");
   strcat (n_file, ".name");
   strcat (l_file, ".lev");
   yyin = fopen (b_file, "r");      /* Open bench file */
   if (!yyin) {
      //fprintf(stderr, "Can't open %s\n", b_file);
      //fprintf(stderr, "Usage: %s <circuit_name>\n", argv[0]);
      TLog << "Error: Can't open " << b_file << std::endl;
      TLog << "Usage: " << argv[0] << " <circuit_name>" << std::endl;
      return -1;
   }
   name_file = fopen (n_file, "w"); /* Open Name file */
   lev_file = fopen (l_file, "w");  /* Open Level file */
   /* printf ("Parsing tokens\n"); */
   if (verbose) {
      TLog << "Parsing tokens" << std::endl;
   }
   parse_tokens();
   /* printf ("Finishing structure\n"); */
   if (verbose) {
      TLog << "Finishing structure" << std::endl;
   }
   finish_structure();
   /* printf ("Levelizing\n"); */
   if (verbose) {
      TLog << "Levelizing" << std::endl;
   }
   levelize();

   if ((retval = asynch_detect()) == 1) {
      // fprintf(stderr, "Warning: Asynchronous Circuit\n");
      TLog << "Warning: Asynchronous Circuit" << std::endl;
   } else if (retval == 2) {
      // fprintf(stderr, "Warning: Circuit benchfile has some errors\n" );
      TLog << "Warning: Circuit benchfile has some errors" << std::endl;
   } else {
      if (verbose) {
         // printf ("Circuit Approved\n");
         TLog << "Circuit Approved" << std::endl;
      }
   }
   // printf ("Calculating controllabilities\n");
   if (verbose) {
      TLog << "Calculating controllabilities" << std::endl;
   }
   calc_control();
   // printf ("Calculating observabilities\n");
   if (verbose) {
      TLog << "Calculating observabilities" << std::endl;
   }

   calc_observe();
   print_output();
   fclose (yyin);
   fclose (name_file);
   fclose (lev_file);
   return 0;
}

/*--------------------------------------------------------------
  Convert net symbol to net number using hash table.  If symbol
  already exists, return existing number.  Otherwise use next
  number in sequence (0, 1, 2, 3, ...).
---------------------------------------------------------------*/
int net_num()
{
   int i;
   int l;
   int s;
   int last;
   int curr;
   s = 0;

   l = strlen (yytext);      /* lex stores the symbol in yytext */
   if (l > MAX_STRING_SIZE) {
      // fprintf(stderr, "gate %s exceeds MAX string size of %d\n",
      //          yytext, MAX_STRING_SIZE);
      TLog << "Error: gate " << yytext << " exceeds MAX string size "
           << MAX_STRING_SIZE << std::endl;

   }
   for (i = 0; i < l; i++)
      s += yytext[i];        /* s is sum of symbol characters */

   s = s % HASH_SIZE;
   last = -1;
   curr = hash_first[s];

   while (curr != -1) {
      if (!strcmp (name[curr], yytext)) {
         return (curr);      /* net symbol already exists as curr */
      }
      last = curr;
      curr = hash_next[curr];
   }

                             /* new symbol found -- */
                             /* add name to end of name array */
   strcpy (name[nextname], yytext);
   if (last == -1) {
      hash_first[s] = nextname; /* first symbol with this checksum */
   } else {
      hash_next[last] = nextname; /* last symbol with this checksum */
   }
   nextname++;
   return (nextname - 1);
}

/*---------------------------------------------------------
   Parsing Tokens
-----------------------------------------------------------*/
int parse_tokens()
{
   int tok;
   int i;
   int tmp;

   /* NOTE: circuit bench file may or may not end with a END keyword
      In the latter case, yylex returns a 0 upon encountering EOF */
   while (((tok = yylex()) != T_END) && tok) {
      switch (tok) {
         case T_INPUT:
         {
            tok = yylex();
            if (tok != T_NET) {
               //fprintf(stderr, "Unknown character encountered on line %d\n",
               // lineno);
               //fprintf(stderr, "Expected character (\n");
               TLog << "Error: Unknown character encountered on line "
                    << lineno << std::endl;
               TLog << "Expected character (" << std::endl;
               return -1;
            }
            tok = -1 * yylex();
            if (tok < 0) {
               //fprintf(stderr, "Unknown character encountered on line %d\n",
               // lineno);
               TLog << "Error: Unknown character encountered on line "
                    << lineno << std::endl;
               return -1;
            }
            prim_in[curr_pi] = curr_node;
            curr_pi++;
            node[curr_node].logic = T_INPUT;
            node[curr_node].net_number = tok;
            node[curr_node].final_node_number = -1;
            node[curr_node].level = 0;
            node[curr_node].num_pred = 0;
            node[curr_node].inq = 0;
            for (i = 0; i < MAX_FANIN; i++) {
               node[curr_node].pred[i] = -1;
               node[curr_node].pred_net_num[i] = -1;
            }
            curr_node++;
            tok = yylex();
            if (tok != T_END_NETS) {
               //fprintf(stderr, "Unknown character encountered on line %d\n",
               // lineno);
               //fprintf(stderr, "Expected character )\n");
               TLog << "Error: Unknown character encountered on line "
                    << lineno << std::endl;
               TLog << "Expected character )" << std::endl;
               return -1;
            }
         }
         break;
         case T_OUTPUT:
         {
            tok = yylex();
            if (tok != T_NET) {
               //fprintf(stderr, "Unknown character encountered on line %d\n",
               // lineno);
               //fprintf(stderr, "Expected character (\n");
               TLog << "Error: Unknown character encountered on line "
                    << lineno << std::endl;
               TLog << "Expected character (" << std::endl;
               return -1;
            }
            tok = -1 * yylex();
            if (tok < 0) {
               //fprintf(stderr, "Unknown character encountered on line %d\n",
               // lineno);
               TLog << "Error: Unknown character encountered on line "
                    << lineno << std::endl;
               return -1;
            }
            node[curr_node].logic = T_OUTPUT;
            sprintf (name[nextname], "%s_$OUTPUT", name[tok]);
            node[curr_node].net_number = nextname;
            nextname++;
            node[curr_node].final_node_number = -1;
            node[curr_node].level = -1;
            node[curr_node].num_pred = 1;
            node[curr_node].inq = 0;
            for (i = 0; i < MAX_FANIN; i++) {
                    node[curr_node].pred[i] = -1;
                    node[curr_node].pred_net_num[i] = -1;
                 }
            node[curr_node].pred_net_num[0] = tok;
            prim_out[curr_po] = node[curr_node].net_number ;
            curr_node++;
            curr_po++;
            tok = yylex();
            if (tok != T_END_NETS) {
               //fprintf(stderr, "Unknown character encountered on line %d\n",
               // lineno);
               //fprintf(stderr, "Expected character )\n");
               TLog << "Error: Unknown character encountered on line "
                    << lineno << std::endl;
               TLog << "Expected character )" << std::endl;
               return -1;
            }
         }
         break;
         case T_TIE1:
         case T_TIE0:
         case T_TIEZ:
         case T_TIEX:
         {
            tmp = tok;
            tok = yylex();
            if (tok != T_NET) {
               //fprintf(stderr, "Unknown character encountered on line %d\n",
               // lineno);
               //fprintf(stderr, "Expected character (\n");
               TLog << "Error: Unknown character encountered on line "
                    << lineno << std::endl;
               TLog << "Expected character (" << std::endl;
               return -1;
            }
            tok = -1 * yylex();
            if (tok < 0) {
               //fprintf(stderr, "Unknown character encountered on line %d\n",
               // lineno);
               TLog << "Error: Unknown character encountered on line "
                    << lineno << std::endl;
               return -1;
            }
            /* prim_in[curr_pi] = curr_node; */
            /* curr_pi++;                    */
            node[curr_node].logic = tmp;
            node[curr_node].net_number = tok;
            node[curr_node].final_node_number = -1;
            node[curr_node].level = 0;
            node[curr_node].num_pred = 0;
            node[curr_node].inq = 0;
            for (i=0;i<MAX_FANIN;i++) {
                    node[curr_node].pred[i] = -1;
                    node[curr_node].pred_net_num[i] = -1;
                 }
            curr_node++;
            tok = yylex();
            if (tok != T_END_NETS) {
               //fprintf(stderr, "Unknown character encountered on line %d\n",
               // lineno);
               //fprintf(stderr, "Expected character )\n");
               TLog << "Error: Unknown character encountered on line "
                    << lineno << std::endl;
               TLog << "Expected character )" << std::endl;
               return -1;
            }
         }
         break;
         default:
         {
            int logic = 0;
            int parm_count = 0;
                                /* handle "Gate = gate_func (gate_list)" */
                 tok = -1 * tok;
            node[curr_node].net_number = tok; /* tok holds Gate's number*/
            node[curr_node].inq = 0;
                 tok = yylex();      /* "=" token */
            if (tok != T_EQUAL) {
               //fprintf(stderr, "Expected character = at line %d\n", lineno);
               TLog << "Error: Expected character = at line "
                    << lineno << std::endl;
               return -1;
            }
            tok = yylex();      /* gate type */
                 logic = tok;
            node[curr_node].logic = tok;
            if (tok == T_DFF || tok == T_DFF_C || tok == T_DFF_CP || tok == T_DFF_P ) {
               node[curr_node].level = 0;
            } else if (tok == T_AND || tok == T_NAND || tok == T_OR ||
                tok == T_NOR || tok == T_NOT || tok == T_XOR ||
                tok == T_XNOR || tok == T_BUF || tok == T_BUS ||
                          tok == T_BUS_GOHIGH || tok == T_BUS_GOLOW ||
                          tok == T_TRISTATE || tok == T_TRISTATE1 ||
                          tok == T_TRISTATEINV || tok == T_MUX_2 ||
                          tok == T_CMOS || tok == T_NMOS || tok == T_PMOS) {
               node[curr_node].level = -1;
            } else {
               //fprintf(stderr, "illegal/unimplemented gate type on line %d\n",
               // lineno );
               TLog << "Error: illegal/unimplemented gate type on line "
                    << lineno << std::endl;
               return -1;
            }

            node[curr_node].num_pred = 0;
            node[curr_node].final_node_number = -1;
            for (i = 0; i < MAX_FANIN; i++) {
               node[curr_node].pred[i] = -1;
               node[curr_node].pred_net_num[i] = -1;
            }
            tok = yylex();      /* left paren */
            while (tok == T_NET) {
               tok = -1 * yylex();
               node[curr_node].pred_net_num[node[curr_node].num_pred] = tok;
               node[curr_node].num_pred++;
               if (node[curr_node].num_pred >= MAX_FANIN) {
                  //fprintf(stderr, "expand MAX_FANIN to %d (line %d)\n",
                  //              node[curr_node].num_pred, lineno);
                  TLog << "Error: expand MAX_FANIN to " << node[curr_node].num_pred
                       << " (line " << lineno << ")" << std::endl;
               }
               parm_count++;
               tok = yylex();   /* comma */
            }
            if ( parm_count < 1 ) {
               //fprintf(stderr, "Gate without inputs on line %d\n", lineno);
               TLog << "Error: Gate without inputs on line " << lineno << std::endl;
               return -1;
            }
            if ((logic == T_DFF || logic == T_DFF_C|| logic == T_DFF_CP ||
                 logic == T_DFF_P || logic == T_NOT || logic == T_BUF)
                 && parm_count > 1) {
               //fprintf(stderr, "Warning: Gate has more than one input on line %d\n",lineno);
               TLog << "Warning: Gate has more than one input on line "
                    << lineno << std::endl;
            }
            if ((logic == T_AND || logic == T_NAND || logic == T_OR ||
                 logic == T_NOR || logic == T_XOR || logic == T_XNOR ) &&
                 parm_count < 2) {
               //fprintf(stderr, "Warning: Gate has less than two inputs on line %d\n", lineno);
               TLog << "Warning: Gate has less than two inputs on line "
                    << lineno << std::endl;
            }
            curr_node++;
         }
      }
   }
   return 0;
}

/*------------------------------------------------------------------
 Complete gate node structure by putting in successor lists (mainly)
-------------------------------------------------------------------*/
int finish_structure()
{
   int i=0, j=0;
                                /* Initialize net numbers */
   for (i = 0; i < MAX_NETS; i++)
      net_to_num[i] = -1;

   //printf ("Preparing reverse translation table\n");
   if (verbose) {
      TLog << "Preparing reverse translation table" << std::endl;
   }
                                /* prepare reverse translation table i.e., */
                                /*  net token number -> net number */
   for (i = 0; i < curr_node; i++) {
      if (net_to_num[node[i].net_number] == -1)
         net_to_num[node[i].net_number] = i;
      else {
         if (strlen (name[node[i].net_number])) {
            //fprintf(stderr, "Warning: gate %s may be redefined\n",
            //         name[node[i].net_number]);
            TLog << "Warning: gate " << name[node[i].net_number]
                 << " may be redefined" << std::endl;
         }
      }
   }
   for (i = 0; i < curr_node; i++) { /* get predecessor node numbers */
      for (j = 0; j < node[i].num_pred; j++) {
    node[i].pred[j] = net_to_num[node[i].pred_net_num[j]];
         if (node[i].pred[j] < 0) {
            //fprintf(stderr, "Warning: unassigned gate %s found\n",
            //         name[node[i].pred_net_num[j]]);
            //fprintf(stderr,  "Unassigned gate assumed to be first input\n");
            TLog << "Warning: unassigned gate " << name[node[i].pred_net_num[j]]
                 << " found" << std::endl;
            TLog << "Unassigned gate assumed to be first input" << std::endl;
            node[i].pred[j] = 0;
         }
      }
   }
                                /* Insert BUFs between PIs, FFs, POs */
   for (i = 0; i < curr_node; i++) {
      for (j = 0; j < node[i].num_pred; j++) {
         if (node[i].logic == T_DFF) {
            if ((node[node[i].pred[j]].logic == T_INPUT) ||
                (node[node[i].pred[j]].logic == T_DFF)) {
                                /* add a BUF */
               /*
               sprintf (yytext, "%s_$BUF%d",
                        name[node[i].pred_net_num[j]], extra_buf_count++);
               printf ("   Adding BUF %s between %s and %s\n", yytext,
                       name[node[i].pred_net_num[j]],
                       name[node[i].net_number]);
               */
               char bufname[1024];
               if( verbose ) {
                  sprintf (bufname, "%s_$BUF%d",
                           name[node[i].pred_net_num[j]], extra_buf_count++);
                  TLog << "   Adding BUF " << bufname
                       << " between " << name[node[i].pred_net_num[j]]
                       << " and " << name[node[i].net_number]
                       << std::endl;
               }
               node[curr_node].net_number = net_num();
               node[curr_node].inq = 0;
               node[curr_node].logic = T_BUF;
               node[curr_node].level = -1;
               node[curr_node].final_node_number = -1;
               node[curr_node].num_pred = 1;
               node[curr_node].pred[0] = node[i].pred[j];
               node[curr_node].pred_net_num[0] =
                 node[node[i].pred[j]].net_number;
               node[i].pred[j] = curr_node;
               curr_node++;
            }
         }
      }
   }
   //printf ("Preparing successor list\n");
   if (verbose) {
      TLog << "Preparing successor list" << std::endl;
   }
   /* Count number of successors for each gate */
   /* -- bench file only determines predecessors */
   for (i = 0; i < curr_node; i++) {
      node[i].num_succ = 0;
   }
   for (i = 0; i < curr_node; i++) {
      for (j = 0; j < node[i].num_pred; j++)
         node[node[i].pred[j]].num_succ++;
   }

   /* Allocate space for storage of successors */
   //printf("Start Allocate space for storage of successors.\n");
   if (verbose) {
      TLog <<  "Start Allocate space for storage of successors." << std::endl;
   }
   for (i = 0; i < curr_node; i++) {
      node[i].succ = (int *)malloc (node[i].num_succ * sizeof (int));
      if(!node[i].succ) {
         //printf("Not enough space at: node[i].succ = malloc. Exit!\n");
         TLog << "Not enough space at: node[i].succ = malloc. Exit!"
              << std::endl;
         return -1;
      }
      node[i].num_succ = 0;
   }
   /* Create successor list */
   //printf ("Start Create successor list.\n");
   if (verbose) {
      TLog << "Start Create successor list." << std::endl;
   }
   for (i = 0; i < curr_node; i++) {
      for (j = 0; j < node[i].num_pred; j++) {
         /* Messy code ! */
         /* put node i into succ list of its pred */
         node[node[i].pred[j]].succ[node[node[i].pred[j]].num_succ] = i;
         node[node[i].pred[j]].num_succ++;
      }
   }
   /* Intialize controllability/observability */
   //printf ("Initializing SCOAP values\n");
   if (verbose) {
      TLog << "Initializing SCOAP values" << std::endl;
   }
   for (i = 0; i < curr_node; i++) {
      node[i].C0 = node[i].C1 = node[i].ob = MAXCON;
   }

   return 0;
}

/*--------------------------------------------------------------------
   Queue operations
---------------------------------------------------------------------*/
/*
int queue[Q_SIZE];
int f_q= 0,b_q= 0;
int q_c=0;
*/
int enque(int num)
{
   q_c++;
   node[num].inq = 1;
   queue[f_q] = num;
   f_q = (f_q+1)%Q_SIZE;
   if (f_q == b_q) {
      //printf("Queue out of space\n");
      TLog << "Error: Queue out of space" << std::endl;
      return -1;
      //fprintf(stderr, "Expand Queue\n");
      TLog << "Expand Queue" << std::endl;
   }
   return 0;
}

int deque()
{
   int i;
   if (q_c <= 0)
      return (-1);

   q_c --;
   i = queue[b_q];
   b_q = (b_q + 1) % Q_SIZE;
   node[i].inq = 0;
   return (i);
}

int topque()
{
   int i;
   if (q_c <= 0)
     return (-1);
   else
     i = queue[b_q];
   return (i);
}

/*-------------------------------------------------------------------
   Levelize the circuit
--------------------------------------------------------------------*/
void levelize()
{
   int i, n, fn, j, lev;
   int ic;
   int *q_count;

   q_count = (int *) malloc (sizeof (int) * curr_node);

   fn = 1;
   /* Put all the successors of PIs into queue if: */
   /* (1) They have not been assigned a level, i.e., level = -1    */
   /* (2) They are not already queued, i.e., node[i].inq = 0       */

   for (i = 0; i < curr_pi; i++) {
      node[prim_in[i]].final_node_number = fn++;
      for (j = 0; j < node[prim_in[i]].num_succ; j++) {
    if (node[node[prim_in[i]].succ[j]].level < 0 &&
        !node[node[prim_in[i]].succ[j]].inq)
       enque (node[prim_in[i]].succ[j]);
      }
   }
                                /* Vdd & GND elements also given level 0 */
   for (i = 0; i < curr_node; i++) {
      if (node[i].logic == T_TIE1 || node[i].logic == T_TIE0 ||
          node[i].logic == T_TIEX || node[i].logic == T_TIEZ){
         node[i].final_node_number = fn++;
         for (j = 0; j < node[i].num_succ; j++) {
            if (node[node[i].succ[j]].level < 0 &&
                !node[node[i].succ[j]].inq)
               enque (node[i].succ[j]);
         }
      }
   }
                                /* Storage elements given level 0 */
   for (i = 0; i < curr_node; i++) {
      q_count[i] = 0;
      if (node[i].logic == T_DFF || node[i].logic == T_DFF_CP ||
     node[i].logic == T_DFF_C || node[i].logic == T_DFF_P) {
    node[i].final_node_number = fn++;
    for (j = 0; j < node[i].num_succ; j++) {
       if (node[node[i].succ[j]].level < 0 &&
                !node[node[i].succ[j]].inq)
               enque (node[i].succ[j]);
    }
      }
   }
   max_lev = 0;
   ic = 0;
   while ((n = deque()) != -1) { /* while there are elements in the queue */
      ic++;
      if (ic == (ic/1000) * 1000)
      //printf ("%d\n", ic);   /* just for visual effect :-) */
      if (verbose) {
         TLog << ic << std::endl;
      }
      lev = 1;
      for (j = 0; j < node[n].num_pred; j++) {
         /* node n should have a level > its preds */
         if (node[n].level < node[node[n].pred[j]].level + 1) {
            node[n].level = node[node[n].pred[j]].level + 1;
         }
         if (node[node[n].pred[j]].level < 0) {
            lev = 0;  /* pred has an unassigned level */
         }
      }

      /* Prevent infinite loop */
      if (q_count[n] > 1000000) {
         /* This node has been in and out of the queue too many times */
         /* there is probably an asynchronism or a hanging gate somewhere */
         /* Cut asynchronous loop at this node */
         node[n].level = 0;
         //fprintf(stderr, "Cutting asynchronous feedback at gate %s\n",
         //         name[node[n].net_number]);
         TLog << "Error: Cutting asynchronous feedback at gate "
              << name[node[n].net_number] << std::endl;
         for (i = 0; i < curr_node; i++) q_count[n] = 0; /* reset count */
         lev = 1;
      } else {
         q_count[n]++;
      }
      if (!lev) {
         /* send n back to queue: one of its preds has unassigned level */
         node[n].level = -1;
         enque (n);
      } else {                  /* n has been assigned its correct level */
         if (max_lev < node[n].level)
         max_lev = node[n].level; /* update max level if reqd */
         /* Put all the successors of n into queue if: */
         /* (1) They have not been assigned a level  i.e., level = -1    */
         /* (2) They are not already queued i.e., node[i].inq = 0        */
         /* Asynchronous cycles would create trouble */
         for (j = 0; j < node[n].num_succ; j++) {
            if (node[node[n].succ[j]].level < 0 && !node[node[n].succ[j]].inq) {
               enque (node[n].succ[j]);
            }
         }
      }
   }
   /* All gates have been assigned a level */
   /* Renumber nodes in increasing level order */
   for (lev = 1; lev <= max_lev; lev++) {
      for (i = 0; i < curr_node; i++) {
         if (node[i].level == lev) {
            node[i].final_node_number = fn;
            fn++;
         }
      }
   }
}

/*------------------------------------------------------------
   Detect Asynchronous Feedback loops
-------------------------------------------------------------*/
int asynch_detect()
{
   int i, j;
   int  retval1 = 0;
   int  retval2 = 0;
   for (i = 0; i < curr_node; i++) {
      if ((node[i].num_succ == 0) && (node[i].logic != T_OUTPUT)) {
         //fprintf(stderr, "Warning: Gate %s has no outputs\n",
         //         name[node[i].net_number]);
         TLog << "Warning: Gate " << name[node[i].net_number]
              << " has no outputs" << std::endl;
         retval1 = 2;
      }
      if ((node[i].num_pred == 0) && (node[i].logic != T_INPUT) &&
          (node[i].logic != T_TIE1) && (node[i].logic != T_TIE0) &&
          (node[i].logic != T_TIEX) && (node[i].logic != T_TIEZ)) {
         //fprintf(stderr, "Warning: Gate %s has no inputs\n",
         //         name[node[i].net_number]);
         TLog << "Warning: Gate " << name[node[i].net_number]
              << " has no inputs" << std::endl;
         retval1 = 2;
      }
      if (node[i].level < 0) {
         //fprintf(stderr, "Warning: Gate %s has no assigned level\n",
         //         name[node[i].net_number]);
         TLog << "Warning: Gate " << name[node[i].net_number]
              << " has no assigned level" << std::endl;
         retval1 = 2;
         continue;
      }
      if (node[i].level == 0) {
         if (node[i].logic == T_DFF || node[i].logic == T_DFF_CP ||
             node[i].logic == T_DFF_C || node[i].logic == T_DFF_P ||
             node[i].logic == T_TIE1 || node[i].logic == T_TIE0 ||
             node[i].logic == T_TIEX || node[i].logic == T_TIEZ ||
             node[i].logic == T_INPUT)
            continue; /* proceed to some other gate */
         else {
            //fprintf(stderr, "Warning:Gate %s has level 0\n",
            //         name[node[i].net_number]);
            TLog << "Warning:Gate " << name[node[i].net_number]
                 << " has level 0" << std::endl;
            retval1 = 2;
            continue;
         }
      }
      for (j = 0; j < node[i].num_pred; j++) {
         if (node[node[i].pred[j]].level >= node[i].level){
            //fprintf(stderr, "Warning:Gate %s has a feedback\n",
            //         name[node[i].net_number]);
            TLog << "Warning:Gate " << name[node[i].net_number]
                 << " has a feedback" << std::endl;
            retval2 = 1;
         }
      }
   }
   if (retval2)
      return (retval2);
   else
      return (retval1);
}

/*---------------------------------------------------------------------
   Print the specified node in the output file.
----------------------------------------------------------------------*/
void print_node(int i)
{
   int j,po,k,t;

   fprintf(lev_file,"%d %d %d %d ", node[i].final_node_number,
            node[i].logic, 5*node[i].level, node[i].num_pred);
   if (node[i].net_number >= 0) {
      fprintf(name_file,"%d %s\n",
               node[i].final_node_number,name[node[i].net_number]);
#ifdef SCOAP_DEBUG
      fprintf(dfile, "%d, %s, %d  %d  %d\n", node[i].final_node_number,
         name[node[i].net_number], node[i].C0, node[i].C1, node[i].ob);
#endif
   } else {                           /* printing output name */
      fprintf(name_file,"%d %s\n",node[i].final_node_number,
               name[-1*node[i].net_number]);
#ifdef SCOAP_DEBUG
      fprintf(dfile, "%d, %s, %d  %d  %d\n", node[i].final_node_number,
        name[-1*node[i].net_number], node[i].C0, node[i].C1, node[i].ob );
#endif
   }
   if ((node[i].logic != T_DFF_L) && (node[i].logic != T_MUX_2) &&
       (node[i].logic != T_TRISTATE) && (node[i].logic != T_TRISTATE1) &&
       (node[i].logic != T_TRISTATEINV) && (node[i].logic != T_CMOS) &&
       (node[i].logic != T_NMOS) && (node[i].logic != T_PMOS)) {
                                      /* order preds by C0 */
      for (j = 0; j < node[i].num_pred; j++) {
         for (k = j + 1; k < node[i].num_pred; k++) {
            if (node[node[i].pred[k]].C0 > node[node[i].pred[j]].C0) {
               t = node[i].pred[j];
               node[i].pred[j] = node[i].pred[k];
               node[i].pred[k] = t;
            }
         }
      }
   }
   for (j = 0; j < node[i].num_pred; j++) {
      fprintf(lev_file, "%d ", node[node[i].pred[j]].final_node_number);
   }
   if ((node[i].logic != T_DFF_L) && (node[i].logic != T_MUX_2) &&
       (node[i].logic != T_TRISTATE) && (node[i].logic != T_TRISTATE1) &&
       (node[i].logic != T_TRISTATEINV) && (node[i].logic != T_CMOS) &&
       (node[i].logic != T_NMOS) && (node[i].logic != T_PMOS)) {
                                      /* order preds by C1 */
      for (j = 0; j < node[i].num_pred; j++) {
         for (k = j+1; k < node[i].num_pred; k++) {
            if (node[node[i].pred[k]].C1 > node[node[i].pred[j]].C1) {
               t = node[i].pred[j];
               node[i].pred[j] = node[i].pred[k];
               node[i].pred[k] = t;
            }
         }
      }
   }
   for (j = 0; j < node[i].num_pred; j++) {
      fprintf(lev_file,"%d ", node[node[i].pred[j]].final_node_number);
   }
   fprintf(lev_file, "%d ", node[i].num_succ);
   for (j = 0; j < node[i].num_succ; j++) {
      for (k = j + 1; k < node[i].num_succ; k++) {
         if (node[node[i].succ[k]].C1 > node[node[i].succ[j]].C1) {
            t = node[i].succ[j];
            node[i].succ[j] = node[i].succ[k];
            node[i].succ[k] = t;
         }
      }
   }
   for (j = 0; j < node[i].num_succ; j++) {
      fprintf(lev_file, "%d ", node[node[i].succ[j]].final_node_number);
   }
   fprintf(lev_file, "%d ", node[i].ob);
   po = 0;
   for (j = 0; j < curr_po; j++)
      if (node[i].net_number == prim_out[j])
         po = 1;
         if (po)
            fprintf(lev_file, "O");
         else
            fprintf(lev_file, ";");
         fprintf(lev_file, " %d %d\n", node[i].C0, node[i].C1);
}

/*---------------------------------------------------------------------
   Print the output
----------------------------------------------------------------------*/
void print_output()
{
   int i, j, lev, found, max_level0;
#ifdef SCOAP_DEBUG
   FILE *dfile;
   dfile = fopen ("scoap", "w");
   if (!dfile) {
      //fprintf(stderr, "Unable to open scoap file\n");
      TLog << "Error: Unable to open scoap file" << std::endl;
      return -1;
   }
#endif

   fprintf(lev_file, "%d\n%d\n", curr_node + 1, 10);

   max_level0 = 0;  /* highest node number having level 0 */
   for (i = 0; i < curr_node; i++) {
      if ((node[i].level == 0) && (node[i].final_node_number > max_level0))
         max_level0 = node[i].final_node_number;
   }

   for (i = 0; i <= max_level0; i++) {
      found = 0;
      for (j = 0; (!found && j < curr_node); j++) {
         if ((node[j].level == 0) && (node[j].final_node_number == i)) {
            print_node (j);
            found = 1;
         }
      }
   }
   for (lev = 1; lev <= max_lev; lev++) {
      for (i = 0; i < curr_node; i++) {
    if (node[i].level == lev) {
            print_node (i);
         }
      }
   }
}

/*------------------------------------------------------------------
  Calculate Controllability
-------------------------------------------------------------------*/
void calc_control()
{
   int i,j;
   int lev;
   int converged;
   int min0, max0, sum0;
   int min1, max1, sum1;
   int pass;
   int nc0, nc1;
   int minnc0, minnc1;
   int k;
   int depth;

   for (i = 0; i < curr_node; i++) {
      if (node[i].logic == T_INPUT) {
         node[i].C0 = node[i].C1 = 0;
      }
      if (node[i].logic == T_TIE1) {
         node[i].C0 = MAXCON;
         node[i].C1 = 0;
      }
      if (node[i].logic == T_TIE0) {
         node[i].C0 = 0;
         node[i].C1 = MAXCON;
      }
      if (node[i].logic == T_TIEX) {
         node[i].C0 = MAXCON;
         node[i].C1 = MAXCON;
      }
      if (node[i].logic == T_TIEZ) {
         node[i].C0 = MAXCON;
         node[i].C1 = MAXCON;
      }
   }
   converged = 0;
   pass = 1;
   depth = 0;
   while (!converged) {
      converged = 1;
      //printf (".");
      //fflush (stdout);
      if (verbose) {
         TLog << ".";
         TLog.flush();
      }
      if (pass == 1) {
         converged = 0;
         pass = 2;
      }
      depth++;

      for (lev = 0; lev <= max_lev; lev++) {
         for (i = 0; i < curr_node; i++) {
            if ((node[i].logic != T_INPUT) && (node[i].logic != T_TIE1) &&
                (node[i].logic != T_TIE0) && (node[i].logic != T_TIEX) &&
                (node[i].logic != T_TIEZ)) {
              if (node[i].level == lev) {
                 sum0 = max0 = sum1 = max1 = 0;
                 min0 = min1 = MAXCON;
                 for (j = 0; j < node[i].num_pred; j++) {
                    sum0 += node[node[i].pred[j]].C0;
                    sum1 += node[node[i].pred[j]].C1;
                    if (node[node[i].pred[j]].C0 < min0)
                       min0 = node[node[i].pred[j]].C0;
                    if (node[node[i].pred[j]].C0 > max0)
                       max0 = node[node[i].pred[j]].C0;
                    if (node[node[i].pred[j]].C1 < min1)
                       min1 = node[node[i].pred[j]].C1;
                    if (node[node[i].pred[j]].C1 > max1)
                       max1 = node[node[i].pred[j]].C1;
                 }
                 switch (node[i].logic) {
                    case T_AND:
                    {
                       node[i].C0 = min0 + 1;
                       node[i].C1 = sum1 + 1;
#ifdef FAN
                       node[i].C0 += node[i].num_succ - 1;
                       node[i].C1 += node[i].num_succ - 1;
#endif
                    }
                    break;
                    case T_NAND:
                    {
                       node[i].C0 = sum1 + 1;
                       node[i].C1 = min0 + 1;
#ifdef FAN
                       node[i].C0 += node[i].num_succ - 1;
                       node[i].C1 += node[i].num_succ - 1;
#endif
                    }
                    break;
                    case T_OR:
                    {
                       node[i].C0 = sum0 + 1;
                       node[i].C1 = min1 + 1;
#ifdef FAN
                       node[i].C0 += node[i].num_succ - 1;
                       node[i].C1 += node[i].num_succ - 1;
#endif
                    }
                    break;
                    case T_NOR:
                    {
                       node[i].C0 = min1 + 1;
                       node[i].C1 = sum0 + 1;
#ifdef FAN
                       node[i].C0 += node[i].num_succ - 1;
                       node[i].C1 += node[i].num_succ - 1;
#endif
                    }
                    break;
                    case T_OUTPUT:
                    {
                       node[i].C0 = sum0;
                       node[i].C1 = sum1;
                    }
                    break;
                    case T_NOT:
                    {
                       node[i].C0 = sum1;
                       node[i].C1 = sum0;
#ifdef FAN
                       node[i].C0 += node[i].num_succ - 1;
                       node[i].C1 += node[i].num_succ - 1;
#endif
                    }
                    break;
                    case T_BUF:
                    {
                       node[i].C0 = sum0;
                       node[i].C1 = sum1;
#ifdef FAN
                       node[i].C0 += node[i].num_succ - 1;
                       node[i].C1 += node[i].num_succ - 1;
#endif
                    }
                    break;
                    case T_MUX_2:
                    {
                       node[i].C0 = node[node[i].pred[0]].C0 +
                                    node[node[i].pred[2]].C0;
                       if (node[i].C0 > node[node[i].pred[1]].C0 +
                           node[node[i].pred[2]].C1) {
                          node[i].C0 = node[node[i].pred[1]].C0 +
                                       node[node[i].pred[2]].C1;
                       }
                       if (node[i].C0 > node[node[i].pred[1]].C0 +
                           node[node[i].pred[0]].C0) {
                          node[i].C0 = node[node[i].pred[1]].C0 +
                                       node[node[i].pred[0]].C0;
                       }
                       node[i].C1 = node[node[i].pred[0]].C1 +
                                    node[node[i].pred[2]].C0;
                       if (node[i].C1 > node[node[i].pred[1]].C1 +
                           node[node[i].pred[2]].C1) {
                          node[i].C1 = node[node[i].pred[1]].C1 +
                                       node[node[i].pred[2]].C1;
                       }
                       if (node[i].C1 > node[node[i].pred[1]].C1 +
                           node[node[i].pred[0]].C1) {
                          node[i].C1 = node[node[i].pred[1]].C1 +
                                       node[node[i].pred[0]].C1;
                       }
                    }
                    break;
                    case T_XOR:
                    {
                       nc0 =  node[node[i].pred[0]].C0 +
                              node[node[i].pred[1]].C0;
                       nc1 = node[node[i].pred[0]].C0 +
                             node[node[i].pred[1]].C1;
                       if (nc0 > node[node[i].pred[0]].C1 +
                           node[node[i].pred[1]].C1)
                          nc0 = node[node[i].pred[0]].C1 +
                                node[node[i].pred[1]].C1;
                       if (nc1 >  node[node[i].pred[0]].C1 +
                           node[node[i].pred[1]].C0)
                          nc1 =  node[node[i].pred[0]].C1 +
                                 node[node[i].pred[1]].C0;
                       if (nc0 > MAXCON)
                          nc0 = MAXCON;
                       if (nc1 > MAXCON)
                          nc1 = MAXCON;
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_XNOR:
                    {
                       nc0 = node[node[i].pred[0]].C1 +
                             node[node[i].pred[1]].C0;
                       nc1 = node[node[i].pred[0]].C1 +
                             node[node[i].pred[1]].C1;
                       if (nc0 > node[node[i].pred[0]].C0 +
                           node[node[i].pred[1]].C1)
                          nc0 = node[node[i].pred[0]].C0 +
                                node[node[i].pred[1]].C1;
                       if (nc1 > node[node[i].pred[0]].C0 +
                           node[node[i].pred[1]].C0)
                          nc1 =  node[node[i].pred[0]].C0 +
                                 node[node[i].pred[1]].C0;
                       if (nc0 > MAXCON)
                          nc0 = MAXCON;
                       if (nc1 > MAXCON)
                          nc1 = MAXCON;
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_DFF_C:
                    case T_DFF_CP:
                    case T_DFF_P:
                    case T_DFF:
                    {
                       nc0 = sum0;
                       nc1 = sum1;
#ifdef FAN
                       nc0 += node[i].num_succ - 1;
                       nc1 += node[i].num_succ - 1;
#endif
                       if (node[i].logic ==  T_DFF_C ||
                           node[i].logic ==  T_DFF_CP) {
                          if (nc0 > node[node[i].pred[1]].C0)
                             nc0 = node[node[i].pred[1]].C0;
                       }
                       if (node[i].logic ==  T_DFF_P) {
                          if (nc1 > node[node[i].pred[1]].C0)
                              nc1 = node[node[i].pred[1]].C0;
                       }
                       if (node[i].logic ==  T_DFF_CP) {
                          if (nc1 > node[node[i].pred[2]].C0)
                             nc1 = node[node[i].pred[2]].C0;
                       }
                       if (nc0 > MAXCON)
                          nc0 = MAXCON;
                       if (nc1 > MAXCON)
                          nc1 = MAXCON;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_DFF_L:
                    case T_DFF_LC:
                    case T_DFF_LCP:
                    case T_DFF_LP:
                    {
                       nc0 = node[node[i].pred[0]].C0 +
                             node[node[i].pred[1]].C0 +
                             node[node[i].pred[1]].C1;
                       nc1 = node[node[i].pred[0]].C1 +
                             node[node[i].pred[1]].C0 +
                             node[node[i].pred[1]].C1;
                       if (node[i].logic ==  T_DFF_LC ||
                           node[i].logic ==  T_DFF_LCP) {
                          if (nc0 > node[node[i].pred[1]].C0)
                             nc0 = node[node[i].pred[1]].C0;
                       }
                       if (node[i].logic ==  T_DFF_LP) {
                          if (nc1 > node[node[i].pred[1]].C0)
                             nc1 = node[node[i].pred[1]].C0;
                       }
                       if (node[i].logic ==  T_DFF_LCP) {
                          if (nc1 > node[node[i].pred[2]].C0)
                             nc1 = node[node[i].pred[2]].C0;
                       }
                       if (nc0 > MAXCON)
                          nc0 = MAXCON;
                       if (nc1 > MAXCON)
                          nc1 = MAXCON;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_NOR_LATCH:
                    case T_NAND_LATCH:
                    {      /* Set Reset */
                       nc0 = node[node[i].pred[0]].C1 +
                             node[node[i].pred[1]].C0;
                       nc1 = node[node[i].pred[0]].C0 +
                             node[node[i].pred[1]].C1;
                       if (nc0 > MAXCON)
                          nc0 = MAXCON;
                       if (nc1 > MAXCON)
                          nc1 = MAXCON;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_BUS:
                    {
                       minnc1 = minnc0 = MAXCON;
                       for (j=0;j<node[i].num_pred;j++) {
                          nc0 = node[node[node[i].pred[j]].pred[0]].C0;
                          nc1 = node[node[node[i].pred[j]].pred[0]].C1;
                          for (k=0;k<node[i].num_pred;k++) {
                             if (j != k) {
                                if (node[node[i].pred[k]].logic ==
                                    T_TRISTATE1) {
                                   nc0 +=
                                     node[node[node[i].pred[k]].pred[1]].C0;
                                   nc1 +=
                                     node[node[node[i].pred[k]].pred[1]].C0;
                                } else {
                                   nc0 +=
                                     node[node[node[i].pred[k]].pred[1]].C1;
                                   nc1 +=
                                     node[node[node[i].pred[k]].pred[1]].C1;
                                }
                             }
                          }
                          if (nc0 < minnc0)
                             minnc0 = nc0;
                          if (nc1 < minnc1)
                             minnc1 = nc1;
                       }
                       nc0 = minnc0;
                       nc1 = minnc1;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_BUS_GOLOW:
                    {
                       minnc1 = minnc0 = MAXCON;
                       for (j = 0; j < node[i].num_pred; j++) {
                          nc0 = node[node[node[i].pred[j]].pred[0]].C0;
                          nc1 = node[node[node[i].pred[j]].pred[0]].C1;
                          for (k = 0; k < node[i].num_pred; k++) {
                             if (j != k) {
                                if (node[node[i].pred[k]].logic ==
                                    T_TRISTATE1) {
                                   nc0 +=
                                     node[node[node[i].pred[k]].pred[1]].C0;
                                   nc1 +=
                                     node[node[node[i].pred[k]].pred[1]].C0;
                                } else {
                                   nc0 +=
                                     node[node[node[i].pred[k]].pred[1]].C1;
                                   nc1 +=
                                     node[node[node[i].pred[k]].pred[1]].C1;
                                }
                             }
                          }
                          if (nc0 < minnc0)
                             minnc0 = nc0;
                          if (nc1 < minnc1)
                             minnc1 = nc1;
                       }
                       nc0 = 0;
                       for (j = 0; j < node[i].num_pred; j++) {
                          if (node[node[i].pred[j]].logic == T_TRISTATE1) {
                             nc0 += node[node[node[i].pred[j]].pred[1]].C0;
                          } else {
                             nc0 += node[node[node[i].pred[j]].pred[1]].C1;
                          }
                       }
                       if (nc0 < minnc0)
                          minnc0 = nc0;
                       nc0 = minnc0;
                       nc1 = minnc1;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_BUS_GOHIGH:
                    {
                       minnc1 = minnc0 = MAXCON;
                       for (j = 0; j < node[i].num_pred; j++) {
                          nc0 = node[node[node[i].pred[j]].pred[0]].C0;
                          nc1 = node[node[node[i].pred[j]].pred[0]].C1;
                          for (k = 0; k < node[i].num_pred; k++) {
                             if (j != k) {
                                if (node[node[i].pred[k]].logic ==
                                    T_TRISTATE1) {
                                   nc0 +=
                                     node[node[node[i].pred[k]].pred[1]].C0;
                                   nc1 +=
                                     node[node[node[i].pred[k]].pred[1]].C0;
                                } else {
                                   nc0 +=
                                     node[node[node[i].pred[k]].pred[1]].C1;
                                   nc1 +=
                                     node[node[node[i].pred[k]].pred[1]].C1;
                                }
                             }
                          }
                          if (nc0 < minnc0)
                             minnc0 = nc0;
                          if (nc1 < minnc1)
                             minnc1 = nc1;
                       }
                       nc1 = 0;
                       for (j = 0; j < node[i].num_pred; j++) {
                          if (node[node[i].pred[j]].logic == T_TRISTATE1) {
                             nc1 += node[node[node[i].pred[j]].pred[1]].C0;
                          } else {
                             nc1 += node[node[node[i].pred[j]].pred[1]].C1;
                          }
                       }
                       if (nc1 < minnc1)
                          minnc1 = nc1;
                       nc0 = minnc0;
                       nc1 = minnc1;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_TRISTATE:
                    case T_PMOS:
                    {
                       nc0 = node[node[i].pred[0]].C0 +
                             node[node[i].pred[1]].C0;
                       nc1 = node[node[i].pred[0]].C1 +
                             node[node[i].pred[1]].C0;
                       if (nc0 > MAXCON)
                          nc0 = MAXCON;
                       if (nc1 > MAXCON)
                          nc1 = MAXCON;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_TRISTATE1:
                    case T_NMOS:
                    {
                       nc0 = node[node[i].pred[0]].C0 +
                             node[node[i].pred[1]].C1;
                       nc1 = node[node[i].pred[0]].C1 +
                             node[node[i].pred[1]].C1;
                       if (nc0 > MAXCON)
                          nc0 = MAXCON;
                       if (nc1 > MAXCON)
                          nc1 = MAXCON;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_TRISTATEINV:
                    {
                       nc1 = node[node[i].pred[0]].C0 +
                             node[node[i].pred[1]].C0;
                       nc0 = node[node[i].pred[0]].C1 +
                             node[node[i].pred[1]].C0;
                       if (nc0 > MAXCON)
                          nc0 = MAXCON;
                  if (nc1 > MAXCON)
                          nc1 = MAXCON;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    case T_CMOS:
                    {                /* input, n-gate, p-gate */
                       nc0 = node[node[i].pred[0]].C0 +
                             node[node[i].pred[2]].C0;
                       nc1 = node[node[i].pred[0]].C1 +
                             node[node[i].pred[2]].C0;
                       minnc0 = node[node[i].pred[0]].C0 +
                                node[node[i].pred[1]].C1;
                       minnc1 = node[node[i].pred[0]].C1 +
                                node[node[i].pred[1]].C1;
                       if (minnc0 < nc0)
                          nc0 = minnc0;
                       if (minnc1 < nc1)
                          nc1 = minnc1;
                       if (nc0 > MAXCON)
                          nc0 = MAXCON;
                       if (nc1 > MAXCON)
                          nc1 = MAXCON;
                       if (node[i].C0 != nc0 || node[i].C1 != nc1) {
                          converged = 0;
                       }
                       node[i].C0 = nc0;
                       node[i].C1 = nc1;
                    }
                    break;
                    default:
                    {
                       //fprintf(stderr,"Undefined logic %d\n", node[i].logic);
                       TLog << "Error: Undefined logic " << node[i].logic
                            << std::endl;
                    }
                    break;
                 }
                 if (node[i].C0 > MAXCON) node[i].C0 = MAXCON;
                 if (node[i].C1 > MAXCON) node[i].C1 = MAXCON;
              }
           }
         }
      }
   }
   //printf("\nDepth %d\n",depth);
   if (verbose) {
      TLog << "\nDepth " << depth << std::endl;
   }
}

/*------------------------------------------------------------------
  Calculate Observability
-------------------------------------------------------------------*/
void calc_observe()
{
   int i, j, k;
   int lev;
   int converged;
   int sum0;
   int sum1;
   int no;

   for (i = 0; i < curr_node; i++) {
      node[i].ob = MAXCON;
      for (j = 0; j < curr_po; j++) {
         if (node[i].net_number == prim_out[j])
            node[i].ob = 0;
      }
   }
   converged = 0;

   while (!converged) {
      converged = 1;
      for (lev = max_lev; lev >= 0; lev--) {
         for (i = 0; i < curr_node; i++) {
            if (node[i].level == lev && node[i].logic != T_INPUT &&
                node[i].logic != T_TIE1 && node[i].logic != T_TIE0 &&
                node[i].logic != T_TIEX && node[i].logic != T_TIEZ) {
               for (j = 0; j < node[i].num_pred; j++) {
                  sum0 = sum1 = 0;
                  for (k = 0; k < node[i].num_pred; k++) {
                     if (k != j) {
                        sum0 += node[node[i].pred[k]].C0;
                        sum1 += node[node[i].pred[k]].C1;
                     }
                  }
                  switch (node[i].logic) {
                     case T_AND:
                     case T_NAND: {
                        no = sum1 + 1 + node[i].ob;
                        if (no > MAXCON) no = MAXCON;

                        if (node[node[i].pred[j]].ob > no) {
                           node[node[i].pred[j]].ob = no;
                           converged = 0;
                        }
                        break;
                     }
                     case T_OR:
                     case T_NOR:
                     {
                        no = sum0 + 1 + node[i].ob;
                        if (no > MAXCON) no = MAXCON;

                        if (node[node[i].pred[j]].ob > no) {
                           node[node[i].pred[j]].ob = no;
                           converged = 0;
                        }
                        break;
                     }
                     case T_OUTPUT:
                     case T_NOT:
                     case T_BUF:
                     {
                        if (node[node[i].pred[j]].ob > node[i].ob) {
                           node[node[i].pred[j]].ob = node[i].ob;
                           converged = 0;
                        }
                        break;
                     }
                     case T_MUX_2:
                     {
                        switch(j) {
                           case 0:
                           {
                              no = node[node[i].pred[2]].C0 + node[i].ob + 1;
                              break;
                           }
                           case 1:
                           {
                              no = node[node[i].pred[2]].C1 + node[i].ob + 1;
                              break;
                           }
                           case 2:
                           {
                              no = node[node[i].pred[0]].C0 +
                                   node[node[i].pred[1]].C1 + node[i].ob + 1;
                              if (no > node[node[i].pred[0]].C1 +
                                  node[node[i].pred[1]].C0 + node[i].ob + 1) {
                                 no = node[node[i].pred[0]].C1 +
                                    node[node[i].pred[1]].C0 + node[i].ob + 1;
                              }
                              break;
                           }
                        }
                        if (no > MAXCON) no = MAXCON;
                        if (node[node[i].pred[j]].ob > no) {
                           node[node[i].pred[j]].ob = no;
                           converged = 0;
                        }
                        break;
                     }
                     case T_XOR:
                     case T_XNOR:
                     {
                        if (sum1 > sum0) {
                           no = sum0 + node[i].ob + 1;
                        } else {
                           no = sum1 + node[i].ob + 1;
                        }
                        if (no > MAXCON) no = MAXCON;
                        if (node[node[i].pred[j]].ob > no) {
                           node[node[i].pred[j]].ob = no;
                           converged = 0;
                        }
                        break;
                     }
                     case T_DFF:
                     case T_DFF_C:
                     case T_DFF_CP:
                     case T_DFF_P:
                     {
                        if (node[i].ob < node[node[i].pred[j]].ob) {
                           converged = 0;
                           node[node[i].pred[j]].ob = node[i].ob;
                        }
                        break;
                     }
                     case T_DFF_L:
                     case T_DFF_LC:
                     case T_DFF_LCP:
                     case T_DFF_LP:
                     {
                        no = node[node[i].pred[0]].C0 + node[node[i].pred[0]].C1;
                        if (no > MAXCON) no = MAXCON;
                        if (no < node[node[i].pred[1]].ob) {
                           node[node[i].pred[1]].ob = no;
                           converged = 0;
                        }
                        no = node[node[i].pred[1]].C0 + node[node[i].pred[1]].C1;
                        if (no > MAXCON) no = MAXCON;
                        if (no < node[node[i].pred[0]].ob) {
                           node[node[i].pred[0]].ob = no;
                           converged = 0;
                        }
                        if (j > 1) {
                           if (node[i].ob < node[node[i].pred[j]].ob) {
                              converged = 0;
                              node[node[i].pred[j]].ob = node[i].ob;
                           }
                        }
                        break;
                     }
                     case T_NAND_LATCH:
                     case T_NOR_LATCH:
                     {
                        no = node[node[i].pred[0]].C1 + node[i].ob;
                        if (no > MAXCON) no = MAXCON;
                        if (no < node[node[i].pred[1]].ob) {
                           node[node[i].pred[1]].ob = no;
                           converged = 0;
                        }
                        no = node[node[i].pred[1]].C1 + node[i].ob;
                        if (no > MAXCON) no = MAXCON;
                        if (no < node[node[i].pred[0]].ob) {
                           node[node[i].pred[0]].ob = no;
                           converged = 0;
                        }
                        break;
                     }
                     case T_TRISTATE:
                     case T_TRISTATEINV:
                     case T_PMOS:
                     {
                        switch(j) {
                           case 0:
                           {
                              no = node[node[i].pred[1]].C0 + node[i].ob + 1;
                              break;
                           }
                           case 1:
                           {
                              if (sum1 > sum0) {
                                 no = sum0 + node[i].ob + 1;
                              } else {
                                 no = sum1 + node[i].ob + 1;
                              }
                              break;
                           }
                        }
                        if (no > MAXCON) no = MAXCON;
                        if (node[node[i].pred[j]].ob > no) {
                           node[node[i].pred[j]].ob = no;
                           converged = 0;
                        }
                        break;
                     }
                     case T_TRISTATE1:
                     case T_NMOS:
                     {
                        switch(j) {
                           case 0:
                           {
                              no = node[node[i].pred[1]].C1 + node[i].ob + 1;
                              break;
                           }
                           case 1:
                           {
                              if (sum1 > sum0) {
                                 no = sum0 + node[i].ob + 1;
                              } else {
                                 no = sum1 + node[i].ob + 1;
                              }
                              break;
                           }
                        }
                        if (no > MAXCON) no = MAXCON;
                        if (node[node[i].pred[j]].ob > no) {
                           node[node[i].pred[j]].ob = no;
                           converged = 0;
                        }
                        break;
                     }
                     case T_BUS:
                     case T_BUS_GOHIGH:
                     case T_BUS_GOLOW:
                     {
                        int other = 0;
                        for (k = 0; k < node[i].num_pred; k++) {
                           if (k != j) {
                              if (node[node[i].pred[k]].logic == T_TRISTATE1) {
                                 other += node[node[i].pred[k]].C0;
                              } else {
                                 other += node[node[i].pred[k]].C1;
                              }
                           }
                        }
                        no = node[i].ob + 1 + other;

                        if (no > MAXCON) no = MAXCON;
                        if (no < node[node[i].pred[j]].ob) {
                           node[node[i].pred[j]].ob = no;
                           converged = 0;
                        }
                        break;
                     }
                     default:
                     {
                        //printf ("ob Undefined logic %d\n", node[i].logic);
                        TLog << "ob Undefined logic " << node[i].logic << std::endl;
                        break;
                     }
                  }
               }
            }
         }
      }
   }
}

